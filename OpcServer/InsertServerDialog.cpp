// InsertServerDialog.cpp : 
//

#include "stdafx.h"
#include "OpcServer.h"
#include "InsertServerDialog.h"
#include "OpcComServerItem.h"



// CInsertServerDialog 

IMPLEMENT_DYNAMIC(CInsertServerDialog, CDialog)

CInsertServerDialog::CInsertServerDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CInsertServerDialog::IDD, pParent)
	,	m_szServer(_T(""))
	,	m_szDriverName(_T(""))
	,	m_strStopbit(_T("1"))
	,	m_strPort(_T(""))
	,	m_strDatabits(_T("8"))
	,	m_strParitybit(_T("NONE")) 
	,	m_strBaudrate(_T("9600"))
	,	m_strFlowctrl(_T("NONE"))
	,	m_strConfigFile(_T(""))
	
{
	
}

CInsertServerDialog::~CInsertServerDialog()
{
}

void CInsertServerDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_SERVEREDIT, m_szServer);
	DDV_MaxChars(pDX, m_szServer, 32);
	DDX_Text(pDX, IDC_DRIVERNAMEEDIT, m_szDriverName);
	DDV_MaxChars(pDX, m_szDriverName, 64);
	DDX_Control(pDX, IDC_COMBO_PORT, m_Port);
	DDX_Control(pDX, IDC_COMBO_BAUDRATE, m_BaudRate);
	DDX_Control(pDX, IDC_COMBO_DATABITS, m_DataBit);
	DDX_Control(pDX, IDC_COMBO_CHECK, m_ParityBit);
	DDX_Control(pDX, IDC_COMBO_STOPBIT, m_StopBit);
	DDX_Control(pDX, IDC_COMBO_FLOWBIT, m_FlowCtrl);
	DDX_Control(pDX, IDC_CONFIGFILEEDIT, m_configFile);
	// DDV_MaxChars(pDX, m_strConfigFile, 64);
	DDX_Control(pDX, IDC_DRIVERAUTOSTARTCHECK, m_DriverAutoStart);
}


BEGIN_MESSAGE_MAP(CInsertServerDialog, CDialog)
	ON_BN_CLICKED(IDSELECTDIR, &CInsertServerDialog::OnBnClickedSelectdir)
	ON_CBN_SELCHANGE(IDC_COMBO_BAUDRATE, &CInsertServerDialog::OnCbnSelchangeComboBaudrate)
	ON_CBN_SELCHANGE(IDC_COMBO_DATABITS, &CInsertServerDialog::OnCbnSelchangeComboDatabits)
	ON_CBN_SELCHANGE(IDC_COMBO_CHECK, &CInsertServerDialog::OnCbnSelchangeComboCheck)
	ON_CBN_SELCHANGE(IDC_COMBO_PORT, &CInsertServerDialog::OnCbnSelchangeComboPort)
	ON_CBN_SELCHANGE(IDC_COMBO_STOPBIT, &CInsertServerDialog::OnCbnSelchangeComboStopbit)
	ON_CBN_SELCHANGE(IDC_COMBO_FLOWBIT, &CInsertServerDialog::OnCbnSelchangeComboFlowbit)
	ON_BN_CLICKED(IDC_ADVANCE_BTN, &CInsertServerDialog::OnBnClickedAdvanceBtn)
	ON_EN_KILLFOCUS(IDC_CONFIGFILEEDIT, &CInsertServerDialog::OnEnKillfocusConfigfileedit)
	ON_BN_CLICKED(IDOK, &CInsertServerDialog::OnBnClickedOk)
END_MESSAGE_MAP()


// CInsertServerDialog 


void CInsertServerDialog::OnBnClickedSelectdir()
{
	// TODO: Add your control notification handler code here
		CString strTitle = _T("Select a plugin file");
		CFileDialog ofn(TRUE,_T("*.DLL"),
			_T(""),						
			OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, //
			_T("Plugin Files (*.dll)|*.DLL|"),this,0,TRUE);								
		ofn.m_ofn.lpstrTitle = strTitle;

		if (ofn.DoModal() == IDOK) 
		{
			CWnd* pWnd = GetDlgItem(IDC_DRIVERNAMEEDIT);
			pWnd->SetWindowText(ofn.GetFileName());
				
		}
		
}


void CInsertServerDialog::OnCbnSelchangeComboBaudrate()
{
	int index;
	index = m_BaudRate.GetCurSel();
	m_BaudRate.GetLBText(index,m_strBaudrate);	
}


void CInsertServerDialog::OnCbnSelchangeComboDatabits()
{
		int index;
	index = m_DataBit.GetCurSel();
	switch(index)
	{
	case 0:
		m_strDatabits="8";
		break;
	case 1: 
		m_strDatabits="7"; //.Format("%d",7);
		break;
	default:
		break;
	}
	m_DataBit.GetLBText(index,m_strDatabits);	
}


void CInsertServerDialog::OnCbnSelchangeComboCheck()
{
	// TODO: Add your control notification handler code here
		int index;
	index = m_ParityBit.GetCurSel();
	m_ParityBit.GetLBText(index,m_strParitybit);
	//m_strParitybit.Format(_T("%d"),index);
}


void CInsertServerDialog::OnCbnSelchangeComboPort()
{
	int index;
	index = m_Port.GetCurSel();
	m_Port.GetLBText(index,m_strPort);
	//m_strPort.Format(_T("%d"),++index);
}


void CInsertServerDialog::OnCbnSelchangeComboStopbit()
{
	int index;
	index = m_StopBit.GetCurSel();
	switch(index)
	{
	case 0:
		m_strStopbit="1";
		break;
	case 1:
		m_strStopbit="1.5";
		break;
	case 2: 
		m_strStopbit="2";
		break;
	default:
		break;
	}
	m_StopBit.GetLBText(index,m_strStopbit);	
}


void CInsertServerDialog::OnCbnSelchangeComboFlowbit()
{
	// TODO: Add your control notification handler code here
	int index;
	index = m_FlowCtrl.GetCurSel();
	m_FlowCtrl.GetLBText(index,m_strFlowctrl);
	//m_strFlowctrl.Format(_T("%d"),++index);
}


BOOL CInsertServerDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
		m_Port.AddString(_T("NONE"));
	for(int i=1;i<21;i++)
	{
		CString strData;
		strData.Format(_T("COM%d"),i);
		m_Port.AddString(strData);
	}
	//
	m_BaudRate.AddString(_T("110"));
	m_BaudRate.AddString(_T("134"));
	m_BaudRate.AddString(_T("150"));
	m_BaudRate.AddString(_T("300"));
	m_BaudRate.AddString(_T("600"));
	m_BaudRate.AddString(_T("1200"));
	m_BaudRate.AddString(_T("2400"));
	m_BaudRate.AddString(_T("4800"));
	m_BaudRate.AddString(_T("9600"));
	m_BaudRate.AddString(_T("19200"));
	m_BaudRate.AddString(_T("38400"));
	m_BaudRate.AddString(_T("57600"));
	m_BaudRate.AddString(_T("115200"));
	//
	m_ParityBit.AddString(_T("NONE"));
	m_ParityBit.AddString(_T("ODD"));
	m_ParityBit.AddString(_T("EVEN"));
	m_ParityBit.AddString(_T("SPC"));
	m_ParityBit.AddString(_T("MRK"));
	//
	m_DataBit.AddString(_T("8"));
	m_DataBit.AddString(_T("7"));

	//
	m_StopBit.AddString(_T("1"));
	m_StopBit.AddString(_T("2"));
	//
	m_FlowCtrl.AddString(_T("NONE"));
	m_FlowCtrl.AddString(_T("CTS/RTS"));
	m_FlowCtrl.AddString(_T("DTR/DSR"));
	m_FlowCtrl.AddString(_T("XOn/XOff"));

	//
	int nIndex;
	nIndex = m_Port.SelectString(-1,m_strPort);
	nIndex = m_BaudRate.SelectString(-1,m_strBaudrate);
	nIndex = m_ParityBit.SelectString(-1, m_strParitybit);
	nIndex = m_DataBit.SelectString(-1,m_strDatabits);
	nIndex = m_StopBit.SelectString(-1,m_strStopbit);
	nIndex = m_FlowCtrl.SelectString(-1,m_strFlowctrl);

	m_configFile.SetWindowTextW(m_strConfigFile);
	if (m_Title.TrimLeft() !=_T(""))
	this->SetWindowTextW(m_Title);
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

CString CInsertServerDialog::Get_AppPath()
{ 

TCHAR szAppPath[_MAX_PATH + 1];

GetModuleFileName(NULL, szAppPath, _MAX_PATH); 

TCHAR* pszTemp = _tcsrchr(szAppPath, _T('\\'));

int n = (int)(pszTemp - szAppPath + 1);

szAppPath [ n ] = 0;
return szAppPath;
}


void CInsertServerDialog::OnBnClickedAdvanceBtn()
{
	// TODO: Add your control notification handler code here

	if (m_strConfigFile.TrimLeft() ==_T("")) 
	{
		AfxMessageBox(_T("Please Enter Advance Config Filename."));
	return;
	}
			CWnd* pWnd = GetDlgItem(IDC_DRIVERNAMEEDIT);
			CString dName;
			CString fname;
			pWnd->GetWindowText(dName);
			m_szDriverName = dName;
			//fName = GetAppPath()+
			fname=	_T("plugins\\")+dName ;

		HMODULE h_mod = ::LoadLibrary(fname);///m_szDriverName);
		IPlugin *p_plugin;
		if (h_mod != NULL)
		{

			 PLUGIN_FACTORY p_factory_function = (PLUGIN_FACTORY) ::GetProcAddress (h_mod, "Create_Plugin");
			 PLUGIN_CLEANUP p_cleanup_function = (PLUGIN_CLEANUP) ::GetProcAddress (h_mod, "Release_Plugin");
			 SHOW_SETUP p_showsetup_function = (SHOW_SETUP) ::GetProcAddress (h_mod, "Show_Setup");

			if ((p_factory_function != NULL) && (p_cleanup_function != NULL))
			{
				
				p_plugin = (*p_factory_function)();
				//p_plugin->ShowSetup(m_strConfigFile); ///m_Advance AfxGetPath
				CString AdvCfgFile;
				m_configFile.GetWindowTextW(AdvCfgFile);
				CString appPath= GetAppPath();
				CString drName;
				CWnd* pWnd = GetDlgItem(IDC_DRIVERNAMEEDIT);
				pWnd->GetWindowText(drName);

				m_fullpathFile.Format(_T("%s%s%s_%s.cfg"),(LPCTSTR) appPath, _T("\\cfg\\"),(LPCTSTR)drName,(LPCTSTR) AdvCfgFile);
				p_plugin->ShowSetup( m_fullpathFile) ;
				//(p_showsetup_function)( this ,m_fullpathFile);
			}
			(p_cleanup_function)(p_plugin);
		}
		else
			AfxMessageBox(_T("Plugin File Missing : ")+fname);

}


void CInsertServerDialog::OnEnKillfocusConfigfileedit()
{
	// TODO: Add your control notification handler code here
	m_configFile.GetWindowTextW(m_strConfigFile);
}


void CInsertServerDialog::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialog::OnOK();
}
