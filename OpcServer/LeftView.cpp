
// LeftView.cpp 
//

#include "stdafx.h"
#include "OpcServer.h"

#include "OpcServerDoc.h"
#include "LeftView.h"
#include "OpcComServerItem.h"
#include "OpcGroupItem.h"
#include "InsertServerDialog.h"
#include "InsertGroupDialog.h"
#include "NewItemDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern  CMyOpcServer my_CF;
// CLeftView

IMPLEMENT_DYNCREATE(CLeftView, CTreeView)

BEGIN_MESSAGE_MAP(CLeftView, CTreeView)
	//ON_WM_CONTEXTMENU()
	ON_NOTIFY_REFLECT(NM_RCLICK, &CLeftView::OnNMRClick)
	ON_UPDATE_COMMAND_UI(ID_SERVER_ADD, &CLeftView::OnUpdateServerAdd)
	ON_COMMAND(ID_SERVER_EDIT, &CLeftView::OnServerEdit)
	ON_UPDATE_COMMAND_UI(ID_GROUP_ADD, &CLeftView::OnUpdateGroupAdd)
	ON_UPDATE_COMMAND_UI(ID_GROUP_EDIT, &CLeftView::OnUpdateGroupEdit)
	ON_COMMAND(ID_SERVER_ADD, &CLeftView::OnServerAdd)
	ON_UPDATE_COMMAND_UI(ID_SERVER_EDIT, &CLeftView::OnUpdateServerEdit)
	ON_COMMAND(ID_GROUP_ADD, &CLeftView::OnGroupAdd)
	ON_COMMAND(ID_ITEM_ADD, &CLeftView::OnItemAdd)
	ON_UPDATE_COMMAND_UI(ID_ITEM_ADD, &CLeftView::OnUpdateItemAdd)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, &CLeftView::OnTvnSelchanged)
	ON_COMMAND(ID_GROUP_EDIT, &CLeftView::OnGroupEdit)
	ON_UPDATE_COMMAND_UI(ID_EDIT_DELETE, &CLeftView::OnUpdateEditDelete)
	ON_COMMAND(ID_EDIT_DELETE, &CLeftView::OnEditDelete)
	ON_COMMAND(ID_IMPORTTAG, &CLeftView::OnImport)
	ON_UPDATE_COMMAND_UI(ID_IMPORTTAG, &CLeftView::OnUpdateImport)
END_MESSAGE_MAP()


// CLeftView 

CLeftView::CLeftView()
{
	
	UINT uiBmpId = theApp.m_bHiColorIcons ? IDR_USERIMAGE : IDR_USERIMAGE;
	CBitmap bmp;
	if (!bmp.LoadBitmap(uiBmpId))
	{
		TRACE(_T("Unable to load bitmap: %x\n"), uiBmpId);
		ASSERT(FALSE);
		return;
	}
	BITMAP bmpObj;
	bmp.GetBitmap(&bmpObj);
	UINT nFlags = ILC_MASK;
	nFlags |= (theApp.m_bHiColorIcons) ? ILC_COLOR24 : ILC_COLOR4;
	m_imageList.Create(16, bmpObj.bmHeight, nFlags, 0, 0);
	m_imageList.Add(&bmp, RGB(255, 0, 0));

}

CLeftView::~CLeftView()
{
}

BOOL CLeftView::PreCreateWindow(CREATESTRUCT& cs)
{
	
	cs.style |= TVS_HASLINES | TVS_LINESATROOT |TVS_HASBUTTONS; 
	return CTreeView::PreCreateWindow(cs);
}

void CLeftView::OnInitialUpdate()
{
	CTreeView::OnInitialUpdate();
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	TreeCtrl.SetImageList(&m_imageList,TVSIL_NORMAL); 
    //TreeCtrl.DeleteAllItems();
	
}


// CLeftView 

#ifdef _DEBUG
void CLeftView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CLeftView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}

COpcServerDoc* CLeftView::GetDocument() // 
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(COpcServerDoc)));
	return (COpcServerDoc*)m_pDocument;
}
#endif //_DEBUG


// CLeftView 

void CLeftView::OnContextMenu(CWnd* pWnd, CPoint point)
{
	GetDocument();
    theApp.GetContextMenuManager()->ShowPopupMenu(IDR_TREEMENU, point.x, point.y, this, TRUE);
}


void CLeftView::OnNMRClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	CTreeCtrl &cTree = GetTreeCtrl ();
	CPoint point;
	GetCursorPos(&point);
	CPoint cpoint(point);
	ScreenToClient(&cpoint);
	UINT uHitFlags;
	HTREEITEM hItem;
	hItem = cTree.HitTest (cpoint, &uHitFlags);
	if (hItem)// && (uHitFlags & TVHT_ONITEM)) 
	{
		UINT u = cTree.GetItemState(hItem,TVIS_SELECTED); 
		if ((u & TVIS_SELECTED)!=TVIS_SELECTED)
		    cTree.Select (hItem, TVGN_CARET);
	}
	OnContextMenu(this, point);
	*pResult = 0;
}

void CLeftView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	switch (lHint)
	{
		case SERVER_ADD:
			{
			  COpcComServerItem * pServer = (COpcComServerItem *)pHint;
			  HTREEITEM hItem = TreeCtrl.InsertItem(pServer->GetName(),0); 
			  TreeCtrl.SetItemData(hItem,DWORD(pServer)); 
			  TreeCtrl.SetItemImage(hItem,1,1); 
			  pServer->SetTreeItem(hItem); 
			}
			break;
		case FILENEW:
			TreeCtrl.DeleteAllItems();
			break;
		case SERVER_GROUP_ADD:
			{
				COpcGroupItem * pGroup = (COpcGroupItem *)pHint;
				HTREEITEM hItem = TreeCtrl.InsertItem(pGroup->GetName(),pGroup->GetServer()->GetTreeItem()); 				
				pGroup->SetTreeItem(hItem);
				TreeCtrl.SetItemImage(hItem,3,3); 
				TreeCtrl.SetItemData(hItem,DWORD(pGroup)); 
				TreeCtrl.Expand(pGroup->GetServer()->GetTreeItem(),TVE_EXPAND); 
				//TreeCtrl.RedrawWindow();  
			}
			break;
	}
}

void CLeftView::OnUpdateServerAdd(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(); 
}

void CLeftView::OnGroupEdit()
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM hItem = TreeCtrl.GetSelectedItem();
	COpcGroupItem * pGroup = (COpcGroupItem * )TreeCtrl.GetItemData(hItem); 
	if (pGroup)
	{
	    CInsertGroupDialog Dialog(this);
		Dialog.m_szGroup = pGroup->GetName(); 	
		Dialog.m_IPAddress = pGroup->GetIPAddr();
		int devAddr=  pGroup->GetDevAddr();
		CString Str;
		Dialog.m_igroupAddress.Format(_T("%x"),devAddr);
		Dialog.m_igroupPort= pGroup->GetIPPort();
		Dialog.m_Title = _T("Edit Device or Group");
		if (Dialog.DoModal()==IDOK)
		{	
			pGroup->SetName(Dialog.m_szGroup);
			pGroup->SetIPAddr(Dialog.m_IPAddress);
			int i=0;
			_stscanf(Dialog.m_igroupAddress, _T("%x"), &i);
			pGroup->SetDevAddr(i);
			pGroup->SetIPPort(Dialog.m_igroupPort);
			TreeCtrl.SetItemText(hItem,Dialog.m_szGroup);  
			LogMsg(IDS_MSG,_T("Re-open file or restart exe to take effect !"));
			GetDocument()->SetModifiedFlag();
		}
	}
}


void CLeftView::OnServerEdit()
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM hItem = TreeCtrl.GetSelectedItem();
	COpcComServerItem * pServer = (COpcComServerItem * )TreeCtrl.GetItemData(hItem); 
	if (pServer)
	{
	    CInsertServerDialog Dialog(this);
		Dialog.m_szServer = pServer->GetName(); 	
		Dialog.m_szDriverName = pServer->GetDriverName();
		Dialog.m_strBaudrate = pServer->GetComm().strBaudrate;
		Dialog.m_strDatabits = pServer->GetComm().strDatabits;
		Dialog.m_strFlowctrl = pServer->GetComm().strFlowctrl;
		Dialog.m_strStopbit = pServer->GetComm().strStopbit;
		Dialog.m_strParitybit = pServer->GetComm().strParitybit;
		Dialog.m_strPort = pServer->GetComm().strPort;
		Dialog.m_strConfigFile = pServer->GetAdvanceConFig();
		Dialog.m_Title = _T("Edit Driver or Server");
		//Dialog.SetCapture(

		if (Dialog.DoModal()==IDOK)
		{	
		ServerComm Comm;
		Comm.strBaudrate=Dialog.m_strBaudrate;
		Comm.strDatabits=Dialog.m_strDatabits;
		Comm.strFlowctrl=Dialog.m_strFlowctrl;
		Comm.strStopbit=Dialog.m_strStopbit;
		Comm.strParitybit=Dialog.m_strParitybit;
		Comm.strPort=Dialog.m_strPort;
		
		pServer->SetComm(Comm);
		pServer->SetName(Dialog.m_szServer);
		pServer->SetDriverName(Dialog.m_szDriverName);
		pServer->SetAdvanceConFig(Dialog.m_strConfigFile);

		TreeCtrl.SetItemText(hItem,Dialog.m_szServer);  
		LogMsg(IDS_MSG,_T("Re-open file or restart exe to take effect !!"));
		GetDocument()->SetModifiedFlag();
		}
	}


}

void CLeftView::OnUpdateGroupAdd(CCmdUI *pCmdUI)
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM hItem = TreeCtrl.GetSelectedItem();
	HTREEITEM h = TreeCtrl.GetParentItem(hItem); 
	pCmdUI->Enable(h==NULL && hItem); 
}

void CLeftView::OnUpdateGroupEdit(CCmdUI *pCmdUI)
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM hItem = TreeCtrl.GetSelectedItem();
	HTREEITEM h = TreeCtrl.GetParentItem(hItem); 
	pCmdUI->Enable(h!=NULL && hItem); 
}

void CLeftView::OnUpdateServerEdit(CCmdUI *pCmdUI)
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM hItem = TreeCtrl.GetSelectedItem();
	HTREEITEM h = TreeCtrl.GetParentItem(hItem); 
	pCmdUI->Enable(h==NULL && hItem); 	
}

void CLeftView::OnServerAdd()
{
	COpcServerDoc * pDoc = GetDocument();

	CInsertServerDialog Dialog(this);
	if (Dialog.DoModal()==IDOK)
	{	
		ServerComm Comm;
		Comm.strBaudrate=Dialog.m_strBaudrate;
		Comm.strDatabits=Dialog.m_strDatabits;
		Comm.strFlowctrl=Dialog.m_strFlowctrl;
		Comm.strStopbit=Dialog.m_strStopbit;
		Comm.strParitybit=Dialog.m_strParitybit;
		Comm.strPort=Dialog.m_strPort;
		pDoc->AddServerItem(Dialog.m_szServer,Dialog.m_szDriverName,Dialog.m_strConfigFile,&Comm);
	}
}


void CLeftView::OnGroupAdd()
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM item = TreeCtrl.GetSelectedItem();
	COpcComServerItem * pServer = (COpcComServerItem * )TreeCtrl.GetItemData(item); 
	if (pServer)
	{
		CInsertGroupDialog Dialog(this);
		if (Dialog.DoModal()==IDOK)
		{	
			int addr=0;
			_stscanf(Dialog.m_igroupAddress, _T("%d"), &addr);

			pServer->AddGroup(Dialog.m_szGroup,Dialog.m_IPAddress, addr,Dialog.m_igroupPort); 
			

		}
	}
	
}
void CLeftView::OnItemAdd()
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM hItem = TreeCtrl.GetSelectedItem();
	COpcGroupItem * Group = (COpcGroupItem*)TreeCtrl.GetItemData(hItem);
	if (Group)
	{
		CNewItemDialog Dialog(this);
		if (Dialog.DoModal() == IDOK)
		{
			int wrType = 0;
			int dataType = 0;
			switch (Dialog.m_dataType)//LONG;DWORD;BOOL;STRING;FLOAT;DOUBLE; //VT_UI1 byte, VT_UI2 word, VT_UI4 LBCD
			{
			case 0:
				dataType = VT_BOOL;//BOOL
				break;
			case 1:
				dataType = VT_UI1; //BYTE
				break;
			case 2:
				dataType = VT_UI2; //WORD
				break;
			case 3:
				dataType = VT_UI4; //DWORD
				break;
			case 4:
				dataType = VT_UI8; //QWORD
				break;
			case 5:
				dataType = VT_I1;//INT8
				break;
			case 6:
				dataType = VT_I2;//INT16
				break;
			case 7:
				dataType = VT_I4;//INT32
				break;
			case 8:
				dataType = VT_I8;//INT64
				break;
			case 9:
				dataType = VT_R4; // FLOAT
				break;
			case 10:
				dataType = VT_R8; //DOUBLE
				break;
			case 11:
				dataType = VT_BSTR;//STRING
				break;
			}
//			switch (Dialog.m_wrType)
//			{
///			case 0:
//				wrType = OPC_READABLE | OPC_WRITEABLE;
//				break;
//			case 1:
//				wrType = OPC_READABLE;
//				break;
///			case 2:
//				wrType = OPC_WRITEABLE;
//				break;
//			}
			//CString ss;
			//for (int i=0;i<1000;i++)
			//{
				//ss.Format(_T("%d"),i);
				//ss = Dialog.m_szTagName + ss;
			wrType = Dialog.m_wrType;
			COpcItem *Item = GetDocument()->AddTagItem(Group,Dialog.m_szTagName,Dialog.m_szTagAdr,Dialog.m_szTagDesc,dataType,wrType,Dialog.m_Range,Dialog.m_SwapData,Dialog.m_UseBigIndian); 
			


			//}
		}		
	}
}

void CLeftView::OnUpdateItemAdd(CCmdUI *pCmdUI)
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM hItem = TreeCtrl.GetSelectedItem();
	HTREEITEM h = TreeCtrl.GetParentItem(hItem); 
	pCmdUI->Enable(h!=NULL); 
}

void CLeftView::OnTvnSelchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	
	HTREEITEM hItem = pNMTreeView->itemNew.hItem ;
	if (hItem)
	{
	    CTreeCtrl & TreeCtrl = GetTreeCtrl();
		if (TreeCtrl.GetParentItem(hItem)!=NULL)
		{
			COpcGroupItem * Group = (COpcGroupItem*)TreeCtrl.GetItemData(hItem);
			if (Group)
			{
				GetDocument()->UpdateAllViews(NULL,GROUP_VIEW_UPDATE,Group); 
			}
		}
		else
		{
			//ServerItem
			GetDocument()->UpdateAllViews(NULL,GROUP_VIEW_CLEAR,NULL); 
		}
	}
	*pResult = 0;
}



void CLeftView::OnUpdateEditDelete(CCmdUI *pCmdUI)
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM hItem = TreeCtrl.GetSelectedItem();
	pCmdUI->Enable(hItem!=NULL);

}

void CLeftView::OnEditDelete()
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM hItem = TreeCtrl.GetSelectedItem();
	if (hItem)
	{
	     HTREEITEM hRootItem = TreeCtrl.GetParentItem(hItem); 
	     if (hRootItem==NULL)
		 {//Server
	         COpcComServerItem * pServer = (COpcComServerItem * )TreeCtrl.GetItemData(hItem);
			 ASSERT(pServer);			 
			 if (GetDocument()->DeleteServer(pServer))
			 TreeCtrl.DeleteItem(hItem);
 
		 }
		 else//Group
		 {
			 COpcGroupItem * pGroup = (COpcGroupItem * )TreeCtrl.GetItemData(hItem); 
			 ASSERT(pGroup);
			 if (GetDocument()->DeleteGroup(pGroup))
			 {
				 TreeCtrl.DeleteItem(hItem); 
				 GetDocument()->SetModifiedFlag(); 
			 }
		 }
	}
}

void CLeftView::OnImport()
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM hItem = TreeCtrl.GetSelectedItem();
	COpcGroupItem * pGroup = (COpcGroupItem*)TreeCtrl.GetItemData(hItem);
	if (pGroup)
	{	
		CString strTitle = _T("Import from file tags");
		CFileDialog ofn(
			TRUE,									// open dialog
			_T("*.CSV"), 								// Default extension (CSV)
			pGroup->GetName (),						// Suggested file name is group name
			OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, // Flags (hide read only files, prompt if attemt to overwrite)
			_T("CSV Files|*.CSV|"));								// Filter set
		ofn.m_ofn.lpstrTitle = strTitle;
		if (ofn.DoModal () != IDOK)
			return;
		pGroup->Import(ofn.GetPathName());  
	}
}

void CLeftView::OnUpdateImport(CCmdUI *pCmdUI)
{
	CTreeCtrl & TreeCtrl = GetTreeCtrl();
	HTREEITEM hItem = TreeCtrl.GetSelectedItem();
	HTREEITEM h = TreeCtrl.GetParentItem(hItem); 
	pCmdUI->Enable(h!=NULL); 
}
