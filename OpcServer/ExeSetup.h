#pragma once
#include "afxwin.h"


// CExeSetup dialog

class CExeSetup : public CDialog
{
	DECLARE_DYNAMIC(CExeSetup)

public:
	CExeSetup(CWnd* pParent = NULL);   // standard constructor
	virtual ~CExeSetup();
	CString GetProjectStartUp(){return m_projectStartup ;};
	void SetProjectStartUp(LPCTSTR filename ){ m_projectStartup= filename ;};
// Dialog Data
	enum { IDD = IDD_STARTUPDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedSelectfilebtn();
private:
	CString m_projectStartup;

};
