
// OpcServer.cpp 
//
#include "stdafx.h"
#include "afxwinappex.h"
#include "OpcServer.h"
#include "MainFrm.h"

#include "OpcServerDoc.h"
#include "LeftView.h"
#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

BOOL autoClosed = FALSE;
CString fileName=_T("");

// COpcServerApp

BEGIN_MESSAGE_MAP(COpcServerApp, CWinAppEx)
	ON_COMMAND(ID_APP_ABOUT, &COpcServerApp::OnAppAbout)
	//
	ON_COMMAND(ID_FILE_NEW, &CWinAppEx::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinAppEx::OnFileOpen)
END_MESSAGE_MAP()


// COpcServerApp 

COpcServerApp::COpcServerApp()
{
	m_bHiColorIcons = TRUE;
	
	//  InitInstance: important startup all here
}


COpcServerApp theApp;
// COpcServerApp 

BOOL COpcServerApp::InitInstance()
{
	CString strTitle;
	strTitle.LoadString(AFX_IDS_APP_TITLE); 
	HWND hWnd = FindWindow(NULL,strTitle);
	if (hWnd)
	{
		::SetForegroundWindow(hWnd); 
		return FALSE;
	}

	
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();
	
	if (FAILED(CoInitializeEx(NULL, COINIT_MULTITHREADED)))
    {
		return FALSE;
    }
	
	char np[FILENAME_MAX + 32];
	GetModuleFileNameA(NULL, np +1, sizeof(np) - 8);
	np[0] = '"'; strcat(np, "\"");
	
	
	if (m_lpCmdLine)
	{
		if (wcsstr(m_lpCmdLine,_T("/r")))
		{
			loServerRegister(&CLSID_OPCServer,eProgID, eClsidName, np, 0);
			return TRUE;
//			autoClosed = TRUE;
		}
		else if (wcsstr(m_lpCmdLine,_T("/u")))
		{
			loServerUnregister(&CLSID_OPCServer, eProgID);
			return TRUE;
		}
		else if (wcsstr(m_lpCmdLine,_T("-Embedding")))
		{
			loServerRegister(&CLSID_OPCServer,eProgID, eClsidName, np, 0);
			autoClosed = TRUE;
		}
		else
		{	
			loServerRegister(&CLSID_OPCServer,eProgID, eClsidName, np, 0);
			autoClosed = TRUE;
		}
	}

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	
	SetRegistryKey(_T("XceedSys_OpcServer"));
	LoadStdProfileSettings(4);  // 

	InitContextMenuManager();
	InitKeyboardManager();

	InitTooltipManager();
	CMFCToolTipInfo ttParams;
	ttParams.m_bVislManagerTheme = TRUE;
	theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL,
		RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);

	
	
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(COpcServerDoc),
		RUNTIME_CLASS(CMainFrame),       
		RUNTIME_CLASS(CLeftView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);


	// DDE
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	// cmd by dde
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	if (cmdInfo.m_nShellCommand != CCommandLineInfo::FileOpen)
	{
		CString StartupFile = this->GetProfileStringW(_T("Settings"),_T("StartupProject")); 
		CFileStatus stat;
		if ( !CFile::GetStatus(StartupFile, stat) ) 
		fileName = this->GetProfileStringW(_T("Settings"),_T("LastFileName")); 
		else
		fileName = StartupFile;

		cmdInfo.m_strFileName = fileName;

		if ((fileName.TrimLeft() !=_T(""))  && ( CFile::GetStatus(fileName, stat) ))
		{
			cmdInfo.m_nShellCommand = CCommandLineInfo::FileOpen;
		}
		else
		{
		CString fullpath=	GetAppPath();
		fileName = fullpath+_T("project.opc"); //default 
		cmdInfo.m_strFileName = fileName;

		}

	}
	if (!ProcessShellCommand(cmdInfo))
	{
		//delete fileName then put a new one
				CString fullpath= GetAppPath();
				
		fileName = fullpath+_T("project.opc");
		this->WriteProfileStringW(_T("Settings"),_T("LastFileName"),fileName); 
		return FALSE;
	}

	if (autoClosed)
       m_pMainWnd->ShowWindow(SW_MINIMIZE);
	else
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	m_pMainWnd->DragAcceptFiles();
	return TRUE;
}

//
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

//
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // 

// 
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// 
void COpcServerApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// COpcServerApp 

void COpcServerApp::PreLoadState()
{
	BOOL bNameValid;
	CString strName;
	bNameValid = strName.LoadString(IDS_EDIT_MENU);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);
	bNameValid = strName.LoadString(IDS_TREE_MENU);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_TREEMENU);
	bNameValid = strName.LoadString(IDS_EVENTMENU);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_EVENTMENU);
	
}

void COpcServerApp::LoadCustomState()
{

}

//void COpcServerApp::OnFileNew()
//{
//	CString fullpath= GetAppPath();
//	fileName = fullpath+_T("\\project.opc");
//	COpcServerApp::OnFileNew();
//}


void COpcServerApp::SaveCustomState()
{
	if (fileName.TrimLeft() = _T(""))
	{
	CString fullpath= GetAppPath();
	fileName = fullpath+_T("projects\\project.opc");
	}

		this->WriteProfileStringW(_T("Settings"),_T("LastFileName"),fileName); 

}

// COpcServerApp 
int COpcServerApp::ExitInstance()
{

	// TODO:
    CoUninitialize();
	return CWinAppEx::ExitInstance();
}
