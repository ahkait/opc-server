// OpcServerDoc.cpp : COpcServerDoc
//
#include "stdafx.h"
#include "afxwinappex.h"
#include "OpcServer.h"
#include "OpcServerDoc.h"

#include "OpcComServerItem.h"
#include "OpcGroupItem.h"
#include "OpcItem.h"
#include "MainFrm.h"
#include "SetDialog.h"
#include "ExeSetup.h"
#include "CiniFile\SimpleIni.h"

CSimpleIni iniFile; 
CMyOpcServer my_CF;
extern  DWORD objid;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern CString fileName;

CString lpzFileHead = _T("XceedOpcServer File");
// COpcServerDoc

IMPLEMENT_DYNCREATE(COpcServerDoc, CDocument)
BEGIN_MESSAGE_MAP(COpcServerDoc, CDocument)
	ON_COMMAND(ID_SETTING,&COpcServerDoc::OnSetting)
	ON_COMMAND(ID_EDIT_EXESETUP, &COpcServerDoc::OnEditExesetup)
END_MESSAGE_MAP()


// COpcServerDoc

COpcServerDoc::COpcServerDoc():m_ClientSocket(this)
{
	
	m_iserverCount = 0;
	m_itagCount = -1;
	m_strNetIP = _T("127.0.0.1");
	m_iNetPort = 30005;
	m_strUserName = _T("user");
	m_strPassword = _T("password");
	m_iDtuID = 0;
	m_StartRemoteServer= 0;

}

COpcServerDoc::~COpcServerDoc()
{
	Clear();
}

BOOL COpcServerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	Clear();


	if (my_CF.IsInited())
	    my_CF.driver_destroy();
		my_CF.driver_init(0); 

	if (objid==0)
		my_CF.RegClassFactory(); 
			ServerComm Comm;
		Comm.strBaudrate=_T("9600");
		Comm.strDatabits=_T("8");
		Comm.strFlowctrl=_T("NONE");
		Comm.strStopbit=_T("1");
		Comm.strParitybit=_T("N");
		Comm.strPort=_T("1");
	//
	AddServerItem(_T("remote"),_T(""),_T(""),&Comm);
	
	return TRUE;
}

// COpcServerDoc

void COpcServerDoc::StoreHeader()
{
   // Adding sections, keys, and values

	iniFile.SetValue(_T("opcFile"),_T("Name"), lpzFileHead.GetString());
	iniFile.SetLongValue(_T("opcFile"),_T("VER"),CURVERSION);

	iniFile.SetValue(_T("opcFile"),_T("IPAddr"),m_strNetIP);
	iniFile.SetLongValue(_T("opcFile"),_T("IPPort"),m_iNetPort);

	iniFile.SetValue(_T("opcFile"),_T("UserName"),m_strUserName);
	iniFile.SetValue(_T("opcFile"),_T("Password"),m_strPassword);
	iniFile.SetValue(_T("opcFile"),_T("StartFile"),m_strStartUpFile);
	
	iniFile.SetLongValue(_T("opcFile"),_T("DTUid"),m_iDtuID);
	iniFile.SetBoolValue(_T("opcFile"),_T("AutoStart"),m_AutoStartDriver);
	iniFile.SetLongValue(_T("opcFile"),_T("StartRemote"),m_StartRemoteServer);
	iniFile.SetLongValue(_T("opcFile"),_T("sCount"),m_iserverCount);

}

void COpcServerDoc::LoadHeader()
{
	CString value;

	//value =iniFile.GetSection(_T("opcFile"))->GetKey(_T("Name"))->GetValue().c_str() ;
	//iniFile.AddSection(_T("opcFile"))->AddKey(_T("VER"))->SetValue(value.GetString());

	m_strNetIP= iniFile.GetValue(_T("opcFile"),_T("IPAddr"));
	m_iNetPort = iniFile.GetLongValue(_T("opcFile"),_T("IPPort"));

	m_strUserName= iniFile.GetValue(_T("opcFile"),_T("UserName"));
	m_strPassword= iniFile.GetValue(_T("opcFile"),_T("Password"));
	m_strStartUpFile= iniFile.GetValue(_T("opcFile"),_T("StartFile"));
	
	
	m_iDtuID = iniFile.GetLongValue(_T("opcFile"),_T("DTUid"));
	m_AutoStartDriver = iniFile.GetBoolValue(_T("opcFile"),_T("AutoStart"));
	m_StartRemoteServer = iniFile.GetLongValue(_T("opcFile"),_T("StartRemote"));
	m_iserverCount = iniFile.GetLongValue(_T("opcFile"),_T("sCount"));


}


void COpcServerDoc::SerializeIni(CArchive& ar, LPCTSTR key )
{
	CString inisave = ar.m_strFileName+_T(".tmp");

	if (ar.IsStoring())
	{
		DeleteFile(inisave.GetString());
		StoreHeader();
		if (m_iserverCount)
		{
			POSITION pos = m_serverList.GetHeadPosition();
			while (pos)
			{
				COpcComServerItem * server = m_serverList.GetNext(pos);
				if(server)
				{
				iniFile.SetLongValue( _T("Servers"),server->GetName() ,1 );
				CString key =server->GetName();
				server->SerializeIni(ar,key); 
				}
			}
		}
		fileName = ar.m_strFileName; 
		
		iniFile.SaveFile(inisave.GetString());
	}
	else
	{
		fileName = ar.m_strFileName; 
		iniFile.LoadFile(inisave.GetString());

		CString szFileHead;
		szFileHead=	iniFile.GetValue(_T("opcFile"),_T("Name"));
		
		if (szFileHead != lpzFileHead)
		{			
			CMainFrame * mainWnd =	(CMainFrame * )AfxGetApp()->GetMainWnd(); 
			if (mainWnd)
			{
				CString szLog = _T("Load File: ");
				szLog += ar.m_strFileName;
				szLog += _T(" Fail Invalid Format opc data file, delete file and try again ");
				mainWnd->LogEvent(szLog,tEventError); 
				CString fullpath = GetAppPath();
				fileName = fullpath+_T("\\project.opc");
			}
			AfxThrowArchiveException (CArchiveException::genericException);	///header different, need to remove the file
			return;
		}

		LoadHeader();
		//// get server key list
		CSimpleIniW::TNamesDepend keys;
		iniFile.GetAllKeys( _T("Servers"), keys); //we do not know how many itens, but this section contains all
		
		CSimpleIniW::TNamesDepend::const_iterator i;

	for ( i = keys.begin(); i != keys.end(); ++i) 
		{
		
				CString svr= i->pItem;		
				svr= _T("Server\\")+ svr;
				COpcComServerItem * pserver = new COpcComServerItem();
				pserver->pDoc = this;
				pserver->SerializeIni(ar,svr); 
	            m_serverList.AddTail(pserver); 
			
		}
		fileName = ar.m_strFileName; 
	}
}

void COpcServerDoc::ReadWriteIni(LPCTSTR inifname, bool Storing, LPCTSTR key )
{
	CString inisave = inifname;

	if (Storing)
	{
		iniFile.Reset();
		DeleteFile(inisave.GetString());
		StoreHeader();
		if (m_iserverCount)
		{
			POSITION pos = m_serverList.GetHeadPosition();
			while (pos)
			{
				COpcComServerItem * server = m_serverList.GetNext(pos);
				if(server)
				{
				iniFile.SetLongValue( _T("Servers"),server->GetName() ,1 );
				CString key =server->GetName();
				server->ReadWriteIni(inifname,Storing,key); 
				}
			}
		}
		fileName = inifname;
		
		iniFile.SaveFile(inisave.GetString());
	}
	else
	{
		fileName = inifname;
		iniFile.LoadFile(inisave.GetString());

		CString szFileHead;
		szFileHead=	iniFile.GetValue(_T("opcFile"),_T("Name"));
		
		if (szFileHead != lpzFileHead)
		{			
			CMainFrame * mainWnd =	(CMainFrame * )AfxGetApp()->GetMainWnd(); 
			if (mainWnd)
			{
				CString szLog = _T("Load File: ");
				szLog += inifname;
				szLog += _T(" Fail Invalid Format opc data file, delete file and try again ");
				mainWnd->LogEvent(szLog,tEventError); 
				CString fullpath = GetAppPath();
				fileName = fullpath+_T("\\project.opc");
			}
			AfxMessageBox(_T(" Fail Invalid Format opc data file, delete file and try again "));
			return;
		}

		LoadHeader();
		//// get server key list
		CSimpleIniW::TNamesDepend keys;
		iniFile.GetAllKeys( _T("Servers"), keys); //we do not know how many itens, but this section contains all
		
		CSimpleIniW::TNamesDepend::const_iterator i;
		m_iserverCount = 0;
	for ( i = keys.begin(); i != keys.end(); ++i) 
		{
				m_iserverCount++;
				CString svr= i->pItem;		
				svr= _T("Server\\")+ svr;
				COpcComServerItem * pserver = new COpcComServerItem();
				pserver->pDoc = this;
				pserver->ReadWriteIni(inifname,Storing,svr); 

	            m_serverList.AddTail(pserver); 
			
		}
		fileName = inifname;
	}
}



void COpcServerDoc::Serialize(CArchive& ar)
{
	CString key = _T("Servers");
	SerializeIni( ar, key );
	return;

	CString inisave = ar.m_strFileName+_T(".tmp");

	if (ar.IsStoring())
	{
		DeleteFile(inisave.GetString());
		StoreHeader();
/*		ar << lpzFileHead;
		// Net opc
		ar << m_strNetIP;
		ar << m_iNetPort;
		ar << m_strUserName;
		ar << m_strPassword;
		ar << m_iDtuID;
		ar << m_AutoStartDriver;
		ar << m_strStartUpFile ;
		ar<< m_StartRemoteServer;
		ar << CURVERSION;
		ar << m_iserverCount;
*/
		if (m_iserverCount)
		{
			POSITION pos = m_serverList.GetHeadPosition();
			while (pos)
			{
				COpcComServerItem * server = m_serverList.GetNext(pos);
				if(server)
				{
				iniFile.SetLongValue( _T("Servers"),server->GetName() ,1 );
				CString key =server->GetName();
				server->SerializeIni(ar,key); 
				}
			}
		}
		fileName = ar.m_strFileName; 
		iniFile.SaveFile(inisave.GetString());
	}
	else
	{
		fileName = ar.m_strFileName; 
		iniFile.LoadFile(inisave.GetString());

		CString szFileHead;
		szFileHead=	iniFile.GetValue(_T("opcFile"),_T("Name"));

	//	ar >> szFileHead;			
		if (szFileHead != lpzFileHead)
		{			
			CMainFrame * mainWnd =	(CMainFrame * )AfxGetApp()->GetMainWnd(); 
			if (mainWnd)
			{
				CString szLog = _T("Load File: ");
				szLog += ar.m_strFileName;
				///delete this file ar.m_strFileName
				szLog += _T(" Fail Invalid Format opc data file, delete file and try again ");
				mainWnd->LogEvent(szLog,tEventError); 
				CString fullpath = GetAppPath();
				fileName = fullpath+_T("\\project.opc");
			}
			AfxThrowArchiveException (CArchiveException::genericException);	///header different, need to remove the file
			return;
		}
		
/*		ar >> m_strNetIP;
		ar >> m_iNetPort;
		ar >> m_strUserName;
		ar >> m_strPassword;
		ar >> m_iDtuID;
		ar >> m_AutoStartDriver;
		ar >> m_strStartUpFile ;
		ar >> m_StartRemoteServer;
		ar >> dwVersion; //CURVERSION
		ar >> m_iserverCount;
*/
		LoadHeader();
		//// get server key list
		CSimpleIniW::TNamesDepend keys;
		iniFile.GetAllKeys( _T("Servers"), keys);

		
		CSimpleIniW::TNamesDepend::const_iterator i;
	//	for (int i=0;i<m_iserverCount;i++)
	for ( i = keys.begin(); i != keys.end(); ++i) 
		{
		
				CString key= i->pItem;
				COpcComServerItem * pserver = new COpcComServerItem();
				pserver->pDoc = this;
				pserver->SerializeIni(ar,key); 
	            m_serverList.AddTail(pserver); 
			
		}
		fileName = ar.m_strFileName; 
	}
}


// COpcServerDoc 

#ifdef _DEBUG
void COpcServerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void COpcServerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// COpcServerDoc 

COpcComServerItem *  COpcServerDoc::AddServerItem(CString strServerName,CString strDriverName,CString strAdvanceConfigfile, ServerComm *Comm)
{
	CSafeLock cs (&m_csDataLock);	
	COpcComServerItem * pserver = new COpcComServerItem(this,strServerName, strDriverName,strAdvanceConfigfile, Comm );
	if (pserver)
	{
		m_serverList.AddTail(pserver); 
		m_iserverCount++;
		UpdateAllViews(NULL,SERVER_ADD,pserver); 
		//Net Serve use
		if (strServerName == _T("remote"))
		{
			//pserver->AddGroup(_T("ai"));
			//pserver->AddGroup(_T("di"));

			pserver->AddGroup(_T("float"),0,0,0);
			pserver->AddGroup(_T("double"),0,0,0);
			pserver->AddGroup(_T("int"),0,0,0);
			pserver->AddGroup(_T("long"),0,0,0);
			pserver->AddGroup(_T("bool"),0,0,0);
			pserver->AddGroup(_T("string"),0,0,0);

		}
		SetModifiedFlag(); 
	}
	return pserver;
}
BOOL COpcServerDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
    // Call base class function with empty local Serialize function
    // to check file etc
  //  if (!CDocument::OnSaveDocument(lpszPathName))
  //      return FALSE;
	ReadWriteIni(lpszPathName,true,NULL);
	SetModifiedFlag(false); 

    return TRUE;
}
BOOL COpcServerDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	Clear();
//	if (!CDocument::OnOpenDocument(lpszPathName)) ///here change it
//		return FALSE;
	///load ini from here

	   ReadWriteIni(lpszPathName,false,NULL);


	CString szLog = _T("Open File: ");
	szLog += lpszPathName;
	LogMsg(IDS_MSG,szLog);


	if (my_CF.IsInited())
	    my_CF.driver_destroy();
	my_CF.driver_init(0); 
	POSITION pos = m_serverList.GetHeadPosition();
	COpcComServerItem * server;
	while (pos)
	{
		server = m_serverList.GetNext(pos);

		server->CreateOpcTag(); 
		/// if (m_AutoStartDriver)
		server->StartServer(CWnd::FromHandle(this->m_hWndHost),TRUE);
		
	}

	if (objid==0)
		my_CF.RegClassFactory(); 

	m_itagCount = -1;
	if (m_StartRemoteServer==1) 
	ConnectToServer(); //external tcp to main server
	////// start thread to get/set data

	return TRUE;
}

void COpcServerDoc::Clear(void)
{	
	m_aiArray.RemoveAll();
    m_diArray.RemoveAll();

	POSITION pos = m_serverList.GetHeadPosition();
	while (pos)
	{
		COpcComServerItem * server = m_serverList.GetNext(pos);
		server->StartServer( CWnd::FromHandle(this->m_hWndHost) ,FALSE);
		server->m_nTerminateThread = 1;
		Sleep(50);
		server->Remove();
		delete server;
	}
	m_serverList.RemoveAll();
	UpdateAllViews(NULL,FILENEW,NULL); 
	m_iserverCount = 0;
	m_itagCount = 0;
	
}

BOOL COpcServerDoc::SaveModified()
{
	// TODO: 

	return CDocument::SaveModified();
}

void COpcServerDoc::Simulate(void)
{
	POSITION pos = m_serverList.GetHeadPosition();
	while (pos)
	{
		COpcComServerItem * server = m_serverList.GetNext(pos);
		server->Simulate(); 
	}
}

int  COpcServerDoc::GetTagCount()
{
	if (m_itagCount<0 || IsModified())
	{
		int count = 0;
		POSITION pos = m_serverList.GetHeadPosition();
		while (pos)
		{
			COpcComServerItem * server = m_serverList.GetNext(pos);
			count += server->GetTagCount();
		}
		m_itagCount = count;
	}
	return m_itagCount;
}

void  COpcServerDoc::GetSelectItems(COpcGroupItem * pGroup,CObArray & obArray)
{
	if (pGroup)
		pGroup->GetTagItems(obArray); 
}

void  COpcServerDoc::DeleteItems(COpcGroupItem * pGroup,CObArray & obArray)
{
	ASSERT(pGroup!=NULL);
	CSafeLock cs (&m_csDataLock);	
	pGroup->DeleteItems(obArray);	
	UpdateAllViews(NULL,GROUP_VIEW_UPDATE,pGroup); 
	SetModifiedFlag(); 
}

COpcItem * COpcServerDoc::AddTagItem(COpcGroupItem * pGroup,LPCTSTR strName, LPCTSTR strAddress,LPCTSTR strDesc,int datatype,int wrType,int range, int SwapData,int UseBigIndian)
{
	COpcItem * pItem = NULL;
	CSafeLock cs (&m_csDataLock);	
	if (pGroup)
	{		
		pItem = pGroup->AddItem(strName,strAddress,strDesc,datatype,wrType,range);

			pItem->SetName(strName );
			pItem->SetAddress(strAddress);
			pItem->SetDesc(strDesc);
			pItem->SetSwapData(SwapData);
			pItem->SetBigIndia(UseBigIndian);
			pItem->SetWrType(wrType);
			
			pItem->SetType(datatype);
			pItem->SetRange(range);
			//COpcServerView* pView =  GetItemView();
		
	}	
	if (pItem)
	{
		
		UpdateAllViews(NULL,GROUP_VIEW_ADDITEM,pItem); 
		SetModifiedFlag(); 
	}
	return pItem;
}


BOOL COpcServerDoc::DeleteGroup(COpcGroupItem * pGroup)
{
	if (pGroup)
	{   CSafeLock cs (&m_csDataLock);	
		if (pGroup->GetServer()->DeleteGroup(pGroup))
		{
			//SetModifiedFlag(); 
			return TRUE;
		}
	}
	return FALSE;
}

BOOL COpcServerDoc::DeleteServer(COpcComServerItem * pServer)
{
	if (pServer)
	{
		CSafeLock cs (&m_csDataLock);	
		pServer->Remove();
		POSITION pos = 	m_serverList.Find(pServer);
		if (pos)
		{
			m_serverList.RemoveAt(pos);
			delete pServer;
			SetModifiedFlag(); 
			return TRUE;
		}
	}
	return FALSE;
}

COpcComServerItem * COpcServerDoc::GetServerItem(CString strServerName)
{
	COpcComServerItem * pServer = NULL;
	CSafeLock cs (&m_csDataLock);
	POSITION pos = m_serverList.GetHeadPosition();
	while (pos)
	{ 
		pServer = m_serverList.GetNext(pos);
		if (pServer->GetName() == strServerName)
		{
			break;
		}
	}
	return pServer;
}

//NetWork Read
BOOL COpcServerDoc::ConnectToServer()
{
   BOOL bResult = FALSE;
   if (m_ClientSocket.m_hSocket!=INVALID_SOCKET)
	   m_ClientSocket.Close();
   bResult = m_ClientSocket.Create();//20005,SOCK_STREAM,_T("222.185.237.251"));
   if (bResult)
   {
	   bResult = m_ClientSocket.Connect(m_strNetIP,m_iNetPort);
	   CString szLog;
	   if (bResult)
	   {
	     szLog = _T("Successfully connect to the remote : ")+m_strNetIP;
	   }
	   else
		   szLog = _T("Remote not found");
	   LogMsg(IDS_MSG,szLog);
	   //m_strNetIP = _T("192.168.0.1");
	   //m_iNetPort = 30005;
   }
   else
   {
	   CString szLog = _T("Remote not found");
	   LogMsg(IDS_ERMSG,szLog);
   }
   return bResult;
}

//Net Server AddTags
void COpcServerDoc::AddTag(CString strTag,int index,BOOL bAI)
{
	CSafeLock sa(&m_csDataLock);
	if (bAI)
	{		
		COpcComServerItem * pServer = GetServerItem(_T("remote"));
		if (pServer)
		{
			COpcGroupItem * pGroup = pServer->GetGroup(_T("float"));
			if (pGroup)
			{
				COpcItem * pItem = pGroup->AddItem(strTag,_T(""),_T(""),VT_R4,OPC_READABLE | OPC_WRITEABLE,1);
				if (pItem)
				{
					pItem->SetItemID(index); 
					m_aiArray.SetAt(index,pItem);
				    UpdateAllViews(NULL,GROUP_VIEW_ADDITEM,pItem); 
		            SetModifiedFlag(); 
				}
			}
		}
	}
	else //DI
	{		
		COpcComServerItem * pServer = GetServerItem(_T("remote"));
		if (pServer)
		{
			COpcGroupItem * pGroup = pServer->GetGroup(_T("bool"));
			if (pGroup)
			{
				COpcItem * pItem = pGroup->AddItem(strTag,_T(""),_T(""),VT_BOOL,OPC_READABLE | OPC_WRITEABLE,1);
				if (pItem)
				{
					pItem->SetItemID(index); 
					m_diArray.SetAt(index,pItem);
				    UpdateAllViews(NULL,GROUP_VIEW_ADDITEM,pItem); 
		            SetModifiedFlag(); 
				}
			}
		}
	}
}

void COpcServerDoc::SetAiCount(int count)
{
	CSafeLock sa(&m_csDataLock);
	m_aiArray.RemoveAll();
	m_aiArray.SetSize(count);
}

void COpcServerDoc::SetDiCount(int count)
{
	CSafeLock sa(&m_csDataLock);
	m_diArray.RemoveAll();
	m_diArray.SetSize(count);
}

void COpcServerDoc::UpDate(int index,float f)
{
	if (index<m_aiArray.GetCount())
	{
		COpcItem * pItem = (COpcItem*)m_aiArray.GetAt(index);
		if (pItem)
		   pItem->UpDate(f);
	}
}
	
void COpcServerDoc::UpDate(int index,bool b)
{
	if (index<m_diArray.GetCount())
	{
		COpcItem * pItem = (COpcItem*)m_diArray.GetAt(index);
		if (pItem)
		   pItem->UpDate(b);
	}

}

void COpcServerDoc::OnSetting()
{
	CSetDialog dialog(AfxGetApp()->m_pMainWnd);
	dialog.m_dtuID = m_iDtuID;
	dialog.m_strServerIP = m_strNetIP;
	dialog.m_ServerPort = m_iNetPort;
	dialog.m_strPwd = m_strPassword;
	dialog.m_strUser = m_strUserName;
	//dialog.m_StartupFile=m_strStartUpFile ;
	dialog.m_EnableRemoteServer.SetCheck( m_StartRemoteServer );
	if (dialog.DoModal() == IDOK)
	{
		m_iDtuID = dialog.m_dtuID;
		m_strNetIP = dialog.m_strServerIP;
		m_iNetPort = dialog.m_ServerPort;
		m_strPassword = dialog.m_strPwd;
		m_strUserName = dialog.m_strUser;
		m_StartRemoteServer = dialog.m_EnableRemoteServer.GetCheck();
		//m_strStartUpFile = dialog.m_StartupFile;
		SetModifiedFlag();
	}
}

void COpcServerDoc::OnEditExesetup()
{
	// TODO: Add your command handler code here
	
	CExeSetup dialog(AfxGetApp()->m_pMainWnd);
	CString ffile =  theApp.GetProfileStringW(_T("Settings"),_T("StartupProject"));
	dialog.SetProjectStartUp(ffile) ;
	if (dialog.DoModal() == IDOK)
	{

		m_strStartUpFile = dialog.GetProjectStartUp();
		theApp.WriteProfileStringW(_T("Settings"),_T("StartupProject"),m_strStartUpFile); 

		///write to profile
		// SetModifiedFlag();
	}

}
