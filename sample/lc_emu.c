#include <windows.h>
#include <math.h>
#include <time.h>

#include <lcard_ad.h>
#include <..\unilog\unilog.h>

static double time_poll=0.;
static double time_delay=0.;
static short Nchn=0;
static __int64 soft_start=0, freq=1;
static WORD *chans;
static int index=0;

#undef UL_DEBUG
#define UL_DEBUG(x) /* x */
#define LOGID logg,0
extern unilog *logg;

int _EXPORT GET_STATUS() { return 0; }
int _EXPORT PLATA_TEST_CC() { return 0; }
int _EXPORT PLATA_TEST() { return 0; }
int _EXPORT FAST_LOADING_LC010(WORD *BiosCode) { return 0; }
void _EXPORT CONFIG_RX_CHANNEL_LCI01(int Channel) { return; }
void _EXPORT CONFIG_TX_CHANNEL_LCI01(int Channel) { return; }
int _EXPORT FAST_LOADING_LCI01(WORD *BiosCode) { return 0; }
void _EXPORT SET_DEVICE_BINDING(int InterfaceDevice) { return; }
void _EXPORT SET_INTERFACE_TYPE(int Mode) { return; }
void _EXPORT SET_CRAIT_ADDRESS(int Base) { return; }
void _EXPORT SET_BASE_ADDRESS(int Base) { return; }
void _EXPORT SET_BOARD_TYPE(int Base) { return; }
void _EXPORT SELECT_SLOT(int Base) { return; }


void _EXPORT SET_ERROR_HANDLER(PTErrorHandler NewErrorHandler) {}
int  _EXPORT LOADCRAIT(int CraitType, int Adr) { return 0; }

void _EXPORT FORCE_INTER_DELAY_CC(int t)
{ 
	time_delay=(double)t/10.;
	UL_DEBUG((LOGID, "FORCE_INTER_DELAY_CC() - set time_delay=%g",time_delay));
}
void _EXPORT CONFIG_FIFO_CC(int l, int h) {}
void _EXPORT SET_FIFO_SIZE_CC(int FifoSize) {}
void _EXPORT SOFT_CONFIG_CC(int Slot_301, int NChannel, WORD* Channels, int Rate)
{
	time_poll=(double)Rate;
	Nchn=NChannel;
	chans=Channels;
	index=0;
	UL_DEBUG((LOGID, "SOFT_CONFIG_CC() - time_poll=%g Nchn=%u",time_poll,Nchn));
}

int _EXPORT LOAD_BIOS_LC451(int Slot, WORD* BiosCode) { return 0; };
int  _EXPORT SET_CALIBRATION_LC227(int Slot) { return 0x0307; }
void _EXPORT SET_IRQ_MASK_CC(int IrqMask) {}

void _EXPORT SET_LC227DATA_IRQSET_CC() {}
void _EXPORT SET_IRQ_INFOM_CC(int NumCraitSlots, WORD* InfArr) {}
void _EXPORT SET_THRESHOLD_LC451(int Slot, int Chan, int Typ_, int Val) {}
void _EXPORT CONFIG_LC451(int Slot, int Mode, int Scale) {}
void _EXPORT SET_CALIBR_MODE_CC(int CalibrMode) {}
void _EXPORT READ_CALIBR_CC() {}

void _EXPORT SOFT_START_CC() { QueryPerformanceCounter((LARGE_INTEGER *)&soft_start); 
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq);}
void _EXPORT SOFT_STOP_CC() { soft_start=0;}
int _EXPORT GET_NWORDS_CC(unsigned long *n)
{
	double during;
	__int64 now;
	int Np, Nd;
	if(time_poll==0 || time_delay==0. || soft_start == 0) { *n=0; return -1; }
	QueryPerformanceCounter((LARGE_INTEGER *)&now);
	during=(double)(now-soft_start)*1e6/(double)freq;
	Np=(int)(during/time_poll);
	Nd=(int)((during-(double)Np*time_poll)/time_delay);
	if(Nd > Nchn) Nd=Nchn;
	*n = Np*Nchn + Nd;
	if( *n > 8*1024) return -1;
	return 0;
}
void _EXPORT GET_SOFT_DATA_CC(WORD *tcVal, unsigned long n)
{
	int Np, Nd;
	static short tc=1118, d=1;
	__int64 now;

//	if(tc > 1460 || tc < 1100) d = -d;
//	tc += d;

	Np=n/Nchn;
	Nd = n - Np*Nchn;
	soft_start +=  (__int64)((double)Np*time_poll + (double)Nd*time_delay)*freq/1000000;
	while(n) {
		int slot=chans[2*index];

		if(slot==10) {
			*tcVal++ = 512 + (chans[2*index+1]&31) * (1024 / 16);
		}
		else if(slot >= 12 && slot <= 14) *tcVal++ = 1118 + ((slot-12)*8+(chans[2*index+1]&7))*5;
		else *tcVal++ = 0;
		index++; n--;
		if(index >= Nchn) index=0;
	}
	do {
		QueryPerformanceCounter((LARGE_INTEGER *)&now);
	} while( now < soft_start);
}
WORD _EXPORT STATUS_LC451(int Slot) { return 0xff; }
void _EXPORT KADR_LC451(int slot, int mode, int scale, int mask, double* hzVal)
{
	static double v=3141.5926*1.5, dv=8.;
	int i;

	for(i=0; i<8; i++) hzVal[i] = 140.*(1.+sin(v*((double)i+1.)/1000.));
	v += dv;
	if( v > 2*3141.5926) v=0.;
}
void _EXPORT OUT_DIGITS_402(int Slot, WORD Data) {}
DWORD _EXPORT INP_DIGITS_403(int slot)
{
	static DWORD msk=0;
	static int t=0, one=1;

	if(t==10) { t=0; msk <<= 1; msk |= one; if(msk==-1 || msk==0) one ^= 1; }
	t++;
	return msk;
}

static double fast_kadr_delay=0.;

void _EXPORT SET_FAST_PARAMS_CC(short Slot301, short DACDelayValue, short NChannel)
{
	fast_kadr_delay=(double)DACDelayValue/10000.;
}

static WORD *fast_kadr_chans=NULL;

void _EXPORT CONFIGURE_FAST_KADR_CC(short Nch, WORD* Buffer, WORD ModuleType)
{
	fast_kadr_chans=Buffer;
}

void _EXPORT READ_FAST_KADR_CC(short Nch, short* Datad)
{
	int i;
	DWORD slp=(DWORD)(fast_kadr_delay*(double)Nch)+GetTickCount();
	for(i=0; i<Nch; i++) Datad[i]=fast_kadr_chans[i]; //i;
	while(slp > GetTickCount()) {
//		UL_DEBUG((LOGID, "Wait"));
	}
}

void _EXPORT READ_FAST_KADR111_CC(WORD Nch, short* Datad)
{
	READ_FAST_KADR_CC(Nch, Datad);
}

WORD _EXPORT CREATE_FAST_CHAN_CC(short ModuleType, short Slot, WORD LogChan)
 { return ModuleType+Slot+LogChan+1;}
void _EXPORT INITIALIZE_CORRECTION_PC(WORD CalibrMode) {}

WORD _EXPORT SAMPLE_LC301(slot301, slot104, chan104)
{
	return (chan104<<4) | (slot104 & 0xf);
}




