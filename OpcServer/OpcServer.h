
// OpcServer.h : OpcServer 
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // 


// COpcServerApp:
//
//

class COpcServerApp : public CWinAppEx
{
public:
	COpcServerApp();


// 
public:
	virtual BOOL InitInstance();

// 
	BOOL  m_bHiColorIcons;
	virtual void PreLoadState();
	virtual void LoadCustomState();
	virtual void SaveCustomState();
//	virtual void OnFileNew();
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	virtual int ExitInstance();
};

extern COpcServerApp theApp;
