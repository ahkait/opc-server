// MainFrm.cpp : CMainFrame 
//
#include "stdafx.h"
#include "OpcServer.h"
#include "MainFrm.h"
#include "LeftView.h"
#include "EventView.h"
#include "OpcServerView.h"
#include "OpcServerDoc.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern  DWORD objid;
extern  CMyOpcServer my_CF;
extern  BOOL autoClosed;
UINT    uTimer = 1;
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWndEx)

const int  iMaxUserToolbars = 10;
const UINT uiFirstUserToolBarId = AFX_IDW_CONTROLBAR_FIRST + 40;
const UINT uiLastUserToolBarId = uiFirstUserToolBarId + iMaxUserToolbars - 1;

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWndEx)
	ON_WM_CREATE()
	//ON_UPDATE_COMMAND_UI_RANGE(AFX_ID_VIEW_MINIMUM, AFX_ID_VIEW_MAXIMUM, &CMainFrame::OnUpdateViewStyles)
	//ON_COMMAND_RANGE(AFX_ID_VIEW_MINIMUM, AFX_ID_VIEW_MAXIMUM, &CMainFrame::OnViewStyle)
	ON_UPDATE_COMMAND_UI(IDS_CLIENTCOUNT, &CMainFrame::OnUpdatePane)
	ON_COMMAND(ID_VIEW_CUSTOMIZE, &CMainFrame::OnViewCustomize)
	ON_REGISTERED_MESSAGE(AFX_WM_CREATETOOLBAR, &CMainFrame::OnToolbarCreateNew)
	ON_WM_CLOSE()
	ON_WM_TIMER()
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, &CMainFrame::OnUpdateFileNew)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, &CMainFrame::OnUpdateFileOpen)

	ON_MESSAGE(WM_USER+500,OnNotifyMessage)
	ON_WM_SIZE()
	ON_COMMAND(ID_APP_EXIT, &CMainFrame::OnAppExit)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,          
    IDS_CLIENTCOUNT,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,

	AFX_IDS_UNTITLED
};

// CMainFrame 

CMainFrame::CMainFrame()
{
	m_clientCount = 0;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	BOOL bNameValid;

	//
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
	//CMFCVisualManagerOfficeXP
	//CMFCVisualManagerVS2005
	//CMFCVisualManagerOffice2003
	if (!m_wndMenuBar.Create(this))
	{
		TRACE0("Failed to create the menu bar\n");
		return -1;      // 
	}

	m_wndMenuBar.SetPaneStyle(m_wndMenuBar.GetPaneStyle() | CBRS_SIZE_DYNAMIC | CBRS_TOOLTIPS | CBRS_FLYBY);

	// 
	CMFCPopupMenu::SetForceMenuFocus(FALSE);

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(theApp.m_bHiColorIcons ? IDR_MAINFRAME_256 : IDR_MAINFRAME))
	{
		TRACE0("Failed to create the tool bar\n");
		return -1;      // 
	}

	CString strToolBarName;
	bNameValid = strToolBarName.LoadString(IDS_TOOLBAR_STANDARD);
	ASSERT(bNameValid);
	m_wndToolBar.SetWindowText(strToolBarName);

	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);
	m_wndToolBar.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);

	// :
	InitUserToolbars(NULL, uiFirstUserToolBarId, uiLastUserToolBarId);

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // 
	}
	m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));

	
	m_wndMenuBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndMenuBar);
	DockPane(&m_wndToolBar);

	//  Visual Studio 2005
	CDockingManager::SetDockingMode(DT_SMART);
	//  Visual Studio 2005
	EnableAutoHidePanes(CBRS_ALIGN_ANY);

	// 
	EnablePaneMenu(TRUE, ID_VIEW_CUSTOMIZE, strCustomize, ID_VIEW_TOOLBAR);

	// 
	CMFCToolBar::EnableQuickCustomization();

	if (CMFCToolBar::GetUserImages() == NULL)
	{
		//
		if (m_UserImages.Load(_T(".\\UserImages.bmp")))
		{
			m_UserImages.SetImageSize(CSize(16, 16), FALSE);
			CMFCToolBar::SetUserImages(&m_UserImages);
		}
	}

	// 
	// 
	CList<UINT, UINT> lstBasicCommands;

	lstBasicCommands.AddTail(ID_FILE_NEW);
	lstBasicCommands.AddTail(ID_FILE_OPEN);
	lstBasicCommands.AddTail(ID_FILE_SAVE);
	lstBasicCommands.AddTail(ID_FILE_PRINT);
	lstBasicCommands.AddTail(ID_APP_EXIT);
	lstBasicCommands.AddTail(ID_EDIT_CUT);
	lstBasicCommands.AddTail(ID_EDIT_PASTE);
	lstBasicCommands.AddTail(ID_EDIT_UNDO);
	lstBasicCommands.AddTail(ID_APP_ABOUT);
	lstBasicCommands.AddTail(ID_VIEW_STATUS_BAR);
	lstBasicCommands.AddTail(ID_VIEW_TOOLBAR);

	CMFCToolBar::SetBasicCommands(lstBasicCommands);

	uTimer   =	SetTimer(uTimer,500,NULL);
	strClientCount.LoadStringW(IDS_CLIENTCOUNT); 

    m_notify.cbSize = sizeof(NOTIFYICONDATA); 
    m_notify.hWnd = this->m_hWnd; 
    m_notify.uID = 1000; 
    m_notify.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP; 
    m_notify.uCallbackMessage = WM_USER+500;; 
    m_notify.hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME); 
	wcscpy(m_notify.szTip,_T("XceedSys.OpcServer")); 
	wcscpy(m_notify.szInfo,_T("XceedSys.OpcServer")); 
    wcscpy(m_notify.szInfoTitle,_T("OpcServer")); 
	Shell_NotifyIcon(NIM_ADD,&m_notify);

	return 0;
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/,
								CCreateContext* pContext)
{
	//

	if (!m_wndEventView.CreateStatic(this,2,1))
		return FALSE;

	if (!m_wndSplitter.CreateStatic(&m_wndEventView,1,2))
		return FALSE;
	RECT rc;
	GetClientRect (&rc);

	if (!m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CLeftView), CSize (rc.right / 5, 3 * rc.bottom / 4), pContext) ||
		!m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(COpcServerView), CSize (4 * rc.right / 5, 3 * rc.bottom / 4), pContext) ||
		!m_wndEventView.CreateView(1, 0,  RUNTIME_CLASS(CEventView), CSize (rc.right, rc.bottom / 4), pContext)) 
	{
		m_wndSplitter.DestroyWindow();
		return FALSE;
	}

	m_wndEventView.SetRowInfo(0,3 * rc.bottom/4 ,10); 
	m_EventView  = (CEventView *) m_wndEventView.GetPane (1, 0);
	m_LeftView = (CLeftView *)m_wndSplitter.GetPane(0,0); 


	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWndEx::PreCreateWindow(cs) )
		return FALSE;
	cs.style &= ~FWS_ADDTOTITLE;
	
	return TRUE;
}

// CMainFrame 

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWndEx::Dump(dc);
}
#endif //_DEBUG


// CMainFrame 

COpcServerView* CMainFrame::GetItemView()
{
	CWnd* pWnd = m_wndSplitter.GetPane(0, 1);
	COpcServerView* pView = DYNAMIC_DOWNCAST(COpcServerView, pWnd);
	return pView;
}

/*
void CMainFrame::OnUpdateViewStyles(CCmdUI* pCmdUI)
{
	if (!pCmdUI)
		return;
	
	COpcServerView* pView = GetItemView();

	

	if (pView == NULL)
		pCmdUI->Enable(FALSE);
	else
	{
		DWORD dwStyle = pView->GetStyle() & LVS_TYPEMASK;

		

		if (pCmdUI->m_nID == ID_VIEW_LINEUP)
		{
			if (dwStyle == LVS_ICON || dwStyle == LVS_SMALLICON)
				pCmdUI->Enable();
			else
				pCmdUI->Enable(FALSE);
		}
		else
		{
			
			pCmdUI->Enable();
			BOOL bChecked = FALSE;

			switch (pCmdUI->m_nID)
			{
			case ID_VIEW_DETAILS:
				bChecked = (dwStyle == LVS_REPORT);
				break;

			case ID_VIEW_SMALLICON:
				bChecked = (dwStyle == LVS_SMALLICON);
				break;

			case ID_VIEW_LARGEICON:
				bChecked = (dwStyle == LVS_ICON);
				break;

			case ID_VIEW_LIST:
				bChecked = (dwStyle == LVS_LIST);
				break;

			default:
				bChecked = FALSE;
				break;
			}
			pCmdUI->SetRadio(bChecked ? 1 : 0);
		}
	}
}
*/
/*
void CMainFrame::OnViewStyle(UINT nCommandID)
{
	
	COpcServerView* pView = GetItemView();

	
	if (pView != NULL)
	{
		DWORD dwStyle = -1;

		switch (nCommandID)
		{
		case ID_VIEW_LINEUP:
			{
				// 
				CListCtrl& refListCtrl = pView->GetListCtrl();
				refListCtrl.Arrange(LVA_SNAPTOGRID);
			}
			break;

			// 
		case ID_VIEW_DETAILS:
			dwStyle = LVS_REPORT;
			break;

		case ID_VIEW_SMALLICON:
			dwStyle = LVS_SMALLICON;
			break;

		case ID_VIEW_LARGEICON:
			dwStyle = LVS_ICON;
			break;

		case ID_VIEW_LIST:
			dwStyle = LVS_LIST;
			break;
		}

		// 
		if (dwStyle != -1)
			pView->ModifyStyle(LVS_TYPEMASK, dwStyle);
	}
}
*/

void CMainFrame::OnViewCustomize()
{
	CMFCToolBarsCustomizeDialog* pDlgCust = new CMFCToolBarsCustomizeDialog(this, TRUE /* */);
	pDlgCust->EnableUserDefinedToolbars();
	pDlgCust->Create();
}

LRESULT CMainFrame::OnToolbarCreateNew(WPARAM wp,LPARAM lp)
{
	LRESULT lres = CFrameWndEx::OnToolbarCreateNew(wp,lp);
	if (lres == 0)
	{
		return 0;
	}
	CMFCToolBar* pUserToolbar = (CMFCToolBar*)lres;
	ASSERT_VALID(pUserToolbar);
	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);
	pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
	return lres;
}

BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext) 
{
	// 
	if (!CFrameWndEx::LoadFrame(nIDResource, dwDefaultStyle, pParentWnd, pContext))
	{
		return FALSE;
	}
	// 
	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);
	for (int i = 0; i < iMaxUserToolbars; i ++)
	{
		CMFCToolBar* pUserToolbar = GetUserToolBarByIndex(i);
		if (pUserToolbar != NULL)
		{
			pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
		}
	}
	return TRUE;
}


void CMainFrame::OnClose()
{
	
	if (my_CF.GetClientCount() ==0)
	{	
		KillTimer(uTimer);	
		Shell_NotifyIcon(NIM_DELETE,&m_notify);		
		CFrameWndEx::OnClose();
	}
	else
	{
		LogMsg(IDS_ERMSG,_T("Has an active client connections, not allowed to shut down the server!"));
		if (MessageBox(_T("Has an active client connections, you sure you want to exit the server?"),_T("Quit Program"),MB_OKCANCEL|MB_ICONQUESTION)==IDOK)
		{
			KillTimer(uTimer);					
			autoClosed = true;
		    my_CF.ShutDown();	
		}
	}
}

void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	
	if (nIDEvent==uTimer)
	{
		m_clientCount = my_CF.GetClientCount();
		CString strClient;
		int tagCount = GetItemView()->GetDocument()->GetTagCount(); 
		strClient.Format( strClientCount,m_clientCount,tagCount);
		m_wndStatusBar.SetPaneText(1,strClient); 
		
		//if (m_clientCount)
		//	GetItemView()->GetDocument()->Simulate(); 
			
		
	}
	else	
		CFrameWndEx::OnTimer(nIDEvent);
}

void CMainFrame::OnUpdatePane(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
}
void CMainFrame::OnUpdateFileNew(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_clientCount==0); 
}

void CMainFrame::OnUpdateFileOpen(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_clientCount==0);
}

void CMainFrame::LogEvent(CString szLog, EVENTTYPE type)
{
	if (m_EventView)
	{
		m_EventView->LogMsg(type,szLog);
	}
}


LRESULT  CMainFrame::OnNotifyMessage(WPARAM wParam,LPARAM lParam)
{
	switch (lParam)
	{
	case WM_LBUTTONDBLCLK:
		if (!IsWindowVisible())
		{
			ShowWindow(SW_SHOW);
			if (IsIconic())
				OpenIcon();
			BringWindowToTop();
		}
		else
			ShowWindow(SW_MINIMIZE); 
		break;

	}
	return 1;
}
void CMainFrame::OnSize(UINT nType, int cx, int cy)
{	
	if (nType == SIZE_MINIMIZED)
	{
		ShowWindow(SW_HIDE);
		return;
	}
	CFrameWnd::OnSize(nType, cx, cy);

	
}

void CMainFrame::OnAppExit()
{
	// TODO: Add your command handler code here
	///Sleep(100);
}


BOOL CMainFrame::OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	// TODO: Add your specialized code here and/or call the base class

	switch (message)
	{
		case WM_USER+2 :
		{
			LPCTSTR lpszString = (LPCTSTR)lParam;
			LogMsg((UINT)wParam,lpszString);
			Sleep(10);
			pResult = 0;
			break;
		}
	}
	return CFrameWndEx::OnWndMsg(message, wParam, lParam, pResult);
}
