#ifndef _LC_DRV_H
#define _LC_DRV_H

#ifdef __cplusplus
extern "C" {
#endif

extern unilog *logg;
#define LOGID logg,0
#define CHECK	UL_DEBUG((LOGID,"line %d",__LINE__))

#define C_NONE (-300)
#define LIM_NRM	(0)
#define LIM_PH	(1)
#define LIM_WH	(2)
#define LIM_WL	(3)
#define LIM_PL	(4)

struct gItem {short pl, wl, wh, ph, init, min, max, used; };
struct tcItem {
	struct gItem gi; 
	short slot104, chan, mode, gain;
	short I0, R0, alpha;
	short RD, TD;
	char name[8];
	int graf;
};
extern struct tcItem tcItemCfg[];

struct hzItem {
	struct gItem gi;
	short chan, lo, hi;
	int divid, multi;
	char name[8];
};
extern struct hzItem hzItemCfg[];

struct twoItem {
	union {
		struct { short power, presure;} pm;
		struct { short low, hi; } lv;
		short w[2];
	} p;
	short value[4];
	char name[8];
	char msg[4][64];
	short init;
};
extern struct twoItem twoItemCfg[];

struct oneItem {
	short number;
	short value[2];
	char name[8];
	char msg[2][64];
	short init;
};
extern struct oneItem oneItemCfg[];

#define GrafV		(0)
#define GrafN		(1)
#define GrafS		(2)
#define GrafW		(3)
#define GrafC		(4)
#define GrafTotal	(5)
struct scaleItem {
	char name[10];
	short StaticLo, StaticHi, BandMin, BandMax, StubLo, StubHi, used;
	short Max, Min;
	short DynamicHi, DynamicLo, Changed;
};
extern struct scaleItem scaleItemCfg[];

#define tcN		(3*8)	// 48
#define hzN		8
#define uN		16
/**/
#define prNekm	4
#define prNgen	7
#define prN		(prNekm+prNgen)
#define oneN	(prN+3)	/* + pmAir + VVG + ARV */
/*#define oneN	(prN+3)	/* + LvLOil + pmAir + VVG + ARV */
/**/
#define pmNoil	3
#define pmNgp	1
#define pmNwt	2
#define pmN		(pmNoil+pmNgp+pmNwt)
#define twoN	(pmN+2) /* + LvLOil + LvlWht */
/*#define twoN	(pmN+1) /* + LvlWht */
/**/
#define ibN		(oneN+twoN)
/**/
#define msgN	(1<<6)						/* if 6 64 = only power of 2 */
#define evnN	(2+msgN/(8*sizeof(short)))	/* 4 */
/**/
#define TimeLenW	(10)	// HH:MM:SS\0
#define TimeLenB	(sizeof(wchar_t)*TimeLenW)

#define timeStart	(0)
#define timeStop	(1)
#define timeN		(2)

#define spOverload	0
#define spSkip		1
#define spWait		2
#define spN			3
#define unitS	(2*(tcN+hzN+uN)+ibN+spN)
#define tI4		(unitS)
#define tClock	(tI4)		// polls duration (mc) from poll begin 
#define tI4e	(tI4+1)
#define tArea1	(tI4e)
#define tTime	(tArea1)		// time strings area
#define tMsg	(tTime+timeN)	// message string area
#define tArea1e	(tMsg+msgN)
#define tTailI2	(tArea1e)
#define tET		(tTailI2)	// ExitTimer
#define tST		(tTailI2+1)	// StopTimer
#define tVhi	(tTailI2+2)	// Rotate Scale hi
#define tVlo	(tTailI2+3)	// Rotate Scale lo
#define tStartC	(tTailI2+4) // number of starts
#define tStopSh	(tTailI2+5) // show stop indicator
#define tTiker	(tTailI2+6)	// number of updates (polls)
#define tEnd	(tTailI2+7)	// end of simple I2 variables
#define tMsgS	(tEnd)		// message event mask
#define tScale	(tMsgS+evnN)	// temperature scale area
#define tRead	(tScale+2*GrafTotal) // end of upload variables
#define tETc	(tRead)	// download backcounter for programm terminate
#define tSTc	(tRead+1)	// download backcounter for experiment stoping
#define tEnd2	(tRead+2)	// end of simple download I2 variables
#define tEvent	(tEnd2)		// download event control area
#define tScaleC	(tEvent+evnN) // download temperature scale control area
#define tRW		(tScaleC+GrafTotal) /* + outB + cExitTimer + cStopTimer + ScaleControl */
#define tOutB	(tRW)			// download mask for LC-402
#define tTotal	(tRW+1)

union ltcUnit {
	struct {
		short tc[tcN][2];
		short hz[hzN][2];
		short uv[uN][2];
		short ib[ibN];
		short sp[spN];
	} p;                 
	short data[unitS];
};

extern void TagSet(int index, DWORD val);
__inline void TagClr(int index) { TagSet(index, 0); }
extern void TagAdd(int index, DWORD val);
__inline void TagInc(int index) { TagSet(index, 1); }
extern void TagBitSet(int index, DWORD val);
extern void TagBitInvert(int index, DWORD val);
extern void TagBitClr(int index, DWORD val);
extern void TagTime(int index, time_t *tv);

extern int ltcLoadConfig(void);
extern int ltcInit(void);
extern void ltcOut(short val);
extern int ltcPoll(void);
extern void ltcStop(void);
extern union ltcUnit unit;

extern int iChanged[unitS];
extern int nChanged;

extern short outBinary;
extern short ExitTimer, StopTimer;
extern short Vhi, Vlo, Vchanged;
extern short ExitTimerF, StopTimerF;
extern WORD Tiker;
extern DWORD Clock;

#define MsgLenW	(64)						/* number of wchar/char */
#define MsgLenB (sizeof(wchar_t)*MsgLenW)	/* number of bytes */

extern char Msgs[msgN][MsgLenW];
extern int msgUp, msgCount, msgPut;

extern char ts[tTotal][32];
extern char *fullName(char *name);

#ifdef __cplusplus
}
#endif

#endif