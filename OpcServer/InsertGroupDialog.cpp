// InsertGroupDialog.cpp : 
//

#include "stdafx.h"
#include "OpcServer.h"
#include "InsertGroupDialog.h"


// CInsertGroupDialog 

IMPLEMENT_DYNAMIC(CInsertGroupDialog, CDialog)

CInsertGroupDialog::CInsertGroupDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CInsertGroupDialog::IDD, pParent)
	, m_szGroup(_T(""))
	, m_igroupAddress(_T("1"))
	, m_igroupPort(0)
	, m_IPAddress(0)
{

}

CInsertGroupDialog::~CInsertGroupDialog()
{
}

void CInsertGroupDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_GROUPEDIT, m_szGroup);
	DDV_MaxChars(pDX, m_szGroup, 32);
	DDX_Text(pDX, IDC_IPPORTEDIT, m_igroupPort);
	DDV_MinMaxInt(pDX, m_igroupPort, 0, 65536);
	DDX_Text(pDX, IDC_ADDRESSEDIT, m_igroupAddress);
	///DDV_MaxChars(pDX, m_igroupAddress, 8);
	DDX_IPAddress(pDX, IDC_IPEDIT, m_IPAddress);
}
BEGIN_MESSAGE_MAP(CInsertGroupDialog, CDialog)
END_MESSAGE_MAP()


BOOL CInsertGroupDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	if (m_Title.TrimLeft() !=_T(""))
	this->SetWindowTextW(m_Title);
	return TRUE;
}


// CInsertGroupDialog 
