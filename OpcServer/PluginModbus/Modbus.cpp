#pragma once
#include "afx.h"
#include "stdafx.h"
#include "stdio.h"


#include "ModbusSetup.h"
//#include "..\MainFrm.h"

#define WARN_MSG                        15001
#define ERROR_MSG                       16001



#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
#else
    #define DLL_EXPORT __declspec(dllimport)
#endif


class CMainFrame;

typedef void (CMyOpcServer::*pUpDateTag)(loTagValue * ,int);
typedef void (COpcItem::*pItemUpDateData)(VARIANT &, WORD ,SYSTEMTIME *);

////////////////////////////////////////////////////////////////////////
// A concrete plugin implementation
//////////////////////////////////////////////////////////////////////
// Plugin class
// dll must be compile with UNICODE
/// cfg format 		m_fullpathFile.Format(_T("%s%s%s_%s.cfg"),(LPCTSTR) appPath, _T("\\cfg\\"),(LPCTSTR)drName,(LPCTSTR) AdvCfgFile);
static long SpinLock = 0;
#define  ENTER_LOCK_   while( InterlockedExchange( &SpinLock, 1 ) == 1 ){Sleep(0);}
#define  EXIT_LOCK_    InterlockedExchange( &SpinLock, 0 ); 

class Modbus : public IPlugin
{
public:
	Modbus(void);
	~Modbus(void);
	//returns the name of the concrete plugin
	LPCTSTR  Get_Name () const  {return m_DriverName ;}
	LPCTSTR  GetVersion() const {return m_DriverVer;};
	bool  ShowSetup(LPCTSTR addConfile);
	//does the actual data processing threaded from main exe
	int Process_Data(HINSTANCE hInstance, unsigned *nTerminate,LPVOID pParam,LPVOID pMy_CF, LPVOID pUpdateTagFun, LPVOID pUpdateDataFun);
	
	int ClientWrite(LPVOID OpcItem, VARIANT values);

	void GetErrorCode (LPCTSTR errorcode) {errorcode= m_errorCode;};

    void Enable() {enableBit = TRUE;};
    void Disable() {enableBit= FALSE;};
	int DriverStatus(){return 0;};
    void deinit(){};
    LPCTSTR Activation(LPCTSTR code)  {return m_Activate;};
	void SetServerComm (LPVOID psComm);
	void SetUpDateFun(LPVOID pUpDateFun) {};///not used
	void PostAlarmMsg(UINT env, LPCTSTR AlmMsg);

private:
	bool LoadCommSettings(CModbusSetup * modbDlg, modbusSettings * settings);
	bool LoadDefault( ServerComm * settings);

	void Simulate(COpcGroupItem *GroupItem);
	void LoopDevice(COpcGroupItem *GroupItem, unsigned *nTerminate);
	CView* GetActiveViewTrulyFromAnywhere() ;
	CString GetAppPath();
	bool deSerialize(LPCTSTR fullFileName);

	int  Modbus::ReadWriteToDevice(LPVOID OpcItem, VARIANT values, int RW);
	int  Modbus::WriteToDevice(LPVOID OpcItem, VARIANT values);
	int  Modbus::ReadFromDevice(LPVOID OpcItem, VARIANT values);


	bool enableBit;
	CString m_DriverName;
	CString m_DriverVer;
	CString m_errorCode;
	CString m_Activate;
	COpcComServerItem  * pOneServerItem;
	CMyOpcServer *my_CF;
	pUpDateTag *updateTag;
	pItemUpDateData *upItemdateData;

	CString fullpathFile;
	ServerComm Comm;

	MbusMasterFunctions *mbusPtr ;
	void *dataPtr ; 
	void *wdataPtr ; ///for write
	int slaveAddr ;
	int ref ;
	int refCnt ;
	int pollCnt;
	int protocol ;
	int dataType ;
	int swapInts ;
	int swapFloats;
	CString portName;
	int port ; //ip port default 502
	int rs485Mode;
	long baudRate ;
	int dataBits ;
	int stopBits ;
	int parity ;
	int RetryCnt;
    int PollDelay;
	int TimeOut;
	int FlowCtrl;
	int IEEEFloat;
 


protected:
	void closeProtocol();
	int SetMbusProtocol(int protocolNum);

};

	CCriticalSection m_csDataLock;
//static HANDLE hMutex;

void Modbus::PostAlarmMsg(UINT env, LPCTSTR AlmMsg)
{

	HWND handle = FindWindow(NULL,CString("XceedSys.OpcServer"));

	if (handle)
	SendNotifyMessage(handle, WM_USER+2,(WPARAM)(UINT)env,(LPARAM)(LPCTSTR )AlmMsg );
	else 
	AfxMessageBox(_T("XceedSys.OpcServer missing from exe"));

}

bool  Modbus::ShowSetup( LPCTSTR addConfile) //starts here
{
AFX_MANAGE_STATE(AfxGetStaticModuleState())

				CWnd * hw = NULL;//AfxGetApp()->m_pMainWnd;
                CModbusSetup Dialog(hw);// = new CModbusSetup(AfxGetApp()->m_pMainWnd);
				Dialog.m_FileName = addConfile;
				INT_PTR nResponse = Dialog.DoModal();
                 if (nResponse == IDOK)
                 {
					 Sleep(100);
				 }
	return FALSE;
}


Modbus::Modbus(void)
{
	///
	m_DriverName=_T("Modbus");
	m_DriverVer=_T("1.0.0.1");
	m_Activate=_T("demo");
	mbusPtr = NULL;
	dataPtr = NULL; 
	wdataPtr = NULL; 
	slaveAddr = 1;
	ref = 1; /// start addr
	refCnt = 1;
	pollCnt = 0;
	baudRate = 9600;
	dataBits = 8;
	stopBits = 1;
	parity = MbusSerialMasterProtocol::SER_PARITY_EVEN;
	protocol = RTU;
	dataType = T4_REG16;
	swapInts = 0;
	swapFloats = 0;
	portName = _T("COM1");
	port = 502;
	rs485Mode = 0;
	RetryCnt= 3;
    PollDelay= 150;
	TimeOut=300;
	FlowCtrl=0;
	IEEEFloat=0;
//	InitializeCriticalSection(&m_csDataLock);
//	hMutex = CreateMutex( NULL, FALSE, NULL );

};


Modbus::~Modbus(void)
{
//	DeleteCriticalSection(&m_csDataLock);
	Sleep(200);
};

void  Modbus::SetServerComm (LPVOID psComm)
{
	struct ServerComm *pComm = ((struct ServerComm*) psComm);
	Comm = *pComm;
}

int Modbus::Process_Data(HINSTANCE hInstance, unsigned *nTerminate,LPVOID pParam,LPVOID pMy_CF, LPVOID pUpdateTagFun, LPVOID pUpdateDataFun)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	pOneServerItem = (COpcComServerItem  *)pParam;

	my_CF= (CMyOpcServer *)pMy_CF; //points to opcserver
	updateTag =  (pUpDateTag *)pUpdateTagFun ; // points to update opc tags
	upItemdateData =(pItemUpDateData *) pUpdateDataFun;
	CString appPath= GetAppPath();
	CString AdvanceConFig = pOneServerItem->GetAdvanceConFig();	/// path_to cfg file
	CString DriverName = pOneServerItem->GetDriverName();

	
	fullpathFile.Format(_T("%s%s%s_%s.cfg"),(LPCTSTR) appPath, _T("\\cfg\\"),(LPCTSTR)DriverName,(LPCTSTR) AdvanceConFig);

	//open comm channel using AdvanceConFig if any, else use Comm para as default

//	AfxMessageBox(_T("s3"));

	/// load comms para into here
	deSerialize(fullpathFile);
	// load protocol
	SetMbusProtocol(protocol);
	/// need to scan 1 time all tags and make sure it is correct and store into a list array
	/// ScanItems()

///// start of loop
	while (*(nTerminate)==0)
	{

		int iCount  =  pOneServerItem->m_groupList.GetCount();
		POSITION pos =   pOneServerItem->m_groupList.GetHeadPosition();
		for(int i=0 ; i < iCount; i++)
		{
			

			COpcGroupItem *GroupItem = pOneServerItem->m_groupList.GetNext(pos);
			// Simulate(GroupItem);
			LoopDevice(GroupItem, nTerminate);
			Sleep(20);
		}


	}

	Sleep(100);
	return 0;

}


void Modbus::Simulate(COpcGroupItem *GroupItem)
{

	VARIANT varData;
	VariantInit(&varData);

	CMapStringToPtr *m_mptagList =  GroupItem->GetMapObject()   ;
	POSITION pos =  m_mptagList->GetStartPosition(); 
	
	CString strKey;

	while (pos)
	{
		COpcItem * Item = NULL;
		
		m_mptagList->GetNextAssoc(pos,strKey,(void *&)Item); 


		if (!Item->GetActive()) 
			continue;

		VariantClear(&varData);
		
		loTagValue * tag = Item->GetLoTag();

			VariantCopy (&varData,&tag->tvValue);


				bool bData=FALSE;
				float fData =0;
				LONG lnum=1;
				LONG lData = 0;
				LONG lUpper = 0;  //array[5] res=4
				HRESULT hr = S_OK;
		if ((varData.vt & VT_ARRAY) == VT_ARRAY )
		SafeArrayGetUBound(varData.parray,1,&lUpper);


		if (tag)
		{
			switch (varData.vt)
			{
			case VT_BOOL:
				varData.boolVal =  !varData.boolVal;
				break;
			case VT_ARRAY|VT_BOOL:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				bData=FALSE;
				SafeArrayGetElement(varData.parray,&lnum,&bData);
				bData=!bData;
				hr=SafeArrayPutElement(varData.parray,&lnum,&bData);
			}
				break;
			case VT_UI2:
				varData.lVal++;  
				break;
			case VT_ARRAY|VT_UI2:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				lData=0;
				SafeArrayGetElement(varData.parray,&lnum,&lData);
				lData+=1;
				hr=SafeArrayPutElement(varData.parray,&lnum,&lData);
			}
				
				break;

			case VT_I2:
				varData.lVal++;  
				break;
			case VT_ARRAY|VT_I2:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				lData=0;
				SafeArrayGetElement(varData.parray,&lnum,&lData);
				lData+=1;
				hr=SafeArrayPutElement(varData.parray,&lnum,&lData);
			}
				
				break;

			case VT_I4:
				varData.lVal++;  
				break;
			case VT_ARRAY|VT_I4:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				lData=0;
				SafeArrayGetElement(varData.parray,&lnum,&lData);
				lData+=1;
				hr=SafeArrayPutElement(varData.parray,&lnum,&lData);
			}
				
				break;

			case VT_UI4:
				varData.ulVal ++; 
				break;
			case VT_ARRAY|VT_UI4:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				lData=0;
				SafeArrayGetElement(varData.parray,&lnum,&lData);
				lData+=1;
				hr=SafeArrayPutElement(varData.parray,&lnum,&lData);
			}
				break;

			case VT_R4:
				varData.fltVal += 0.1f; 
				break;
			case VT_ARRAY|VT_R4:
				for(lnum=0;lnum<=lUpper;lnum++)
				{
				fData=0.0f;
				SafeArrayGetElement(varData.parray,&lnum,&fData);
				fData +=0.1f;
				hr=SafeArrayPutElement(varData.parray,&lnum,&fData);
				}
				break;
			case VT_R8:
				varData.dblVal += 0.1; 
				break;
			case VT_ARRAY|VT_R8:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				fData=0.0f;
				SafeArrayGetElement(varData.parray,&lnum,&fData);
				fData +=0.1f;
				hr=SafeArrayPutElement(varData.parray,&lnum,&fData);
			}
				break;

			default:
				continue;
				break;
			}
				hr=VariantCopy(&tag->tvValue, &varData);
				
				pUpDateTag UpateDate = *updateTag ; ///deref pointer to pointer

				(my_CF->*UpateDate)(tag,OPC_QUALITY_GOOD);

		}
	}
		
}


/////////////////////////////////////////////////////////////////////////////

/// a must for these 2 external
extern "C"
{
	// Plugin factory function
	__declspec(dllexport) IPlugin* Create_Plugin ()
	{
		//allocate a new object and return it
		return new Modbus ();
	}

	// Plugin cleanup function
	__declspec(dllexport) void Release_Plugin (IPlugin* p_plugin)
	{
		//we allocated in the factory with new, delete the passed object
		delete p_plugin;
	}

	//
	__declspec(dllexport) void EnterClientWrite ( long* SLock)
	{
		
		 SLock =&SpinLock;
	}


__declspec(dllexport) void Show_Setup(CWnd *myCwnd, LPCTSTR addConfile)
{
AFX_MANAGE_STATE(AfxGetStaticModuleState())

// do something with the application
//	BOOL isDLL = AfxIsModuleDll();

CModbusSetup dlg( myCwnd );
dlg.m_FileName = addConfile;
///AfxMessageBox(_T("in."));
INT_PTR nResponse = dlg.DoModal();
                 if (nResponse == IDOK)
                 {
					 Sleep(100);
				 }
		

	}
}

CView* Modbus::GetActiveViewTrulyFromAnywhere() 
{ 
    if (CWnd* pMainWnd = AfxGetMainWnd()) 
    { 
       if (CMDIFrameWnd* pFrame = dynamic_cast<CMDIFrameWnd*>(pMainWnd)) 
       { 
          if (CMDIChildWnd* pChild = pFrame->MDIGetActive()) 
             return pChild->GetActiveView(); 
       } 
       else if (CFrameWnd* pFrame = dynamic_cast<CFrameWnd*>(pMainWnd)) 
          return pFrame->GetActiveView(); 
    } 
    return 0;
}

int Modbus::SetMbusProtocol(int protocolNum)
{
	int result = -1; 
   switch (protocolNum)
   {
      case RTU:
         mbusPtr = new MbusRtuMasterProtocol();
         if (swapInts)
            mbusPtr->configureBigEndianInts();
         if (swapFloats)
            mbusPtr->configureSwappedFloats();

         mbusPtr->setRetryCnt(RetryCnt); //2
         mbusPtr->setPollDelay(PollDelay);  //was 1000
		 mbusPtr->setTimeout(TimeOut);
         if (rs485Mode > 0)
            ((MbusAsciiMasterProtocol *) mbusPtr)->enableRs485Mode(rs485Mode);
         result = ((MbusRtuMasterProtocol *) mbusPtr)->openProtocol(portName, baudRate, dataBits, stopBits, parity);
      break;
      case ASCII:
         mbusPtr = new MbusAsciiMasterProtocol();
         if (swapInts)
            mbusPtr->configureBigEndianInts();
         if (swapFloats)
            mbusPtr->configureSwappedFloats();
         mbusPtr->setRetryCnt(RetryCnt);
         mbusPtr->setPollDelay(PollDelay);
		 mbusPtr->setTimeout(TimeOut);

         if (rs485Mode > 0)
            ((MbusAsciiMasterProtocol *) mbusPtr)->enableRs485Mode(rs485Mode);
         result = ((MbusAsciiMasterProtocol *) mbusPtr)->openProtocol(
                   portName, baudRate, dataBits, stopBits, parity);
      break;
      case TCP:
         mbusPtr = new MbusTcpMasterProtocol();
         if (swapInts)
            mbusPtr->configureBigEndianInts();
         if (swapFloats)
            mbusPtr->configureSwappedFloats();
         mbusPtr->setRetryCnt(RetryCnt);
         mbusPtr->setPollDelay(PollDelay);
		 mbusPtr->setTimeout(TimeOut);
         ((MbusTcpMasterProtocol *) mbusPtr)->setPort((unsigned short) port);
         result = ((MbusTcpMasterProtocol *) mbusPtr)->openProtocol(portName);
      break;
      case RTUOVERTCP:
         mbusPtr = new MbusRtuOverTcpMasterProtocol();
         if (swapInts)
            mbusPtr->configureBigEndianInts();
         if (swapFloats)
            mbusPtr->configureSwappedFloats();
         mbusPtr->setRetryCnt(RetryCnt);
         mbusPtr->setPollDelay(PollDelay);
		 mbusPtr->setTimeout(TimeOut);
         ((MbusRtuOverTcpMasterProtocol *) mbusPtr)->setPort((unsigned short) port);
         result = ((MbusRtuOverTcpMasterProtocol *) mbusPtr)->openProtocol(portName);
      break;
   }
	static CString msg ;///=  _T("Modbus created using :")+ fullpathFile;

   switch (result) // callback logmsg 
   {
      case FTALK_SUCCESS:
		msg.Format(_T("Modbus created "));   
      PostAlarmMsg( WARN_MSG,msg);
      break;
      case FTALK_ILLEGAL_ARGUMENT_ERROR:
      msg.Format(_T("Modbus Configuration setting not supported!"));
	  PostAlarmMsg( ERROR_MSG,msg);
      break;
      case FTALK_TCPIP_CONNECT_ERR:
       msg.Format( _T("Modbus Can't reach server/slave! Check TCP/IP and firewall settings"));
		 PostAlarmMsg(ERROR_MSG, msg ); /// must convert to get error
	   break;
      default:
		  msg.Format(_T("Modbus getBusProtocolErrorText : %d\0"), result);
		 PostAlarmMsg(ERROR_MSG, msg ); /// must convert to get error
      break;
   }


   return result;
}
//////

void  Modbus::closeProtocol()
{
   delete mbusPtr;
   delete [] dataPtr;
   delete [] wdataPtr;
}


CString Modbus::GetAppPath()
{ 

TCHAR szAppPath[_MAX_PATH + 1];

GetModuleFileName(NULL, szAppPath, _MAX_PATH); 

TCHAR* pszTemp = _tcsrchr(szAppPath, _T('\\'));

int n = (int)(pszTemp - szAppPath + 1);

szAppPath [ n ] = 0;
return szAppPath;
}

bool Modbus::deSerialize(LPCTSTR fullFileName)
{
	bool result = FALSE;	
	CModbusSetup SetupFile= new CModbusSetup(0);
	modbusSettings settings;
	
	if( SetupFile.GetArchive(fullFileName, &settings))
	result = LoadCommSettings(&SetupFile, &settings);
	else
	result = LoadDefault(&Comm);

	delete SetupFile;
	return result;
}

bool Modbus::LoadCommSettings(CModbusSetup * modbDlg,modbusSettings *settings)
{
	baudRate = settings->BaudRate;
	dataBits = settings->DataRate;
	stopBits = settings->StopBit;
	parity =  settings->ParityBit;// MbusSerialMasterProtocol::SER_PARITY_EVEN;
	protocol = settings->Protocol;//RTU;

	swapInts = settings->UseBigIndian;
	swapFloats = settings->UseSwapFloat;
	portName = settings->CommStr;
	port = settings->IPport;
	rs485Mode = settings->Use485;
	RetryCnt= settings->Retry;
    PollDelay= settings->PollDelay;
	TimeOut=settings->TimeOut;
	FlowCtrl=settings->FlowCtrl;
	IEEEFloat=settings->UseIEEEFloat;

	return TRUE;
////
}

bool Modbus::LoadDefault(ServerComm *settings)
{
	
	baudRate = swscanf_s(settings->strBaudrate, _T("%d"));
	dataBits = swscanf_s(settings->strDatabits, _T("%d"));
	stopBits = swscanf_s(settings->strStopbit, _T("%d"));
	parity =   swscanf_s(settings->strParitybit, _T("%d")); /// convert N E O
	protocol = RTU;

	swapInts = 0; //fone in
	swapFloats = 0;
	portName = settings->strPort;
	port = 502;
	rs485Mode = 0;
	RetryCnt= 3;
    PollDelay= 100;
	TimeOut=100;
	return TRUE;
}
/*
VT_BOOL(BOOL),VT_BOOL|VT_ARRAY
 VT_I1(8-bit signed integers),VT_I1|VT_ARRAY
 VT_UI1(8-bit unsigned integers),VT_UI1|VT_ARRAY
 VT_I2(16-bit signed integers),VT_I2|VT_ARRAY
 VT_UI2(16-bit unsigned integers),VT_UI2|VT_ARRAY
 VT_I4(32-bit signed integers),VT_I4|VT_ARRAY
 VT_UI4(32-bit unsigned integers),VT_UI4|VT_ARRAY
 VT_R4(32-bit real numbers),VT_R4|VT_ARRAY
 VT_I8(64-bit signed integers),VT_I8|VT_ARRAY
 VT_UI8(64-bit unsigned integers),VT_UI8|VT_ARRAY
 VT_R8(64-bit real numbers),VT_R8|VT_ARRAY
 VT_BSTR(String),VT_BSTR|VT_ARRAY
 VT_ARRAY(Array)
*/


void Modbus::LoopDevice(COpcGroupItem *GroupItem, unsigned *nTerminate)
{
	if (mbusPtr == NULL) return;
	if (GroupItem ==NULL) return;
	CMapStringToPtr *m_mptagList =  GroupItem->GetMapObject()   ;
	POSITION pos =  m_mptagList->GetStartPosition(); 
	
	CString strKey;
	VARIANT varData;
	VariantInit(&varData);

	slaveAddr = GroupItem->GetDevAddr();

	while (pos)
	{
	if (mbusPtr == NULL) break;
	if (*(nTerminate)!=0) break;

	COpcItem * Item = NULL;
	m_mptagList->GetNextAssoc(pos,strKey,(void *&)Item); 
	if (Item ==NULL) continue; //was break

	if (!Item->GetActive()) 	continue;
	if (Item->GetErrorNoScan()) 	continue;
		
		VariantClear(&varData);
		loTagValue * tag = Item->GetLoTag();
		VariantCopy (&varData,&tag->tvValue); //copy opc value to varData

		ReadWriteToDevice(Item, varData,1);
		Sleep(10);
	}

}

int  Modbus::ReadWriteToDevice(LPVOID OpcItem, VARIANT values, int RW)
{
	CSafeLock cs (&m_csDataLock);
//ENTER_LOCK_;

	int res=-1;
	if (RW==0) 
		res= WriteToDevice(OpcItem,values);
	else
		res= ReadFromDevice(OpcItem,values);

//EXIT_LOCK_;
	return res;

}

int  Modbus::ReadFromDevice(LPVOID OpcItem, VARIANT values)
{
	if (mbusPtr == NULL) return -1;
	if (OpcItem == NULL) return -1 ;
	COpcItem * Item = (COpcItem*) OpcItem;

	if (!Item->GetActive()) 	return -1 ;
	if (Item->GetErrorNoScan()) 	return -1 ;


	int result = OPC_QUALITY_BAD;
	VARIANT varData;
	VariantInit(&varData);

	slaveAddr = Item->GetGroup()->GetDevAddr();

	
	VariantClear(&varData);
	loTagValue * tag = Item->GetLoTag();

	 VariantCopy (&varData,&tag->tvValue); //copy opc value to varData
		//// item info need to breakup and send to device
		/// format of start address 0:0000, range function, datatype

			CString Address = Item->GetAddress();
			int Addr;
			int Reg;
			int z;
			int i = Address.Find(_T(":")); // break into Reg: registers x:yyyyy format
			if (i<0) {Item->SetErrorNoScan(TRUE); return -1;}
			_stscanf(Address.Left(i), _T("%d"), &z); 
			Reg = z;
			int j = Address.Find(_T(":"),i+1); // break into Reg: registers x:yyyyy format
			if (j>0) {Item->SetErrorNoScan(TRUE); return -1;};
			//_stscanf(Address.Mid(i+1) , _T("%d"), &z); ;//sscanf(strHex.c_str(), "%x", &decimalValue);
			_stscanf(Address.Mid(i+1) , _T("%x"), &z);
			Addr = z;
			ref=z;
			refCnt = Item->GetRange(); /// num of data to read
			if (refCnt < 1) refCnt = 1;
			int r_n_w = Item->GetWrType();
		

				LONG lnum=1;
				LONG lUpper = 0;  //array[5] res=4
				HRESULT hr = S_OK;
			//VARTYPE vtType;

		if ((varData.vt & VT_ARRAY) == VT_ARRAY )
		SafeArrayGetUBound(varData.parray,1,&lUpper);


		if (tag)
		{
			
			switch (Reg)
			{
			case 0:
				if ((varData.vt == VT_BOOL)	|| (varData.vt == (VT_ARRAY|VT_BOOL)))
				{
					if ((r_n_w & OPC_READABLE) >0)//rw and r must read from device
					{
						dataPtr = new int[refCnt];
						result = mbusPtr->readCoils(slaveAddr, ref, (int *) dataPtr, refCnt);
						if (refCnt==1)
							varData.intVal = ((int*) dataPtr)[0];
						else
							for(lnum=0;lnum< refCnt;lnum++)
							SafeArrayPutElement(varData.parray,&lnum,&((int*) dataPtr)[lnum]);
					}
				
				}
				break;
			case 1:
				if ((varData.vt == VT_BOOL)	|| (varData.vt == (VT_ARRAY|VT_BOOL)))
				{
				dataPtr = new int[refCnt];
				result = mbusPtr->readInputDiscretes(slaveAddr, ref, (int *) dataPtr, refCnt);
						if (refCnt==1)
							varData.intVal = ((int*) dataPtr)[0];
						else
							for(lnum=0;lnum< refCnt;lnum++)
							SafeArrayPutElement(varData.parray,&lnum,&((int*) dataPtr)[lnum]);
				}
				break;

			case 3:
				if ((varData.vt == VT_I1)||(varData.vt == (VT_ARRAY|VT_I1))||
					(varData.vt == VT_UI1)||(varData.vt == (VT_ARRAY|VT_UI1)))
				{
				dataPtr = new short[refCnt];
	            result = mbusPtr->readInputRegisters(slaveAddr, ref, (short *) dataPtr, refCnt);
						if (refCnt==1)
							varData.iVal = ((short*) dataPtr)[0];
						else
							for(lnum=0;lnum< refCnt;lnum++)
							SafeArrayPutElement(varData.parray ,&lnum,&((short*) dataPtr)[lnum]);
				break;
				}


				if ((varData.vt == VT_I2)||(varData.vt == (VT_ARRAY|VT_I2))||
					(varData.vt == VT_UI2)||(varData.vt == (VT_ARRAY|VT_UI2)))
				{ ///16bit
				dataPtr = new short[refCnt];
	            result = mbusPtr->readInputRegisters(slaveAddr, ref, (short *) dataPtr, refCnt);
						if (refCnt==1)
							varData.iVal = ((short*) dataPtr)[0];
						else
							for(lnum=0;lnum< refCnt;lnum++)
							SafeArrayPutElement(varData.parray,&lnum,&((short*) dataPtr)[lnum]);
				break;
				}



				if ((varData.vt == VT_I4)||(varData.vt == (VT_ARRAY|VT_I4))||
					(varData.vt == VT_UI4)||(varData.vt == (VT_ARRAY|VT_UI4)))
				{ ///32bit
				dataPtr = new long[refCnt];
	            result = mbusPtr->readInputLongInts(slaveAddr, ref, (long *) dataPtr, refCnt);
						if (refCnt==1)
							varData.lVal = ((long*) dataPtr)[0];
						else
							for(lnum=0;lnum< refCnt;lnum++)
							SafeArrayPutElement(varData.parray,&lnum,&((long*) dataPtr)[lnum]);
				break;
				}

				//if ((varData.vt == VT_I4)||(varData.vt == VT_ARRAY|VT_I4)||
				//	(varData.vt == VT_UI4)||(varData.vt == VT_ARRAY|VT_UI4))
				//	{ ///32bit not supported
				//dataPtr = new long[refCnt];
		        //   result = mbusPtr->readInputMod10000(slaveAddr, ref,  (long *) dataPtr, refCnt);
				//}
				if ((varData.vt == VT_R4)	|| (varData.vt == (VT_ARRAY|VT_R4))||
					(varData.vt == VT_R8)	|| (varData.vt == (VT_ARRAY|VT_R8)))
				{ ///32bit
				dataPtr = new float[refCnt];
	            result = mbusPtr->readInputFloats(slaveAddr, ref,(float *) dataPtr, refCnt);
						if (refCnt==1)
							varData.fltVal = ((float*) dataPtr)[0];
						else
							for(lnum=0;lnum< refCnt;lnum++)
							SafeArrayPutElement(varData.parray,&lnum,&((float*) dataPtr)[lnum]);
				}

 				break;
			case 4:
				if ((varData.vt == VT_I1)||(varData.vt == (VT_ARRAY|VT_I1))|| (varData.vt == VT_I2)||(varData.vt == (VT_ARRAY|VT_I2))||
					(varData.vt == VT_UI1)||(varData.vt == (VT_ARRAY|VT_UI1))|| (varData.vt == VT_UI2)||(varData.vt == (VT_ARRAY|VT_UI2)))
				{
					if ((r_n_w & OPC_READABLE) >0)//rw and r must read from device
					{
						dataPtr = new short[refCnt];
						result = mbusPtr->readMultipleRegisters(slaveAddr, ref,  (short *) dataPtr, refCnt);
						if (refCnt==1)
							varData.intVal = ((short*) dataPtr)[0];
						else
							for(lnum=0;lnum< refCnt;lnum++)
							SafeArrayPutElement(varData.parray,&lnum,&((short*) dataPtr)[lnum]);
					}
					break;
				}



				if ((varData.vt == VT_I4)||(varData.vt == (VT_ARRAY|VT_I4))||
					(varData.vt == VT_UI4)||(varData.vt == (VT_ARRAY|VT_UI4)))
				{ ///32bit
					if ((r_n_w & OPC_READABLE) >0)//rw and r must read from device
					{
						dataPtr = new long[refCnt];
						result = mbusPtr->readMultipleLongInts(slaveAddr, ref, (long *) dataPtr, refCnt);
						if (refCnt==1)
							varData.lVal = ((long*) dataPtr)[0];
						else
							for(lnum=0;lnum< refCnt;lnum++)
							SafeArrayPutElement(varData.parray,&lnum,&((long*) dataPtr)[lnum]);
					}

					break;
				}

				//if ((varData.vt == VT_DEC)	|| (varData.vt == VT_ARRAY|VT_BOOL))
				//{ ///32bit
				//dataPtr = new long[refCnt];
		        //   result = mbusPtr->readMultipleMod10000(slaveAddr, ref,
                //                                (long *) dataPtr, refCnt);
				//}

				if ((varData.vt == VT_R4)	|| (varData.vt == (VT_ARRAY|VT_R4))||
					(varData.vt == VT_R8)	|| (varData.vt == (VT_ARRAY|VT_R8)))
				{ 
					if ((r_n_w & OPC_READABLE) >0)//rw and r must read from device
					{
						dataPtr = new float[refCnt];
				        result = mbusPtr->readMultipleFloats(slaveAddr, ref, (float *) dataPtr, refCnt);
						if (refCnt==1)
							varData.fltVal = ((float*) dataPtr)[0];
						else
							for(lnum=0;lnum< refCnt;lnum++)
							SafeArrayPutElement(varData.parray,&lnum,&((float*) dataPtr)[lnum]);
					}
				}
				break;
			}

			int opcRes = OPC_QUALITY_BAD ;//OPC_QUALITY_DEVICE_FAILURE;
			pUpDateTag UpateDate = *updateTag ; ///deref pointer to pointer
			pItemUpDateData UpDataItem=  *upItemdateData;
			SYSTEMTIME m_time ;
			GetLocalTime (&m_time); //Item->SetmSystime(m_time);

			if (result == FTALK_SUCCESS) 
			{	
				if (&tag->tvValue != &varData)  
				Item->SetmSystime(m_time);

				opcRes = OPC_QUALITY_GOOD;
				hr=VariantCopy(&tag->tvValue, &varData); 
			}
			else
			{
			static CString msg;
			msg.Format(_T("Modbus %s Write ERROR : %d"), Item->GetName(), result);
				  PostAlarmMsg(ERROR_MSG, msg ); /// must convert to get error

			}
			Item->SetQuality(opcRes);
				(my_CF->*UpateDate)(tag,opcRes); ///update to opc (my_CF->*UpateDate)(tag,OPC_QUALITY_GOOD);

					// delete dataPtr;
		}

	return 0;
}


int  Modbus::ClientWrite(LPVOID OpcItem, VARIANT values)
{
		
	 	//WaitForSingleObject( hMutex, INFINITE );
			
		int res= ReadWriteToDevice(OpcItem,values,0);
		//ReleaseMutex( hMutex );
		//ResetEvent(hMutex);

	return res;
}

int  Modbus::WriteToDevice(LPVOID OpcItem, VARIANT values)
{
	if (mbusPtr == NULL) return -1;
	if (OpcItem == NULL) return -1 ;

	COpcItem * Item = (COpcItem*) OpcItem;

	if (!Item->GetActive()) 	return -1 ;
	if (Item->GetErrorNoScan()) 	return -1 ;

	int result = OPC_QUALITY_BAD;

	VARIANT WritevarData;
	VariantInit(&WritevarData);
	VariantClear(&WritevarData);

//	loTagValue * Writetag = Item->GetLoTag();
	loTagValue * tag = Item->GetLoTag();

	// VariantCopy (&WritevarData,&Writetag->tvValue); //copy opc value to varData
	VariantCopy (&WritevarData, &values);
	
	slaveAddr = Item->GetGroup()->GetDevAddr();

			CString Address = Item->GetAddress();
			int Addr;
			int Reg;
			int z;
			int i = Address.Find(_T(":")); // break into Reg: registers x:yyyyy format
			if (i<0) {	return -1;}
			_stscanf(Address.Left(i), _T("%d"), &z); 
			Reg = z;
			int j = Address.Find(_T(":"),i+1); // break into Reg: registers x:yyyyy format
			if (j>0) { return -1;};
			_stscanf(Address.Mid(i+1) , _T("%x"), &z);
			Addr = z;
			ref=z;
			refCnt = Item->GetRange(); /// num of data to read
			if (refCnt < 1) refCnt = 1;
			int r_n_w = Item->GetWrType();

			LONG lnum=1;
			LONG lUpper = 0;  //array[5] res=4
			HRESULT hr =OPC_QUALITY_BAD;
			//VARTYPE vtType;

		if ((WritevarData.vt & VT_ARRAY) == VT_ARRAY )
		SafeArrayGetUBound(WritevarData.parray,1,&lUpper);


	switch (Reg)
	{
			case 0:
				if ((WritevarData.vt == VT_BOOL)	|| (WritevarData.vt == (VT_ARRAY|VT_BOOL)))
				{
					//3,2,1
					if ((r_n_w & OPC_WRITEABLE) >0)  
					{ //rw and w must write to device
						if (refCnt==1)
						result= mbusPtr->writeCoil(slaveAddr, ref,WritevarData.boolVal);
						else
						{
							dataPtr = new int[refCnt];
							int data=0;	
							for(lnum=0;lnum< refCnt;lnum++)
							{
							SafeArrayGetElement(WritevarData.parray,&lnum,&data);
							((int*)dataPtr)[lnum]= data;
							}
						
							result = mbusPtr->forceMultipleCoils(slaveAddr, ref, (int*)dataPtr, refCnt);
				
						}
					break;
				}

			case 4:
				if ((WritevarData.vt == VT_I1)||(WritevarData.vt == (WritevarData.vt|VT_I1))|| (WritevarData.vt == VT_I2)||(WritevarData.vt == (VT_ARRAY|VT_I2))||
					(WritevarData.vt == VT_UI1)||(WritevarData.vt == (WritevarData.vt|VT_UI1))|| (WritevarData.vt == VT_UI2)||(WritevarData.vt == (VT_ARRAY|VT_UI2)))
				{

					if ((r_n_w & OPC_WRITEABLE) >0)
					{ //rw and w must write to device
						dataPtr = new short[refCnt];
						if (refCnt==1)
						{
							((short*) dataPtr)[0] =(short)WritevarData.intVal;
							result = mbusPtr->writeSingleRegister(slaveAddr, ref, (short)WritevarData.intVal);
						}
						else
						{
							for(lnum=0;lnum< refCnt;lnum++)
							SafeArrayGetElement(WritevarData.parray,&lnum,&((short*) dataPtr)[lnum]);
							result = mbusPtr->writeMultipleRegisters(slaveAddr, ref, (short *) dataPtr, refCnt);
						}

					}
					break;
				}

				if ((WritevarData.vt == VT_I4)||(WritevarData.vt == (VT_ARRAY|VT_I4))||
					(WritevarData.vt == VT_UI4)||(WritevarData.vt == (VT_ARRAY|VT_UI4)))
				{ ///32bit
					if ((r_n_w & OPC_WRITEABLE) >0)
					{ //rw and w must write to device
						dataPtr = new long[refCnt];
						if (refCnt==1)
							((long*) dataPtr)[0] =(long)WritevarData.lVal;
						else
							for(lnum=0;lnum< refCnt;lnum++)
							
							SafeArrayGetElement(WritevarData.parray,&lnum,&((long*) dataPtr)[lnum]);

						result = mbusPtr->writeMultipleLongInts(slaveAddr, ref, (long *) dataPtr, refCnt);

					}
					break;
				}

				if ((WritevarData.vt == VT_R4)	|| (WritevarData.vt == (VT_ARRAY|VT_R4))||
					(WritevarData.vt == VT_R8)	|| (WritevarData.vt == (VT_ARRAY|VT_R8)))
				{ ///32bit

					if ((r_n_w & OPC_WRITEABLE) >0)
					{ //rw and w must write to device
						dataPtr = new float[refCnt];
						if (refCnt==1)
							((float*) dataPtr)[0] =WritevarData.fltVal;
						else
							for(lnum=0;lnum< refCnt;lnum++)
							SafeArrayGetElement(WritevarData.parray,&lnum,&((float*) dataPtr)[lnum]);
							result = mbusPtr->writeMultipleFloats(slaveAddr, ref, (float *) dataPtr, refCnt);

					}
				}
			}			
				break;
	}

			pUpDateTag UpateDate = *updateTag ; ///deref pointer to pointer
			pItemUpDateData UpDataItem=  *upItemdateData;
			int opcRes = OPC_QUALITY_BAD ;//OPC_QUALITY_DEVICE_FAILURE;
			SYSTEMTIME m_time ;
			GetLocalTime (&m_time); //Item->SetmSystime(m_time);


				if (result == FTALK_SUCCESS) 
				{
					//WritevarData-> ->tvState.tsQuality = OPC_QUALITY_GOOD; 
					hr = S_OK;
					Item->SetQuality(OPC_QUALITY_GOOD);
					if (&tag->tvValue != &WritevarData)  
					Item->SetmSystime(m_time);
					VariantCopy(&tag->tvValue, &WritevarData); 
					opcRes= OPC_QUALITY_GOOD;
				}
				else
				{
				static CString msg;
				Item->SetQuality(OPC_QUALITY_BAD);
				msg.Format(_T("Modbus  %s Write ERROR : %d"), Item->GetName(), result);

				PostAlarmMsg(ERROR_MSG, msg ); /// must convert to get error
 				}
				(my_CF->*UpateDate)(tag,opcRes); ///update to opc (my_CF->*UpateDate)(tag,OPC_QUALITY_GOOD);
	
	return hr;
}



/*
extern "C"
void _declspec(dllexport) MyExportedFunction(void)
{
   AFX_MANAGE_STATE( AfxGetStaticModuleState() );
  
   CMyCustomDialog MyCustomDlg;
  
   if( -1 == MyCustomDlg.DoModal() )
   {
      AfxMessageBox("failed");
   }  
}
typedef void (CALLBACK* LPFUNPOINT)(void);

void LoadDLLFunction()
{
   //Load your dll from the path
   HMODULE hm = LoadLibrary("D:\\SAN\\RegularDll\\Debug\\RegularDll.dll");
  
   LPFUNPOINT   lpProc;
   if(NULL != hm)
   {
      lpProc = (LPFUNPOINT)GetProcAddress(hm, _T("MyExportedFunction") );
      if(NULL != lpProc)
         lpProc();
   }
}
*/
