/**
 * @internal
 * @file MbusMasterCexports.h
 *
 * @if NOTICE
 *
 * $Id: MbusMasterCexports.h,v 1.1 2006/10/19 21:18:14 henrik Exp $
 *
 * Copyright (c) 2004-2006 FOCUS Software Engineering Pty Ltd, Australia. All
 * rights reserved. <www.focus-sw.com>
 *
 * USE OF THIS SOFTWARE IS GOVERNED BY THE TERMS AND CONDITIONS OF A
 * SEPARATE LICENSE STATEMENT AND LIMITED WARRANTY.
 *
 * IN PARTICULAR, YOU WILL INDEMNIFY AND HOLD FOCUS SOFTWARE ENGINEERING,
 * ITS RELATED COMPANIES AND ITS SUPPLIERS, HARMLESS FROM AND AGAINST ANY
 * CLAIMS OR LIABILITIES ARISING OUT OF THE USE, REPRODUCTION, OR
 * DISTRIBUTION OF YOUR PROGRAMS, INCLUDING ANY CLAIMS OR LIABILITIES
 * ARISING OUT OF OR RESULTING FROM THE USE, MODIFICATION, OR DISTRIBUTION
 * OF PROGRAMS OR FILES CREATED FROM, BASED ON, AND/OR DERIVED FROM THIS
 * SOURCE CODE FILE.
 *
 * @endif
 */


#ifndef _MBUSMASTERCEXPORTS_H_INCLUDED
#define _MBUSMASTERCEXPORTS_H_INCLUDED


#if defined(_WIN32) || defined(__WIN32__) || defined(_WIN32_WCE)
#  include <windows.h> // Required for CE which defined __stdcall in it's own way...
#  define STDCALL   __stdcall
#else
#  define STDCALL
#endif
#if defined (_WINDLL) || defined(__DLL__)
#  define EXPORT    __declspec(dllexport)
#else
#  define EXPORT
#endif

/* UNICODE support (MS VC++, Win CE) */
#if defined (_UNICODE) || defined(UNICODE)
#  ifndef _TCHAR_DEFINED
      typedef wchar_t TCHAR;
#     define _TCHAR_DEFINED
#  endif
#else
#  ifndef _TCHAR_DEFINED
      typedef char TCHAR;
#     define _TCHAR_DEFINED
#  endif
#endif

// Package header
#include "BusProtocolErrors.h"


/*****************************************************************************
 * Type definitions
 *****************************************************************************/

typedef void * MbusMasterProtocolHdl;


/*****************************************************************************
 * Prototypes
 *****************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************
 * C functions to create and destroy protocol objects
 *****************************************************************************/

EXPORT MbusMasterProtocolHdl STDCALL mbusMaster_createTcpProtocol(void);


EXPORT MbusMasterProtocolHdl STDCALL mbusMaster_createRtuProtocol(void);


EXPORT MbusMasterProtocolHdl STDCALL mbusMaster_createRtuOverTcpProtocol(void);


EXPORT MbusMasterProtocolHdl STDCALL mbusMaster_createAsciiProtocol(void);


EXPORT void STDCALL mbusMaster_destroyProtocol(MbusMasterProtocolHdl mbusHdl);


/*****************************************************************************
 * C wrapper function for 16 Bits Access Modbus Function Codes
 *****************************************************************************/

EXPORT int STDCALL
mbusMaster_readMultipleRegisters(MbusMasterProtocolHdl mbusHdl,
                                 int slaveAddr,
                                 int startRef,
                                 short regArr[], int refCnt);


EXPORT int STDCALL
mbusMaster_readMultipleLongInts(MbusMasterProtocolHdl mbusHdl,
                                int slaveAddr,
                                int startRef,
                                long int32Arr[], int refCnt);


EXPORT int STDCALL
mbusMaster_readMultipleMod10000(MbusMasterProtocolHdl mbusHdl,
                                int slaveAddr,
                                int startRef,
                                long int32Arr[], int refCnt);


EXPORT int STDCALL
mbusMaster_readMultipleFloats(MbusMasterProtocolHdl mbusHdl,
                              int slaveAddr,
                              int startRef,
                              float float32Arr[], int refCnt);


EXPORT int STDCALL
mbusMaster_readInputRegisters(MbusMasterProtocolHdl mbusHdl,
                              int slaveAddr, int startRef,
                              short regArr[], int refCnt);


EXPORT int STDCALL
mbusMaster_readInputLongInts(MbusMasterProtocolHdl mbusHdl,
                             int slaveAddr,
                             int startRef,
                             long int32Arr[], int refCnt);


EXPORT int STDCALL
mbusMaster_readInputMod10000(MbusMasterProtocolHdl mbusHdl,
                             int slaveAddr,
                             int startRef,
                             long int32Arr[], int refCnt);


EXPORT int STDCALL
mbusMaster_readInputFloats(MbusMasterProtocolHdl mbusHdl,
                           int slaveAddr,
                           int startRef,
                           float float32Arr[], int refCnt);


EXPORT int STDCALL
mbusMaster_writeMultipleRegisters(MbusMasterProtocolHdl mbusHdl,
                                  int slaveAddr,
                                  int startRef,
                                  const short regArr[],
                                  int refCnt);


EXPORT int STDCALL
mbusMaster_writeMultipleLongInts(MbusMasterProtocolHdl mbusHdl,
                                 int slaveAddr,
                                 int startRef,
                                 const long int32Arr[],
                                 int refCnt);


EXPORT int STDCALL
mbusMaster_writeMultipleMod10000(MbusMasterProtocolHdl mbusHdl,
                                 int slaveAddr,
                                 int startRef,
                                 const long int32Arr[],
                                 int refCnt);


EXPORT int STDCALL
mbusMaster_writeMultipleFloats(MbusMasterProtocolHdl mbusHdl,
                               int slaveAddr,
                               int startRef,
                               const float float32Arr[],
                               int refCnt);


EXPORT int STDCALL
mbusMaster_writeSingleRegister(MbusMasterProtocolHdl mbusHdl,
                               int slaveAddr,
                               int regAddr,
                               short regVal);


EXPORT int STDCALL
mbusMaster_maskWriteRegister(MbusMasterProtocolHdl mbusHdl,
                             int slaveAddr,
                             int regAddr,
                             short andMask,
                             short orMask);


EXPORT int STDCALL
mbusMaster_readWriteRegisters(MbusMasterProtocolHdl mbusHdl,
                              int slaveAddr,
                              int readRef,
                              short readArr[], int readCnt,
                              int writeRef,
                              const short writeArr[],
                              int writeCnt);


/*****************************************************************************
 * C wrapper function for Bit Access Modbus Function Codes
 *****************************************************************************/

EXPORT int STDCALL
mbusMaster_readCoils(MbusMasterProtocolHdl mbusHdl,
                     int slaveAddr, int startRef,
                     int bitArr[], int refCnt);


EXPORT int STDCALL
mbusMaster_readInputDiscretes(MbusMasterProtocolHdl mbusHdl,
                              int slaveAddr, int startRef,
                              int bitArr[], int refCnt);


EXPORT int STDCALL
mbusMaster_writeCoil(MbusMasterProtocolHdl mbusHdl,
                     int slaveAddr, int bitAddr, int bitVal);


EXPORT int STDCALL
mbusMaster_forceMultipleCoils(MbusMasterProtocolHdl mbusHdl,
                              int slaveAddr, int startRef,
                              const int bitArr[], int refCnt);


/*****************************************************************************
 * C wrapper function for Diagnostics Function Codes
 *****************************************************************************/

EXPORT int STDCALL
mbusMaster_readExceptionStatus(MbusMasterProtocolHdl mbusHdl,
                               int slaveAddr,
                               unsigned char *statusByte);

EXPORT int STDCALL
mbusMaster_returnQueryData(MbusMasterProtocolHdl mbusHdl,
                           int slaveAddr,
                           const unsigned char queryArr[],
                           unsigned char echoArr[], int len);


/*****************************************************************************
 * C functions for Protocol Configuration
 *****************************************************************************/

EXPORT int STDCALL
mbusMaster_setTimeout(MbusMasterProtocolHdl mbusHdl, int timeOut);


EXPORT int STDCALL mbusMaster_getTimeout(MbusMasterProtocolHdl mbusHdl);


EXPORT int STDCALL
mbusMaster_setPollDelay(MbusMasterProtocolHdl mbusHdl, int pollDelay);


EXPORT int STDCALL mbusMaster_getPollDelay(MbusMasterProtocolHdl mbusHdl);


EXPORT int STDCALL
mbusMaster_setRetryCnt(MbusMasterProtocolHdl mbusHdl, int retryCnt);


EXPORT int STDCALL
mbusMaster_getRetryCnt(MbusMasterProtocolHdl mbusHdl);


/*****************************************************************************
 * C functions for Word Order Configuration
 *****************************************************************************/

EXPORT void STDCALL
mbusMaster_configureBigEndianInts(MbusMasterProtocolHdl mbusHdl);


EXPORT void STDCALL
mbusMaster_configureSwappedFloats(MbusMasterProtocolHdl mbusHdl);


EXPORT void STDCALL
mbusMaster_configureLittleEndianInts(MbusMasterProtocolHdl mbusHdl);


EXPORT void STDCALL
mbusMaster_configureIeeeFloats(MbusMasterProtocolHdl mbusHdl);


/*****************************************************************************
* C functions for Transmission Statistic Functions
*****************************************************************************/

EXPORT long STDCALL
mbusMaster_getTotalCounter(MbusMasterProtocolHdl mbusHdl);


EXPORT void STDCALL
mbusMaster_resetTotalCounter(MbusMasterProtocolHdl mbusHdl);


EXPORT long STDCALL
mbusMaster_getSuccessCounter(MbusMasterProtocolHdl mbusHdl);


EXPORT void STDCALL
mbusMaster_resetSuccessCounter(MbusMasterProtocolHdl mbusHdl);


/*****************************************************************************
 * C functions for Utility routines
 *****************************************************************************/

EXPORT void STDCALL mbusMaster_closeProtocol(MbusMasterProtocolHdl mbusHdl);


EXPORT int STDCALL mbusMaster_isOpen(MbusMasterProtocolHdl mbusHdl);


EXPORT TCHAR * STDCALL mbusMaster_getPackageVersion(void);


EXPORT TCHAR * STDCALL mbusMaster_getErrorText(int errCode);


/*****************************************************************************
 * C functions for MbusTcpMasterProtocol class
 *****************************************************************************/

EXPORT int STDCALL
mbusMaster_openTcpProtocol(MbusMasterProtocolHdl mbusHdl, const TCHAR * const hostName);


EXPORT int STDCALL
mbusMaster_setTcpPort(MbusMasterProtocolHdl mbusHdl, unsigned short portNo);


EXPORT unsigned short  STDCALL
mbusMaster_getTcpPort(MbusMasterProtocolHdl mbusHdl);


/*****************************************************************************
 * C functions for MbusSerialMasterProtocol class
 *****************************************************************************/

EXPORT int STDCALL
mbusMaster_openSerialProtocol(MbusMasterProtocolHdl mbusHdl,
                               const TCHAR * const portName,
                               long baudRate, int dataBits,
                               int stopBits, int parity);


EXPORT int STDCALL
mbusMaster_enableRs485Mode(MbusMasterProtocolHdl mbusHdl, int rtsDelay);


#ifdef __cplusplus
}
#endif

#endif /* ifdef ..._H_INCLUDED */

