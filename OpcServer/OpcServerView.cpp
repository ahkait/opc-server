
// OpcServerView.cpp : COpcServerView 
//
#include "stdafx.h"
#include "OpcServer.h"
#include "OpcServerDoc.h"
#include "OpcServerView.h"
#include "OpcGroupItem.h"
#include "OpcItem.h"
#include "NewItemDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int CALLBACK SortFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

// COpcServerView

IMPLEMENT_DYNCREATE(COpcServerView, CListView)

BEGIN_MESSAGE_MAP(COpcServerView, CListView)
	ON_WM_STYLECHANGED()
	ON_NOTIFY_REFLECT(LVN_GETDISPINFO, &COpcServerView::OnLvnGetdispinfo)
	ON_WM_CONTEXTMENU()
	ON_WM_TIMER()
	ON_WM_CHAR()
	ON_UPDATE_COMMAND_UI(ID_EDIT_DELETE, &COpcServerView::OnUpdateEditDelete)
	ON_COMMAND(ID_EDIT_DELETE, &COpcServerView::OnDelete)
	ON_WM_KEYDOWN()
	ON_UPDATE_COMMAND_UI(ID_EDIT_EDIT, &COpcServerView::OnUpdateEditEdit)
	ON_COMMAND(ID_EDIT_EDIT, &COpcServerView::OnEditEdit)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, &COpcServerView::OnUpdateEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, &COpcServerView::OnUpdateEditPaste)
	ON_COMMAND(ID_EDIT_COPY, &COpcServerView::OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, &COpcServerView::OnEditPaste)
	ON_NOTIFY(HDN_ITEMCLICK, 0, &COpcServerView::OnItemclickList)
	
END_MESSAGE_MAP()

// COpcServerView 
#define UPDATE_ITEMPANE_EVENT 10

COpcServerView::COpcServerView()
{
    m_pGroup = NULL;
	m_bPasteAvailable=0;
}

COpcServerView::~COpcServerView()
{
}

BOOL COpcServerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 
	//  CREATESTRUCT cs 
	cs.style &= ~(LVS_ICON | LVS_SMALLICON | LVS_LIST);
	cs.style |= LVS_REPORT  | LVS_OWNERDATA |LVS_SORTDESCENDING; //LVS_NOSORTHEADER ;
	
	return CListView::PreCreateWindow(cs);
}

void COpcServerView::OnInitialUpdate()
{
	CListView::OnInitialUpdate();

	
	//   ListView
}


void COpcServerView::OnContextMenu(CWnd* pWnd, CPoint point)
{
	GetDocument();
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
}


// COpcServerView 

#ifdef _DEBUG
void COpcServerView::AssertValid() const
{
	CListView::AssertValid();
}

void COpcServerView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}

COpcServerDoc* COpcServerView::GetDocument() const //
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(COpcServerDoc)));
	return (COpcServerDoc*)m_pDocument;
}
#endif //_DEBUG


// COpcServerView
void COpcServerView::OnStyleChanged(int nStyleType, LPSTYLESTRUCT lpStyleStruct)
{
	
	CListView::OnStyleChanged(nStyleType,lpStyleStruct);	
}

void COpcServerView::OnLvnGetdispinfo(NMHDR *pNMHDR, LRESULT *pResult)
{
	if (!m_obArray.GetCount())
		return;

	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
    CListCtrl & listctrl = GetListCtrl();
 
	COpcItem * opcItem = (COpcItem *)m_obArray[pDispInfo->item.iItem];

	if (!opcItem){
		*pResult = 0;
		return;
	}
	if (pDispInfo->item.mask & LVIF_TEXT)
	{

		switch (pDispInfo->item.iSubItem)
		{
		case 0://TagName
			lstrcpyn(pDispInfo->item.pszText,opcItem->GetName(),pDispInfo->item.cchTextMax);  
			break;
		case 1://Type
			{
				CString strType;
				StringFromVartype(opcItem->GetType(),strType);
				lstrcpyn(pDispInfo->item.pszText,strType,pDispInfo->item.cchTextMax);  
			}
			break;
		case 2://Val must show array for range > 1
			lstrcpyn(pDispInfo->item.pszText,opcItem->GetStrValue(),pDispInfo->item.cchTextMax); 
			break;
		case 4://Quality
			lstrcpyn(pDispInfo->item.pszText,opcItem->GetStrQuality(),pDispInfo->item.cchTextMax); 
			break;
		case 5://Description
			lstrcpyn(pDispInfo->item.pszText,opcItem->GetTagDesc(),pDispInfo->item.cchTextMax);  
			break;
		case 3://DateTime
				lstrcpyn(pDispInfo->item.pszText,opcItem->FormatTime(),pDispInfo->item.cchTextMax);  
			break;
		case 6://Address
			lstrcpyn(pDispInfo->item.pszText,opcItem->GetAddress(),pDispInfo->item.cchTextMax);  
			break;
		case 7://rw Mode
			lstrcpyn(pDispInfo->item.pszText,opcItem->GetStrwrType(),pDispInfo->item.cchTextMax);  
			break;
		case 8://Range
			TCHAR szNum [32];
			wsprintf(szNum, _T("%u"),opcItem->GetRange());
			lstrcpyn(pDispInfo->item.pszText,szNum,pDispInfo->item.cchTextMax);  
			break;

		default:	
			pDispInfo->item.pszText = _T(""); 
			break;
		}
	}
	if (pDispInfo->item.mask & LVIF_IMAGE)
		pDispInfo->item.iImage = (opcItem->GetActive()) ? 0 : 1;

	
	*pResult = 0;
}

void COpcServerView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	
	CListCtrl & listctrl = GetListCtrl();
	switch (lHint)
	{
	case FILENEW:
	case GROUP_VIEW_CLEAR:
		m_pGroup = NULL;
		if (listctrl.GetItemCount()) 
			listctrl.SetItemCountEx (0, LVSICF_NOSCROLL | LVSICF_NOINVALIDATEALL);

		listctrl.Invalidate (true);
		listctrl.UpdateWindow ();		
		/// clear this listctrl.SetItemData(m_obArray.GetCount(), DWORD(pItem));
	case GROUP_VIEW_UPDATE:
		{
			m_pGroup = (COpcGroupItem*)pHint;
			if (m_pGroup)
			{				
				SetRedraw (false);

				if (listctrl.GetItemCount()) 
			       	listctrl.SetItemCountEx (0, LVSICF_NOSCROLL | LVSICF_NOINVALIDATEALL);			

				m_pGroup = (COpcGroupItem*)pHint;
				m_pGroup->GetTagItems(m_obArray); 			
				
				if (m_obArray.GetCount())
				    listctrl.SetItemCountEx (m_obArray.GetCount(), LVSICF_NOSCROLL | LVSICF_NOINVALIDATEALL);

				SetRedraw();
		        listctrl.Invalidate (true);
		        listctrl.UpdateWindow ();
			}
		}
		break;
	case GROUP_VIEW_ADDITEM:
		{
			COpcItem * pItem = (COpcItem * )pHint;
			ASSERT(pItem);
			m_obArray.Add(pItem);
			listctrl.SetItemCountEx (m_obArray.GetCount(), LVSICF_NOSCROLL | LVSICF_NOINVALIDATEALL);
			//listctrl.SetItemData(m_obArray.GetCount(), pItem->GetItemID());
		}
		break;
	}
}

void COpcServerView::OnTimer(UINT_PTR nIDEvent)
{
	
	if (nIDEvent==UPDATE_ITEMPANE_EVENT)
	{
			CListCtrl &cList = GetListCtrl ();
			// If there are items in the list control, update the view:
			if (cList.GetItemCount () > 0)
			{
				int nTopIndex;
				CRect rc;
				CRect rcitem;				
				// Get index of the first visible item:
				nTopIndex = cList.GetTopIndex ();
				// Get client area rectangle.  We will modify this rectangle to 
				// only include the area of the view we wish to refresh.
				cList.GetClientRect (&rc);
				// Get rectangel that bounds top visible item:
				cList.GetItemRect (nTopIndex, &rcitem, LVIR_BOUNDS);
				// Reset left bound of rc to exclude first column.  There 
				// is no need to refresh this (item ID and image).  We are 
				// interested in refreshing all other properties/columns.
				//rc.left = cList.GetColumnWidth (1);
				// No need to refresh anything above top of uppermost item:
				rc.top = rcitem.top;						
				// Force a repaint of the area:
				cList.InvalidateRect (&rc, FALSE);
			}
	}
	else	
		CListView::OnTimer(nIDEvent);
}

BOOL COpcServerView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
	
	if (!CListView::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext))
		return FALSE;

	CListCtrl & listctrl = GetListCtrl();
	m_cImageList.Create (IDB_ITEMIMAGE, 16, 4, RGB (255, 0, 255));
	m_cImageList.SetBkColor (CLR_NONE);
	listctrl.SetImageList (&m_cImageList, LVSIL_SMALL);

	listctrl.SetExtendedStyle(listctrl.GetExtendedStyle() | LVS_EX_FULLROWSELECT); 
	TCHAR szcol[9][20] = {_T("Item ID"),_T("Data Type"),_T("Value"),_T("TimeStamp"),_T("Quality"),_T("Description"),_T("IO Address"),_T("rw Mode"),_T("Range")};
	int   xcol[9]={100,80,150,100,70,170,80,70,50};
	LVCOLUMN lv;
	for (int i=0;i<9;i++)
	{
		lv.fmt = LVCFMT_LEFT;
		lv.iSubItem = i;
		lv.cx = xcol[i];
		lv.mask = LVCF_FMT | LVCF_WIDTH |LVCF_TEXT| LVCF_SUBITEM; 
		lv.pszText = szcol[i];
		listctrl.InsertColumn(i,&lv);
	}
	SetTimer(UPDATE_ITEMPANE_EVENT,1000,NULL);

	return TRUE;	
}

BOOL COpcServerView::DestroyWindow()
{
	
	KillTimer(UPDATE_ITEMPANE_EVENT);
	return CListView::DestroyWindow();
}

int COpcServerView::GetSelectedItems (CObArray &cItemList)
{
	CListCtrl &cListCtrl = GetListCtrl ();
	DWORD dwSelectedCount = cListCtrl.GetSelectedCount();
	if (dwSelectedCount)
	{
		CListCtrl &cListCtrl = GetListCtrl ();
		int nSelIndex = -1;
		COpcItem *pItem = NULL;
		DWORD dwCount = 0;
		try
		{
			cItemList.SetSize (dwSelectedCount);
			nSelIndex = cListCtrl.GetNextItem (-1, LVNI_ALL | LVNI_SELECTED);
			while ((nSelIndex >= 0) && (dwCount <= dwSelectedCount))
			{
				pItem = (COpcItem *) m_obArray[nSelIndex];
				ASSERT (pItem != NULL);
				cItemList.SetAt (dwCount++, pItem);
				nSelIndex = cListCtrl.GetNextItem (nSelIndex, LVNI_ALL | LVNI_SELECTED);
			}
		}
		catch (...)
		{
			ASSERT (FALSE);
			dwSelectedCount = 0;
		}
	}
	return (dwSelectedCount);
}

void COpcServerView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (nChar == VK_SPACE)
	{
		CListCtrl &cListCtrl = GetListCtrl ();
		if (cListCtrl.GetItemCount())
		{
			CRect rc;
			GetWindowRect (&rc);
			rc.left = (rc.left + rc.right) / 2;
			rc.top = (rc.top + rc.bottom) / 2;
			OnContextMenu(this,CPoint(rc.left,rc.top));
		}
	}
	else
		CListView::OnChar(nChar, nRepCnt, nFlags);
}

void COpcServerView::OnUpdateEditDelete(CCmdUI *pCmdUI)
{
	CListCtrl &cListCtrl = GetListCtrl ();
	DWORD dwSelectedCount = cListCtrl.GetSelectedCount();
	pCmdUI->Enable(dwSelectedCount>0); 
}

void COpcServerView::OnDelete()
{
	CObArray selArray;
	int selcount =	GetSelectedItems(selArray);
	if (selcount)
	{		
		GetDocument()->DeleteItems(m_pGroup,selArray);
	}
}

void COpcServerView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (nChar == VK_DELETE)
		OnDelete();
	else
	   CListView::OnKeyDown(nChar, nRepCnt, nFlags);
}


void COpcServerView::OnUpdateEditEdit(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	CListCtrl &cListCtrl = GetListCtrl ();
	DWORD dwSelectedCount = cListCtrl.GetSelectedCount();
	pCmdUI->Enable(dwSelectedCount==1); 
}


void COpcServerView::OnEditEdit()
{
	// TODO: Add your command handler code here

	CListCtrl &cListCtrl = GetListCtrl ();
	COpcItem *pItem = NULL;
	int nSelIndex = -1;
	nSelIndex = cListCtrl.GetNextItem (-1, LVNI_ALL | LVNI_SELECTED);
	pItem = (COpcItem *) m_obArray[nSelIndex];

	if (pItem)
	{
	    CNewItemDialog Dialog(this);
		Dialog.m_szTagName = pItem->GetName();	
		Dialog.m_szTagAdr = pItem->GetAddress();
		Dialog.m_dataType = GetdataType(pItem->GetType());
		Dialog.m_wrType = pItem->GetWrType();
		Dialog.m_szTagDesc = pItem->GetTagDesc();
		Dialog.m_Range = pItem->GetRange();
		Dialog.m_SwapData = pItem->GetSwapData();
		Dialog.m_UseBigIndian = pItem->GetBigIndia();
		Dialog.m_Title = _T("Edit Tag");
		if (Dialog.DoModal()==IDOK)
		{	
			//CSafeLock cs GetDocument()->m_csDataLock;
			pItem->SetName(Dialog.m_szTagName );
			pItem->SetAddress(Dialog.m_szTagAdr);
			pItem->SetDesc(Dialog.m_szTagDesc);
			pItem->SetSwapData(Dialog.m_SwapData);
			pItem->SetBigIndia(Dialog.m_UseBigIndian);
			pItem->SetRange(Dialog.m_Range);
			
			int wrType = 0;
			int dataType = 0;
			switch (Dialog.m_dataType)
				//was LONG;DWORD;BOOL;STRING;FLOAT;DOUBLE;
				//;;
			//BOOL;BYTE (unsign 8bit);WORD (usign 16bit);DWORD (unsign 32bit);QWORD (unsign 64bit);INT8 (sign 8bit);INT16 (sign 16bit);INT32 (sign 32bit);INT64 (sign 64bit);FLOAT (32bit);DOUBLE (64bit);STRING
			{
			case 0:
				dataType = VT_BOOL;//BOOL
				break;
			case 1:
				dataType = VT_UI1; //BYTE
				break;
			case 2:
				dataType = VT_UI2; //WORD
				break;
			case 3:
				dataType = VT_UI4; //DWORD
				break;
			case 4:
				dataType = VT_UI8; //QWORD
				break;

			case 5:
				dataType = VT_I1;//INT8
				break;
			case 6:
				dataType = VT_I2;//INT16
				break;
			case 7:
				dataType = VT_I4;//INT32
				break;
			case 8:
				dataType = VT_I8;//INT64
				break;
			case 9:
				dataType = VT_R4; // FLOAT
				break;
			case 10:
				dataType = VT_R8; //DOUBLE
				break;
			case 111:
				dataType = VT_BSTR;//STRING
				break;
			}
			switch (Dialog.m_wrType)
			{
			case 0:
				wrType = OPC_READABLE | OPC_WRITEABLE;
				break;
			case 1:
				wrType = OPC_READABLE;
				break;
			case 2:
				wrType = OPC_WRITEABLE;
				break;
			}
			wrType = Dialog.m_wrType;
			pItem->SetWrType(wrType);
			//if (pItem->GetRange()>1)
			//pItem->SetType(VT_ARRAY|dataType);
			//else
			pItem->SetType(dataType);
			
			GetDocument()->SetModifiedFlag();
			// LogMsg(IDS_MSG,_T("Item edited !"));
			
		}
	}


}

int COpcServerView::GetdataType(int dataType)
{
switch (dataType)//LONG;DWORD;BOOL;STRING;FLOAT;DOUBLE;
			{
			case VT_BOOL:
				return 0 ;
				break;
			case VT_UI1:
				return 1;
				break;
			case VT_UI2:
				return 2;
				break;
			case VT_UI4:
				return 3;
				break;
			case VT_UI8:
				return 4;
				break;
			case  VT_I1:
				return 5;
				break;
			case  VT_I2:
				return 6;
				break;
			case  VT_I4:
				return 7;
				break;
			case VT_I8:
				return 8;
				break;
			case  VT_R4:
				return 9;
				break;
			case  VT_R8:
				return 10;
				break;
			case  VT_BSTR:
				return 11;
				break;

			default:
				return -1;
				break;
			}
}
/*int COpcServerView::GetwrType(int wrType)	
{
switch (wrType)
			{
			case OPC_READABLE | OPC_WRITEABLE:
				return 3;
				break;
			case OPC_READABLE:
				return 1;
				break;
			case OPC_WRITEABLE:
				return 2;
				break;
			default:
				return 1;
				break;
			}
}
*/

void COpcServerView::OnUpdateEditCopy(CCmdUI *pCmdUI)
{
		CListCtrl &cListCtrl = GetListCtrl ();
	DWORD dwSelectedCount = cListCtrl.GetSelectedCount();
	pCmdUI->Enable(dwSelectedCount>0); 
}


void COpcServerView::OnUpdateEditPaste(CCmdUI *pCmdUI)
{/// copy buffer must have data then paste
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_bPasteAvailable);
}

void COpcServerView::OnItemclickList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLISTVIEW *pLV = (NMLISTVIEW *) pNMHDR;
		//CListCtrl cListCtrl = GetListCtrl ();
	
	GetListCtrl().SortItems(SortFunc, pLV->iItem);
	
	
	*pResult = 0;
}

int CALLBACK SortFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	int nRetVal=-1;

	switch(lParamSort)
	{
	case 0: // 
		//nRetVal = strcmp(pData1->pszTerm, pData2->pszTerm);
		break;

	default:
		break;
	}

	return nRetVal;
}

void COpcServerView::OnEditPaste()
{

}

void COpcServerView::OnEditCopy()
{
	//return ;//not ready

	if(!OpenClipboard()) return;
	EmptyClipboard();
   HGLOBAL hGlob;// = ::GlobalAlloc(GMEM_MOVEABLE ,sizeof(COpcItem));   
   // if(::IsClipboardFormatAvailable(CF_GROUPITEM))   
   //     TestClipBoard();   
    if(::OpenClipboard(m_hWnd))   
    {   

		CListCtrl &cListCtrl = GetListCtrl ();

		int nSelIndex = -1;
		nSelIndex = cListCtrl.GetNextItem (-1, LVNI_ALL | LVNI_SELECTED);
		int iCount = cListCtrl.GetSelectedCount();
		hGlob = ::GlobalAlloc(GMEM_MOVEABLE ,sizeof(COpcItem)* iCount );   

        POSITION pos = cListCtrl.GetFirstSelectedItemPosition();   

        if(iCount>0)   
        {   
            ::EmptyClipboard();   
            COpcItem *pData = (COpcItem*)::GlobalLock(hGlob);   

			int i = 0;   
            while(pos)   
            {   
                int nItem = cListCtrl.GetNextSelectedItem(pos);   
				pData =  (COpcItem *) m_obArray[nSelIndex];   
            }   
             
            ::SetClipboardData(CF_ITEM,pData);   
			m_bPasteAvailable=1;
            ::GlobalUnlock(hGlob);   
        }   
        else   
        {   
        }   
        ::CloseClipboard();   
    }   
}
