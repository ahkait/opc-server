#pragma once
#include "afx.h"
#include ".\header\IPlugin.h"
#include <afxmt.h>   
#include <afxcoll.h>   
///
class COpcGroupItem;
class COpcServerDoc;
class COpcItem;

 typedef struct ServerComm
{
	CString strStopbit;
	CString	strPort;
	CString	strDatabits;
	CString	strParitybit;
	CString	strBaudrate;
	CString	strFlowctrl;
} ServerComm ;


 
//convenience typedef for the pointers to the 2 functions we expect to find in the plugins
typedef IPlugin* (*PLUGIN_FACTORY)(); 
typedef void (*PLUGIN_CLEANUP)(IPlugin*); 
typedef void (*SHOW_SETUP)(CWnd *,LPCTSTR ); 
typedef void (*PLUGIN_ENTER_WRITE)(long*); 


class COpcComServerItem :
	public CObject
{
public:
	COpcComServerItem(void);
	COpcComServerItem(COpcServerDoc * pDocument,LPCTSTR strName,LPCTSTR szDriverName,LPCTSTR strAdvanceConfigfile, ServerComm *Comm);
	virtual ~COpcComServerItem(void);
	virtual void Serialize(CArchive& ar);
	void SerializeIni(CArchive& ar, LPCTSTR key );
	void ReadWriteIni(LPCTSTR inifname, bool Storing, LPCTSTR key );
	void SetName(LPCTSTR szName);
	void SetDriverName(LPCTSTR szName);
	void SetComm(ServerComm Comm){m_Comm = Comm;};
	void SetAdvanceConFig(LPCTSTR szName){m_AdvanceConFile = szName;};
	void SetDriverAutoStart(int value){m_DriverAutoStart=value;};
	int  GetDriverAutoStart(){return m_DriverAutoStart;};
	void SetEvent(CString Str, UINT evn);
	void SetTreeItem(HTREEITEM h);
	HTREEITEM GetTreeItem(){return hTreeItem;}
	CString GetName(){
	CSafeLock cs (&m_csDataLock);
	return m_szServerName;
	};
	CString GetDriverName(){
	CSafeLock cs (&m_csDataLock);
	return m_szDriverName;
	};
	CString GetAdvanceConFig(){ return m_AdvanceConFile;}
	ServerComm GetComm(){return m_Comm;}
	COpcServerDoc * pDoc;

private:
	void StoreServers();
	void LoadServers(LPCTSTR item);
public:
	CString m_szServerName;
	CString m_szDriverName;
//    CList<COpcGroupItem*,COpcGroupItem*> m_groupList;
	int m_DriverAutoStart;
	ServerComm m_Comm;
	CString m_AdvanceConFile;
	int  m_igroupCount;
	HTREEITEM hTreeItem;

	CCriticalSection m_csDataLock;
public:
	void Remove(void);
	void AddGroup(LPCTSTR strName,DWORD groupIP,int igroupAddress, int igroupPort ); //,LPCTSTR strDriverName);
	int GetGroupCount(){return m_igroupCount;}
	void CreateOpcTag(void);
	void Simulate(void);
	int GetTagCount();
	BOOL DeleteGroup(COpcGroupItem * pGroup);
	COpcItem * GetItemByID(LPCTSTR strGroup,int id);
	BOOL SetOpcItemID(LPCTSTR strGroup,LPCTSTR strItem,int id);
	COpcGroupItem * GetGroup(LPCTSTR strGroup);

	/// needs to hold 1 thread, so that it will spawn and runs the actual hardware reading/writing

	unsigned m_nTerminateThread;
    CWinThread*	m_pThread;
	
	HMODULE h_mod; //handler for our dll
	IPlugin* p_plugin;
	PLUGIN_FACTORY p_factory_function;
	PLUGIN_CLEANUP p_cleanup_function;
	SHOW_SETUP p_showsetup_function;
	PLUGIN_ENTER_WRITE p_plugin_enter_write;
	bool GetDriverActive(){return  m_nTerminateThread == 0 ;};

    void StartServer( CWnd * cwndserver ,BOOL bFlag= FALSE );

	static UINT ThreadLoop(LPVOID pParam);
	 CList<COpcGroupItem*,COpcGroupItem*> m_groupList;

};
