#pragma once


// CEventView 

class CEventView : public CListView
{
	DECLARE_DYNCREATE(CEventView)

protected:
	CEventView();           
	virtual ~CEventView();

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	virtual void OnInitialUpdate();
protected:
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
public:
	bool m_init;
	void LogEvent(CString szLog, int type);
};


