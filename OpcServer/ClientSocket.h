#pragma once
class COpcServerDoc;
// CClientSocket
#define BUFFER_LEN 409600
class CClientSocket : public CSocket
{
public:
	CClientSocket(COpcServerDoc * doc);
	virtual ~CClientSocket();
	virtual void OnReceive(int nErrorCode);
	virtual void OnClose(int nErrorCode);
private:
	COpcServerDoc * pDoc;
    void GetTorken(char * strKey,char Key[]);
    CStringA GetXMLNodeItem(char * strSource, char * strNodeItem);
    void ProcessLineData(char * strInput);
	int m_aiCount,m_diCount;
	char lpBuf[BUFFER_LEN+1];
	int  iCount;
	void Reset()
	{
		m_aiCount = 0;
		m_diCount = 0;
	}
};


