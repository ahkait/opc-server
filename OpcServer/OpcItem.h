#pragma once
#include "afx.h"
class COpcGroupItem;

class COpcItem :
	public CObject
{
public:
	COpcItem(COpcGroupItem * pGroupItem);
	COpcItem(COpcGroupItem * pGroupItem,LPCTSTR strName,LPCTSTR strAddress,LPCTSTR strDesc,int tagType,DWORD wrType,int range);
	~COpcItem(void);
	virtual void Serialize(CArchive& ar);
	void SerializeIni(CArchive& ar, LPCTSTR key );
	void ReadWriteIni(LPCTSTR inifname, bool Storing, LPCTSTR key );
private:
	loTagValue loTag;
	loTagValue loWriteTag;

	int m_iItemID;
	CString m_sztagName;
	CString m_sztagAddress; 
	CString m_sztagdesc;
	COpcGroupItem * m_pGroup;
	int    m_itagType;
	int    m_itagRange; //add range so that can read multiple io address
	DWORD  m_dwwrType;
	DWORD  m_dwActCount;
	WORD   m_wQuality;
	bool m_ErrorNoScan ;

	int	m_Swap;
	int	m_UseBigIndian;
	CCriticalSection m_csDataLock;
	SYSTEMTIME m_systm;
	void StoreItems();
	void LoadItems(LPCTSTR item);
public:

	BOOL GetActive(){return (m_dwActCount>0);}
	void SetActive(BOOL act=TRUE){
        CSafeLock cs (&m_csDataLock);
		if (act)
			m_dwActCount++;
	    else	
			m_dwActCount--;
	}
	WORD GetQuality(){return m_wQuality;}
	void SetQuality(WORD q){m_wQuality=q;}	
	LPCTSTR GetStrQuality();
	LPCTSTR GetStrwrType();
	void SetGroup(COpcGroupItem * pGroupItem){m_pGroup=pGroupItem;}
	COpcGroupItem * GetGroup(){return m_pGroup;}
	CString GetName(){return m_sztagName;}
	CString GetTagDesc(){return m_sztagdesc;}
	void SetName(LPCTSTR s){m_sztagName=s;}
	void SetAddress(LPCTSTR s){m_sztagAddress=s;}
	void SetDesc(LPCTSTR s){m_sztagdesc=s;}
	void SetType(int type){m_itagType = type; loTag.tvValue.vt = type;}
	void SetRange(int range){m_itagRange = range;}
	CString GetAddress(){return m_sztagAddress;}
	int GetType(){return m_itagType;}
	int GetRange(){return m_itagRange;}
	int GetSwapData(){return m_Swap;}
	int GetBigIndia(){return m_UseBigIndian;}
	void SetSwapData(int data){ m_Swap=data;}
	void SetBigIndia(int data){ m_UseBigIndian=data;}
	void SetErrorNoScan(bool value){m_ErrorNoScan = value;} ;
	bool GetErrorNoScan(){return m_ErrorNoScan ;} ;
	void SetmSystime(SYSTEMTIME m_time) {m_systm = m_time;};

	loTagValue * GetLoTag(){return &loTag;}
	loTagValue * GetLoWriteTag(){return &loWriteTag;}

	LONG GetLogTagSize(){LONG lUpper; SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);
			return lUpper ;}
	void SetWrType(DWORD dt){m_dwwrType=dt;}
	DWORD GetWrType(){return m_dwwrType;}
	void GetValue(CString & strValue);	
	CString GetStrValue();
	void GetVariantValue(VARIANT * varData);
    void UpdateData (VARIANT &vtVal, WORD wQuality=OPC_QUALITY_GOOD,SYSTEMTIME * systm=NULL);
	CString FormatTime ();
	int GetItemID()
	{
		return m_iItemID;
	}
	void SetItemID(int id)
	{
		m_iItemID = id;
	}
	void UpDate(float data,WORD quality = OPC_QUALITY_GOOD);
	void UpDate(bool data,WORD quality = OPC_QUALITY_GOOD);
	void UpDate(int data,WORD quality = OPC_QUALITY_GOOD);
    void UpDate(long data,WORD quality = OPC_QUALITY_GOOD);
	void UpDate(double data,WORD quality = OPC_QUALITY_GOOD);
	void UpDate(CString str,WORD quality = OPC_QUALITY_GOOD);
	void UpDate(WORD data,WORD quality = OPC_QUALITY_GOOD);
    void UpDate(short data,WORD quality = OPC_QUALITY_GOOD);
};
