#pragma once
#include "afxwin.h"


// CInsertServerDialog 

class CInsertServerDialog : public CDialog
{
	DECLARE_DYNAMIC(CInsertServerDialog)

public:
	CInsertServerDialog(CWnd* pParent = NULL);   //
	virtual ~CInsertServerDialog();
	CString Get_AppPath();

//
	enum { IDD = IDD_INSERTSERVERDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 

	DECLARE_MESSAGE_MAP()
public:
	CString m_szServer;
	CString m_szDriverName;
	CString m_strStopbit;
	CString	m_strPort;
	CString	m_strDatabits;
	CString	m_strParitybit;
	CString	m_strBaudrate;
	CString	m_strFlowctrl;
	CString m_strConfigFile;
	CString m_Title;
	///add comm port parameters
	afx_msg void OnBnClickedSelectdir();
	afx_msg void OnCbnSelchangeComboBaudrate();
	CComboBox m_Port;
	CComboBox m_BaudRate;
	CComboBox m_DataBit;
	CComboBox m_ParityBit;
	CComboBox m_StopBit;
	CComboBox m_FlowCtrl;
	afx_msg void OnCbnSelchangeComboDatabits();
	afx_msg void OnCbnSelchangeComboCheck();
	afx_msg void OnCbnSelchangeComboPort();
	afx_msg void OnCbnSelchangeComboStopbit();
	afx_msg void OnCbnSelchangeComboFlowbit();
	virtual BOOL OnInitDialog();
	CEdit m_configFile;
	afx_msg void OnBnClickedAdvanceBtn();
	afx_msg void OnEnKillfocusConfigfileedit();
	CString m_fullpathFile;
	CButton m_DriverAutoStart;
	afx_msg void OnBnClickedOk();
};
