// ExeSetup.cpp : implementation file
//

#include "stdafx.h"
#include "OpcServer.h"
#include "ExeSetup.h"
#include "afxdialogex.h"


// CExeSetup dialog

IMPLEMENT_DYNAMIC(CExeSetup, CDialog)

CExeSetup::CExeSetup(CWnd* pParent /*=NULL*/)
	: CDialog(CExeSetup::IDD, pParent)
{

}

CExeSetup::~CExeSetup()
{
}

void CExeSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
//	DDX_Control(pDX, IDC_STARTUPFILEEDIT, m_projectStartup);
	DDX_Text(pDX, IDC_STARTUPFILEEDIT, m_projectStartup);
	DDV_MaxChars(pDX, m_projectStartup, 1024);
}


BEGIN_MESSAGE_MAP(CExeSetup, CDialog)
	ON_BN_CLICKED(IDOK, &CExeSetup::OnBnClickedOk)
	ON_BN_CLICKED(IDC_SELECTFILEBTN, &CExeSetup::OnBnClickedSelectfilebtn)
END_MESSAGE_MAP()


// CExeSetup message handlers


void CExeSetup::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	// ask to save startup string in registry
			CWnd* pWnd = GetDlgItem(IDC_STARTUPFILEEDIT);
			pWnd->GetWindowText(m_projectStartup);
	CDialog::OnOK();

}


void CExeSetup::OnBnClickedSelectfilebtn()
{
	// browse and show the opc file to startup
		// TODO: Add your control notification handler code here
		CString strTitle = _T("Select a opc file");
		CFileDialog ofn( TRUE,_T("*.opc"),
			_T(""),						
			OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, //
			_T("project Files (*.opc)|*.opc|"),this,0,TRUE);								
		ofn.m_ofn.lpstrTitle = strTitle;
		if (ofn.DoModal() == IDOK) 
		{
			CString fullfilePath(ofn.GetPathName());
			
			CWnd* pWnd = GetDlgItem(IDC_STARTUPFILEEDIT);
			pWnd->SetWindowText(fullfilePath);
				
		}
}
