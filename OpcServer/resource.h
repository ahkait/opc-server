//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by OpcServer.rc
//
#define IDSELECTDIR                     3
#define IDD_ABOUTBOX                    100
#define IDP_SOCKETS_INIT_FAILED         104
#define IDS_CLIENTCOUNT                 105
#define IDR_POPUP_EDIT                  119
#define ID_STATUSBAR_PANE1              120
#define ID_STATUSBAR_PANE2              121
#define IDS_STATUS_PANE1                122
#define IDS_STATUS_PANE2                123
#define IDS_TOOLBAR_STANDARD            124
#define IDS_TOOLBAR_CUSTOMIZE           125
#define ID_VIEW_CUSTOMIZE               126
#define ID_VIEW_ARRANGE                 127
#define IDR_MAINFRAME                   128
#define IDR_MAINFRAME_256               129
#define IDS_EDIT_MENU                   306
#define IDR_TREEMENU                    310
#define IDR_USERIMAGE                   311
#define IDD_INSERTSERVERDIALOG          312
#define IDD_INSERTGROUPDIALOG           313
#define IDD_ITEMDIALOG                  314
#define IDB_EVENTIMAGES                 316
#define IDR_EVENTMENU                   317
#define IDB_ITEMIMAGE                   318
#define IDD_SETDIALOG                   319
#define IDD_STARTUPDIALOG               320
#define IDC_SERVEREDIT                  1000
#define IDC_GROUPEDIT                   1001
#define IDC_DRIVERNAMEEDIT              1002
#define IDC_NAMEEDIT                    1003
#define IDC_CONFIGFILEEDIT              1003
#define IDC_ADDRESSEDIT                 1004
#define IDC_DESCEDIT                    1005
#define IDC_IPPORTEDIT                  1006
#define IDC_ADREDIT                     1007
#define IDC_DATATYPECOMBO               1008
#define IDC_WRTYPECOMBO                 1009
#define IDC_PORTEDIT                    1010
#define IDC_DATATYPECOMBO2              1010
#define IDC_SWAPCOMBO                   1010
#define IDC_ADRRANGEEDIT                1011
#define IDC_BUTTON1                     1011
#define IDC_ADVANCE_BTN                 1011
#define IDC_SELECTFILEBTN               1011
#define IDC_USEREDIT                    1012
#define IDC_IPEDIT                      1013
#define IDC_STARTUPFILEEDIT             1013
#define IDC_ENABLEREMOTECHECK           1013
#define IDC_PWDEDIT                     1014
#define IDC_USEBIGINDIAN                1014
#define IDC_DRIVERAUTOSTARTCHECK        1014
#define IDC_IDEDIT                      1015
#define IDC_UNAUTHORIZEDSTATIC          1015
#define IDC_SPINRANGE                   1016
#define IDC_CHECK1                      1016
#define IDC_CHECK_GENERATE              1016
#define IDC_COMBO_PORT                  1049
#define IDC_COMBO_BAUDRATE              1050
#define IDC_COMBO_DATABITS              1051
#define IDC_COMBO_CHECK                 1052
#define IDC_COMBO_STOPBIT               1053
#define IDC_COMBO_FLOWBIT               1054
#define IDS_MSG                         15001
#define IDS_ERMSG                       16001
#define ID_SERVER_ADD                   32775
#define ID_SERVER_EDIT                  32776
#define ID_GROUP_ADD                    32777
#define ID_GROUP_EDIT                   32778
#define ID_SERVER_32779                 32779
#define ID_ITEM_ADD                     32780
#define IDS_TREE_MENU                   32781
#define ID_32782                        32782
#define IDS_EVENTMENU                   32782
#define ID_32783                        32783
#define ID_VIEW_CLEAR                   32784
#define ID_VIEW_ERRORONLY               32785
#define IDS_EVENT_MENU                  32786
#define ID_SERVER_32787                 32787
#define ID_EDIT_DELETE                  32788
#define ID_SERVER_32793                 32793
#define ID_IMPORTTAG                    32794
#define ID_32795                        32795
#define ID_SETTING                      32796
#define ID_Menu                         32797
#define ID_ITEM_EDIT                    32798
#define ID_EDIT_EDIT                    32799
#define ID_EDIT_EXESETUP                32800

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        321
#define _APS_NEXT_COMMAND_VALUE         32801
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
