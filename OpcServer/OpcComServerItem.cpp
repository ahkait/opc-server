#include "stdafx.h"
#include "OpcServer.h"
#include "OpcComServerItem.h"
#include "OpcGroupItem.h"
#include "OpcServerDoc.h"
#include "OpcItem.h"
#include "MainFrm.h"
#include "CiniFile\SimpleIni.h"

extern CSimpleIni iniFile; 
extern  CMyOpcServer my_CF;
//typedef void (*pLogMessage)(UINT , ...  );
typedef void (CMyOpcServer::*pUpDateTag)(loTagValue * ,int);
typedef void (COpcItem::*pItemUpDateData)(VARIANT &, WORD ,SYSTEMTIME *);

COpcComServerItem::COpcComServerItem(void)
{
	m_szServerName = _T("");
	m_szDriverName = _T("");
	m_AdvanceConFile = _T("");
	m_Comm.strStopbit=_T("1");
	m_Comm.strPort=_T("1");
	m_Comm.strDatabits=_T("8");
	m_Comm.strParitybit=_T("0"); 
	m_Comm.strBaudrate=_T("9600");
	m_Comm.strFlowctrl=_T("0");
	m_igroupCount = 0;
	hTreeItem = NULL;
	pDoc = NULL;

	h_mod = NULL;
	p_plugin = NULL;
	p_factory_function = NULL ;
	p_cleanup_function = NULL ;
}

COpcComServerItem::COpcComServerItem(COpcServerDoc * pDocument,LPCTSTR strName,LPCTSTR szDriverName, LPCTSTR strAdvanceConfigfile, ServerComm *Comm)
{
	m_szServerName = strName;
	m_szDriverName = szDriverName;
	m_AdvanceConFile = strAdvanceConfigfile;
	m_Comm.strStopbit=Comm->strStopbit;
	m_Comm.strPort=Comm->strPort;
	m_Comm.strDatabits=Comm->strDatabits;
	m_Comm.strParitybit=Comm->strParitybit; 
	m_Comm.strBaudrate=Comm->strBaudrate;
	m_Comm.strFlowctrl=Comm->strFlowctrl;
	m_igroupCount = 0;
	hTreeItem = NULL;
	pDoc = pDocument;

    
	h_mod = NULL;
	p_plugin = NULL;
	p_factory_function = NULL ;
	p_cleanup_function = NULL ;
}

COpcComServerItem::~COpcComServerItem(void)
{
		/// inform dll close
	
		//(CWinThread*)m_pThread->SuspendThread();
		m_nTerminateThread = 1;
		Sleep(100);
		//(CWinThread*)m_pThread->ter
		m_pThread = NULL;
		delete m_pThread;

	if (m_groupList.GetCount())
		Remove();
}

void COpcComServerItem::StoreServers()
{

	CString driver = _T("Server\\")+m_szServerName ;

	iniFile.SetValue(driver,_T("Name"),m_szServerName);
	iniFile.SetValue(driver,_T("DriverName"),m_szDriverName);
	iniFile.SetValue(driver,_T("ConfigFile"),m_AdvanceConFile);
	iniFile.SetValue(driver,_T("Baudrate"), m_Comm.strBaudrate);
	iniFile.SetValue(driver,_T("Databits"), m_Comm.strDatabits);
	iniFile.SetValue(driver,_T("Flowctrl"), m_Comm.strFlowctrl);
	iniFile.SetValue(driver,_T("Paritybit"), m_Comm.strParitybit);
	iniFile.SetValue(driver,_T("Port"), m_Comm.strPort);
	iniFile.SetValue(driver,_T("Stopbit"),m_Comm.strStopbit);

	iniFile.SetBoolValue(driver,_T("DriverAutoStart"),m_DriverAutoStart);
	iniFile.SetLongValue(driver,_T("igroupCount"),m_igroupCount);
}

void COpcComServerItem::LoadServers(LPCTSTR item)
{
	
	m_szServerName = iniFile.GetValue(item,_T("Name"));
	m_szDriverName = iniFile.GetValue(item,_T("DriverName"));
	m_AdvanceConFile = iniFile.GetValue(item,_T("ConfigFile"));
	m_Comm.strBaudrate= iniFile.GetValue(item,_T("Baudrate"));
	m_Comm.strDatabits = iniFile.GetValue(item,_T("Databits"));
	m_Comm.strFlowctrl= iniFile.GetValue(item,_T("Flowctrl"));
	m_Comm.strParitybit = iniFile.GetValue(item,_T("Paritybit"));
	m_Comm.strPort = iniFile.GetValue(item,_T("Port"));
	m_Comm.strStopbit = iniFile.GetValue(item,_T("Stopbit"));

	m_DriverAutoStart = iniFile.GetBoolValue(item,_T("DriverAutoStart"));
	m_igroupCount= iniFile.GetLongValue(item,_T("igroupCount"));

}

void COpcComServerItem::ReadWriteIni(LPCTSTR inifname, bool Storing, LPCTSTR key )
{
	if (Storing)
	{
		StoreServers();

		if (m_igroupCount)
		{
		POSITION pos = m_groupList.GetHeadPosition();
		while (pos)
		{
			COpcGroupItem * group = m_groupList.GetNext(pos);
			if(group)
			{
			CString list = _T("Server\\")+group->GetServer()->GetName()+_T("\\Groups");
			iniFile.SetLongValue(list,group->GetName(),1);
			group->ReadWriteIni(inifname,Storing,group->GetName());
			}
		}
		}
		///// plugin data ?
	}
	else
	{
		CString loadkey =  key;
		LoadServers(loadkey);
	  ///  ar.m_pDocument->UpdateAllViews(NULL,SERVER_ADD,this); 
		pDoc->UpdateAllViews(NULL,SERVER_ADD,this); 
		CString sect = loadkey+_T("\\Groups");

		//// get list
		CSimpleIniW::TNamesDepend keys;
		iniFile.GetAllKeys(sect, keys); //we do not know how many itens, but this section contains all
		
		CSimpleIniW::TNamesDepend::const_iterator i;
		m_igroupCount=0;
	for ( i = keys.begin(); i != keys.end(); ++i) 
		{
				m_igroupCount++;
				CString head= key;
				CString grp= i->pItem;		
				grp= loadkey+_T("\\Group\\")+grp;

			COpcGroupItem * group = new COpcGroupItem(this);
			//if (group->GetName() ==_T("")) continue;
			group->ReadWriteIni(inifname,Storing,grp); 
			m_groupList.AddTail(group); 
			// ar.m_pDocument->UpdateAllViews(NULL,SERVER_GROUP_ADD,group);  
			pDoc->UpdateAllViews(NULL,SERVER_GROUP_ADD,group); 

		}
		///// plugin data ?
	}


}


void COpcComServerItem::SerializeIni(CArchive& ar, LPCTSTR key )
{
	if (ar.IsStoring())
	{
		StoreServers();

		if (m_igroupCount)
		{
		POSITION pos = m_groupList.GetHeadPosition();
		while (pos)
		{
			COpcGroupItem * group = m_groupList.GetNext(pos);
			if(group)
			{
			CString list = _T("Server\\")+group->GetServer()->GetName()+_T("\\Groups");
			iniFile.SetLongValue(list,group->GetName(),1);
			group->SerializeIni(ar,group->GetName());
			}
		}
		}
		///// plugin data ?
	}
	else
	{
		CString loadkey =  key;
		LoadServers(loadkey);
	    ar.m_pDocument->UpdateAllViews(NULL,SERVER_ADD,this); 

		CString sect = loadkey+_T("\\Groups");

		//// get list
		CSimpleIniW::TNamesDepend keys;
		iniFile.GetAllKeys(sect, keys); //we do not know how many itens, but this section contains all
		
		CSimpleIniW::TNamesDepend::const_iterator i;

	for ( i = keys.begin(); i != keys.end(); ++i) 
		{
				CString head= key;
				CString grp= i->pItem;		
				grp= loadkey+_T("\\Group\\")+grp;

			COpcGroupItem * group = new COpcGroupItem(this);
			//if (group->GetName() ==_T("")) continue;
			group->SerializeIni(ar,grp); 
			m_groupList.AddTail(group); 
			ar.m_pDocument->UpdateAllViews(NULL,SERVER_GROUP_ADD,group);  
		}
		///// plugin data ?
	}


}


void COpcComServerItem::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
	//	StoreServers();
		ar << m_szServerName; 
		ar << m_szDriverName; //need add comm para.
		ar << m_AdvanceConFile;
		ar << m_Comm.strBaudrate;
		ar << m_Comm.strDatabits;
		ar << m_Comm.strFlowctrl;
		ar << m_Comm.strParitybit;
		ar << m_Comm.strPort;
		ar << m_Comm.strStopbit;
		ar << m_DriverAutoStart;
		ar << m_igroupCount;

		if (m_igroupCount)
		{
		POSITION pos = m_groupList.GetHeadPosition();
		while (pos)
		{
			COpcGroupItem * group = m_groupList.GetNext(pos);
			if(group)
			{
			CString list = _T("Server\\")+group->GetServer()->GetName()+_T("\\Groups");

			iniFile.SetLongValue(list,group->GetName(),1);
			group->Serialize(ar);
			}
		}
		}
		///// plugin data ?
	}
	else
	{
	//	LoadServers();
		ar >> m_szServerName;
		ar >> m_szDriverName; 
		ar >> m_AdvanceConFile;
		ar >> m_Comm.strBaudrate;
		ar >> m_Comm.strDatabits;
		ar >> m_Comm.strFlowctrl;
		ar >> m_Comm.strParitybit;
		ar >> m_Comm.strPort;
		ar >> m_Comm.strStopbit;
		ar >> m_DriverAutoStart;
		ar >> m_igroupCount;

	    ar.m_pDocument->UpdateAllViews(NULL,SERVER_ADD,this); 
		for (int i=0;i<m_igroupCount;i++)
		{
			COpcGroupItem * group = new COpcGroupItem(this);
			//if (group->GetName() ==_T("")) continue;
			group->Serialize(ar); 
			m_groupList.AddTail(group); 
			ar.m_pDocument->UpdateAllViews(NULL,SERVER_GROUP_ADD,group);  
		}
		///// plugin data ?
	}
}



void COpcComServerItem::Remove(void)
{
	CSafeLock cs (&m_csDataLock);
	if (m_igroupCount)
	{
		POSITION pos = m_groupList.GetHeadPosition();
		while (pos)
		{
			COpcGroupItem * group = m_groupList.GetNext(pos);
			group->Remove();
			delete group;
		}
		m_igroupCount = 0;
		m_groupList.RemoveAll(); 
	}
}

void COpcComServerItem::SetName(LPCTSTR szName)
{
	CSafeLock cs (&m_csDataLock);
	m_szServerName = szName;
}
void COpcComServerItem::SetDriverName(LPCTSTR szName)
{
	CSafeLock cs (&m_csDataLock);
	m_szDriverName = szName;
}

void COpcComServerItem::SetTreeItem(HTREEITEM h)
{
	CSafeLock cs (&m_csDataLock);
    hTreeItem = h;
}

//CString COpcComServerItem::GetName()
//{
//	CSafeLock cs (&m_csDataLock);
//	return m_szServerName;
//}

//CString COpcComServerItem::GetDriverName()
//{
//	CSafeLock cs (&m_csDataLock);
//	return m_szDriverName;
//}

void COpcComServerItem::AddGroup(LPCTSTR strName,DWORD groupIP,int igroupAddress, int igroupPort )
{
	CSafeLock cs (&m_csDataLock);
	COpcGroupItem * group = new COpcGroupItem(this,strName,0,0,0);

			group->SetName(strName);
			group->SetIPAddr(groupIP);
			group->SetDevAddr(igroupAddress);
			group->SetIPPort(igroupPort);



	m_groupList.AddTail(group); 
	m_igroupCount++;
	pDoc->UpdateAllViews(NULL,SERVER_GROUP_ADD,group);  
	pDoc->SetModifiedFlag(); 
}

void COpcComServerItem::CreateOpcTag(void)
{
   CSafeLock cs (&m_csDataLock);
   POSITION pos = m_groupList.GetHeadPosition();
	while (pos)
	{
		COpcGroupItem * group = m_groupList.GetNext(pos);
	
		group->CreatOpcTag(GetName());
	}
}

void COpcComServerItem::Simulate(void)
{
   CSafeLock cs (&m_csDataLock);
   POSITION pos = m_groupList.GetHeadPosition();
	while (pos)
	{
		COpcGroupItem * group = m_groupList.GetNext(pos);
		group->Simulate();
	}
}

int COpcComServerItem::GetTagCount()
{
   CSafeLock cs (&m_csDataLock);
   int count = 0;
   POSITION pos = m_groupList.GetHeadPosition();
	while (pos)
	{
		COpcGroupItem * group = m_groupList.GetNext(pos);
		count += group->GetTagCount(); 
	}
	return count;
}

//delete OpcGroup
BOOL COpcComServerItem::DeleteGroup(COpcGroupItem * pGroup)
{
	if (pGroup)
	{
	    CSafeLock cs (&m_csDataLock);	
		POSITION pos = m_groupList.Find(pGroup); 
		if (pos)
		{
			m_groupList.RemoveAt(pos); 
			pGroup->Remove();
			delete pGroup;
			m_igroupCount--;
			return TRUE;
		}
		else
			return FALSE;
	}
	else
		return FALSE;
}

COpcGroupItem * COpcComServerItem::GetGroup(LPCTSTR strGroup)
{
	COpcGroupItem * pGroup = NULL;
	if (strGroup!=_T(""))
	{
		CSafeLock cs (&m_csDataLock);
		POSITION pos = m_groupList.GetHeadPosition();
		while (pos)
		{
			pGroup = m_groupList.GetNext(pos);
			if (pGroup->GetName() == strGroup)
			{
				break;
			}
		}

	}
	return pGroup;
}

//Net Server use
COpcItem * COpcComServerItem::GetItemByID(LPCTSTR strGroup,int id)
{
	COpcItem * pItem = NULL;
	if (m_szServerName == _T("remote"))
	{
		CSafeLock cs (&m_csDataLock);
		POSITION pos = m_groupList.GetHeadPosition();
		while (pos)
		{
			COpcGroupItem * group = m_groupList.GetNext(pos);
			if (group->GetName() == strGroup)
			{
				pItem = group->GetItemByID(id); 
				if (pItem)
					break;
			}
		}
	}
	return pItem;
}

//Net Server use
BOOL COpcComServerItem::SetOpcItemID(LPCTSTR strGroup,LPCTSTR strItem,int id)
{
	BOOL result = FALSE;
	if (m_szServerName == _T("remote"))
	{
		CSafeLock cs (&m_csDataLock);
		POSITION pos = m_groupList.GetHeadPosition();
		while (pos)
		{
			COpcGroupItem * group = m_groupList.GetNext(pos);
			if (group->GetName() == strGroup)
			{
				if (group->SetItemID(strItem,id))
				{
					result = TRUE;
					break;
				}
			}
		}
	}
	return result;
}

void COpcComServerItem::SetEvent(CString Str, UINT evn)
{
	LogMsg(evn,Str);
}

static int started =0;
void COpcComServerItem::StartServer( CWnd * cwndserver ,BOOL bFlag )
{
		if (started==0){
		CString szLog =_T("location : ")+GetAppPath();
		LogMsg(IDS_MSG,szLog);
		started = 1;
		/// load settings
		}

    if ( bFlag )
    { 
		/// load plugin here
		CString fname =GetAppPath()+ _T("plugins\\")+GetDriverName();
		h_mod = ::LoadLibrary (fname);
		if (h_mod != NULL)
		{
			 p_factory_function = (PLUGIN_FACTORY) ::GetProcAddress (h_mod, "Create_Plugin");
			 p_cleanup_function = (PLUGIN_CLEANUP) ::GetProcAddress (h_mod, "Release_Plugin");
			 p_showsetup_function = (SHOW_SETUP) ::GetProcAddress (h_mod, "Show_Setup");
			 p_plugin_enter_write = (PLUGIN_ENTER_WRITE) ::GetProcAddress (h_mod, "EnterClientWrite");
			if (p_factory_function != NULL && p_cleanup_function != NULL)
			{
				//yes, this DLL exposes the 2 functions we need, it is a plugin we can use!
				//invoke the factory to create the plugin object
				p_plugin = (*p_factory_function)();
				///check 
				p_plugin->GetVersion();
				//before thread starts set all
				ServerComm settings= GetComm();
				p_plugin->SetServerComm( &settings ); 
	///testing	( p_showsetup_function)(_T("test"));
							
				 m_nTerminateThread = 0;
				
				 m_pThread = AfxBeginThread( ThreadLoop, (LPVOID) this ,THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);

				//AfxBeginThread( DL_Enroll_Base, &ARGUMENT, THREAD_PRIORITY, STACK_SIZE, CREATE_SUSPENDED);
				//AfxBeginThread(RUNTIME_CLASS(CMonitorThread),THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
				m_pThread->m_bAutoDelete=FALSE;

				m_pThread->ResumeThread();
				 CString szLog =_T("Server : ")+ GetName()+_T(", Loading Plugin : ")+p_plugin->Get_Name()+_T(" Ver: ") +p_plugin->GetVersion() ;//
				LogMsg(IDS_MSG,szLog);

			}
			else
					{
					CString szLog =_T("Server : ")+ GetName()+ _T(", Invoke p_plugin error : ")+GetDriverName() ;
					LogMsg(IDS_ERMSG,szLog);

					}
		}
		else
		{
					CString szLog =_T("Server : ")+ GetName()+_T(" using no driver file ") ;
					LogMsg(IDS_ERMSG,szLog);

		}
    }
    else
    {
		        m_nTerminateThread = 1;
				Sleep(200);
				if (h_mod != NULL)
				{
					(*p_cleanup_function) (p_plugin);
					CString szLog =_T("Server : ")+ GetName()+_T(", Releaseing plugin ")+GetDriverName() ;
					LogMsg(IDS_MSG,szLog);
					::FreeLibrary (h_mod);
				}
    }
}



UINT COpcComServerItem::ThreadLoop(LPVOID pParam)
{
    COpcComServerItem *pDlg = (COpcComServerItem *)pParam;
    ASSERT( pDlg );
	//CMainFrame *pMainWnd = (CMainFrame *)AfxGetMainWnd (); do not used null

	//enter service loop
	pUpDateTag pUpdateTagFun = &CMyOpcServer::UpDateTag;// gets pointer to member function and pass to plugin
	pItemUpDateData pItemUpdateDataFun = &COpcItem::UpdateData;// gets pointer to member function and pass to plugin

	pDlg->p_plugin->Process_Data(AfxGetInstanceHandle(), &(pDlg->m_nTerminateThread),pParam, &my_CF, &pUpdateTagFun,&pItemUpdateDataFun );

    //release resources
    pDlg->m_pThread = NULL;

    return 0;
}

