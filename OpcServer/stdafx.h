


#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS     

// 
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // 
#include <afxext.h>         // MFC 
#include <afxcview.h>
#include <afxmt.h>   
#include <afxcoll.h>   

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // 
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // 
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     //

#include <afxsock.h>            // 
  

#include <ole2.h>
#include <olectl.h>
#include <oleauto.h>
#include <process.h>
#include <errno.h>
#include <locale.h>

#include "opcda.h"
#include "opcerror.h"
#include "opccomn.h"
#include "lightopc.h"
#include "unilog.h"

#include "safelock.h"
#include "timestmp.h"
#include "fixedsharedfile.h"
//#include ".\header\IPlugin.h"
// event defines/types
#define EVENT_FIRSTINFO		15000
#define EVENT_LASTINFO		15999
#define EVENT_FIRSTERROR	16000
#define EVENT_LASTERROR		16999

typedef enum
	{
	tEventInformation = 0,	// define resource IDs between EVENT_FIRSTINFO and LASTINFO
	tEventError				// define resource IDs between EVENT_FIRSTERROR and LASTERROR
	} EVENTTYPE;




// clipboard formats for cut/copy/paste
extern UINT CF_SERVER;
extern UINT CF_GROUP;
extern UINT CF_ITEM;

// function prototypes
void LogMsg (UINT nResID, ...);

VARTYPE VartypeFromString (LPCTSTR lpszType);
void StringFromVartype (VARTYPE vtType, CString &strType);
CString GetAppPath();
#define LOGID log,0
#include "MyOpcServer.h"

// {B65D2214-171D-4DE7-A921-75681D23190E}
static const GUID CLSID_OPCServer = { 0xb65d2214, 0x171d, 0x4de7, { 0xa9, 0x21, 0x75, 0x68, 0x1d, 0x23, 0x19, 0xe } };
//{ 0x7a203225, 0xeb9, 0x40d4, { 0xa0, 0x8c, 0xc5, 0x54, 0x62, 0xba, 0x5a, 0xe7 } };



const char eClsidName[] = "opc server";
const char eProgID[] = "XceedSys.OpcServer";

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


