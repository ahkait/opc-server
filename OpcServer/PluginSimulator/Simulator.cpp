#pragma once
#include "afx.h"
#include "stdafx.h"



class CMainFrame;
//class CMyOpcServer;
//class COpcGroupItem;


typedef void (CMyOpcServer::*pUpDateTag)(loTagValue * ,int);
typedef void (COpcItem::*pItemUpDateData)(VARIANT &, WORD ,SYSTEMTIME *);

////////////////////////////////////////////////////////////////////////
// A concrete plugin implementation
////////////////////////////////////////////////////////////////////////
// Plugin class
class Simulator : public IPlugin	
{
public:
	Simulator(void);
	~Simulator(void);
	//returns the name of the concrete plugin
	LPCTSTR  Get_Name () const  {return m_DriverName ;}
	LPCTSTR  GetVersion() const {return m_DriverVer;};
	bool  ShowSetup(LPCTSTR addConfile){return FALSE;};
	//does the actual data processing threaded from main exe
	int Process_Data(HINSTANCE hInstance, unsigned *nTerminate,LPVOID pParam,LPVOID pMy_CF, LPVOID pUpdateTagFun, LPVOID pUpdateDataFun);
	
	void GetErrorCode (LPCTSTR errorcode) {errorcode= m_errorCode;};

    void Enable() {enableBit = TRUE;};
    void Disable() {enableBit= FALSE;};
	int DriverStatus(){return 0;};
    void deinit(){};
    LPCTSTR Activation(LPCTSTR code)  {return m_Activate;};
	void SetServerComm (LPVOID psComm);
	void SetLogMsgFun(LPVOID pLogMsgFun) {};
	void SetUpDateFun(LPVOID pUpDateFun) {};
	int ClientWrite(LPVOID OpcItem, VARIANT values){return 0 ;};

private:
	void Simulate(COpcGroupItem *GroupItem);

	bool enableBit;
	CString m_DriverName;
	CString m_DriverVer;
	CString m_errorCode;
	CString m_Activate;
	CCriticalSection m_csDataLock;
	COpcComServerItem  * pOneServerItem;
	CMyOpcServer *my_CF;
	pUpDateTag *updateTag;
	pItemUpDateData *upItemdateData;
	
	ServerComm *Comm;
};


Simulator::Simulator(void)
{
	///
	m_DriverName=_T("Simulator");
	m_DriverVer=_T("1.0.0.1");
	m_Activate=_T("demo");
};


Simulator::~Simulator(void)
{
	
	Sleep(200);
};

void  Simulator::SetServerComm (LPVOID psComm)
{
	Comm = 	(ServerComm *)psComm;
}

int Simulator::Process_Data(HINSTANCE hInstance, unsigned *nTerminate,LPVOID pParam,LPVOID pMy_CF, LPVOID pUpdateTagFun, LPVOID pUpdateDataFun)
{
	pOneServerItem = (COpcComServerItem  *)pParam;

	my_CF= (CMyOpcServer *)pMy_CF; //points to opcserver
	updateTag =  (pUpDateTag *)pUpdateTagFun ; // points to update opc tags
	updateTag =  (pUpDateTag *)pUpdateTagFun ; // points to update opc tags
	upItemdateData =(pItemUpDateData *) pUpdateDataFun;

	while (*(nTerminate)==0)
	{

		int iCount  =  pOneServerItem->m_groupList.GetCount();
		POSITION pos =   pOneServerItem->m_groupList.GetHeadPosition();
		for(int i=0 ; i < iCount; i++)
		{

			COpcGroupItem *GroupItem = pOneServerItem->m_groupList.GetNext(pos);
			
			Simulate(GroupItem);
		}

		Sleep(100);
	}
	return 0;

}

void Simulator::Simulate(COpcGroupItem *GroupItem)
{
	CSafeLock cs (&m_csDataLock);
	//SafeLock
	VARIANT varData;
	VariantInit(&varData);

	CMapStringToPtr *m_mptagList =  GroupItem->GetMapObject()   ;
	POSITION pos =  m_mptagList->GetStartPosition(); 
	
	CString strKey;

	while (pos)
	{
		COpcItem * Item = NULL;
		
		m_mptagList->GetNextAssoc(pos,strKey,(void *&)Item); 


		if (!Item->GetActive()) 
			continue;

		VariantClear(&varData);
		
		loTagValue * tag = Item->GetLoTag();

			CSafeLock cs (&m_csDataLock);
			VariantCopy (&varData,&tag->tvValue);


				bool bData=FALSE;
				float fData =0;
				LONG lnum=1;
				LONG lData = 0;
				LONG lUpper = 0;  //array[5] res=4
				HRESULT hr = S_OK;
		if ((varData.vt & VT_ARRAY) == VT_ARRAY )
		SafeArrayGetUBound(varData.parray,1,&lUpper);


		if (tag)
		{
			switch (varData.vt)
			{
			case VT_BOOL:
				varData.boolVal =  !varData.boolVal;
				break;
			case VT_ARRAY|VT_BOOL:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				bData=FALSE;
				SafeArrayGetElement(varData.parray,&lnum,&bData);
				bData=!bData;
				hr=SafeArrayPutElement(varData.parray,&lnum,&bData);
			}
				break;
			case VT_I4:
				varData.lVal++;  
				break;
			case VT_ARRAY|VT_I4:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				lData=0;
				SafeArrayGetElement(varData.parray,&lnum,&lData);
				lData+=1;
				hr=SafeArrayPutElement(varData.parray,&lnum,&lData);
			}
				
				break;

			case VT_UI4:
				varData.ulVal ++; 
				break;
			case VT_ARRAY|VT_UI4:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				lData=0;
				SafeArrayGetElement(varData.parray,&lnum,&lData);
				lData+=1;
				hr=SafeArrayPutElement(varData.parray,&lnum,&lData);
			}
				break;

			case VT_R4:
				varData.fltVal += 0.1f; 
				break;
			case VT_ARRAY|VT_R4:
				for(lnum=0;lnum<=lUpper;lnum++)
				{
				fData=0.0f;
				SafeArrayGetElement(varData.parray,&lnum,&fData);
				fData +=0.1f;
				hr=SafeArrayPutElement(varData.parray,&lnum,&fData);
				}
				break;
			case VT_R8:
				varData.dblVal += 0.1; 
				break;
			case VT_ARRAY|VT_R8:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				fData=0.0f;
				SafeArrayGetElement(varData.parray,&lnum,&fData);
				fData +=0.1f;
				hr=SafeArrayPutElement(varData.parray,&lnum,&fData);
			}
				break;

			default:
				continue;
				break;
			}
//				hr=VariantCopy(&tag->tvValue, &varData);
//				pUpDateTag UpateDate = *updateTag ; ///deref pointer to pointer
//				(my_CF->*UpateDate)(tag,OPC_QUALITY_GOOD);
			int opcRes = OPC_QUALITY_GOOD;
			
			hr=VariantCopy(&tag->tvValue, &varData); 
			pUpDateTag UpateDate = *updateTag ; ///deref pointer to pointer
			pItemUpDateData UpDataItem=  *upItemdateData;
			if (tag->tvState.tsQuality != OPC_QUALITY_LOCAL_OVERRIDE) //do not update as client have data to device
			{(my_CF->*UpateDate)(tag,opcRes); ///update to opc
			(Item->*UpDataItem)(varData,(UINT)opcRes,NULL); //update to display


		}
	}
}
}


/////////////////////////////////////////////////////////////////////////////

/// a must for these 2 external
extern "C"
{
	// Plugin factory function
	__declspec(dllexport) IPlugin* Create_Plugin ()
	{
		//allocate a new object and return it
		return new Simulator ();
	}

	// Plugin cleanup function
	__declspec(dllexport) void Release_Plugin (IPlugin* p_plugin)
	{
		//we allocated in the factory with new, delete the passed object
		delete p_plugin;
	}

	__declspec(dllexport) void EnterClientWrite ( long* SLock)
	{
		
		// SLock =&SpinLock;
	}

	__declspec(dllexport) void Show_Setup(CWnd *myCwnd, LPCTSTR addConfile)
	{
	}

}




