//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Modbus.rc
//
#define IDD_MODBUSVIEW                  100
#define IDC_COMBO_PORT                  1000
#define IDC_COMBO_BAUDRATE              1001
#define IDC_COMBO_DATABITS              1002
#define IDC_COMBO_CHECK                 1003
#define IDC_COMBO_STOPBIT               1004
#define IDC_COMBO_FLOWBIT               1005
#define IDC_PROTOCOLCOMBO               1007
#define IDC_TIMEOUTEDIT                 1009
#define IDC_POLLDELAYEDIT               1010
#define IDC_RETRYEDIT                   1011
#define IDC_IPPORTEDIT                  1012
#define IDC_IPEDIT                      1013
#define IDC_USEBIGINDIAN                1014
#define IDC_ENABLERS485                 1015
#define IDC_SWAPFLOAT                   1017
#define IDC_ENABLEIEEEFLOAT             1018

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
