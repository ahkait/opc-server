// NewItemDialog.cpp 
//

#include "stdafx.h"
#include "OpcServer.h"
#include "NewItemDialog.h"


// CNewItemDialog 

IMPLEMENT_DYNAMIC(CNewItemDialog, CDialog)

CNewItemDialog::CNewItemDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CNewItemDialog::IDD, pParent)
	, m_szTagName(_T(""))
	, m_szTagAdr(_T(""))
	, m_dataType(0)
	, m_wrType(0)
	, m_Range(1)
	, m_szTagDesc(_T(""))
	, m_SwapData(0)
	, m_UseBigIndian(0)
{

}

CNewItemDialog::~CNewItemDialog()
{
}

void CNewItemDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_NAMEEDIT, m_szTagName);
	DDV_MaxChars(pDX, m_szTagName, 32);
	DDX_Text(pDX, IDC_ADREDIT, m_szTagAdr);
	DDV_MaxChars(pDX, m_szTagAdr, 32);
	DDX_CBIndex(pDX, IDC_DATATYPECOMBO, m_dataType);
	DDV_MinMaxInt(pDX, m_dataType, 0, 11);
	DDX_CBIndex(pDX, IDC_WRTYPECOMBO, m_wrType);
//	DDV_MinMaxInt(pDX, m_wrType, 0, 3);
	DDX_Text(pDX, IDC_DESCEDIT, m_szTagDesc);
	DDV_MaxChars(pDX, m_szTagDesc, 128);
	DDX_Text(pDX, IDC_ADRRANGEEDIT, m_Range);
	DDV_MinMaxInt(pDX, m_Range, 1, 1024);
	//	DDX_Control(pDX, IDC_SPINRANGE, m_spinBtn);
	DDX_CBIndex(pDX, IDC_SWAPCOMBO, m_SwapData);
	DDV_MinMaxInt(pDX, m_SwapData, 0, 5);

	DDX_Check(pDX, IDC_USEBIGINDIAN, m_UseBigIndian);
	DDX_Control(pDX, IDC_CHECK_GENERATE, m_generate_each);
}

void CNewItemDialog::SetComboIndex()
{
	
}
BOOL CNewItemDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	if (m_Title.TrimLeft() !=_T(""))
	this->SetWindowTextW(m_Title);
	return TRUE;
}

BEGIN_MESSAGE_MAP(CNewItemDialog, CDialog)
	ON_CBN_SELCHANGE(IDC_DATATYPECOMBO, &CNewItemDialog::OnCbnSelchangeDatatypecombo)
END_MESSAGE_MAP()

// CNewItemDialog 



void CNewItemDialog::OnCbnSelchangeDatatypecombo()
{
	// TODO: Add your control notification handler code here
}
