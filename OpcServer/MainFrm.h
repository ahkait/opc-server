
// MainFrm.h : CMainFrame 
//

#pragma once
class COpcServerView;
class CEventView;
class CLeftView;

class CMainFrame : public CFrameWndEx
{
	
protected: //
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// 
protected:
	CSplitterWnd m_wndSplitter;
    CSplitterWnd m_wndEventView;
	NOTIFYICONDATA m_notify;
public:

// 
public:

// 
public:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL LoadFrame(UINT nIDResource, DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, CWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);

// 
public:
	virtual ~CMainFrame();
	COpcServerView* GetItemView();
	CEventView * GetEventView(){ return m_EventView;}
	CLeftView * GetLeftView(){return m_LeftView;}
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 
	CMFCMenuBar       m_wndMenuBar;
	CMFCToolBar       m_wndToolBar;
	CMFCStatusBar     m_wndStatusBar;
	CMFCToolBarImages m_UserImages;
	CEventView * m_EventView;
	CLeftView * m_LeftView;
	CString strClientCount;
	LONG m_clientCount;
// 
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//afx_msg void OnUpdateViewStyles(CCmdUI* pCmdUI);
	//afx_msg void OnViewStyle(UINT nCommandID);
	afx_msg void OnViewCustomize();
	afx_msg LRESULT OnToolbarCreateNew(WPARAM wp, LPARAM lp);
	afx_msg void OnUpdatePane(CCmdUI* pCmdUI);
    afx_msg void OnSize(UINT nType, int cx, int cy); 
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnClose();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnUpdateFileNew(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFileOpen(CCmdUI *pCmdUI);
	void LogEvent(CString szLog, EVENTTYPE type=tEventInformation);
    afx_msg LRESULT OnNotifyMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnAppExit();
	virtual BOOL OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);
};


