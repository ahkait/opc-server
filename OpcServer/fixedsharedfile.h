// **************************************************************************
// fixedsharedfile.h
//
// Description:
//	This is a wrapper for MFCs CSharedFile class that fixes a bug where 
//	the global memory handle is not unlocked on detach.
// **************************************************************************

#ifndef _FIXEDSHAREDFILE_H
#define _FIXEDSHAREDFILE_H

// **************************************************************************
class CFixedSharedFile : public CSharedFile
	{
	public:
		CFixedSharedFile (int nGrowBy = 128) : CSharedFile (GMEM_DDESHARE | GMEM_MOVEABLE, nGrowBy)
			{
			}

		// Enhancement to allow data to be transfered to the clipboard
		BOOL CopyToClipboard (UINT uFmt)
			{
			// Open the clipboard
			if (!::OpenClipboard (NULL))
				{
				TRACE (_T("Shared Memory: Failed to open the clipboard\n"));
				return (false);
				}

			TRACE (_T("Copying %u bytes to the clipboard (uFmt == %u)\n"), GetLength (), uFmt);
			ASSERT (GetLength ());

			// Clear out current contents
			::EmptyClipboard ();

			// Stick the data in
			HANDLE hData = ::SetClipboardData (uFmt, Detach ());
			::CloseClipboard ();

			// Check for success
			if (!hData)
				{
				TRACE (_T("SetClipboardData () failed [OS Error == %u]\n"), GetLastError ());
				ASSERT (FALSE);
				}

			return (hData != NULL);
			}

	protected:

	};

#endif	// _FIXEDSHAREDFILE_H
