#pragma once
#include "unknwn.h"

class CMyOpcServer : public IClassFactory
{
public:
	CMyOpcServer(void);
	virtual ~CMyOpcServer(void);
	/* Do nothing: we're static, he-he */  
	STDMETHODIMP_(ULONG) AddRef(void) { return 1; }
	STDMETHODIMP_(ULONG) Release(void) { return 1; }
	STDMETHODIMP QueryInterface(REFIID iid, LPVOID *ppInterface)
	{
		if (ppInterface == NULL)
			return E_INVALIDARG;
		if (iid == IID_IUnknown || iid == IID_IClassFactory)
		{
			//UL_DEBUG((LOGID, "myClassFactory::QueryInterface() Ok"));
			*ppInterface = this;
			AddRef();
			return S_OK;
		}
		//UL_DEBUG((LOGID, "myClassFactory::QueryInterface() Failed"));
		*ppInterface = NULL;
		return E_NOINTERFACE;
	}

	STDMETHODIMP LockServer(BOOL fLock)
	{
		if (fLock) 
			serverAdd();
		else 
			serverRemove();
		return S_OK;
	}
	volatile LONG lk_count; /* go 0 when unloading initiated */
	//CRITICAL_SECTION lk_count;  /* protect server_count */
public:
	void serverAdd(void);
	void serverRemove(void);
	static void local_text(WCHAR buf[32], unsigned char nn, LCID lcid);
	STDMETHODIMP CreateInstance(LPUNKNOWN pUnkOuter, REFIID riid,
		LPVOID *ppvObject);
	static void ConvertTags(const loCaller *ca,
		unsigned count, const loTagPair taglist[],
		VARIANT *values, WORD *qualities, HRESULT *errs,
		HRESULT *master_err, HRESULT *master_qual,
		const VARIANT src[], const VARTYPE vtypes[], LCID lcid);
	static void activation_monitor(const loCaller *ca, int count, loTagPair *til);
	static int WriteTags(const loCaller *ca,
		unsigned count, loTagPair taglist[],
		VARIANT values[], HRESULT error[], HRESULT *master, LCID lcid);
	static loTrid ReadTags(const loCaller *ca,
		unsigned count, loTagPair taglist[],
		VARIANT *values, WORD *qualities,
		FILETIME *stamps, HRESULT *errs,
		HRESULT *master_err, HRESULT *master_qual,
		const VARTYPE vtypes[], LCID lcid);
	static HRESULT AskItemID(const loCaller *ca, loTagId *ti, 
		void **acpa, const loWchar *itemid, 
		const loWchar *accpath, int vartype, int goal);
	int driver_init(int lflags);
	void driver_destroy(void);
	void simulate(unsigned pause);
	LONG GetClientCount(void);
	DWORD AddTag(void * key, TCHAR * wszTag, DWORD dwWriteable, VARIANT * var);
	BOOL IsInited(void);
	void UpDateTag(loTagValue * tag,int state=OPC_QUALITY_GOOD);
	void UpDateTags(loTagValue tagList[],int count,int state=OPC_QUALITY_GOOD);
	int RegClassFactory(void);
	void ShutDown();
};
