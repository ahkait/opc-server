#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CNewItemDialog

class CNewItemDialog : public CDialog
{
	DECLARE_DYNAMIC(CNewItemDialog)

public:
	CNewItemDialog(CWnd* pParent = NULL);   // 
	virtual ~CNewItemDialog();
	enum { IDD = IDD_ITEMDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CString m_Title;
	CString m_szTagName;
	CString m_szTagAdr;
	int m_Range;
	int m_dataType;
	int m_wrType;

	CString m_szTagDesc;
	void SetComboIndex();
	///CSpinButtonCtrl m_spinBtn;
	int m_SwapData;
	int m_UseBigIndian;
	CButton m_generate_each;
	afx_msg void OnCbnSelchangeDatatypecombo();
};
