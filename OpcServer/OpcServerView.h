
// OpcServerView.h : COpcServerView 
//


#pragma once
class COpcGroupItem;

class COpcServerView : public CListView
{
protected:
	COpcServerView();
	DECLARE_DYNCREATE(COpcServerView)

//
public:
	COpcServerDoc* GetDocument() const;

// 
public:
//	int GetwrType(int wrType)	;
	int GetdataType(int dataType)	;
// 
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // 

// 
public:
	virtual ~COpcServerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 
protected:
	afx_msg void OnStyleChanged(int nStyleType, LPSTYLESTRUCT lpStyleStruct);
	afx_msg void OnFilePrintPreview();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLvnGetdispinfo(NMHDR *pNMHDR, LRESULT *pResult);
protected:
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
private:
	COpcGroupItem * m_pGroup;
	CImageList m_cImageList;
	CObArray  m_obArray;
	BOOL m_bPasteAvailable;
    int GetSelectedItems(CObArray &cItemList);
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual BOOL DestroyWindow();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnUpdateEditDelete(CCmdUI *pCmdUI);
	afx_msg void OnDelete();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnUpdateEditEdit(CCmdUI *pCmdUI);
	afx_msg void OnEditEdit();
	afx_msg void OnUpdateEditCopy(CCmdUI *pCmdUI);
	afx_msg void OnUpdateEditPaste(CCmdUI *pCmdUI);
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnItemclickList(NMHDR* pNMHDR, LRESULT* pResult); 
};

#ifndef _DEBUG  // OpcServerView.cpp 
inline COpcServerDoc* COpcServerView::GetDocument() const
   { return reinterpret_cast<COpcServerDoc*>(m_pDocument); }
#endif

