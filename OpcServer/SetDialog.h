#pragma once
#include "afxwin.h"


// CSetDialog 

class CSetDialog : public CDialog
{
	DECLARE_DYNAMIC(CSetDialog)

public:
	CSetDialog(CWnd* pParent = NULL);   
	virtual ~CSetDialog();

// �Ի�������
	enum { IDD = IDD_SETDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	CString m_strServerIP;
	int m_ServerPort;
	CString m_strUser;
	CString m_strPwd;
	int m_dtuID;
	CString m_StartupFile;
	CButton m_EnableRemoteServer;
};
