// ModbusSetup.cpp : implementation file
//

#include "stdafx.h"
#include "ModbusSetup.h"
#include "afxdialogex.h"
#include "io.h" 

// CModbusSetup dialog
//IMPLEMENT_SERIAL (CModbusSetup, CObject, 1)
IMPLEMENT_DYNAMIC(CModbusSetup, CDialog)

CModbusSetup::CModbusSetup(CWnd* pParent /*=NULL*/)
	: CDialog(CModbusSetup::IDD, pParent)
	,m_FileName(_T(""))
{

}

CModbusSetup::~CModbusSetup()
{
}

BOOL CModbusSetup::OnInitDialog()
{
	///AfxMessageBox(_T("CModbusSetup::OnInitDialog()"));
	CDialog::OnInitDialog();

	this->EnableWindow();
	// TODO:  Add extra initialization here
		m_CommPort.AddString(_T("NONE"));
	for(int i=1;i<64;i++)
	{
		CString strData;
		strData.Format(_T("COM%d"),i);
		m_CommPort.AddString(strData);
	}
	//
//	m_BaudRate.AddString(_T("110"));
//	m_BaudRate.AddString(_T("134"));
//	m_BaudRate.AddString(_T("150"));
//	m_BaudRate.AddString(_T("300"));
//	m_BaudRate.AddString(_T("600"));
	m_BaudRate.AddString(_T("1200"));
	m_BaudRate.AddString(_T("2400"));
	m_BaudRate.AddString(_T("4800"));
	m_BaudRate.AddString(_T("9600"));
	m_BaudRate.AddString(_T("19200"));
	m_BaudRate.AddString(_T("38400"));
	m_BaudRate.AddString(_T("57600"));
	m_BaudRate.AddString(_T("115200"));
	//
	m_ParityBit.AddString(_T("NONE"));
	m_ParityBit.AddString(_T("ODD"));
	m_ParityBit.AddString(_T("EVEN"));
//	m_ParityBit.AddString(_T("SPC"));
//	m_ParityBit.AddString(_T("MRK"));
	//
	m_DataRate.AddString(_T("7"));
	m_DataRate.AddString(_T("8"));

	//
	m_StopBit.AddString(_T("1"));
	m_StopBit.AddString(_T("2"));
	//
	m_FlowCtrl.AddString(_T("NONE"));
	m_FlowCtrl.AddString(_T("CTS/RTS"));
//	m_FlowCtrl.AddString(_T("DTR/DSR"));
//	m_FlowCtrl.AddString(_T("XOn/XOff"));

	m_Protocol.AddString(_T("RTU"));
	m_Protocol.AddString(_T("ASCII"));
	m_Protocol.AddString(_T("TCP"));
	m_Protocol.AddString(_T("RTU OVER TCP"));

	//
	//load from 
	LoadRecords(m_FileName);

	return TRUE;
}


void CModbusSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_PORT, m_CommPort);
	DDX_Control(pDX, IDC_COMBO_BAUDRATE, m_BaudRate);
	DDX_Control(pDX, IDC_COMBO_DATABITS, m_DataRate);
	DDX_Control(pDX, IDC_COMBO_STOPBIT, m_StopBit);
	DDX_Control(pDX, IDC_COMBO_FLOWBIT, m_FlowCtrl);
	DDX_Control(pDX, IDC_PROTOCOLCOMBO, m_Protocol);
	DDX_Control(pDX, IDC_TIMEOUTEDIT, m_TimeOut);
	DDX_Control(pDX, IDC_RETRYEDIT, m_Retry);
	DDX_Control(pDX, IDC_IPEDIT, m_IP);
	DDX_Control(pDX, IDC_IPPORTEDIT, m_IPport);
	DDX_Control(pDX, IDC_ENABLERS485, m_Use485);
	DDX_Control(pDX, IDC_ENABLEIEEEFLOAT, m_UseIEEEFloat);
	DDX_Control(pDX, IDC_COMBO_CHECK, m_ParityBit);
	DDX_Control(pDX, IDC_POLLDELAYEDIT, m_PollDelay);
	DDX_Control(pDX, IDC_SWAPFLOAT, m_SwapFloat);
	DDX_Control(pDX, IDC_USEBIGINDIAN, m_BigIndian);
}


void CModbusSetup::SaveData()
{

	iniFile.SetLongValue(_T("Config"),_T("Baudrate"), m_BaudRate.GetCurSel());
	iniFile.SetLongValue(_T("Config"),_T("Databits"), m_DataRate.GetCurSel());
	iniFile.SetLongValue(_T("Config"),_T("Flowctrl"), m_FlowCtrl.GetCurSel());
	iniFile.SetLongValue(_T("Config"),_T("Paritybit"), m_ParityBit.GetCurSel());
	iniFile.SetLongValue(_T("Config"),_T("Port"), m_CommPort.GetCurSel());
	iniFile.SetLongValue(_T("Config"),_T("Stopbit"),  m_StopBit.GetCurSel());
	iniFile.SetLongValue(_T("Config"),_T("Protocol"),  m_Protocol.GetCurSel());

	CString text;
	m_CommPort.GetWindowText(text);
	iniFile.SetValue(_T("Config"),_T("CommPort"), text);
	m_TimeOut.GetWindowText(text);
	iniFile.SetValue(_T("Config"),_T("TimOut"), text);
	m_Retry.GetWindowText(text);
	iniFile.SetValue(_T("Config"),_T("Retry"), text);
	m_PollDelay.GetWindowText(text);
	iniFile.SetValue(_T("Config"),_T("PollDelay"), text);


//	iniFile.SetBoolValue(_T("Config"),_T("UseRS485"),  m_Use485.GetCheck());



	m_IP.GetWindowText(text);
	iniFile.SetValue(_T("Config"),_T("IP"), text);
	m_IPport.GetWindowText(text);
	iniFile.SetValue(_T("Config"),_T("IPPort"), text);

	iniFile.SetBoolValue(_T("Config"),_T("UseRS485"),  m_Use485.GetCheck());
	iniFile.SetBoolValue(_T("Config"),_T("IEEEFloat"),  m_UseIEEEFloat.GetCheck());

	iniFile.SetBoolValue(_T("Config"),_T("SwapFloat"),  m_SwapFloat.GetCheck());
	iniFile.SetBoolValue(_T("Config"),_T("BigIndian"),  m_BigIndian.GetCheck());


}

void CModbusSetup::LoadData()
{
	int index;
	index=iniFile.GetLongValue(_T("Config"),_T("Baudrate"));m_BaudRate.SetCurSel(index);
	index=iniFile.GetLongValue(_T("Config"),_T("Databits")); m_DataRate.SetCurSel(index);
	index=iniFile.GetLongValue(_T("Config"),_T("Flowctrl"));m_FlowCtrl.SetCurSel(index);
	index=iniFile.GetLongValue(_T("Config"),_T("Paritybit")); m_ParityBit.SetCurSel(index);
	index=iniFile.GetLongValue(_T("Config"),_T("Port"));m_CommPort.SetCurSel(index);
	index=iniFile.GetLongValue(_T("Config"),_T("Stopbit")); m_StopBit.SetCurSel(index);
	index=iniFile.GetLongValue(_T("Config"),_T("Protocol")); m_Protocol.SetCurSel(index);

	CString text;
	//text=iniFile.GetValue(_T("Config"),_T("CommPort"));
	//m_CommPort.SetWindowText(text);
	text=iniFile.GetValue(_T("Config"),_T("TimOut"));
	m_TimeOut.SetWindowText(text);
	text=iniFile.GetValue(_T("Config"),_T("Retry"));
	m_Retry.SetWindowText(text);
	text=iniFile.GetValue(_T("Config"),_T("PollDelay"));
	m_PollDelay.SetWindowText(text);

	text=iniFile.GetValue(_T("Config"),_T("IP"));
	m_IP.SetWindowText(text);

	text=iniFile.GetValue(_T("Config"),_T("IPPort"));
	m_IPport.SetWindowText(text);

	int i=0;
	i=iniFile.GetBoolValue(_T("Config"),_T("UseRS485"));
	m_Use485.SetCheck(i);
	i=iniFile.GetBoolValue(_T("Config"),_T("IEEEFloat"));
	m_UseIEEEFloat.SetCheck(i);

	i=iniFile.GetBoolValue(_T("Config"),_T("SwapFloat"));
	 m_SwapFloat.SetCheck(i);
	i=iniFile.GetBoolValue(_T("Config"),_T("BigIndian"));
	 m_BigIndian.SetCheck(i);
}




void CModbusSetup::Serialize(CArchive& ar)
{
		if (ar.IsStoring())
		{
			int nport = m_CommPort.GetCurSel();
			ar << nport;
			ar << m_BaudRate.GetCurSel();
			ar << m_DataRate.GetCurSel();
			ar << m_StopBit.GetCurSel();
			ar << m_FlowCtrl.GetCurSel();
			ar << m_Protocol.GetCurSel();
			ar << m_ParityBit.GetCurSel();

			CString text;
			m_CommPort.GetWindowText(text);
			ar << 	text;
			m_TimeOut.GetWindowText(text);
			ar << 	text;
			m_Retry.GetWindowText(text);
			ar << 	text;
			m_IP.GetWindowText(text);
			ar << 	text;
			m_IPport.GetWindowText(text);
			ar << 	text;
			m_PollDelay.GetWindowText(text);
			ar << 	text;
			int state = 0;
			state = m_Use485.GetCheck();
				ar << 	state ;
			state = m_UseIEEEFloat.GetCheck();
				ar << 	state ;
			state = m_SwapFloat.GetCheck();
				ar << 	state ;
			state = m_BigIndian.GetCheck();
				ar << 	state ;

		}
		else /// nned to update GetArchive if settings changes
		{
			int index = -1;
			ar >> index; m_CommPort.SetCurSel(index);
			ar >> index; m_BaudRate.SetCurSel(index);
			ar >> index; m_DataRate.SetCurSel(index);
			ar >> index; m_StopBit.SetCurSel(index);
			ar >> index; m_FlowCtrl.SetCurSel(index);
			ar >> index; m_Protocol.SetCurSel(index);
			ar >> index; m_ParityBit.SetCurSel(index);
			CString text;
			ar >>text;
			m_CommPort.SetWindowText(text);
			ar >>text;
			m_TimeOut.SetWindowText(text);
			ar >>text; 	
			m_Retry.SetWindowText(text);
			ar >>text; 	
			m_IP.SetWindowText(text);
			ar >>text;
			m_IPport.SetWindowText(text);
			ar >>text;
			m_PollDelay.SetWindowText(text);
			int state;
			ar >>state;
			m_Use485.SetCheck(state);
			ar >>state;
			m_UseIEEEFloat.SetCheck(state);
			ar >> state ;
			 m_SwapFloat.SetCheck(state);
			ar >> state ;
			m_BigIndian.SetCheck(state);

		}
}

void CModbusSetup::SerializeIni(LPCTSTR inifname, bool Storing, LPCTSTR key )
{

	if (Storing)
		{
			SaveData();
		}
		else 
		{
			LoadData();
		}
}

//  Code to serialize and save the data
bool CModbusSetup::SaveRecords(LPCTSTR filename)                  
{
		bool result = FALSE;
/*    UINT nFlags = CFile::typeBinary | CFile::modeWrite;
	BOOL m_bCanSave = FALSE;
	CFileStatus stat;
	bool result = FALSE;
    if ( !CFile::GetStatus(filename, stat) )
    {
        nFlags |= CFile::modeCreate;            
        // The file doesn't exist, so create it
        
        m_bCanSave = TRUE;
    }
    else
    {
 
        // Check Read Write Permissions
		if( (stat.m_attribute & 1) ==0)
            m_bCanSave = TRUE;
    }

    if (m_bCanSave)
    {
        CFile file;
        CFileException fe;

        // The file exists with read & write permissions
        if (file.Open(filename, nFlags, &fe))
        {
            CArchive ar(&file, CArchive::store);
            UpdateData(TRUE);
            this->Serialize(ar); // Serialize the data
			result = TRUE;
        }
    }   
	return result;
*/

			this->SerializeIni(filename,true,NULL); // Serialize the data
			UpdateData(TRUE);		
			iniFile.SaveFile(filename);
			result = true;
//        }
//    }   
	return result;
}

bool CModbusSetup::LoadRecords(LPCTSTR filename)
{
			bool result = FALSE;
/*	CFileStatus stat;
	bool result = FALSE;
    if (CFile::GetStatus(filename, stat)) // If File Exists
    {
        CFile file;
        CFileException fe; 

        if (file.Open(filename,CFile::typeBinary | 
            CFile::modeRead, &fe))
        {
            CArchive ar(&file, CArchive::load);
            this->Serialize(ar);
        }

    }
    result = UpdateData(FALSE);
	return result;
*/

		 iniFile.LoadFile(filename);
		this->SerializeIni(filename,false,NULL); // Serialize the data

 //       }

 //   }
    result = UpdateData(FALSE);
	return result;
}

bool CModbusSetup::GetArchive(LPCTSTR filename, modbusSettings *msettings)
{

	iniFile.Reset();
    iniFile.LoadFile(filename);

	int index;
	index=iniFile.GetLongValue(_T("Config"),_T("Baudrate"));msettings->BaudRate= GetIntBaud(index);
	index=iniFile.GetLongValue(_T("Config"),_T("Databits"));msettings->DataRate= index +7;
	index=iniFile.GetLongValue(_T("Config"),_T("Flowctrl"));msettings->FlowCtrl= index;
	index=iniFile.GetLongValue(_T("Config"),_T("Paritybit"));msettings->ParityBit=index;
	index=iniFile.GetLongValue(_T("Config"),_T("Port")); msettings->CommPort=index;
	index=iniFile.GetLongValue(_T("Config"),_T("Stopbit"));msettings->StopBit=index +1;
	index=iniFile.GetLongValue(_T("Config"),_T("Protocol")), msettings->Protocol= index;

	msettings->Protocol= index;

	CString text;
	int i=0;
	text=iniFile.GetValue(_T("Config"),_T("CommPort"));
	 msettings->CommStr = _T("\\\\.\\")+text; //modbus needs
	text=iniFile.GetValue(_T("Config"),_T("TimOut"));
	_stscanf_s(text, _T("%d"), &i); msettings->TimeOut =i  ;
	text=iniFile.GetValue(_T("Config"),_T("Retry"));
	_stscanf_s(text, _T("%d"), &i); msettings->Retry =i  ;
	text=iniFile.GetValue(_T("Config"),_T("PollDelay"));
	_stscanf_s(text, _T("%d"), &i); msettings->PollDelay=i ;

	msettings->IP= iniFile.GetValue(_T("Config"),_T("IP"));
	msettings->IPport = iniFile.GetLongValue(_T("Config"),_T("IPPort"));

	 msettings->Use485=iniFile.GetBoolValue(_T("Config"),_T("UseRS485"));
	msettings->UseIEEEFloat=iniFile.GetBoolValue(_T("Config"),_T("IEEEFloat"));


	msettings->UseSwapFloat=iniFile.GetBoolValue(_T("Config"),_T("SwapFloat"));
	
	 msettings->UseBigIndian=iniFile.GetBoolValue(_T("Config"),_T("BigIndian"));

 
		return TRUE;
}



BEGIN_MESSAGE_MAP(CModbusSetup, CDialog)
	ON_BN_CLICKED(IDOK, &CModbusSetup::OnBnClickedOk)
END_MESSAGE_MAP()


// CModbusSetup message handlers

int CModbusSetup::GetIntBaud(int rate)
{
	int BaudRate= 9600;
	switch (rate)
	{	case 0 : BaudRate = 1200 ;break;
		case 1 : BaudRate = 2400 ;break;
		case 2 : BaudRate = 4800 ;break;
		case 3 : BaudRate = 9600 ;break;
		case 4 : BaudRate = 19200 ;break;
		case 5 : BaudRate = 38400 ;break;
		case 6 : BaudRate = 57600 ;break;
		case 7 : BaudRate = 115200 ;break;
		default : BaudRate = 9600 ;break;

	}
	return BaudRate;
}


void CModbusSetup::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	///modbusSettings settings;
	SaveRecords(m_FileName);
	///GetArchive(m_FileName, &settings);
	//AfxMessageBox(settings.IP+_T("   ")+ settings.CommStr);

	CDialog::OnOK();
}
