//#pragma once

#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"
#include <afxtempl.h>
#include "..\CiniFile\SimpleIni.h"
// CModbusSetup dialog
//	IMPLEMENT_SERIAL (CModbusSetup, CObject, 1)
//	DECLARE_SERIAL (CModbusSetup)


typedef struct modbusSettings
{
 	int CommPort;
	int BaudRate;
	int DataRate;
	int StopBit;
	int FlowCtrl;
	int Protocol;
	int ParityBit;
	////
	int TimeOut;
	int  Retry;
	int PollDelay;
	CString IP;
	int IPport;
	int Use485;
	int UseIEEEFloat;
	int UseSwapFloat;
	int UseBigIndian;
	CString CommStr;
} modbusSettings;
	


class CModbusSetup : public CDialog
{
	
	DECLARE_DYNAMIC(CModbusSetup)

public:
	CModbusSetup(CWnd* pParent = NULL);   // standard constructor
//	CModbusSetup(){} ;// for  Serialize
	virtual ~CModbusSetup();
	void Serialize(CArchive& ar);

// Dialog Data
	enum { IDD = IDD_MODBUSVIEW };
	bool SaveRecords(LPCTSTR filename)   ;
	bool LoadRecords(LPCTSTR filename)   ;
	bool GetArchive(LPCTSTR filename, modbusSettings *msettings);
	void SerializeIni(LPCTSTR inifname, bool Storing, LPCTSTR key );
	void LoadData();
	void SaveData();

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()
   
public:
	CComboBox m_CommPort;
	CComboBox m_BaudRate;
	CComboBox m_DataRate;
	CComboBox m_StopBit;
	CComboBox m_FlowCtrl;
	CComboBox m_Protocol;
	CEdit m_TimeOut;
	CEdit m_Retry;
	CIPAddressCtrl m_IP;
	CEdit m_IPport;
	CButton m_Use485;
	CButton m_UseIEEEFloat;
	CString m_FileName;
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	CComboBox m_ParityBit;
		CSimpleIni iniFile; 
//protected:
//		DECLARE_SERIAL(CModbusSetup)
	CEdit m_PollDelay;
	CButton m_SwapFloat;
	CButton m_BigIndian;
private:
int GetIntBaud(int rate);
};

