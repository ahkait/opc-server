// EventView.cpp 
//

#include "stdafx.h"
#include "OpcServer.h"
#include "EventView.h"
#include "OpcServerDoc.h"

// CEventView

IMPLEMENT_DYNCREATE(CEventView, CListView)

CEventView::CEventView()
: m_init(false)
{


}

CEventView::~CEventView()
{
}

BEGIN_MESSAGE_MAP(CEventView, CListView)
END_MESSAGE_MAP()


// CEventView

#ifdef _DEBUG
void CEventView::AssertValid() const
{
	CListView::AssertValid();
}

#ifndef _WIN32_WCE
void CEventView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}
#endif
#endif //_DEBUG


// CEventView 

BOOL CEventView::PreCreateWindow(CREATESTRUCT& cs)
{
	
	cs.style |= LVS_REPORT;
	return CListView::PreCreateWindow(cs);
}

void CEventView::OnInitialUpdate()
{
	CListView::OnInitialUpdate();
	if (!m_init)
	{
		CListCtrl & listctrl = GetListCtrl();
		listctrl.SetExtendedStyle(listctrl.GetExtendedStyle() | LVS_EX_FULLROWSELECT); 
		TCHAR szcol[5][20] = {_T("ʱ��"),_T("��Ϣ")};
		LVCOLUMN lv;
		for (int i=0;i<2;i++)
		{
			lv.fmt = LVCFMT_LEFT;
			lv.iSubItem = i;
			if (i==1)
				lv.cx = 400;
			else
				lv.cx = 100;
			lv.mask = LVCF_FMT | LVCF_WIDTH |LVCF_TEXT| LVCF_SUBITEM; 
			lv.pszText = szcol[i];
			listctrl.InsertColumn(i,&lv);
		}
		m_init = true;
	}
/*
	LVITEM lvitem;
	lvitem.mask = LVIF_TEXT ;//| LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
    lvitem.iItem = 0;
	lvitem.iSubItem = 0;
	lvitem.pszText = _T("tag");

	listctrl.InsertItem(&lvitem);
    
    lvitem.iSubItem = 1;
	lvitem.pszText = _T("int");
	listctrl.SetItem(&lvitem);
*/	


}

void CEventView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	
	CDocument * pDoc =	GetDocument();
	CListCtrl & listctrl = GetListCtrl();
	switch (lHint)
	{
	case FILENEW:
		listctrl.DeleteAllItems();
		break;
	}

}

void CEventView::LogEvent(CString szLog, int type)
{
	CListCtrl & listctrl = GetListCtrl();
	CTime d = CTime::GetCurrentTime();
	CString str = d.Format(_T("%H:%M:%S")); 
	
	LVITEM lvitem;
	memset(&lvitem,0,sizeof(LVITEM));
	lvitem.mask = LVIF_TEXT ;//| LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
    lvitem.iItem = 0;
	lvitem.iSubItem = 0;
	lvitem.pszText = str.AllocSysString(); 

	int nItem = listctrl.InsertItem(&lvitem);

	memset(&lvitem,0,sizeof(LVITEM));
	lvitem.iItem = nItem;
	lvitem.mask = LVIF_TEXT ;
    lvitem.iSubItem = 1;
	lvitem.pszText = szLog.AllocSysString();
	listctrl.SetItem(&lvitem);

}
