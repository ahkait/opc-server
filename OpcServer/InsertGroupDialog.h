#pragma once


// CInsertGroupDialog

class CInsertGroupDialog : public CDialog
{
	DECLARE_DYNAMIC(CInsertGroupDialog)

public:
	CInsertGroupDialog(CWnd* pParent = NULL);   //
	virtual ~CInsertGroupDialog();

// 
	enum { IDD = IDD_INSERTGROUPDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CString m_szGroup;
	CString m_igroupAddress;
	int m_igroupPort;
	// variable for ip address
	DWORD m_IPAddress;
	CString m_Title;
};
