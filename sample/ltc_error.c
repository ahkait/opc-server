#include <windows.h>
#include <stdio.h>

static struct myEHStringTable {DWORD code; char *string; } myEHSTable[] = {
//  ���� ��������� ��� ���� ������.
	{ 1001,   "Ldrv Device Driver" },
	{ 1002,   "PutData"   },
	{ 1003,   "GetData"   },
	{ 1004,   "Ldrv In Function InByte"   },
	{ 1005,   "Ldrv In Function InWord"   },
	{ 1006,   "Ldrv In Function OutByte"  },
	{ 1007,   "Ldrv In Function OutWord"  },
	{ 1008,   "One Is Not In Time Is Too Late."    },
	{ 1009,   "KERNEL32.DLL" },
	{ 1010,   "PutWordIDE"   },
	{ 1011,   "GetWordIDE"   },
	{ 1012,   "SendCommandIDE" },
	{ 1013,   "RunCraitCommand" },
	{ 1014,   "DoneCraitCommand" },
	{ 1015,   "WINDRVR Device Driver" },
	{ 1016,   "Can't Get WinDriver Version" },
	{ 1017,   "WinDriver" },
	{ 1018,   "Can't Free DMA Buffer" },
	{ 1019,   "WinDriver In Function InByte"   },
	{ 1020,   "WinDriver In Function InWord"   },
	{ 1021,   "WinDriver In Function OutByte"  },
	{ 1022,   "WinDriver In Function OutWord"  },
	{ 1023,   "COMMAND_1221" },
	{ 0, "12345678901234567890empty" }
};

char *myEHS(DWORD code)
{
	struct myEHStringTable *p=myEHSTable;

	while(p->code && p->code != code) p++;
	if(p->code == 0) sprintf(p->string,"%ld",code);
	return p->string;
}

#if 0
//  ���� ��������� � ����� ������ � ����� ���������� DWORD.
    0x1001, "You Are Call Unsupported Function %s."
    0x1002, "Unknown Error: %lu, Info %lu."

//  ���� ��������� � ����� ������ ����� ����������� DWORD, ...
    0x2001, "Unable To Open The %s, Info %lu."    
    0x2002, "Time Out In Function %s"
    0x2003, "DeviceIOControl Failed, %s, Info %lu."
    0x2004, "Unable To Get Procedure Address: %s, Info: %lu"
    0x2005, "Failed To Close The %s, Info %lu."  
    0x2006, "Unable To Free %s, Info %lu."
    0x2007, "Incorrect %s Version: %lu.%lu, Needs Version At Least: %u.%u."
            

    ordSELECT_SLOT                      "SELECT_SLOT"
    ordPLATA_TEST                       "PLATA_TEST"
    ordSET_BOARD_TYPE                   "SET_BOARD_TYPE"
    ordGET_BOARD_TYPE                   "GET_BOARD_TYPE"
    ordSET_BASE_ADDRESS                 "SET_BASE_ADDRESS"
    ordENABLE_INT                       "ENABLE_INT"
    ordCREATE_CHANNEL                   "CREATE_CHANNEL"
    ordMEMORY_STATE                     "MEMORY_STATE"
    ordMEMORY_PM_STATE                  "MEMORY_PM_STATE"
    ordSET_INTER_DELAY                  "SET_INTER_DELAY"
    ordSET_ADSP_SPEED                   "SET_ADSP_SPEED"
    ordSET_WAIT_STATE                   "SET_WAIT_STATE"
    ordSET_TIMER_SCALE                  "SET_TIMER_SCALE"
    ordTIMER_L154_L164                  "TIMER_L154_L164"
    ordWAIT_TICK_L154_L164              "WAIT_TICK_L154_L164"
    ordGET_TIMER_L154_L164              "GET_TIMER_L154_L164"
    ordCALIBRATION                      "CALIBRATION"
    ordINIT_COMPARATOR                  "INIT_COMPARATOR"
    ordLOW_POWER                        "LOW_POWER"
    ordGET_SLOT_CODE                    "GET_SLOT_CODE"
    ordRESET_ALL                        "RESET_ALL"
    ordMAKE_CHANNEL                     "MAKE_CHANNEL"
    ordPROGRAM_FILTER                   "PROGRAM_FILTER"
    ordPROGRAM_LM_201                   "PROGRAM_LM_201"
    ordPROGRAM_LM_102                   "PROGRAM_LM_102"
    ordPROGRAM_LM_501                   "PROGRAM_LM_501"
    ordPROGRAM_LM_DAC                   "PROGRAM_LM_DAC"
    ordGET_LM_401                       "GET_LM_401"
    ordLM_404                           "LM_404"
    ordPROGRAM_LM_TTLOUT                "PROGRAM_LM_TTLOUT"
    ordSETCHANNEL                       "SETCHANNEL"
    ordSAMPLE                           "SAMPLE"
    ordADCHAN                           "ADCHAN"
    ordKADR                             "KADR"
    ordSTREAM                           "STREAM"
    ordSOFT                             "SOFT"
    ordSOFT_HUGE                        "SOFT_HUGE"
    ordDMAONE                           "DMAONE"
    ordDMAALL                           "DMAALL"
    ordDMA_ALL_DA                       "DMA_ALL_DA"
    ordOUTDA                            "OUTDA"
    ordSET_DA_NUMBER                    "SET_DA_NUMBER"
    ordSTREAM_OUTDA                     "STREAM_OUTDA"
    ordDASTREAM                         "DASTREAM"
    ordDASTREAM_HUGE                    "DASTREAM_HUGE"
    ordDADMASTREAM                      "DADMASTREAM"
    ordOUTBYTE                          "OUTBYTE"
    ordINPBYTE                          "INPBYTE"
    ordDSP_INPBYTE                      "DSP_INPBYTE"
    ordDSP_OUTBYTE                      "DSP_OUTBYTE"
    ordINTR_SETUP                       "INTR_SETUP"
    ordSTOP_INTR                        "STOP_INTR"
    ordRESET_IRQ                        "RESET_IRQ"
    ordINIT_SIMPLE_INTR                 "INIT_SIMPLE_INTR"
    ordSTREAM_INTR                      "STREAM_INTR"
    ordSOFT_INTR                        "SOFT_INTR"
    ordREAD_DATA                        "READ_DATA"
    ordDATA_READY                       "DATA_READY"
    ordSYNCHRO_MODE                     "SYNCHRO_MODE"
    ordSOFT_MEMORY_START                "SOFT_MEMORY_START"
    ordTEST_FOR_INPUT_END               "TEST_FOR_INPUT_END"
    ordGET_INPUT_DATA                   "GET_INPUT_DATA"
    ordSOFT_MEMORY_RESTART              "SOFT_MEMORY_RESTART"
    ordDMA_OFF                          "DMA_OFF"
    ordDMA_TEST                         "DMA_TEST"
    ordDMA_COUNTER_IN                   "DMA_COUNTER_IN"
    ordDMA_COUNTER_OUT                  "DMA_COUNTER_OUT"
    ordGET_BUFFER                       "GET_BUFFER"
    ordGET_BUFFER_HALF                  "GET_BUFFER_HALF"
    ordPUT_BUFFER                       "PUT_BUFFER"
    ordPUT_BUFFER_HALF                  "PUT_BUFFER_HALF"
    ordCONFIG_FIFO                      "CONFIG_FIFO"
    ordCONFIG_2FIFO_TYPE                "CONFIG_2FIFO_TYPE"
    ordREAD_FIFO                        "READ_FIFO"
    ordSTREAM_FIFO2_BEGIN               "STREAM_FIFO2_BEGIN"
    ordGET_DIGITS_1056                  "GET_DIGITS_1056"
    ordOUT_DIGITS_1056                  "OUT_DIGITS_1056"
    ordSTREAM_1056                      "STREAM_1056"
    ordSTREAM_DMA_1056                  "STREAM_DMA_1056"
    ordOUT_STREAM_1056                  "OUT_STREAM_1056"
    ordOUT_STREAM_DMA_1056              "OUT_STREAM_DMA_1056"
    ordINIT_INTR_1056                   "INIT_INTR_1056"
    ordSET_SYNCHRO_MODE_1056            "SET_SYNCHRO_MODE_1056"
    ordZERO_MODE_1208                   "ZERO_MODE_1208"
    ordOUTDA_1208                       "OUTDA_1208"
    ordKADR_1208                        "KADR_1208"
    ordSOFT_1208                        "SOFT_1208"
    ordSOFT_FON_1208                    "SOFT_FON_1208"
    ordDMASOFT_1208                     "DMASOFT_1208"
    ordSOFT_INTR_1208                   "SOFT_INTR_1208"
    ordGET_DATA                         "GET_DATA"
    ordPUT_DATA                         "PUT_DATA"
    ordREAD_INT_MEMORY                  "READ_INT_MEMORY"
    ordWRITE_INT_MEMORY                 "WRITE_INT_MEMORY"
    ordLOADBIOS                         "LOADBIOS"
    ordPOWER_AND_PRINTER_E330           "POWER_AND_PRINTER_E330"
    ordSET_EPP_MODE_E330                "SET_EPP_MODE_E330"
    ordSET_FIFO_PARAMETERS_E330         "SET_FIFO_PARAMETERS_E330"
    ordSET_SOFT_PARM_E330               "SET_SOFT_PARM_E330"
    ordSET_TIME_PARAMETERS_E330         "SET_TIME_PARAMETERS_E330"
    ordTIMER_INPUT_START_E330           "TIMER_INPUT_START_E330"
    ordSTOP_FUNC_E330                   "STOP_FUNC_E330"
    ordTIMER_INPUT_E330                 "TIMER_INPUT_E330"
    ordSOFT_MEMORY_PREPARE_E330         "SOFT_MEMORY_PREPARE_E330"
    ordSOFT_MEMORY_START_E330           "SOFT_MEMORY_START_E330"
    ordTIMER_INPUT_IRQ_START_E330       "TIMER_INPUT_IRQ_START_E330"
    ordIRQ_SLOW_START_E330              "IRQ_SLOW_START_E330"
    ordSET_TEST_SPEED_MODE_E330         "SET_TEST_SPEED_MODE_E330"
    ordWRITE_BY_ADDRESS_E330            "WRITE_BY_ADDRESS_E330"
    ordREAD_BY_ADDRESS_E330             "READ_BY_ADDRESS_E330"
    ordBOARD_SPECIFIC_INIT              "BOARD_SPECIFIC_INIT"
    ordSELECT_BOARD                     "SELECT_BOARD"
    ordSETBASEADDRESS                   "SETBASEADDRESS"
    ordEXEC_ADSP_COMMAND                "EXEC_ADSP_COMMAND"
    ordGET_STATUS                       "GET_STATUS"
    ordALLOCATEBUFFER                   "ALLOCATEBUFFER"
    ordCONFIG_TX_CHANNEL_LCI01          "CONFIG_TX_CHANNEL_LCI01"
    ordFREEBUFFER                       "FREEBUFFER"
    ordGET_HALF_FIFO                    "GET_HALF_FIFO"
    ordGET_INPUT_POINTS                 "GET_INPUT_POINTS"
    ordINSTALL_ERROR_HANDLER            "INSTALL_ERROR_HANDLER"
    ordLOADBIOSFROMARRAY_L1211          "LOADBIOSFROMARRAY_L1211"
    ordLOAD_BIOS_LC010                  "LOAD_BIOS_LC010"
    ordLOAD_BIOS_LC014                  "LOAD_BIOS_LC014"
    ordREAD_ARRAY_FROM_MEMORY           "READ_ARRAY_FROM_MEMORY"
    ordREAD_CALIBR_CC                   "READ_CALIBR_CC"
    ordCONFIG_RX_CHANNEL_LCI01          "CONFIG_RX_CHANNEL_LCI01"
    ordSELECT_DEVICE                    "SELECT_DEVICE"
    ordSET_AD_MODE                      "SET_AD_MODE"
    ordSET_BASE_ADDRESS_LC014           "SET_BASE_ADDRESS_LC014"
    ordSET_CHANNELS                     "SET_CHANNELS"
    ordSET_CRAIT_ADDRESS_LCI01          "SET_CRAIT_ADDRESS_LCI01"
    ordSET_DAC_BUFER_E330               "SET_DAC_BUFER_E330"
    ordSET_TIME                         "SET_TIME"
    ordSET_TIMER_SCALE_LCI01            "SET_TIMER_SCALE_LCI01"
    ordSTART_LOOP_DAC_E330              "START_LOOP_DAC_E330"
    ordSTOP_INPUT                       "STOP_INPUT"
    ordTIMER_INPUT                      "TIMER_INPUT"
    ordTIMER_INPUT_INTR                 "TIMER_INPUT_INTR"
    ordWRITE_ARRAY_TO_MEMORY            "WRITE_ARRAY_TO_MEMORY"
    ordGET_TABLE                        "GET_TABLE"
    ordCONFIG_LC227                     "CONFIG_LC227"
    ordGET_SAMPLE_LC227                 "GET_SAMPLE_LC227"
    ordSTATUS_LC227                     "STATUS_LC227"
    ordGET_KADR_LC227                   "GET_KADR_LC227"
    ordLOAD_BIOS_LC227                  "LOAD_BIOS_LC227"
    ordGET_ERROR_HANDLER                "GET_ERROR_HANDLER"
    ordGET_TIMEOUT                      "GET_TIMEOUT"
    ordPLATA_TEST_CC                    "PLATA_TEST_CC"
    ordMEMORY_STATE_CC                  "MEMORY_STATE_CC"
    ordMEMORY_PM_STATE_CC               "MEMORY_PM_STATE_CC"
    ordEXECUTE_COMMAND_CC               "EXECUTE_COMMAND_CC"
    ordSET_CALIBR_MODE_CC               "SET_CALIBR_MODE_CC"
    ordSET_ERROR_HANDLER                "SET_ERROR_HANDLER"
    ordOUTDA_LC301                      "OUTDA_LC301"
    ordSAMPLE_LC301                     "SAMPLE_LC301"
    ordSAMPLE_LC111                     "SAMPLE_LC111"
    ordSET_FIFO_SIZE_CC                 "SET_FIFO_SIZE_CC"
    ordCONFIG_FIFO_CC                   "CONFIG_FIFO_CC"
    ordSOFT_CONFIG_CC                   "SOFT_CONFIG_CC"
    ordSOFT_START_CC                    "SOFT_START_CC"
    ordSOFT_STOP_CC                     "SOFT_STOP_CC"
    ordGET_NWORDS_CC                    "GET_NWORDS_CC"
    ordGET_SOFT_DATA_CC                 "GET_SOFT_DATA_CC"
    ordSOFT_CC                          "SOFT_CC"
    ordFAST_SOFT_IRQ_START_CC           "FAST_SOFT_IRQ_START_CC"
    ordGET_FAST_SOFT_DATA               "GET_FAST_SOFT_DATA"
    ordFAST_SOFT_START_CC               "FAST_SOFT_START_CC"
    ordGET_FAST_SOFT_IRQ_DATA           "GET_FAST_SOFT_IRQ_DATA"
    ordFAST_SOFT_IRQ_STOP_CC            "FAST_SOFT_IRQ_STOP_CC"
    ordWAIT_SOFT_DATA_CC                "WAIT_SOFT_DATA_CC"
    ordGET_CTRL_LC301                   "GET_CTRL_LC301"
    ordSET_CTRL_LC301                   "SET_CTRL_LC301"
    ordREAD_REG_7710                    "READ_REG_7710"
    ordWRITE_REG_7710                   "WRITE_REG_7710"
    ordLOAD_BIOS_LC351                  "LOAD_BIOS_LC351"
    ordMEMORY_STATE_LC351               "MEMORY_STATE_LC351"
    ordSET_CTRL_LC351                   "SET_CTRL_LC351"
    ordOUTDA_LC351                      "OUTDA_LC351"
    ordLOAD_DATA_LC351                  "LOAD_DATA_LC351"
    ordSTART_OUT_LC351                  "START_OUT_LC351"
    ordLOAD_BIOS_LC451                  "LOAD_BIOS_LC451"
    ordINP_DIGITS_401                   "INP_DIGITS_401"
    ordENABLE_IRQ_401                   "ENABLE_IRQ_401"
    ordOUT_DIGITS_402                   "OUT_DIGITS_402"
    ordENABLE_IRQ_402                   "ENABLE_IRQ_402"
    ordOUT_DIGITS_403                   "OUT_DIGITS_403"
    ordINP_DIGITS_403                   "INP_DIGITS_403"
    ordENABLE_IRQ_403                   "ENABLE_IRQ_403"
    ordENABLE_OUT_403                   "ENABLE_OUT_403"
    ordREAD_IO_403                      "READ_IO_403"
    ordRESET_ANSW_403                   "RESET_ANSW_403"
    ordPROGRAM_FILTER_LC201             "PROGRAM_FILTER_LC201"
    ordPROGRAM_USIL_LC201               "PROGRAM_USIL_LC201"
    ordPROGRAM_FILTER_LC210             "PROGRAM_FILTER_LC210"
    ordPROGRAM_USIL_LC210               "PROGRAM_USIL_LC210"
    ordREAD_FLASH_CC                    "READ_FLASH_CC"
    ordWRITE_FLASH_CC                   "WRITE_FLASH_CC"
    ordWRITE_EN_DS_CC                   "WRITE_EN_DS_CC"
    ordPUT_REMOTE_WORD_CC               "PUT_REMOTE_WORD_CC"
    ordGET_REMOTE_WORD_CC               "GET_REMOTE_WORD_CC"
    ordPUT_REMOTE_ARRAY_CC              "PUT_REMOTE_ARRAY_CC"
    ordGET_REMOTE_ARRAY_CC              "GET_REMOTE_ARRAY_CC"
    ordSET_IRQ_MASK_CC                  "SET_IRQ_MASK_CC"
    ordGET_MODULE_IRQ                   "GET_MODULE_IRQ"
    ordSET_TIMEOUT                      "SET_TIMEOUT"
    ordLOW_POWER_CC                     "LOW_POWER_CC"
    ordOUTDA_LC302                      "OUTDA_LC302"
    ordKADR_LC302                       "KADR_LC302"
    ordSET_CTRL_LC302                   "SET_CTRL_LC302"
    ordPROGRAM_AD8402                   "PROGRAM_AD8402"
    ordPROGRAM_MAX                      "PROGRAM_MAX"
    ordOUTDA_AD8842                     "OUTDA_AD8842"
    ordRELEASEDMACHANNEL                "RELEASEDMACHANNEL"
    ordFORCE_INTER_DELAY_CC             "FORCE_INTER_DELAY_CC"
    ordCONNECT_LC501                    "CONNECT_LC501"
    ordADCHAN_L241                      "ADCHAN_L241"
    ordSELFCALIBR_L241                  "SELFCALIBR_L241"
    ordSET_INTERFACE_TYPE               "SET_INTERFACE_TYPE"
    ordSET_DEVICE_BINDING               "SET_DEVICE_BINDING"
    ordSET_CRAIT_ADDRESS                "SET_CRAIT_ADDRESS"
    ordGET_INI_SETTINGS                 "GET_INI_SETTINGS"
    ordCONFIG_LC451                     "CONFIG_LC451"
    ordSTATUS_LC451                     "STATUS_LC451"
    ordGET_SAMPLE_LC451                 "GET_SAMPLE_LC451"
    ordGET_KADR_LC451                   "GET_KADR_LC451"
    ordSET_THRESHOLD_LC451              "SET_THRESHOLD_LC451"
    ordSET_IRQ_INFOM_CC                 "SET_IRQ_INFOM_CC"
    ordSET_LC227DATA_IRQSET_CC          "SET_LC227DATA_IRQSET_CC"
    ordGET_IRQ1DATA_CC                  "GET_IRQ1DATA_CC"
    ordGET_LC227CHMASK                  "GET_LC227CHMASK"
    ordREQUESTDMACHANNEL                "REQUESTDMACHANNEL"
    ordCALIBRLC201_STEP1_BEG            "CALIBRLC201_STEP1_BEG"
    ordCALIBRLC201_STEP1_CYCLE          "CALIBRLC201_STEP1_CYCLE"
    ordCALIBRLC201_STEP1_FIN            "CALIBRLC201_STEP1_FIN"
    ordCALIBRLC201_STEP2_BEG            "CALIBRLC201_STEP2_BEG"
    ordCALIBRLC201_STEP2_CYCLE_CHANV0   "CALIBRLC201_STEP2_CYCLE_CHANV0"
    ordCALIBRLC201_STEP2_CYCLE_CHANV5   "CALIBRLC201_STEP2_CYCLE_CHANV5"
    ordCALIBRLC201_STEP2_CYCLE_CHANZG   "CALIBRLC201_STEP2_CYCLE_CHANZG"
    ordCALIBRLC201_STEP2_CYCLE_GAIN     "CALIBRLC201_STEP2_CYCLE_GAIN"
    ordCALIBRLC201_STEP2_FIN            "CALIBRLC201_STEP2_FIN"
    ordCALIBR_LC201_STEP1               "CALIBR_LC201_STEP1"
    ordCALIBR_LC201_STEP2               "CALIBR_LC201_STEP2"
    ordGET_DFLTCALIBRSET_LC201          "GET_DFLTCALIBRSET_LC201"
    ordSETU_LC301                       "SETU_LC301"
    ordSET_DFLTCALIBRSET_LC201          "SET_DFLTCALIBRSET_LC201"
    ordSET_TRIM_DAC_LC201_FILE          "SET_TRIM_DAC_LC201_FILE"
    ordREAD_REG_7710_CRAIT              "READ_REG_7710_CRAIT"
    ordWRITE_REG_7710_CRAIT             "WRITE_REG_7710_CRAIT"
    ordCHECK_OVERFLOW_7710_CRAIT        "CHECK_OVERFLOW_7710_CRAIT"
    ordENABLE_7710_CRAIT                "ENABLE_7710_CRAIT"
    ordFINISH_READ_7710_CRAIT           "FINISH_READ_7710_CRAIT"
    ordFINISH_READ_CODE_7710_CRAIT      "FINISH_READ_CODE_7710_CRAIT"
    ordIS_7710_PRESENT_CRAIT            "IS_7710_PRESENT_CRAIT"
    ordIS_7710_READY_CRAIT              "IS_7710_READY_CRAIT"
    ordREAD_7710_CRAIT                  "READ_7710_CRAIT"
    ordREAD_CODE_7710_CRAIT             "READ_CODE_7710_CRAIT"
    ordRESET_FILTER_7710_CRAIT          "RESET_FILTER_7710_CRAIT"
    ordSELF_CALIBR_7710_CRAIT           "SELF_CALIBR_7710_CRAIT"
    ordSET_CHANNEL_7710_CRAIT           "SET_CHANNEL_7710_CRAIT"
    ordCREATE_CHANNEL_LC201             "CREATE_CHANNEL_LC201"

#endif