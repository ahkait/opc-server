#include "stdafx.h"
#include "OpcServer.h"
#include "OpcGroupItem.h"
#include "OpcItem.h"
#include "OpcServerDoc.h"
#include "CiniFile\SimpleIni.h"

extern CSimpleIni iniFile; 
extern CMyOpcServer my_CF;

COpcGroupItem::COpcGroupItem(COpcComServerItem * pServerItem)
{
	m_groupName = _T("");
	m_groupIP = 0 ;
	m_igroupAddress=0;
	m_igroupPort=0;

	m_pServerItem = pServerItem;
	hTreeItem = NULL;
	m_itagCount = 0;
}

COpcGroupItem::COpcGroupItem(COpcComServerItem * pServerItem,LPCTSTR groupName,DWORD groupIP,int igroupAddress, int igroupPort)
{
	m_pServerItem = pServerItem;
	m_groupIP = groupIP ;
	m_igroupAddress= igroupAddress;
	m_igroupPort=igroupPort;
	hTreeItem = NULL;
	m_groupName = groupName;
	m_itagCount = 0;
}

COpcGroupItem::~COpcGroupItem(void)
{
	if (m_mptagList.GetCount())
		Remove();
}

void COpcGroupItem::StoreGroups()
{
	 
	CString group =_T("Server\\")+ m_pServerItem->m_szServerName+_T("\\Group\\")+m_groupName ;

	iniFile.SetValue(group,_T("Driver"),m_pServerItem->m_szDriverName);
	iniFile.SetValue(group,_T("Name"),m_groupName);

	iniFile.SetLongValue(group,_T("Addr"),m_igroupAddress);
	iniFile.SetLongValue(group,_T("IP"),m_groupIP);
	iniFile.SetLongValue(group,_T("Port"),m_igroupPort);
	iniFile.SetLongValue(group,_T("Count"),m_itagCount);


}
void COpcGroupItem::LoadGroups(LPCTSTR item)
{
	//iniFile.GetValue(item,_T("Driver"),m_pServerItem->m_szDriverName);
	m_groupName= iniFile.GetValue(item,_T("Name"));

	m_igroupAddress=iniFile.GetLongValue(item,_T("Addr"));
	m_groupIP=iniFile.GetLongValue(item,_T("IP"));
	m_igroupPort=iniFile.GetLongValue(item,_T("Port"));
	m_itagCount=iniFile.GetLongValue(item,_T("Count"));


}

void  COpcGroupItem::ReadWriteIni(LPCTSTR inifname, bool Storing, LPCTSTR key )
{
	if (Storing)
	{
		StoreGroups();
		if (m_itagCount)
		{
			CString strKey;
			POSITION pos = m_mptagList.GetStartPosition(); 
			while (pos)
			{
				COpcItem * Item = NULL;
				m_mptagList.GetNextAssoc(pos,strKey,(void *&)Item); 
				if (Item)
				{
				CString list =_T("Server\\")+ m_pServerItem->m_szServerName+_T("\\Group\\")+m_groupName+_T("\\Items") ;
				iniFile.SetLongValue(list,Item->GetName(),1);
				Item->ReadWriteIni(inifname,Storing,list); 
				}
			}
		}
	}
	else
	{
		CString loadkey =  key;
		LoadGroups(loadkey);

		CString sect = loadkey+_T("\\Items");

		//// get list
		CSimpleIniW::TNamesDepend keys;
		iniFile.GetAllKeys(sect, keys); //we do not know how many itens, but this section contains all
		
		CSimpleIniW::TNamesDepend::const_iterator i;
		m_itagCount=0;
	for ( i = keys.begin(); i != keys.end(); ++i) 
		{
				m_itagCount++;
				CString head= key;
				CString grp= i->pItem;		
				grp= loadkey+_T("\\Item\\")+grp;

			COpcItem * Item = new COpcItem(this);
			Item->ReadWriteIni(inifname,Storing,grp);
			{
			    m_mptagList.SetAt(Item->GetName(),Item);  
			}
		}
	}
}



void  COpcGroupItem::SerializeIni(CArchive& ar, LPCTSTR key )
{
	if (ar.IsStoring())
	{
		StoreGroups();
		if (m_itagCount)
		{
			CString strKey;
			POSITION pos = m_mptagList.GetStartPosition(); 
			while (pos)
			{
				COpcItem * Item = NULL;
				m_mptagList.GetNextAssoc(pos,strKey,(void *&)Item); 
				if (Item)
				{
				CString list =_T("Server\\")+ m_pServerItem->m_szServerName+_T("\\Group\\")+m_groupName+_T("\\Items") ;
				iniFile.SetLongValue(list,Item->GetName(),1);
				Item->SerializeIni(ar,list); 
				}
			}
		}
	}
	else
	{
		CString loadkey =  key;
		LoadGroups(loadkey);

		CString sect = loadkey+_T("\\Items");

		//// get list
		CSimpleIniW::TNamesDepend keys;
		iniFile.GetAllKeys(sect, keys); //we do not know how many itens, but this section contains all
		
		CSimpleIniW::TNamesDepend::const_iterator i;

	for ( i = keys.begin(); i != keys.end(); ++i) 
		{
				CString head= key;
				CString grp= i->pItem;		
				grp= loadkey+_T("\\Item\\")+grp;

			COpcItem * Item = new COpcItem(this);
			Item->SerializeIni(ar,grp);
			{
			    m_mptagList.SetAt(Item->GetName(),Item);  
			}
		}
	}
}


void COpcGroupItem::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		StoreGroups();
		ar << m_groupName;
		ar << m_igroupAddress;
		ar << m_groupIP;
		ar << m_igroupPort;
		ar << m_itagCount;
		if (m_itagCount)
		{
			CString strKey;
			POSITION pos = m_mptagList.GetStartPosition(); 
			while (pos)
			{
				COpcItem * Item = NULL;
				m_mptagList.GetNextAssoc(pos,strKey,(void *&)Item); 
				if (Item)
				{
				CString list =_T("Server\\")+ m_pServerItem->m_szServerName+_T("\\Group\\")+m_groupName+_T("\\Items") ;
				iniFile.SetLongValue(list,Item->GetName(),1);
				Item->Serialize(ar); 
				}
			}
		}
	}
	else
	{
	//	LoadGroups();
		//CString text;
		ar >> m_groupName; 
		//ar >>text; _stscanf(text, _T("%x"), &m_igroupAddress);
		ar >> m_igroupAddress;
		ar >> m_groupIP;
		ar >> m_igroupPort;
		ar >> m_itagCount;
		for (int i=0;i<m_itagCount;i++)
		{
			COpcItem * Item = new COpcItem(this);
			Item->Serialize(ar);
			{
			    m_mptagList.SetAt(Item->GetName(),Item);  
			}
		}
	}
}
void COpcGroupItem::Remove(void)
{
	CSafeLock cs (&m_csDataLock);
	if (m_itagCount)
	{
		m_itagCount = 0;	
		CString strKey;
		POSITION pos = m_mptagList.GetStartPosition(); 
		while (pos)
		{
			COpcItem * Item = NULL;
			m_mptagList.GetNextAssoc(pos,strKey,(void *&)Item); 
			if (Item)
			    delete Item;
		}
		m_mptagList.RemoveAll(); 
	}
}

COpcItem * COpcGroupItem::AddItem(LPCTSTR strName, LPCTSTR strAddress,LPCTSTR strDesc,int datatype,int wrType,int range)
{
	COpcItem * pItem = NULL;
	{
		CSafeLock cs (&m_csDataLock);
		pItem = FindOpcItem(strName);
		if (!pItem) 
		{		
			pItem = new COpcItem(this,strName,strAddress,strDesc,datatype,wrType,range);
/*

	SAFEARRAYBOUND arrayBounds; 
	arrayBounds.lLbound = 0; // lower bound 
	arrayBounds.cElements = opcItem->GetRange(); // element count 
	int numArr =  opcItem->GetLogTagSize();
	SAFEARRAY* psa = SafeArrayCreate(  unsigned short(opcItem->GetType()) , 1, &arrayBounds );

	if ((logTag->tvValue.vt & VT_ARRAY) == VT_ARRAY ) 
		logTag->tvValue.parray = psa;
	*/

			m_itagCount++;
			m_mptagList.SetAt(pItem->GetName(),pItem); 

			CString szTagp = GetServer()->GetName();
			CString szT = szTagp.MakeLower();
			if (szT != "remote")
				szTagp+= _T("/") + GetName() + _T("/");
			else
				szTagp = _T("");
			CString sName = szTagp + strName; 
			if (my_CF.AddTag(pItem,sName.GetBuffer(),pItem->GetWrType(),NULL)==0)
			{
				sName += _T(" item Built Fail\n");
				TRACE(sName);
			}
		}
		else
		{
			//repeat item Name
			CString szLog = _T("Item Name Exist, Create error : ")+pItem->GetName();
			LogMsg(IDS_MSG,szLog);
		}
	}
	return pItem;
}

int COpcGroupItem::CreatOpcTag(LPCTSTR szServer)
{
	CSafeLock cs (&m_csDataLock);
	int count = 0;
	
	CString szTagp = szServer ;

	CString szT = szTagp.MakeLower();
	if (szT != "remote")
	{
	szTagp += _T("/") ;
	szTagp += GetName();
	szTagp += _T("/");
	}
	else
		szTagp = _T("");

	CString szTag;

	POSITION pos = m_mptagList.GetStartPosition(); 
	CString strKey;
	while (pos)
	{
		COpcItem * Item = NULL;
		m_mptagList.GetNextAssoc(pos,strKey,(void *&)Item); 
		if (Item)
		{
			szTag = szTagp + Item->GetName(); 
			if (my_CF.AddTag(Item,szTag.GetBuffer(),Item->GetWrType(),NULL)==0)
			{
				szTag += _T(" Establishment failure\n");
				TRACE(szTag);
			}
			else
				count++;
		}
	}
	return count;
}

void COpcGroupItem::Simulate(void)
{
	CSafeLock cs (&m_csDataLock);
	//SafeLock
	VARIANT varData;
	VariantInit(&varData);

	POSITION pos = m_mptagList.GetStartPosition(); 
	CString strKey;
	while (pos)
	{
		COpcItem * Item = NULL;
		
		m_mptagList.GetNextAssoc(pos,strKey,(void *&)Item); 
		if (!Item->GetActive()) 
			continue;

		VariantClear(&varData);


		Item->GetVariantValue(&varData);
		loTagValue * tag = Item->GetLoTag();
				BOOL bData=FALSE;
				float fData =0;
				LONG lnum=1;
				LONG lData = 0;
				LONG lUpper = 0;  //array[5] res=4
		if ((varData.vt & VT_ARRAY) == VT_ARRAY )
		SafeArrayGetUBound(varData.parray,1,&lUpper);


		if (tag)
		{
			switch (varData.vt)
			{
			case VT_BOOL:
				varData.boolVal =  !varData.boolVal;
				break;
			case VT_ARRAY|VT_BOOL:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				bData=FALSE;
				SafeArrayGetElement(varData.parray,&lnum,&bData);
				bData=!bData;
				SafeArrayPutElement(varData.parray,&lnum,&bData);
			}
				break;
			case VT_I4:
				varData.lVal++;  
				break;
			case VT_ARRAY|VT_I4:

			for(lnum=0;lnum<=lUpper;lnum++)
			{
				lData=0;
				SafeArrayGetElement(varData.parray,&lnum,&lData);
				lData+=1;
				SafeArrayPutElement(varData.parray,&lnum,&lData);
			}
				
				break;

			case VT_UI4:
				varData.ulVal ++; 
				break;
			case VT_ARRAY|VT_UI4:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				lData=0;
				SafeArrayGetElement(varData.parray,&lnum,&lData);
				lData+=1;
				SafeArrayPutElement(varData.parray,&lnum,&lData);
			}
				break;

			case VT_R4:
				varData.fltVal += 0.1f; 
				break;
			case VT_ARRAY|VT_R4:
				for(lnum=0;lnum<=lUpper;lnum++)
				{
				fData=0.0f;
				SafeArrayGetElement(varData.parray,&lnum,&fData);
				fData +=0.1f;
				SafeArrayPutElement(varData.parray,&lnum,&fData);
				}
				break;
			case VT_R8:
				varData.dblVal += 0.1; 
				break;
			case VT_ARRAY|VT_R8:
			for(lnum=0;lnum<=lUpper;lnum++)
			{
				fData=0.0f;
				SafeArrayGetElement(varData.parray,&lnum,&fData);
				fData +=0.1f;
				SafeArrayPutElement(varData.parray,&lnum,&fData);
			}
				break;

			default:
				continue;
				break;
			}
			Item->UpdateData(varData); 
			my_CF.UpDateTag(tag);
		}
	}
}

COpcItem * COpcGroupItem::FindOpcItem(LPCTSTR strItem)
{
	COpcItem * result = NULL;
	if (m_mptagList.Lookup(strItem,(void *&)result))
	{
		return result;
	}
	else
		return NULL;

}

void COpcGroupItem::GetTagItems(CObArray & obArray)
{
	CSafeLock cs (&m_csDataLock);
	if (obArray.GetCount())
		obArray.RemoveAll();
	if (m_itagCount)
		obArray.SetSize(m_itagCount); 

	POSITION pos = m_mptagList.GetStartPosition(); 
	CString strKey;
	int nIndex=0;
	while (pos)
	{
		COpcItem * Item = NULL;
		m_mptagList.GetNextAssoc(pos,strKey,(void *&)Item); 
		obArray.SetAt(nIndex++,Item); 
	}

}
void COpcGroupItem::DeleteItems(CObArray & obArray)
{
		CSafeLock cs (&m_csDataLock);
		COpcItem * pItem = NULL;
		int dwIndex = obArray.GetCount(); 
		for (int i=0;i<dwIndex;i++)
		{
			pItem = (COpcItem *) obArray.GetAt(i);
			if (!pItem->GetActive()) 
			{
				LPCTSTR key = NULL;
				if (m_mptagList.LookupKey(pItem->GetName(),key ))
				{			
					if (m_mptagList.RemoveKey(key))			  
					{
						m_itagCount--;
						delete pItem;					  
					}
				}
			}
		}
}

//Tag A;TagName
void COpcGroupItem::Import(CString strFile)
{
	CStdioFile sf;
	if (sf.Open(strFile,CFile::typeText|CFile::modeRead))
	{
		CString str;
		CString strTag;
		while (sf.ReadString(str))
		{
			TCHAR * buf = str.GetBuffer(); 
			if (*buf == 'A')
			{
				buf+=2;
				strTag = buf;
				AddItem(strTag,_T(""),_T(""),VT_R4,OPC_READABLE | OPC_WRITEABLE,1);///need change for range
			}
			else if (*buf == 'D')
			{
				buf+=2;
				strTag = buf;
				AddItem(strTag,_T(""),_T(""),VT_BOOL,OPC_READABLE | OPC_WRITEABLE,1);
			}
			else if (*buf == 'R')
			{
				buf+=2;
				strTag = buf;
				AddItem(strTag,_T(""),_T(""),VT_R8,OPC_READABLE | OPC_WRITEABLE,1);
			}
		}
	}
}

//Net Server use
COpcItem * COpcGroupItem::GetItemByID(int id)
{
	COpcItem * pItem = NULL;
	//Only global group 
	{
		CSafeLock cs (&m_csDataLock);
		POSITION pos = m_mptagList.GetStartPosition(); 
		CString strKey;
		int nIndex=0;
		while (pos)
		{
			COpcItem * Item = NULL;
			m_mptagList.GetNextAssoc(pos,strKey,(void *&)Item); 
			if (Item->GetItemID() == id)
			{
				pItem = Item;
				break;
			}
		}
	}
	return pItem;
}


//Net Server use
BOOL COpcGroupItem::SetItemID(LPCTSTR strItem,int id)
{
	CSafeLock cs (&m_csDataLock);
	COpcItem * pItem = FindOpcItem(strItem);
	if (pItem)
	{
		pItem->SetItemID(id);
		return TRUE;
	}
	return FALSE;
}

#define STRIDE_FACTOR 3
void COpcGroupItem::SortItems(CObArray & obArray, int WhichCol)
{
		CSafeLock cs (&m_csDataLock);
		COpcItem * pItem = NULL;

		BOOL bFound;
		int iElements =  obArray.GetCount(); 
		int iInner,iOuter,iStride = 1;
	
	while (iStride <= iElements)
		iStride = iStride * STRIDE_FACTOR + 1;
	
	while (iStride > (STRIDE_FACTOR - 1))
	{
		iStride = iStride / STRIDE_FACTOR;
		for (iOuter = iStride; iOuter < iElements; iOuter++)
		{
			bFound = 0;
			iInner = iOuter - iStride;
			while ((iInner >= 0) && !bFound)
			{

				COpcItem * pItemTmp = (COpcItem *)obArray.GetAt(iInner+iStride);
				COpcItem * pItemTmp2 = (COpcItem *)obArray.GetAt(iInner+iStride);
			
				if (-lstrcmp((LPCTSTR)pItemTmp->GetName() ,(LPCTSTR)pItemTmp2->GetName()) < 0 )
				{
					obArray.SetAt(iInner+iStride, (COpcItem *) obArray.GetAt(iInner));
					obArray.SetAt(iInner,pItemTmp) ;
					iInner -= iStride;
				}
				else
					bFound = 1;
			}
		}
	}
}
