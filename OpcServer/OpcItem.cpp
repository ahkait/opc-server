#include "stdafx.h"
#include "OpcServer.h"
#include "OpcItem.h"
#include "OpcGroupItem.h"
#include "CiniFile\SimpleIni.h"

extern CSimpleIni iniFile; 
extern CMyOpcServer my_CF;

COpcItem::COpcItem(COpcGroupItem * pGroupItem)
{
	memset(&m_systm,0,sizeof(SYSTEMTIME));
	memset(&loTag,0,sizeof(loTagValue));
	VariantClear(&loTag.tvValue);	 
	m_sztagName  = _T("");
	m_sztagAddress = _T("");
	m_iItemID = -1;
	m_sztagdesc = _T("");
	m_itagRange = 1;
	m_pGroup = pGroupItem;
	m_itagType = 0;
	m_dwActCount = 0;
	m_ErrorNoScan = FALSE;
	m_dwwrType = OPC_READABLE | OPC_WRITEABLE;
	m_wQuality = OPC_QUALITY_BAD;
	if (m_itagRange > 1)
	loTag.tvValue.vt = VT_ARRAY | unsigned short(m_itagType); 
	else
	loTag.tvValue.vt =  unsigned short(m_itagType); 

}

COpcItem::COpcItem(COpcGroupItem * pGroupItem,LPCTSTR strName,LPCTSTR strAddress,LPCTSTR strDesc,int tagType,DWORD wrType, int range)
{
	
	memset(&m_systm,0,sizeof(SYSTEMTIME));
	memset(&loTag,0,sizeof(loTagValue));
	VariantClear(&loTag.tvValue);	 
	m_sztagName  = strName;
	m_sztagAddress = strAddress;
	m_sztagdesc = strDesc;
	m_pGroup = pGroupItem;
	m_itagType = tagType;
	m_itagRange = range;
	m_dwActCount = 0;
	m_dwwrType = wrType;//OPC_READABLE | OPC_WRITEABLE;
	m_wQuality = OPC_QUALITY_BAD;
	///must trigger opc to add tag info, rather hten re-start exe
	m_ErrorNoScan = FALSE;
	if (range>1)
	{
	HRESULT   hr=E_FAIL;
	SAFEARRAYBOUND arrayBounds; 
	arrayBounds.lLbound = 0; // lower bound 
	arrayBounds.cElements = range; // element count 
	SAFEARRAY* psa = SafeArrayCreate(  unsigned short(m_itagType), 1, &arrayBounds );
	loTag.tvValue.vt = VT_ARRAY | unsigned short(m_itagType); 
	//was loTag.tvValue.vt =  unsigned short(m_itagType); 
	loTag.tvValue.parray = psa;
	}
	else
	loTag.tvValue.vt =  unsigned short(m_itagType); 

}


COpcItem::~COpcItem(void)
{
}

void COpcItem::StoreItems()
{ /// _T("Drivers\\")+ m_pServerItem->m_szServerName+_T("\\Group\\")+m_groupName ;
	
	CString item = _T("Server\\")+m_pGroup->GetDriverName() +_T("\\Group\\")+m_pGroup->GetName()+"\\Item\\"+m_sztagName;

	iniFile.SetValue(item,_T("TagName"),m_sztagName);

	iniFile.SetLongValue(item,_T("Type"),m_itagType);

	iniFile.SetValue(item,_T("Addr"),m_sztagAddress);

	iniFile.SetLongValue(item,_T("TagType"),m_dwwrType);

	iniFile.SetLongValue(item,_T("Range"),m_itagRange);

	iniFile.SetLongValue(item,_T("Swap"),m_Swap);

	iniFile.SetLongValue(item,_T("UseBigIndian"),m_UseBigIndian);

}

void COpcItem::LoadItems( LPCTSTR item)
{
	m_sztagName=iniFile.GetValue(item,_T("TagName"));
	m_itagType=iniFile.GetLongValue(item,_T("Type"));
	m_sztagAddress=iniFile.GetValue(item,_T("Addr"));
	m_dwwrType=iniFile.GetLongValue(item,_T("TagType"));
	m_itagRange= iniFile.GetLongValue(item,_T("Range"));
	m_Swap=iniFile.GetLongValue(item,_T("Swap"));
	m_UseBigIndian=iniFile.GetLongValue(item,_T("UseBigIndian"));
}

void COpcItem::ReadWriteIni(LPCTSTR inifname, bool Storing, LPCTSTR key )
{
	if (Storing)
	{
		StoreItems();
	}
	else
	{
	LoadItems(key);

	if (m_itagRange > 1)
	{
	HRESULT   hr=E_FAIL;
	SAFEARRAYBOUND arrayBounds; 
	arrayBounds.lLbound = 0; // lower bound 
	arrayBounds.cElements = m_itagRange; // element count 
	SAFEARRAY* psa = SafeArrayCreate(  unsigned short(m_itagType), 1, &arrayBounds );
	loTag.tvValue.vt = VT_ARRAY | unsigned short(m_itagType); 
	loTag.tvValue.parray = psa;
	}
	else
	loTag.tvValue.vt =  unsigned short(m_itagType); 
	}
}



void COpcItem::SerializeIni(CArchive& ar, LPCTSTR key )
{
	if (ar.IsStoring())
	{
		StoreItems();
	}
	else
	{
	LoadItems(key);

	if (m_itagRange > 1)
	{
	HRESULT   hr=E_FAIL;
	SAFEARRAYBOUND arrayBounds; 
	arrayBounds.lLbound = 0; // lower bound 
	arrayBounds.cElements = m_itagRange; // element count 
	SAFEARRAY* psa = SafeArrayCreate(  unsigned short(m_itagType), 1, &arrayBounds );
	loTag.tvValue.vt = VT_ARRAY | unsigned short(m_itagType); 
	loTag.tvValue.parray = psa;
	}
	else
	loTag.tvValue.vt =  unsigned short(m_itagType); 
	}
}


void COpcItem::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		StoreItems();
		ar << m_itagType;
		ar << m_sztagName;
		ar << m_sztagAddress;
		ar << m_sztagdesc;
		ar << m_dwwrType;
		ar << m_itagRange;
		ar << m_Swap;
		ar << m_UseBigIndian;
	}
	else
	{
		//LoadItems();
		ar >> m_itagType;
		ar >> m_sztagName;
		ar >> m_sztagAddress;
		ar >> m_sztagdesc;
		ar >> m_dwwrType;
		ar >> m_itagRange;
		ar >> m_Swap;
		ar >> m_UseBigIndian;

	if (m_itagRange > 1)
	{
	HRESULT   hr=E_FAIL;
	SAFEARRAYBOUND arrayBounds; 
	arrayBounds.lLbound = 0; // lower bound 
	arrayBounds.cElements = m_itagRange; // element count 
	SAFEARRAY* psa = SafeArrayCreate(  unsigned short(m_itagType), 1, &arrayBounds );
	loTag.tvValue.vt = VT_ARRAY | unsigned short(m_itagType); 
	loTag.tvValue.parray = psa;
	}
	else
	loTag.tvValue.vt =  unsigned short(m_itagType); 
	}
}


void COpcItem::GetValue(CString & strValue)
{
	CSafeLock cs (&m_csDataLock);
	// If item is active and valid, we can rely on server's data:
	TCHAR szNum [32];
	LONG lUpper =0;
	if ((loTag.tvValue.vt & VT_ARRAY) == VT_ARRAY )
	SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);

	switch (loTag.tvValue.vt)
	{
	case VT_BOOL: 
		wsprintf (szNum, _T("%d"), loTag.tvValue.boolVal ? 1 : 0);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_BOOL: 
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				bool bData=FALSE;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%d;"),bData ? 1:0 );
				strValue +=szNum;
			}
			break;
	case VT_UI1:
		wsprintf (szNum, _T("%u"), loTag.tvValue.bVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_UI1:
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				WORD bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%u;"),bData );
				strValue +=szNum;
			}
			break;

	case VT_I1:
		wsprintf (szNum, _T("%d"), loTag.tvValue.cVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_I1:
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				WORD bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%d;"),bData );
				strValue +=szNum;
			}
		break;

	case VT_UI2:
		wsprintf (szNum, _T("%u"), loTag.tvValue.uiVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_UI2:
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				DWORD bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%u;"),bData );
				strValue +=szNum;
			}
			break;

	case VT_I2:
		wsprintf (szNum, _T("%d"), loTag.tvValue.iVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_I2:
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				DWORD bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%d;"),bData );
				strValue +=szNum;
			}
		break;

	case VT_UI4:
		wsprintf (szNum, _T("%u"), loTag.tvValue.ulVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_UI4:
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				DWORD bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%u;"),bData );
				strValue +=szNum;
			}
			break;

	case VT_I4:
		wsprintf (szNum, _T("%d"), loTag.tvValue.lVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_I4:
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				int bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%d;"),bData );
				strValue +=szNum;
			}
		break;

	case VT_UI8:
		wsprintf (szNum, _T("%u"), loTag.tvValue.ulVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_UI8:
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				QWORD bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%u;"),bData );
				strValue +=szNum;
			}
			break;

	case VT_I8:
		wsprintf (szNum, _T("%d"), loTag.tvValue.lVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_I8:
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				int bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%d;"),bData );
				strValue +=szNum;
			}
		break;


	case VT_R4:
		_stprintf (szNum, _T("%G"), loTag.tvValue.fltVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_R4:
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				double bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%G;"),bData );
				strValue +=szNum;
			}
		break;

	case VT_R8:
		_stprintf (szNum, _T("%G"), loTag.tvValue.dblVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_R8:
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				double bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%G;"),bData );
				strValue +=szNum;
			}
		break;

	case VT_BSTR:		
		strValue =  loTag.tvValue.bstrVal;
		//return loTag.tvValue.bstrVal;
		break;
	default:
		strValue = _T("");
		break;
	}

}

//
void COpcItem::UpdateData (VARIANT &vtVal, WORD wQuality,SYSTEMTIME * systm)
{
	CSafeLock cs (&m_csDataLock);
	VariantCopy (&loTag.tvValue, &vtVal);
	m_wQuality = wQuality; 
	if (systm)
	{
		memcpy(&m_systm,systm,sizeof(SYSTEMTIME));
	}
	else
	{
		GetLocalTime (&m_systm);
	}
}

void  COpcItem::GetVariantValue(VARIANT * varData)
{
	CSafeLock cs (&m_csDataLock);
	VariantCopy (varData,&loTag.tvValue);
}

CString COpcItem::GetStrValue() ///need to break as array
{
	CSafeLock cs (&m_csDataLock);
	// If item is active and valid, we can rely on server's data:
	TCHAR szNum [32];
	CString strValue;

	LONG lUpper =0;
	//if (loTag.tvValue.vt & VT_ARRAY == VT_ARRAY )
	// SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);
	

	switch (loTag.tvValue.vt)
	{
	case VT_BOOL: 
		wsprintf (szNum, _T("%d"), loTag.tvValue.boolVal ? 1 : 0);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_BOOL: 
			SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				BOOL bData=FALSE;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%d;"),bData ? 1:0 );
				strValue +=szNum;
			}
			break;
	case VT_UI1:
		wsprintf (szNum, _T("%u"), loTag.tvValue.bVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_UI1:
			SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				WORD bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%u;"),bData );
				strValue +=szNum;
			}
			break;

	case VT_I1:
		wsprintf (szNum, _T("%d"), loTag.tvValue.cVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_I1:
			SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				WORD bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%d;"),bData );
				strValue +=szNum;
			}
		break;

	case VT_UI2:
		wsprintf (szNum, _T("%u"), loTag.tvValue.uiVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_UI2:
			SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				DWORD bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%u;"),bData );
				strValue +=szNum;
			}
			break;

	case VT_I2:
		wsprintf (szNum, _T("%d"), loTag.tvValue.iVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_I2:
			SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				DWORD bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%d;"),bData );
				strValue +=szNum;
			}
		break;

	case VT_UI4:
		wsprintf (szNum, _T("%u"), loTag.tvValue.ulVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_UI4:
			SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				QWORD bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%u;"),bData );
				strValue +=szNum;
			}
			break;

	case VT_I4:
		wsprintf (szNum, _T("%d"), loTag.tvValue.lVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_I4:
			SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				int bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%d;"),bData );
				strValue +=szNum;
			}
		break;

	case VT_R4:
		_stprintf (szNum, _T("%G"), loTag.tvValue.fltVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_R4:
			SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				double bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%G;"),bData );
				strValue +=szNum;
			}
		break;

	case VT_R8:
		_stprintf (szNum, _T("%G"), loTag.tvValue.dblVal);
		strValue = szNum;
		break;
	case VT_ARRAY|VT_R8:
			SafeArrayGetUBound(loTag.tvValue.parray,1,&lUpper);
			for(LONG lnum=0;lnum<=lUpper;lnum++)
			{
				double bData=0;
				SafeArrayGetElement(loTag.tvValue.parray,&lnum,&bData);
				wsprintf (szNum, _T("%G;"),bData );
				strValue +=szNum;
			}
		break;

	case VT_BSTR:		
		strValue =  loTag.tvValue.bstrVal;
		//return loTag.tvValue.bstrVal;
		break;
	default:
		strValue = _T("");
		break;
	}
	
	return strValue;
}

LPCTSTR COpcItem::GetStrQuality()
{
	if (m_wQuality == OPC_QUALITY_GOOD)
		return _T("Good");
	else
		return _T("Bad");
}

LPCTSTR COpcItem::GetStrwrType()	
{
		

switch (m_dwwrType)
			{
			case OPC_READABLE | OPC_WRITEABLE:
				return _T("rw");
				break;
			case OPC_READABLE:
				return _T("r");
				break;
			case OPC_WRITEABLE:
				return _T("w");
				break;
			default:
				return _T("--");
				break;
			}
			
}



CString COpcItem::FormatTime ()
{
	CSafeLock cs (&m_csDataLock);
	//GetLocalTime (&m_systm);
	// Localize and round up to the nearest second:
	unsigned short wMinute = m_systm.wMinute;
	unsigned short wSecond = m_systm.wSecond;
	unsigned short wHour = m_systm.wHour;
	// Need to round seconds up if milliseconds are >= 500:
	if (m_systm.wMilliseconds >= 500)
	{
		if (++wSecond == 60)
		{
			wSecond = 0;
			if (++wMinute == 60)
			{
				wMinute = 0;
				if (++wHour == 24)
				{
					wHour = 0;
				}
			}
		}
	}
	CString str;
	str.Format(_T("%2d:%02d:%02d"), wHour,  wMinute, wSecond);
	return str;
}

void COpcItem::UpDate(float data,WORD quality)
{
	if (m_wQuality!=quality || loTag.tvValue.fltVal!= data)
	{
		CSafeLock cs (&m_csDataLock);
		loTag.tvValue.fltVal = data; 
		m_wQuality = OPC_QUALITY_GOOD; 
		GetLocalTime (&m_systm);
		my_CF.UpDateTag(&loTag);
	}
}

void COpcItem::UpDate(bool data,WORD quality)
{
	if ((m_wQuality!=quality) || (loTag.tvValue.boolVal!= data))
	{
		CSafeLock cs (&m_csDataLock);
		loTag.tvValue.boolVal = data;
		m_wQuality = OPC_QUALITY_GOOD; 
		GetLocalTime (&m_systm);
		my_CF.UpDateTag(&loTag);
	}
}

void COpcItem::UpDate(int data,WORD quality)
{
	if (m_wQuality!=quality || loTag.tvValue.iVal!= data)
	{
		CSafeLock cs (&m_csDataLock);
		loTag.tvValue.lVal = data;
		m_wQuality = OPC_QUALITY_GOOD; 
		GetLocalTime (&m_systm);
		my_CF.UpDateTag(&loTag);
	}
}

void COpcItem::UpDate(long data,WORD quality)
{
	if (m_wQuality!=quality || loTag.tvValue.iVal!= data)
	{
		CSafeLock cs (&m_csDataLock);
		loTag.tvValue.ulVal = data;
		m_wQuality = OPC_QUALITY_GOOD; 
		GetLocalTime (&m_systm);
		my_CF.UpDateTag(&loTag);
	}
}

void COpcItem::UpDate(WORD data,WORD quality)
{
	if (m_wQuality!=quality || loTag.tvValue.iVal!= data)
	{
		CSafeLock cs (&m_csDataLock);
		loTag.tvValue.uiVal = data;
		m_wQuality = OPC_QUALITY_GOOD; 
		GetLocalTime (&m_systm);
		my_CF.UpDateTag(&loTag);
	}
}


void COpcItem::UpDate(short data,WORD quality)
{
	if (m_wQuality!=quality || loTag.tvValue.iVal!= data)
	{
		CSafeLock cs (&m_csDataLock);
		loTag.tvValue.iVal = data;
		m_wQuality = OPC_QUALITY_GOOD; 
		GetLocalTime (&m_systm);
		my_CF.UpDateTag(&loTag);
	}
}


void COpcItem::UpDate(double data,WORD quality)
{
	if (m_wQuality!=quality || loTag.tvValue.dblVal!= data)
	{
		CSafeLock cs (&m_csDataLock);
		loTag.tvValue.dblVal = data;
		m_wQuality = OPC_QUALITY_GOOD; 
		GetLocalTime (&m_systm);
		my_CF.UpDateTag(&loTag);
	}
}

void COpcItem::UpDate(CString str,WORD quality)
{
	if (m_wQuality!=quality)
	{
		CSafeLock cs (&m_csDataLock);
		SysFreeString(loTag.tvValue.bstrVal);
		loTag.tvValue.bstrVal = str.AllocSysString();
		m_wQuality = OPC_QUALITY_GOOD; 
		GetLocalTime (&m_systm);
		my_CF.UpDateTag(&loTag);
	}
}