// stdafx.cpp 
// OpcServer.pch
// stdafx.obj
#include "stdafx.h"
#include "MainFrm.h"

#define _WIN32_DCOM
DWORD objid = 0;




// Clipboard formats for cut/copy/paste:
UINT CF_SERVER = RegisterClipboardFormat (_T("OpcServer"));
UINT CF_GROUP = RegisterClipboardFormat (_T("OpcGroup"));
UINT CF_GROUPITEM = RegisterClipboardFormat (_T("COpcGroupItem"));
UINT CF_ITEM = RegisterClipboardFormat (_T("OpcItem"));



// **************************************************************************
// LogMsg ()
//
// Description:
//	Log a message to our application's event window.
//
// Parameters:
//  UINT		nResID		String resource ID
//							  15000 - 15999 info
//							  16000 - 16999 error
//							Additional arguments can be strings or 
//							  numerical values to be used in message string.
//							  Place format specifiers in string resource.
//
// Returns:
//  void
// **************************************************************************
void LogMsg (UINT nResID, ...)
	{
	EVENTTYPE eType;

	// Get pointer to additional argument list (which starts after nResID):
	va_list args;
	va_start (args, nResID);

	// Allocate a buffer for message text:
	int nBuf;
	TCHAR szBuffer [512];
	
	// Load the specified string resource.  It should contain format 
	// specifiers if additional arguments are given.
	CString strFmt;
	strFmt.LoadString (nResID);

	// Contruct the message string:
	nBuf = _vsntprintf (szBuffer, sizeof (szBuffer) / sizeof (szBuffer[0]), (LPCTSTR)strFmt, args);

	// Chect to see if there was there an error, indicated by nBuf=0.  Debug only.
	// (was the expanded string too long?)
	ASSERT (nBuf >= 0);

	// Determine the type of message implied by string resource number.
	// (see range in function header above)
	if (nResID >= EVENT_FIRSTINFO && nResID <= EVENT_LASTINFO)
		eType = tEventInformation;
	else if (nResID >= EVENT_FIRSTERROR && nResID <= EVENT_LASTERROR)
		eType = tEventError;
	else
		{
		// String resource ID not in range.  Programmer error.
		ASSERT (FALSE);
		}

	// Add the message to the queue.

	// First get pointer to the application's main window:
	CMainFrame *pMainWnd = (CMainFrame *)AfxGetMainWnd ();

	// If pointer looks valid, call LogMsg() function to place message
	// in queue.
	
	if ((pMainWnd ) && ::IsWindow (pMainWnd->m_hWnd)) 
		pMainWnd->LogEvent (szBuffer,eType);

	// Reset additional arguments pointer:
	va_end (args);
	}


// **************************************************************************
// VartypeFromString ()
//
// Description:
//	Returns a vartype based on a string description.
//
// Parameters:
//  LPCTSTR		lpszType		Variant type (Boolean, Byte, etc)
//
// Returns:
//  VARTYPE - Type (VT_BOOL, VTU1, etc.), or VT_EMPTY if input invalid.
// **************************************************************************
VARTYPE VartypeFromString (LPCTSTR lpszType)
	{
	VARTYPE vtType;

	// Compare input type string with supported types and return the
	// corresponding variant type.  (A match is found when lstrcmpi returns
	// zero.)  If specified type is not supported, then return VT_EMPTY.
	// These strings must match those used below in StringFromVartype().
	if (lstrcmpi (lpszType, _T("Boolean")) == 0)
		vtType = VT_BOOL;
	else if (lstrcmpi (lpszType, _T("Byte")) == 0)
		vtType = VT_UI1;
	else if (lstrcmpi (lpszType, _T("Byte Array")) == 0)
		vtType = VT_UI1 | VT_ARRAY;
	else if (lstrcmpi (lpszType, _T("Char")) == 0)
		vtType = VT_I1;
	else if (lstrcmpi (lpszType, _T("Char Array")) == 0)
		vtType = VT_I1 | VT_ARRAY;
	else if (lstrcmpi (lpszType, _T("Word")) == 0)
		vtType = VT_UI2;
	else if (lstrcmpi (lpszType, _T("Word Array")) == 0)
		vtType = VT_UI2 | VT_ARRAY;
	else if (lstrcmpi (lpszType, _T("Short")) == 0)
		vtType = VT_I2;
	else if (lstrcmpi (lpszType, _T("Short Array")) == 0)
		vtType = VT_I2 | VT_ARRAY;
	else if (lstrcmpi (lpszType, _T("DWord")) == 0)
		vtType = VT_UI4;
	else if (lstrcmpi (lpszType, _T("DWord Array")) == 0)
		vtType = VT_UI4 | VT_ARRAY;
	else if (lstrcmpi (lpszType, _T("Long")) == 0)
		vtType = VT_I4;
	else if (lstrcmpi (lpszType, _T("Long Array")) == 0)
		vtType = VT_I4 | VT_ARRAY;
	else if (lstrcmpi (lpszType, _T("QWord")) == 0)
		vtType = VT_UI8;
	else if (lstrcmpi (lpszType, _T("QWord Array")) == 0)
		vtType = VT_UI8 | VT_ARRAY;
	else if (lstrcmpi (lpszType, _T("Long Long")) == 0)
		vtType = VT_I8;
	else if (lstrcmpi (lpszType, _T("Long Long Array")) == 0)
		vtType = VT_I8 | VT_ARRAY;
	else if (lstrcmpi (lpszType, _T("Float")) == 0)
		vtType = VT_R4;
	else if (lstrcmpi (lpszType, _T("Float Array")) == 0)
		vtType = VT_R4 | VT_ARRAY;
	else if (lstrcmpi (lpszType, _T("Double")) == 0)
		vtType = VT_R8;
	else if (lstrcmpi (lpszType, _T("Double Array")) == 0)
		vtType = VT_R8 | VT_ARRAY;
	else if (lstrcmpi (lpszType, _T("String")) == 0)
		vtType = VT_BSTR;
	else
		vtType = VT_EMPTY;

	// Return variant type:
	return (vtType);
	}

// **************************************************************************
// StringFromVartype ()
//
// Description:
//	Returns a string description based on a vartype.
//
// Parameters:
//	VARTYPE		vtType		Input variant type (VT_BOOL, VT_UI1, etc.)
//	CString		&strType	Output string corresponding to input type. If no
//							  type specified, then return "Native".
//
// Returns:
//	void
// **************************************************************************
void StringFromVartype (VARTYPE vtType, CString &strType)
	{
	// Return a string describing the specified variant type.  These strings
	// must match those used above in VartypeFromString().
	switch (vtType & ~VT_ARRAY)
		{
		case VT_BOOL:	strType = _T("Boolean");	break;
		case VT_UI1:	strType = _T("Byte");		break;
		case VT_I1:		strType = _T("Char");		break;
		case VT_UI2:	strType = _T("Word");		break;
		case VT_I2:		strType = _T("Short");		break;
		case VT_UI4:	strType = _T("DWord");		break;
		case VT_I4:		strType = _T("Long");		break;
		case VT_UI8:		strType = _T("QWord");	break;
		case VT_I8:		strType = _T("Long Long");	break;
		case VT_R4:		strType = _T("Float");		break;
		case VT_R8:		strType = _T("Double");		break;
		case VT_BSTR:	strType = _T("String");		break;

		case VT_BOOL | VT_ARRAY:	strType = _T("Boolean Array");	break;
		case VT_UI1 | VT_ARRAY:		strType = _T("Byte Array");		break;
		case VT_I1 | VT_ARRAY:		strType = _T("Char Array");		break;
		case VT_UI2 | VT_ARRAY:		strType = _T("Word Array");		break;
		case VT_I2 | VT_ARRAY:		strType = _T("Short Array");		break;
		case VT_UI4 | VT_ARRAY:		strType = _T("DWord Array");		break;
		case VT_I4 | VT_ARRAY:		strType = _T("Long Array");		break;
		case VT_UI8 | VT_ARRAY:		strType = _T("QWord Array");		break;
		case VT_I8 | VT_ARRAY:		strType = _T("Long Long Array");	break;
		case VT_R4 | VT_ARRAY:		strType = _T("Float Array");		break;
		case VT_R8 | VT_ARRAY:		strType = _T("Double Array");		break;
		case VT_BSTR | VT_ARRAY:	strType = _T("String Array");		break;


		default:		strType = _T("Native");		break;
		}

	// Append " Array" if a variant array type:
	if ((vtType & VT_ARRAY) != 0)
		strType += _T(" Array");
	}

CString GetAppPath()
{ 

TCHAR szAppPath[_MAX_PATH + 1];

GetModuleFileName(NULL, szAppPath, _MAX_PATH); 

TCHAR* pszTemp = _tcsrchr(szAppPath, _T('\\'));

int n = (int)(pszTemp - szAppPath + 1);

szAppPath [ n ] = 0;
return szAppPath;
//_tcscpy(pszPath, szAppPath);

}