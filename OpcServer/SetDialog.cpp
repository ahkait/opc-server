// SetDialog.cpp :
//

#include "stdafx.h"
#include "OpcServer.h"
#include "SetDialog.h"


// CSetDialog 
IMPLEMENT_DYNAMIC(CSetDialog, CDialog)

CSetDialog::CSetDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CSetDialog::IDD, pParent)
	, m_strServerIP(_T("192.168.0.1"))
	, m_ServerPort(30005)
	, m_strUser(_T("user"))
	, m_strPwd(_T("password"))
	, m_dtuID(0)
{

}

CSetDialog::~CSetDialog()
{
}

void CSetDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_SERVEREDIT, m_strServerIP);
	DDV_MaxChars(pDX, m_strServerIP, 32);
	DDX_Text(pDX, IDC_PORTEDIT, m_ServerPort);
	DDX_Text(pDX, IDC_USEREDIT, m_strUser);
	DDX_Text(pDX, IDC_PWDEDIT, m_strPwd);
	DDV_MaxChars(pDX, m_strPwd, 32);
	DDX_Text(pDX, IDC_IDEDIT, m_dtuID);
	//	DDX_Text(pDX, IDC_STARTUPFILEEDIT, m_StartupFile);
	//	DDV_MaxChars(pDX, m_StartupFile, 512);
	DDX_Control(pDX, IDC_ENABLEREMOTECHECK, m_EnableRemoteServer);
}


BEGIN_MESSAGE_MAP(CSetDialog, CDialog)
END_MESSAGE_MAP()


// CSetDialog 
