// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
// Windows Header Files:
//#include <windows.h>
//#include <WinUser.h>

//#define _AFXDLL 1

#define _AFX_ALL_WARNINGS
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxmt.h>   
#include <afxcoll.h>   

#include "..\safelock.h"
#include "..\opc\opcda.h"
#include "..\opc\opcerror.h"
#include "..\opc\opccomn.h"
#include "..\opc\lightopc.h"
#include "..\opc\unilog.h"

#include "..\MyOpcServer.h"
#include "..\OpcComServerItem.h"
#include "..\OpcGroupItem.h"
#include "..\OpcItem.h"

// Include FieldTalk package header
#include "header\MbusRtuMasterProtocol.hpp"
#include "header\MbusAsciiMasterProtocol.hpp"
#include "header\MbusRtuOverTcpMasterProtocol.hpp"
#include "header\MbusSerialMasterProtocol.hpp"
#include "header\MbusTcpMasterProtocol.hpp"


/// modbus use
enum
{
   T0_BIT,     ///< bit data types, discrete output table
   T1_BIT,     ///< bit data types, discrete input table
   T3_REG16,    ///< 16-bit register (short) types, input table
   T3_HEX16,    ///< 16-bit register types with hex display, input table
   T3_INT32,    ///< 32-bit integer types (2 16-bit registers), input table
   T3_MOD10000, ///< 32-bit modulo 10000 types, input table
   T3_FLOAT32,  ///< Float types (2 16-bit registers), input table
   T4_REG16,    ///< 16-bit register (short) types, output table
   T4_HEX16,    ///< 16-bit register types with hex display, output table
   T4_INT32,    ///< 32-bit integer types (2 16-bit registers), output table
   T4_MOD10000, ///< 32-bit modulo 10000 types, output table
   T4_FLOAT32   ///< Float types (2 16-bit registers), output table
};
enum
{
   RTU,       ///< Modbus RTU protocol
   ASCII,     ///< Modbus ASCII protocol
   TCP,       ///< MODBUS/TCP protocol
   RTUOVERTCP ///< Encapsulated RTU over TCP
};

const char versionStr[]= "modbus 2.4.0";
///CString GetAppPath();
// TODO: reference additional headers your program requires here
