// OpcServerDoc.h : COpcServerDoc 
//
#pragma once
#include "ClientSocket.h"
#include "OpcComServerItem.h"


#define SERVER_ADD 1
#define FILENEW 2
#define SERVER_GROUP_ADD 3
#define GROUP_VIEW_UPDATE 4
#define GROUP_VIEW_CLEAR 5
#define GROUP_VIEW_ADDITEM 6
#define GROUP_DELETE   7
#define SERVER_DELETE  8

#define CURVERSION 0x01010000

class COpcComServerItem;
class COpcGroupItem;
class COpcItem;

class COpcServerDoc : public CDocument
{
protected: // 
	COpcServerDoc();
	DECLARE_DYNCREATE(COpcServerDoc)

// 
public:
    CList<COpcComServerItem*,COpcComServerItem*> m_serverList;
	int GetServerCount(){return m_iserverCount;}
private:
	int m_iserverCount;
	CCriticalSection m_csDataLock;
	//Net Server use
	CObArray m_aiArray;
	CObArray m_diArray;
	CClientSocket m_ClientSocket;
// 
//public:
//		
// 
public:
	virtual BOOL OnNewDocument();
	//virtual 
	void Serialize(CArchive& ar);
	void SerializeIni(CArchive& ar, LPCTSTR key );
	void ReadWriteIni(LPCTSTR inifname, bool Storing, LPCTSTR key );
	BOOL ConnectToServer();
	void StoreHeader();
	void LoadHeader();

// 
public:
	virtual ~COpcServerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	DWORD dwVersion;
	int m_itagCount;

	//Net Information
	CString m_strNetIP;
	int     m_iNetPort;
	CString m_strUserName;
	CString m_strPassword;
	int     m_iDtuID;
	bool	m_AutoStartDriver;
	CString m_strStartUpFile;
	int m_StartRemoteServer;

// 
protected:
	DECLARE_MESSAGE_MAP()
public:
	COpcComServerItem * AddServerItem(CString strServerName,CString strDriverName,CString strAdvanceConfigfile, ServerComm *Comm);
    COpcComServerItem * GetServerItem(CString strServerName);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);

	void Clear(void);
	void GetSelectItems(COpcGroupItem * pGroup,CObArray & obArray);
	void DeleteItems(COpcGroupItem * pGroup,CObArray & obArray);
	COpcItem * AddTagItem(COpcGroupItem * pGroup,LPCTSTR strName, LPCTSTR strAddress,LPCTSTR strDesc,int datatype,int wrType, int range, int SwapData,int UseBigIndian);
protected:
	virtual BOOL SaveModified();
	afx_msg void OnSetting();
public:
	void Simulate(void);
	int  GetTagCount();
	BOOL DeleteGroup(COpcGroupItem * pGroup);
	BOOL DeleteServer(COpcComServerItem * pServer);
	void ResetAiArray()
	{
		CSafeLock sa(&m_csDataLock);
		m_aiArray.RemoveAll();
	}
	void ResetDiArray()
	{
		CSafeLock sa(&m_csDataLock);
		m_diArray.RemoveAll();
	}
	void AddTag(CString strTag,int index,BOOL bAI=TRUE);
	void SetAiCount(int count);
	void SetDiCount(int count);
	void UpDate(int index,float f);
	void UpDate(int index,bool b);

	afx_msg void OnEditExesetup();
};
