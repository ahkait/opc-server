// **************************************************************************
// eventview.h
//
// Description:
//	Defines a CListView derived class.  This the bottom pane of our GUI which
//	shows event messages.
//
// **************************************************************************


#ifndef _EVENTVIEW_H
#define _EVENTVIEW_H

class CKEvent;


// **************************************************************************
class CEventView : public CListView
	{
	protected:
		CEventView ();           // protected constructor used by dynamic creation
		DECLARE_DYNCREATE (CEventView)

	// Attributes
	public:

	// Operations
	public:

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CEventView)
	public:
		virtual BOOL Create (LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd *pParentWnd, UINT nID, CCreateContext *pContext = NULL);
	protected:
		virtual void OnDraw (CDC *pDC);      // overridden to draw this view
		virtual BOOL PreCreateWindow (CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

	protected:
		CCriticalSection m_csLog;

		CKEvent **m_pEventList;
		int m_cnEvents;
		int m_cnAllocEvents;

		CKEvent **m_pPendingEventList;
		int m_cnPendingEvents;
		int m_cnPendingAllocEvents;

		bool m_bAutoScroll;
		bool m_bLogErrorsOnly;

		CImageList m_cImageList;

	// Implementation
	public:
		void LogMsg (EVENTTYPE eType, LPCTSTR lpszMessage);
		
		int GetEventCount () {return (m_cnEvents);}
		bool LogErrorsOnly () {return (m_bLogErrorsOnly);}

	protected:
		bool AddEvent (CKEvent *pEvent);

	protected:
		virtual ~CEventView ();
	#ifdef _DEBUG
		virtual void AssertValid () const;
		virtual void Dump (CDumpContext& dc) const;
	#endif

		// Generated message map functions
	protected:
		//{{AFX_MSG(CEventView)
		afx_msg void OnDestroy ();
		afx_msg void OnGetDispInfo (NMHDR *pNMHDR, LRESULT *pResult);
		afx_msg void OnClear ();
		afx_msg void OnRButtonDown (UINT nFlags, CPoint point);
		afx_msg void OnTimer (UINT nIDEvent);
		afx_msg void OnChar (UINT nChar, UINT nRepCnt, UINT nFlags);
		afx_msg void OnLogErrorsOnly ();
		//}}AFX_MSG

		DECLARE_MESSAGE_MAP ()
	public:
		afx_msg void OnUpdateViewClear(CCmdUI *pCmdUI);
		afx_msg void OnUpdateViewErroronly(CCmdUI *pCmdUI);
};

// **************************************************************************
// Define a class to contain information about an event and to aid in 
// creating strings describing the event.
// **************************************************************************
class CKEvent
	{
	public:
		CKEvent () {}
		CKEvent (LPCTSTR lpszMessage, EVENTTYPE eType)
			{
			m_strMsg = lpszMessage;
			m_eType = eType;
			m_cTimeStamp.Assign ();
			}

		LPCTSTR GetMessage () {return (m_strMsg);}
		EVENTTYPE GetType () {return (m_eType);}

		void FormatDate (LPTSTR lpsz, int cnBytes) {m_cTimeStamp.FormatDate (lpsz, cnBytes);}
		void FormatTime (LPTSTR lpsz, int cnBytes) {m_cTimeStamp.FormatTime (lpsz, cnBytes, true);}

	private:
		CString m_strMsg;
		CTimeStamp m_cTimeStamp;
		EVENTTYPE m_eType;
	};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // _EVENTVIEW_H
