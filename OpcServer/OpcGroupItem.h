#pragma once
#include "afx.h"
#include "OpcComServerItem.h"


class COpcItem;
class COpcGroupItem :
	public CObject
{
public:
	COpcGroupItem(COpcComServerItem * pServerItem,LPCTSTR groupName,DWORD groupIP,int igroupAddress, int igroupPort);
	COpcGroupItem(COpcComServerItem * pServerItem);
	COpcComServerItem * GetServer(){return m_pServerItem;}
	virtual ~COpcGroupItem(void);
	virtual void Serialize(CArchive& ar);
	void SerializeIni(CArchive& ar, LPCTSTR key );
	void ReadWriteIni(LPCTSTR inifname, bool Storing, LPCTSTR key );
	HTREEITEM GetTreeItem(){return hTreeItem;}
	void SetTreeItem(HTREEITEM h){hTreeItem=h;}
	CString GetName(){return m_groupName;}
	CString GetDriverName(){return m_pServerItem->GetName();}
	void SetName(LPCTSTR strName){m_groupName = strName;}
	DWORD GetIPAddr(){return m_groupIP;}
	void SetIPAddr(DWORD IP){m_groupIP = IP;}
	int GetIPPort(){return m_igroupPort;}
	void SetIPPort(int port){m_igroupPort = port;}
	int GetDevAddr(){return m_igroupAddress;}
	void SetDevAddr(int addr){m_igroupAddress = addr;}

private:
	CString m_groupName; /// address, ip and port need to add
	int m_igroupAddress;
	DWORD m_groupIP;
	int m_igroupPort;
	COpcComServerItem * m_pServerItem;
	HTREEITEM hTreeItem;
	int m_itagCount;
	CCriticalSection m_csDataLock;
	void StoreGroups();
	void LoadGroups(LPCTSTR item);

public:
	//CList<COpcItem*,COpcItem*> m_tagList;
	CMapStringToPtr * GetMapObject(	)
	{
		//CSafeLock cs (&m_csDataLock);
		return &m_mptagList;
	}
	void Remove(void);
	COpcItem *  AddItem(LPCTSTR strName, LPCTSTR strAddress, LPCTSTR strDesc,int datatype,int wrType,int range);
	int GetTagCount(){return m_itagCount;}
	int CreatOpcTag(LPCTSTR szServer);
	void Simulate(void);
	COpcItem * FindOpcItem(LPCTSTR strItem);
	void GetTagItems(CObArray & obArray);
	void DeleteItems(CObArray & obArray);
	void SortItems(CObArray & obArray, int WhichCol);
	void Import(CString strFile);
	COpcItem * GetItemByID(int id);
	BOOL SetItemID(LPCTSTR strItem,int id);
	CMapStringToPtr m_mptagList;
};
