
#include "stdafx.h"
#include "OpcServer.h"
#include "MyOpcServer.h"
#include "OpcItem.h"
#include "OpcComServerItem.h"
#include "OpcGroupItem.h"
#include "OpcServerDoc.h"

// #include <Propvarutil.h>

extern  DWORD objid;
extern  CMyOpcServer my_CF;
extern  BOOL autoClosed;

static const loVendorInfo vendor = {
	1 /*Major */ , 0 /*Minor */ , 0 /*Build */ , 0 /*Reserv */ ,
	"XceedSys.OPCServer"
};

static void server_finished(void *arg, loService *b, loClient *c);
loService *my_service;
CRITICAL_SECTION lk_values;
CRITICAL_SECTION lk_update;

CMyOpcServer::CMyOpcServer(void)
{
	lk_count = 0;
}

CMyOpcServer::~CMyOpcServer(void)
{
	if (InterlockedExchangeAdd(&lk_count,0))
		ShutDown();
	if (FAILED(CoRevokeClassObject(objid)))
	{

	}
	my_CF.driver_destroy();
	TRACE0("Driver destory\n");
}

void CMyOpcServer::serverAdd(void)
{
	LONG l = InterlockedIncrement(&lk_count);
}

void CMyOpcServer::serverRemove(void)
{
    LONG l = InterlockedDecrement(&lk_count);	
	if (l==0 &&  autoClosed)
	{
		AfxGetApp()->GetMainWnd()->PostMessageW(WM_CLOSE,0,0);  
	}
}

void CMyOpcServer::local_text(WCHAR buf[32], unsigned char nn, LCID lcid)
{
	char sbt[40];
	long lcp = CP_ACP;
	unsigned nx;

	nn %= 7;

	if (0 == GetLocaleInfoA(lcid, LOCALE_IDEFAULTANSICODEPAGE, sbt, sizeof(sbt)-1))
		goto Failure;
	lcp = strtoul(sbt, 0, 10);

	/* How does called "Monday"+nn in LCID country? */
	if (0 == GetLocaleInfoA(lcid, LOCALE_SDAYNAME1 + nn, sbt, sizeof(sbt) - 1))
		goto Failure;

	nx = strlen(sbt);
	if (sizeof(sbt) - nx > 12)
	{
		sbt[nx++] = ' '; /* Append language name [OPTIONAL] */
		if (0 == GetLocaleInfoA(lcid, LOCALE_SENGLANGUAGE, sbt + nx,
			sizeof(sbt) - nx - 1) && 
			0 == GetLocaleInfoA(lcid, LOCALE_SENGCOUNTRY, sbt + nx,
			sizeof(sbt) - nx - 1) )
			sbt[--nx] = 0; /* ... or the country name */
	}
	if (0 == MultiByteToWideChar(lcp, 0, sbt, -1, buf, 32))
	{
Failure:
		swprintf(buf, L"%d [string]", nn);
	}
}


STDMETHODIMP CMyOpcServer::CreateInstance(LPUNKNOWN pUnkOuter, REFIID riid,LPVOID *ppvObject)
{
	if (!my_service)
		return S_FALSE;

	IUnknown *server = 0;
	HRESULT hr = S_OK;

	OLECHAR *userid = 0;
	wchar_t *cuserid=0;
	LONG userno;
	static LONG usercount;

	userno = InterlockedIncrement(&usercount);

	CoQueryClientBlanket(0, 0, 0, 0, 0, (RPC_AUTHZ_HANDLE*)&userid, 0);
	if (!userid) userid = L"{unknown}";

	if (cuserid = (wchar_t*)malloc((wcslen(userid) + 16) * sizeof(wchar_t)))
		swprintf(cuserid, L"#%ld %ls", userno, userid);

	if (pUnkOuter)
	{
		if (riid != IID_IUnknown) 
			return CLASS_E_NOAGGREGATION;
	}

	serverAdd();  /* the lock for a_server_finished() */

	{
		IUnknown *inner = 0;
		if (loClientCreate_agg(my_service, (loClient**)&server, 
			pUnkOuter, &inner,
			0, &vendor, server_finished, cuserid/*this*/))
		{
			serverRemove();
			hr = E_OUTOFMEMORY;
		}
		else if (pUnkOuter) *ppvObject = (void*)inner; /*aggregation requested*/
		else /* no aggregation */
		{
			/* loClientCreate(my_service, (loClient**)&server, 
			0, &vendor, server_finished, cuserid) - with no aggregation */
			/* Initally the created SERVER has RefCount=1 */
			hr = server->QueryInterface(riid, ppvObject); /* Then 2 (if success) */
			server->Release(); /* Then 1 (on behalf of client) or 0 (if QI failed) */
		}
	}
	if (SUCCEEDED(hr))
	{
		loSetState(my_service, (loClient*)server,
			loOP_OPERATE, (int)OPC_STATUS_RUNNING, /* other states are possible */
			"Finished by client");
	}

	free(cuserid);
	return hr;
}

void CMyOpcServer::ConvertTags(const loCaller *ca,
							   unsigned count, const loTagPair taglist[],
							   VARIANT *values, WORD *qualities, HRESULT *errs,
							   HRESULT *master_err, HRESULT *master_qual,
							   const VARIANT src[], const VARTYPE vtypes[], LCID lcid)
{
	unsigned ii;
	for(ii = 0; ii < count; ii++)
	{
		HRESULT hr = S_OK;
		VARTYPE reqtype = vtypes[ii];
		if (reqtype == VT_EMPTY) reqtype = V_VT(&src[ii]);
		switch((int)taglist[ii].tpTi)
		{
		case 0: /* ignore */
			break;
		default:
			if (reqtype == V_VT(&src[ii]))
			{
				if (values == src) hr = S_OK;
				else hr = VariantCopy(values + ii, (VARIANT*)&src[ii]);
			}
			else
				hr = VariantChangeType(values + ii, (VARIANT*)&src[ii], 0, reqtype);
			break;
		} 

		if (S_OK != hr)
		{
			errs[ii] = hr;
			qualities[ii] = OPC_QUALITY_BAD;
			*master_err = *master_qual = S_FALSE;
		}
	} 
}

void CMyOpcServer::activation_monitor(const loCaller *ca, int count, loTagPair *til)
{
	if (count >0)
	{
		for (int i=0;i<count;i++)
		{	
			COpcItem * Item = (COpcItem *)til->tpRt; 		
			Item->SetActive(); 
			//TRACE(_T("act id= %d,tagname=%s\n") ,Item->GetLoTag()->tvTi,Item->GetName());
			til ++;
		}
	}
	if (0 > count)
	{
		for (int i=count;i<0;i++)
		{	
			COpcItem * Item = (COpcItem *)til->tpRt; 		
			if (Item) ///added ahkait 03feb15
			Item->SetActive(FALSE); 
			//TRACE(_T("deact id= %d,tagname=%s\n") ,Item->GetLoTag()->tvTi,Item->GetName());
			til ++;
		}
	}
}


int CMyOpcServer::WriteTags(const loCaller *ca,
							unsigned count, loTagPair taglist[],
							VARIANT values[], HRESULT error[], HRESULT *master, LCID lcid)
{
	unsigned ii;
	HRESULT hr = S_OK;
    loTagId clean = 0;
	long  slock=1;
	EnterCriticalSection(&lk_values);
	VARIANT v;	
	VariantInit(&v);

	for(ii = 0; ii < count; ii++)// count is always 1 tag with array
	{
		if (taglist[ii].tpTi)
		{	
			COpcItem * OpcItem = (COpcItem *)taglist[ii].tpRt;
			if (OpcItem)
			{ 
				///	loTagValue* lovalue= OpcItem->GetLoWriteTag();
				///loTagValue * tag = Item->GetLoTag();
				int ttype = OpcItem->GetType();
				int refCnt = OpcItem->GetRange(); 
				//if (((&values[ii].vt) & VT_ARRAY) == VT_ARRAY )
				VariantClear(&v);

					COpcComServerItem*  sItem = OpcItem->GetGroup()->GetServer();

						if (refCnt==1)
						{
						hr=VariantChangeType(&v, &values[ii], 0, ttype);
						hr=sItem->p_plugin->ClientWrite( OpcItem, v);
						}
						else
						hr=sItem->p_plugin->ClientWrite( OpcItem, values[ii]);

							//for(int lnum=0;lnum< refCnt;lnum++)
							//{
							//SafeArrayPutElement(v.parray,lnum,&((int*) dataPtr)[lnum]);
							//	hr=VariantChangeType(&v, &values[ii], 0, ttype);//|VT_ARRAY);
						//	}

				//if SUCCEEDED(VariantChangeType(&OpcItem->GetLoTag()->tvValue, &values[ii], 0, OpcItem->GetType()))
				//{

						

				//}
			}
		}
		if (S_OK != hr) 
		{
			*master = S_FALSE;
			error[ii] = hr;
				
		}
				taglist[ii].tpTi = clean; /* clean if ok */
	}



/*	for(ii = 0; ii < count; ii++)// count is always 1 tag with array
	{
		if (taglist[ii].tpTi)
		{	
			COpcItem * OpcItem = (COpcItem *)taglist[ii].tpRt;
			if (OpcItem)
			{ 
				if (OpcItem->GetRange()>1)// (values->vt | VT_ARRAY)
				{
					//hr  = S_FALSE;
					/// array changes
					loTagValue* lovalue= OpcItem->GetLoTag();
					hr=SafeArrayCopy(values->parray, &lovalue->tvValue.parray);
					if SUCCEEDED(hr) 
					OpcItem->GetLoTag()->tvState.tsQuality= OPC_QUALITY_LOCAL_OVERRIDE;
					///lovalue->tvState.tsQuality = OPC_QUALITY_LOCAL_OVERRIDE;
				}
				else
				{
					hr=VariantChangeType(&OpcItem->GetLoTag()->tvValue, &values[ii], 0, OpcItem->GetType());
					if SUCCEEDED(hr) 					///single change
					OpcItem->GetLoTag()->tvState.tsQuality= OPC_QUALITY_LOCAL_OVERRIDE;
				}
			}
		}
		if (S_OK != hr) 
		{
			*master = S_FALSE;
			error[ii] = hr;
			
				
		}
		else
			*master = S_OK;
	}
*/
	LeaveCriticalSection(&lk_values);

	return loDW_TOCACHE; /* put to the cache all tags unhandled here */
	// loDW_ALLDONE; 
}

loTrid CMyOpcServer::ReadTags(const loCaller *ca,
							  unsigned count, loTagPair taglist[],
							  VARIANT *values, WORD *qualities,
							  FILETIME *stamps, HRESULT *errs,
							  HRESULT *master_err, HRESULT *master_qual,
							  const VARTYPE vtypes[], LCID lcid)
{
	return loDR_CACHED; /* perform actual reading from cache */
}


HRESULT CMyOpcServer::AskItemID(const loCaller *ca, loTagId *ti, 
				  void **acpa, const loWchar *itemid, 
				  const loWchar *accpath, int vartype, int goal)  /* Dynamic tags */
{
	HRESULT hr = OPC_E_INVALIDITEMID;//OK;
	//   Driver may return OPC_E_INVALIDITEMID, OPC_E_UNKNOWNITEMID, OPC_E_UNKNOWNPATH or E_FAIL when the tag cannot be created.
	//VARIANT var;
	//VariantInit(&var);  
	//V_R4(&var) = 3;     /* God likes 3 */
	//V_VT(&var) = VT_R4;
	////UL_NOTICE((LOGID, "AskItemID %ls type = %u(0x%x)", itemid, vartype, vartype));
	//if (VT_EMPTY != vartype) /* check conversion */
	//	hr = VariantChangeType(&var, &var, 0, vartype);

	//if (S_OK == hr) /* we got a value of requested type */
	//{
	//	int rv;
	//	rv =
	//		loAddRealTag_aW(ca->ca_se, ti, 0, itemid, 0,
	//		OPC_READABLE | OPC_WRITEABLE, &var, 0, 100);
	//	if (rv)
	//	{
	//		if (rv == EEXIST) *ti = 0; /* Already there? - Use existing one! */
	//		else hr = E_OUTOFMEMORY;
	//	}
	//}
	//VariantClear(&var);
	return hr;
}


//INIT OPCServer
int CMyOpcServer::driver_init(int lflags)
{
	loDriver ld;
	int ecode;
	setlocale(LC_CTYPE, "");
	if (my_service) 
	{
		TRACE("driver_init already inited\n");
		return 0;
	}
	memset(&ld, 0, sizeof(ld));   /* basic server parameters: */
	//  ld.ldRefreshRate = 3;//10;
	ld.ldSubscribe = activation_monitor;
	ld.ldWriteTags = WriteTags;
	ld.ldReadTags = ReadTags;
	ld.ldConvertTags = ConvertTags;
	//ld.ldAskItemID = AskItemID;
	ld.ldFlags = lflags | loDF_IGNCASE |  /*loDf_FREEMARSH | loDf_BOTHMODEL | */
		/*loDF_NOCOMP| */ loDf_NOFORCE & 0 /*| loDF_SUBSCRIBE_RAW*/;
	/*Fix the Bug in ProTool *//*|loDF_IGNCASE */ ;
	ld.ldBranchSep = '/'; /* Hierarchial branch separator */
	ecode = loServiceCreate(&my_service, &ld, 2500 /* number of tags in the cache */);
	/* 500000 is ok too */ 
	if (ecode){
		my_service = 0;
		TRACE("driver_init failed\n");
		return -1;
	}
	InitializeCriticalSection(&lk_values);
	InitializeCriticalSection(&lk_update);
	
	TRACE("driver_init ok\n");
	return 0;
}


void CMyOpcServer::driver_destroy(void)
{
	if (my_service)
	{
		int ecode = loServiceDestroy(my_service);
		DeleteCriticalSection(&lk_values);
		DeleteCriticalSection(&lk_update);
		
		my_service = 0;
		TRACE("driver_destroy\n");
	}
}

void CMyOpcServer::simulate(unsigned pause)
{

}


static void server_finished(void *arg, loService *b, loClient *c)
{
	my_CF.serverRemove();
}

LONG CMyOpcServer::GetClientCount(void)
{
	return  InterlockedExchangeAdd(&lk_count,0);

}

DWORD CMyOpcServer::AddTag(void * key, TCHAR * wszTag, DWORD dwWriteable, VARIANT * var)
{
	loTagId tti;
	int ecode = 0;
	if (key)
	{
		COpcItem * opcItem = (COpcItem*)key;
		loTagValue * logTag = opcItem->GetLoTag();
		if (dwWriteable==2) dwWriteable=3;
		if (var)
		{
			ecode = loAddRealTagW(my_service,     
				&tti,    /* returned TagId */
				(loRealTag)opcItem,     /* != 0 driver's key */
				wszTag,
				0,       /* loTF_ Flags */
				dwWriteable, var, 0, 0);//OPC_READABLE | OPC_WRITEABLE
		}
		else
		{
			ecode = loAddRealTagW(my_service,     
				&tti,    /* returned TagId */
				(loRealTag)opcItem,     /* != 0 driver's key */
				wszTag,
				0,       /* loTF_ Flags */
				dwWriteable, &logTag->tvValue, 0, 0);//OPC_READABLE | OPC_WRITEABLE
		}
		if (ecode)
		{
			TRACE(_T("Create Tag:%s Failed,"),wszTag);
			TRACE("Error Code=%d\n,",ecode);
		}
		else
		{
		   logTag->tvTi = tti;
		   logTag->tvState.tsError = S_OK;
		   logTag->tvState.tsQuality = OPC_QUALITY_GOOD; 
		}
	}
	return tti;

}

BOOL CMyOpcServer::IsInited(void)
{
	return (my_service!=0);
}
/*
#define    OPC_QUALITY_MASK            0xC0
#define    OPC_STATUS_MASK             0xFC
#define    OPC_LIMIT_MASK              0x03
#define    OPC_QUALITY_BAD             0x00
#define    OPC_QUALITY_UNCERTAIN       0x40
#define    OPC_QUALITY_GOOD            0xC0
#define    OPC_QUALITY_CONFIG_ERROR    0x04
#define    OPC_QUALITY_NOT_CONNECTED   0x08
#define    OPC_QUALITY_DEVICE_FAILURE  0x0c
#define    OPC_QUALITY_SENSOR_FAILURE  0x10
#define    OPC_QUALITY_LAST_KNOWN      0x14
#define    OPC_QUALITY_COMM_FAILURE    0x18
#define    OPC_QUALITY_OUT_OF_SERVICE  0x1C
#define    OPC_QUALITY_LAST_USABLE     0x44
#define    OPC_QUALITY_SENSOR_CAL      0x50
#define    OPC_QUALITY_EGU_EXCEEDED    0x54
#define    OPC_QUALITY_SUB_NORMAL      0x58
#define    OPC_QUALITY_LOCAL_OVERRIDE  0xD8
*/
void CMyOpcServer::UpDateTag(loTagValue * tag,int state)
{
		FILETIME ft;
		GetSystemTimeAsFileTime(&ft); 
		EnterCriticalSection(&lk_update);
		tag->tvState.tsTime = ft;
		tag->tvState.tsError = S_OK;
		tag->tvState.tsQuality = state; 
		loCacheUpdate(my_service, 1, tag, 0);
		LeaveCriticalSection(&lk_update);
}


void CMyOpcServer::UpDateTags(loTagValue tagList[],int count,int state)
{
		FILETIME ft;
		GetSystemTimeAsFileTime(&ft); 
		for (int i=0;i<count;i++)
		{
			tagList[i].tvState.tsTime = ft;
			tagList[i].tvState.tsQuality = state;
		}
		EnterCriticalSection(&lk_update);
		loCacheUpdate(my_service, count, tagList, 0);
		LeaveCriticalSection(&lk_update);
}


int CMyOpcServer::RegClassFactory(void)
{
	if (objid)
	{
		TRACE0("Create ClassFactory Failed, OBJID not NULL\n");
		AfxMessageBox(_T("Create ClassFactory Failed, OBJID not NULL"));
		return -2;
	}
	if 
		(FAILED(CoRegisterClassObject(CLSID_OPCServer, &my_CF,
		CLSCTX_LOCAL_SERVER |
		CLSCTX_REMOTE_SERVER |
		CLSCTX_INPROC_SERVER,
		REGCLS_MULTIPLEUSE, &objid)))
	{
		TRACE0("Create ClassFactory Failed\n");
				AfxMessageBox(_T("Create ClassFactory Failed"));

		return -1;
	}
	else
	{
		TRACE0("Success Create ClassFactory\n");
	}
	return 0;
}

void CMyOpcServer::ShutDown()
{
	if (my_service)
	{
	   loSetState(my_service, 0, loOP_SHUTDOWN, 0, "Server Shut down");
	   loSetState(my_service, 0, loOP_DISCONNECT, 0, "Server close");
	}
}