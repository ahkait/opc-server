
// LeftView.h : CLeftView
//


#pragma once

class COpcServerDoc;

class CLeftView : public CTreeView
{
protected: //
	CLeftView();
	DECLARE_DYNCREATE(CLeftView)

// 
public:
	COpcServerDoc* GetDocument();

// 
public:

// 
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); 

// 
public:
	virtual ~CLeftView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CImageList m_imageList;

//
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnNMRClick(NMHDR *pNMHDR, LRESULT *pResult);
protected:
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
public:
	afx_msg void OnUpdateServerAdd(CCmdUI *pCmdUI);
	afx_msg void OnServerEdit();
	afx_msg void OnUpdateGroupAdd(CCmdUI *pCmdUI);
	afx_msg void OnUpdateGroupEdit(CCmdUI *pCmdUI);
	afx_msg void OnServerAdd();
	afx_msg void OnUpdateServerEdit(CCmdUI *pCmdUI);
	afx_msg void OnGroupAdd();
	afx_msg void OnItemAdd();
	afx_msg void OnUpdateItemAdd(CCmdUI *pCmdUI);
	afx_msg void OnTvnSelchanged(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnGroupEdit();
	afx_msg void OnUpdateEditDelete(CCmdUI *pCmdUI);
	afx_msg void OnEditDelete();
	afx_msg void OnImport();
	afx_msg void OnUpdateImport(CCmdUI *pCmdUI);
};

#ifndef _DEBUG  // LeftView.cpp
inline COpcServerDoc* CLeftView::GetDocument()
   { return reinterpret_cast<COpcServerDoc*>(m_pDocument); }
#endif

