


/////////////////////////////////////////////////////////////////////////////////////
// Abstract base class ("interface") for the concrete plugin implementations
//
///////////////////////////////////////////////////////////////

//WM_USER+2 used by sendmessage for alarm msg
 // struct ServerComm;



class IPlugin 
{
public:
	//Add whatever functions each plugin needs to implement
	//Those below are just dummy examples to illustrate the principle

	//returns the name of the concrete plugin
	virtual LPCTSTR  Get_Name () const = 0;
	virtual LPCTSTR  GetVersion () const = 0;
	virtual bool  ShowSetup(LPCTSTR addConfile) = 0; /// const char* 
	//does the actual data processing
	virtual int Process_Data (HINSTANCE hInstance, unsigned *nTerminate,LPVOID pParam,LPVOID pMy_CF, LPVOID pUpdateFun, LPVOID pUpdateDataFun) = 0;
	virtual void GetErrorCode (LPCTSTR errorcode) = 0;

	virtual int ClientWrite(LPVOID OpcItem, VARIANT values)=0;

	virtual void SetServerComm (LPVOID psComm) = 0;

    virtual void Enable() = 0;
    virtual void Disable() = 0;
	virtual int DriverStatus()=0;
    virtual void deinit() = 0;
    virtual LPCTSTR Activation(LPCTSTR code) = 0;
    virtual void SetUpDateFun(LPVOID pUpDateFun) = 0;


};

/////////////////////////////////////////////////////////////////////////////////////
// Extern "C" functions that each plugin must implement in order to be recognized 
// as a plugin by us.

// Plugin factory function
//extern "C" IPlugin* Create_Plugin ();

// Plugin cleanup function
//extern "C" void Release_Plugin (IPlugin* p_plugin);


