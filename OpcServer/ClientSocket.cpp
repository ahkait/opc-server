// ClientSocket.cpp : 
//
#define LINES_MAXBYTE 1024
#include "stdafx.h"
#include "OpcServer.h"
#include "ClientSocket.h"
#include "OpcServerDoc.h"

// CClientSocket

CClientSocket::CClientSocket(COpcServerDoc * doc)
{
	pDoc = doc;
	m_aiCount = 0;
	m_diCount = 0;
	iCount = 0;
}

CClientSocket::~CClientSocket()
{
}


// CClientSocket 

void CClientSocket::OnReceive(int nErrorCode)
{
	
	char szbuf[LINES_MAXBYTE+1]="";
	
	int len = Receive(lpBuf+iCount,BUFFER_LEN - iCount); 
	if (len>0)
	{
		TRACE("Receive Len=%d \n",len);
		lpBuf[len] = '\0';

		char  e = lpBuf[len-1];
		if ((lpBuf[0] != '<') || ((e!=NULL) && (e!='\n') && (e!='>')))
		{
			TRACE("Need Data\n");
			return;
		}
		iCount = 0;

		//conver to lower case
		CStringA strBuf;
		strBuf = lpBuf;
		strBuf.MakeLower();
		//USES_CONVERSION;
		//LPSTR lpstr = T2A(strBuf);
		strcpy(lpBuf,strBuf.GetBuffer()); 
		
		szbuf[0] = NULL;

		char * nexttok = NULL;
		char * strline = NULL;			
		char tok[] = "\r\n";
		strline = strtok_s(lpBuf,tok,&nexttok);
		while (strline!=NULL)
		{
			if (strlen(strline)<LINES_MAXBYTE)
			{
				strcpy_s(szbuf,LINES_MAXBYTE,strline);
				ProcessLineData(szbuf);
				//Process Line				    
			}
			strline = strtok_s(NULL,tok,&nexttok);
		}					

	}
	CSocket::OnReceive(nErrorCode);
}

void CClientSocket::OnClose(int nErrorCode)
{
	
	TRACE("Closed\n");
	CSocket::OnClose(nErrorCode);
}

void CClientSocket::GetTorken(char * strKey,char Key[])
{	
	char * p = strKey;
	int i=0;
	while (*p)
	{
		if (*p=='<')		
			p++;
		else if (*p==0x20)		
		{
			if (i==0)			
				p++;
			else
			{
				Key[i] = 0;
				break;
			}
		}
		else {
			Key[i++] = *p++;
			if (i==LINES_MAXBYTE) {Key[0]=0;break;}
		}
	}
}

CStringA CClientSocket::GetXMLNodeItem(char * strSource, char * strNodeItem)
{
	CStringA strReslut;
	char * p = strstr(strSource,strNodeItem);	
	if (p)
	{
		bool bFirst = false;
		p += strlen(strNodeItem);
		while (*p)
		{
			if (!bFirst && *p==0x20)			
				p++;
			else if (*p == '\"')
			{
				if (!bFirst)				
					bFirst = true;
				else
					break;
			}
			else
				strReslut += *p;
			p++;
		}
	}	
	return strReslut;
}

//
void CClientSocket::ProcessLineData(char * strInput)
{		
	char torken[LINES_MAXBYTE+1] = "";
	GetTorken(strInput,torken);
	if (strcmp(torken,"login")==0)
	{
		char buf[100];
		sprintf(buf,"<login user=\"%s\" password=\"%s\" />","user","password");
		Send(buf,strlen(buf));
	}
	if (strcmp(torken,"loginok")==0)
	{
		 char  buf[50];
		 strcpy(buf,"<tags />\r\n");
		 Send(buf,strlen(buf));
	}
	else if (strcmp(torken,"aicount")==0)
	{
		CStringA str = GetXMLNodeItem(strInput,"c=");
		m_aiCount = atoi(str.GetBuffer()); 
		pDoc->SetAiCount(m_aiCount);
		//Set AiCount
	}
	else if (strcmp(torken,"ai")==0)
	{		
		CStringA strStart = GetXMLNodeItem(strInput,"s=");
		CStringA strData = GetXMLNodeItem(strInput,"d=");
		int index = atoi(strStart.GetBuffer()); 
		CString strTag;
		int curPos = 0;
        CStringA resToken= strData.Tokenize(";",curPos);
        while (resToken != "")
        {
			strTag = resToken;
			pDoc->AddTag(strTag,index);
			index++;
            resToken = strData.Tokenize(";", curPos);
        }
	}
	else if (strcmp(torken,"dicount")==0)
	{
		CStringA str = GetXMLNodeItem(strInput,"c=");
		m_diCount = atoi(str.GetBuffer()); 
		pDoc->SetDiCount(m_diCount);
		//Set DiCount
	}
	else if (strcmp(torken,"di")==0)
	{
		CStringA strStart = GetXMLNodeItem(strInput,"s=");
		CStringA strData = GetXMLNodeItem(strInput,"d=");
		int index = atoi(strStart.GetBuffer()); 
		CString strTag;
		int curPos = 0;
        CStringA resToken= strData.Tokenize(";",curPos);
        while (resToken != "")
        {
			strTag = resToken;
			pDoc->AddTag(strTag,index,FALSE);
			index++;
            resToken = strData.Tokenize(";", curPos);
        }
	} //Tag Name End
	else if (strcmp(torken,"/tags")==0)
	{
		char buf[100];
		strcpy(buf,"<start />\r\n");
		Send(buf,strlen(buf));
	}
	else if (strcmp(torken,"av")==0)
	{
		CStringA strStart = GetXMLNodeItem(strInput,"s=");
		CStringA strData = GetXMLNodeItem(strInput,"v=");
		int index = atoi(strStart.GetBuffer()); 
		float fData = 0.0f;
		int curPos = 0;
		CStringA strValue = strData.Tokenize(";",curPos); 
		while (strValue!="")
		{
			fData = atof(strValue.GetBuffer());
			pDoc->UpDate(index,fData);
			index++;
			strValue = strData.Tokenize(";",curPos); 			
		}
	}
	else if (strcmp(torken,"dv")==0)
	{
		CStringA strStart = GetXMLNodeItem(strInput,"s=");
		CStringA strData = GetXMLNodeItem(strInput,"v=");
		int index = atoi(strStart.GetBuffer()); 
		bool bData = false;
		int curPos = 0;
		for (int i=0;i<strData.GetLength();i++)
		{
			bData = (strData[i]=='1');
			pDoc->UpDate(index,bData);
			index++;
		}
	}
}
/*

// 
 BOOL WINAPI CreateProcess(
   __in_opt     LPCTSTR lpApplicationName,
   __inout_opt  LPTSTR lpCommandLine,
   __in_opt     LPSECURITY_ATTRIBUTES lpProcessAttributes,
   __in_opt     LPSECURITY_ATTRIBUTES lpThreadAttributes,
   __in         BOOL bInheritHandles,
   __in         DWORD dwCreationFlags,
   __in_opt     LPVOID lpEnvironment,
   __in_opt     LPCTSTR lpCurrentDirectory,
   __in         LPSTARTUPINFO lpStartupInfo,
   __out        LPPROCESS_INFORMATION lpProcessInformation
 );
 
 //
 DWORD WINAPI WaitForInputIdle(
   __in  HANDLE hProcess,
   __in  DWORD dwMilliseconds
 );
 Return code/value Description 
 0
  The wait was satisfied successfully.
  
 WAIT_TIMEOUT
  The wait was terminated because the time-out interval elapsed.
  
 WAIT_FAILED
  An error occurred.
  
 
 // ?????
         STARTUPINFO startup;
         PROCESS_INFORMATION procinfo;
         ::ZeroMemory(&startup, sizeof(startup));
         startup.cb = sizeof(startup);
         startup.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
         startup.wShowWindow = SW_HIDE; // hidden console window
         startup.hStdInput = NULL; // not used
         startup.hStdOutput = hPipeOutW;
         startup.hStdError = hPipeErrW;
 
         BOOL started = ::CreateProcess(NULL,        // command is part of input string
                         _command,         // (writeable) command string
                         NULL,        // process security
                         NULL,        // thread security
                         TRUE,        // inherit handles flag
                         CREATE_SUSPENDED,           // flags
                         NULL,        // inherit environment
                         _curDir,        // inherit directory
                         &startup,    // STARTUPINFO
                         &procinfo);  // PROCESS_INFORMATION
 if(!started)
 {
        error(TEXT("CreateProcess"), result, 1002);
 }
 else
 {
        if (WaitForInputIdle(pinfo.hProcess, 30000) == 0)  
        {
              // ......
        }
 }
 
              CloseHandle(procinfo.hThread);  
              CloseHandle(procinfo.hProcess);



*/
/*
char *hexToAscii (char *srcHex, int *destElements, int srcElements)
{
    int        index, destIndex;
    unsigned char    valUpper, valLower, *ascArray;
    int        lowerFlag;
    
    lowerFlag = 0;
    ascArray = NULL;
    *destElements = srcElements/2;
    ascArray = (char *)malloc (*destElements);
    if (ascArray == NULL) {
        return NULL;
    }
    for (destIndex = 0; destIndex < *destElements; destIndex++) {
        //  Every hex value will be 2 nibbles long and will take up 2 character
        //  space in the character array (Input)
        for (index = 0; index < 2; index++) {
            if (lowerFlag == 0) {
                if (srcHex[(destIndex * 2) + index] >= '0' && 
                    srcHex[(destIndex * 2) + index] <= '9') {
                    valUpper = srcHex[(destIndex * 2) + index] - '0';
                } else if (tolower (srcHex[(destIndex * 2) + index]) >= 'a' || 
                           tolower (srcHex[(destIndex * 2) + index]) <= 'f'){
                    valUpper = tolower(srcHex[(destIndex * 2) + index]) - 'a' + 10;
                }
                //  The first character represent the upper nibble (upper 4 bits)
                //  of the hex value. Shift the converted value left 4 bits
                valUpper <<= 4;
                lowerFlag = 1;
            } else {
                if (srcHex[(destIndex * 2) + index] >= '0' && 
                    srcHex[(destIndex * 2) + index] <= '9') {
                    valLower = srcHex[(destIndex * 2) + index] - '0';
                } else if (tolower (srcHex[(destIndex * 2) + index]) >= 'a' || 
                           tolower (srcHex[(destIndex * 2) + index]) <= 'f'){
                    valLower = tolower (srcHex[(destIndex * 2) + index]) - 'a' + 10;
                }  
                lowerFlag = 0;
           }
    }
       //  ANDing upper and lower values will get the proper 8 bit ASCII converted val
       ascArray[destIndex] = valUpper | valLower;
    }
    return ascArray;
}
*/
