#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
#include <time.h>
#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <wchar.h>
#include <stdarg.h>

#include "../unilog/unilog.h"
#include "lcard_ad.h"
#include "../dll/error.h"

#include "lc_drv.h"

#define slot451	(7)
#define slot403 (8)
#define slot402	(9)
#define	slot111	(10)
#define Slot104	(12)	// 11 12 13 14
#define slot301	(15)
#define FIFOSIZE (8*1024)

/**************************************
***************************************/

	// cloking
	//
	// 50 ��� - ��������������� �������� ����� � ������ �.�.
	// 50 ���. �������� � �������, 50 �������� � �����������
	// 20 ��� ������������ �������� ��� �.�.

	// ��������� ���������� - ��� ��������� ����������� ���� 1%
	// ���� ������� ����� 22,5 ��������� ���������, �������� 25
	// ��������� ��������� ������ ������������ ������� ������ 16 ��.
	// �� ��� ����� ����� �������� 25 �������� � 16-�� �������
	// ��� ������������ �������� 40 ���. 16=25*16*0,04
	// ��� ��������� ���������� ����� ������� ������������� �������� 
	// ��������� �������� ��������� ���������� � ������� �������������� 
	// ����� �������
	//
// N111 is 16
#define N111	4
#define N111c	4
#define N111t	(N111*N111c)
#define N111n	(50)					// ���� ����� 360/16 - ����������� ���� ����� 1%
#define N111v	(N111*N111n)			// ���� ����� 16*25=400

#define N111d	(35) // 100 // (150)			// ���
#define N111r	(500) // (N111d*N111+125)
	
	short chans111[2*N111], vals111[N111v], result111[N111t], osc111[N111t][N111n];
	int reset111[N111t];
	int c111=0;

	// 104
	// ��������� ������������� - ��� ��������� ������������� ����� ���������
	// ���������� �������� ����� 950 ��.
	// �� ��� ����� ��������� 8 ����� ������ ��������� ������������� � 4-�
	// ������� ������� ���� (��� ���������� ������� �� ������)
	// ������ ����� ������ 120 ��., �.�. 8 ����� �� 120 �������� 950 ��.
	// ����������� ������������ �������� ���������� 20 ���, �.�.
	// �� 120 �� ����� ��������� 6000 ��������� ��� 6000/4=1500 ���������
	// ��� ������ ������
	//
// N104 is 4
#define N104	3
#define N104c	8
#define N104t	(N104*N104c)
#define N104n	200 // 100 // (200)
#define N104v	(N104*N104n)
	short chans104[2*N104], vals104[N104v], result104[N104t], *res, osc104[N104t][N104n];
	double desp104[N104t], *desp;
	int c104=0;

#define N104d	150 // 200 // (150)	// ���
#define N104r	500 // 1000 //(500)	// ��� N111d*N111+...

#define TC_UP	1920 //1920	// T=0.23(code-1018)
#define TC_DOWN	700 //1018	// R=0.05208*code R=code/19.2
#define TC_DLT	300
#define GAIN (3<<3)

#if tcN != N104t
#error tcN must be quiv with N104t
#endif

DWORD start;

/**************************************
***************************************/

short tcNused=0;
struct tcItem tcItemCfg[tcN];
short tcIndex[tcN];

short mode451=1;
short scale451=199;
short mask451=0;
short hzNused=0;
struct hzItem hzItemCfg[hzN];
short hzIndex[hzN];
double hzVal[hzN];

struct oneItem oneItemCfg[oneN];
struct twoItem twoItemCfg[twoN];

union ltcUnit unit;
short outBinary=0;
short ExitTimer=-1, StopTimer=-1;
short ExitTimerF=0, StopTimerF=0;
DWORD Clock;
WORD Tiker;

int iChanged[unitS];
int nChanged;
int *pChanged;

static WORD *BiosLCI01=NULL, *BiosLC010=NULL, *BiosLC451=NULL;

/******************************************************/

int EH=0;
#define ehCANCEL	100
#define ehCONTINUE	102
#define ehDIALOG	0

static int TError myErrorHandler(DWORD code, DWORD Info)
{
	DWORD *info=(DWORD *)Info;
	extern char *myEHS(DWORD code);
	EH=1;
	switch(code) {
	case errTimeout:
		UL_ERROR((LOGID,"ErrorHandler: Timeout Error (Info1=0x%lx Info2=0x%lx)",info[0],info[1]));
		break;
	case errDeviceIoControl:
		UL_ERROR((LOGID,"ErrorHandler: Device IO Control Error (%s) Info=%ld",myEHS(info[1]),info[0]));
		break;
	default:
		UL_ERROR((LOGID,"ErrorHandler: code=0x%lx info1=%ld info2=%ld", code, info[0], info[1]));
		break;
	}
	return ehCONTINUE;
}

/********************************************/

static int getBios(WORD **mem, char *name)
{
	struct _stat st;
	int f;
	size_t i, t;
	char *cp;
		
	name=fullName(name);
	if(*mem) { UL_WARNING((LOGID,"getBios: %s already load",name)); return 0; }
	if(-1==(f=_open(name,_O_RDONLY | _O_BINARY))) {	UL_ERROR((LOGID,"%!E getBios: Can't open %s - ", name)); return 1;	}
	if(_fstat(f,&st)==-1) {	UL_ERROR((LOGID,"%!E getBios: Can't fstat %s - ", name)); return 1;	}
	t=st.st_size+20;
	if(NULL==(cp=(char *)malloc(t))) { UL_ERROR((LOGID,"getBios: loading %s - out of mem",name)); return 1; }
	memset(cp,0,t); *mem = (WORD *) cp;
	for(i=st.st_size; i && t; i -= t, cp += t) if(-1==(t=_read(f,cp,i))) {
		UL_ERROR((LOGID,"%!E getBios: reading %s - ", name));
		return 1;
	}
	if(i!=0) { UL_ERROR((LOGID,"getBios:  %s reading - unexpected EOF",name)); return 1;}
	return 0;
}

/******************************************************/

char Msgs[msgN][MsgLenW];
int msgUp=0, msgCount=0, msgPut=0;

static void PutMsg(char *fmt,...)
{
	va_list varg;
	char cp[4096];

	memset(Msgs[msgPut], 0, MsgLenW);
	va_start(varg, fmt);
	vsprintf(cp,fmt,varg);
	cp[MsgLenW-2]='\0';
	strcpy(Msgs[msgPut],cp);
	msgPut++; msgPut &= msgN-1; msgCount++;
}

/********************************************************/

#define VNlev (3)
#define Vl1 (600)
const struct {
	const short lo, hi;
} Vlevels[VNlev] = { { 0, Vl1}, {400,2100}, {2000,3000}};

short Vlo=0, Vhi=Vl1, Vchanged=1;

#define LoRotateDam (5)	// rotate pro minute

void checkV(int V)
{
	static int Vlev=0, Vprev=0;
	static int Operation=0;
	static time_t RotateStart=0;

	if(Vprev == V) return;
	Vprev=V;

	if(RotateStart==0 && V > LoRotateDam ) {
		char now[40];
		RotateStart=time(NULL)+1; // delay for message send
		if(0==strftime(now,sizeof(now),"%c",localtime(&RotateStart))) now[0]='\0';
		Operation++;
		TagSet(tStartC,Operation);
		TagTime(tTime+timeStart, &RotateStart);
		TagClr(tStopSh);
		PutMsg("���� %d - ����� %s",Operation,now);
	} else if(V==0 && RotateStart) {
		char now[40];
		time_t RotateStop=time(NULL)+1; // delay for messages send
		int total=RotateStop-RotateStart;
		int hours=total/3600, sec=total-hours*3600, min=sec/60;
		if(0==strftime(now,sizeof(now),"%c",localtime(&RotateStop))) now[0]='\0';
		TagTime(tTime+timeStop, &RotateStop);
		TagSet(tStopSh,1);
		PutMsg("���� %d - �������� %s ����� %0d:%02d:%02d",Operation,now,hours,min,sec-min*60);
		RotateStart=0;
	}

	if(V > Vlevels[Vlev].hi && Vlev < (VNlev-1)) Vchanged=1;
	else if (V < Vlevels[Vlev].lo && Vlev > 0) Vchanged=-1;
	else return;

	if(Vchanged) {
		Vlev += Vchanged;
		Vhi=Vlevels[Vlev].hi;
		Vlo=Vlevels[Vlev].lo;
	}
}

struct scaleItem scaleItemCfg[GrafTotal] = {
	{"ScaleV", 0, 100, 5, 10, 3, 3, 0, 0, 250, 0, 0, 0},
	{"ScaleN", 0, 100, 5, 10, 3, 3, 0, 0, 250, 0, 0, 0},
	{"ScaleS", 0, 100, 5, 10, 3, 3, 0, 0, 250, 0, 0, 0},
	{"ScaleW", 0,  60, 5, 10, 3, 3, 0, 0, 250, 0, 0, 0},
	{"ScaleC", 0, 140, 5, 10, 3, 3, 0, 0, 250, 0, 0, 0}
};

void scaleCalc(int val, int type)
{
	struct scaleItem *item=&scaleItemCfg[type];

	if(item->used == 2) {
		if(val < item->Min) item->Min=val;
		if(val > item->Max) item->Max=val;
	}
}

void scaleUpdate(void)
{
	int i;
	struct scaleItem *item=scaleItemCfg;

	for(i=0; i < GrafTotal; i++, item++) if(item->used==2) {
		int d1=item->DynamicHi - item->Max;
		int d2=item->Min - item->DynamicLo;
		int d;

		if(d1 < 0) d1=-d1; if(d2 < 0) d2=-d2;
		if(d2 < d1) { d=d1; d1=d2; d2=d1; }
		if(d1 < item->BandMin || d2 > item->BandMax) {
			item->Changed=1;
			item->DynamicHi = item->Max + item->BandMin + item->StubHi;
			item->DynamicHi += 5;item->DynamicHi /= 10;item->DynamicHi *= 10;
			item->DynamicLo = item->Min - item->BandMin - item->StubLo;
			if(item->DynamicLo > 0) {
				item->DynamicLo /= 10;item->DynamicLo *= 10; 
			} else item->DynamicLo=0;
		}
		UL_DEBUG((LOGID, "scaleUpdate() - graf %s min=%d max=%d scale %d:%d", 
			item->name, item->Min, item->Max, item->DynamicLo, item->DynamicHi));
		item->Max = 0;
		item->Min = 250;
	} else {
		if(item->DynamicHi != item->StaticHi) {
			item->DynamicHi = item->StaticHi;
			item->Changed=1;
		}
		if(item->DynamicLo != item->StaticLo) {
			item->DynamicLo = item->StaticLo;
			item->Changed=1;
		}
		if(item->used) item->used=2, item->Max=0, item->Min=250;
	}
}


/******************************************************/
static int setIfneedTC(short val, int index, struct gItem gi)
{
	short ind=2*index, *w1=&unit.data[ind], *w2=w1+1;

	if(gi.min != C_NONE && val < gi.min) val = gi.min;
	else if(gi.max != C_NONE && val > gi.max) val=gi.max;
	if(*w1 != val) {
//		UL_DEBUG((LOGID, "setIfneedTC() - new value #%d with index %d val=%d", nChanged, ind+1, val));
		*w1 = val;
		*pChanged++=ind;
		nChanged++;
		if(gi.ph != C_NONE && val > gi.ph)
			if(*w2 != LIM_PH) { 
				char *name=strrchr(ts[ind],'/');
				PutMsg("%s - ������� ������� ��������� �������", name ? name+1 : ts[ind]);
				*w2=LIM_PH; goto changed; } else goto ret;
		if(gi.wh != C_NONE && val > gi.wh)
			if(*w2 != LIM_WH) {
				char *name=strrchr(ts[ind],'/');
				PutMsg("%s - ��������� � ������� ������������� ���������", name ? name+1 : ts[ind]);
				*w2=LIM_WH; goto changed; } else goto ret;
		if(gi.pl != C_NONE && val < gi.pl)
			if(*w2 != LIM_PL) {
				char *name=strrchr(ts[ind],'/');
				PutMsg("%s - ������� ������ ��������� �������", name ? name+1 : ts[ind]);
				*w2=LIM_PL; goto changed; } else goto ret;
		if(gi.wl != C_NONE && val < gi.wl)
			if(*w2 != LIM_WL) { 
				char *name=strrchr(ts[ind],'/');
				PutMsg("%s - ��������� � ������ ������������� ���������", name ? name+1 : ts[ind]);
				*w2=LIM_WL; goto changed; } else goto ret;
		if(*w2 != LIM_NRM) { 
				char *name=strrchr(ts[ind],'/');
				PutMsg("%s - ��������� � ���������� ���������", name ? name+1 : ts[ind]);
				*w2=LIM_NRM; goto changed; }
		goto ret;
changed:
//		UL_DEBUG((LOGID, "setIfneedTC() - new value #%d with index %d (%s) val=%d",
//			nChanged, ind+1, ts[ind+1], *w2));
		*pChanged++=ind+1;
		nChanged++;
	}
ret:
	return val;
}
#define setIfneedHZ(x,y,z)	setIfneedTC(x,(y+tcN),z)

static int setIfneedU(short val, int index)
{
	short ind=2*(tcN+hzN+index), *w1=&unit.data[ind], *w2=w1+1;

	if(*w1 != val) {
//		UL_DEBUG((LOGID, "setIfneedU() - new value #%d with index %d (%s) val=%d",
//			nChanged, ind, ts[ind], val));
		*w1 = val;
		*pChanged++=ind;
		nChanged++;
		return 1;
	}
	return 0;
}

static int setIfneedIB(short val, int index)
{
	short ind=2*(tcN+hzN+uN)+index, *w1=&unit.data[ind];

	if(*w1 != val) {
//		UL_DEBUG((LOGID, "setIfneedIB() - new value #%d with index %d (%s) val=%d",
//			nChanged, ind, ts[ind], val));
		*w1 = val;
		*pChanged++=ind;
		nChanged++;
		return 1;
	}
	return 0;
}

/******************************************/
#if 0
static void incSkip(int delta)
{
	unit.p.sp[spSkip] += (short)delta;
	*pChanged++=2*(tcN+hzN)+ibN+spSkip;
	nChanged++;
}

static void incOver(void)
{
	unit.p.sp[spOverload] ++;
	*pChanged++=2*(tcN+hzN)+ibN+spOverload;
	nChanged++;
}

static void calcSP(DWORD xx)
{
	int index=2*(tcN+hzN+uN)+ibN+spWait;

	if((short)xx > unit.p.sp[spWait]) {
		unit.p.sp[spWait]= (short)(0xffff & xx);
		*pChanged++=index+1;
		nChanged++;
	}
	if(Tiker >= (8*60*60)) { // about 8 hours
		int hour=Clock/(3600*1000);
		int sec=(Clock/1000-hour*3600);
		int msec=Clock-1000*sec;
		int mnt=sec/60;
		UL_INFO((LOGID, "Tiker - ����� ���� ����� ���������� ����������� ������"));
		PutMsg("������ %0d:%02d:%02d.%d - ������ ������������ ...",hour,mnt,sec-60*mnt,msec);
		Tiker=1;
	} else
		Tiker++;
	if(StopTimer > 0) StopTimer--;
	if(ExitTimer > 0) ExitTimer--;
}
#endif

/**********************************************************/

static void tcCalc(void)
{
	int i, j, k;
	double sum;
	struct tcItem *p;

	for(i=0; i<tcNused; i++) {
		k=tcIndex[i]; p=&tcItemCfg[k];
		j = p->chan+8*(p->slot104-Slot104);
		if(j<0 || j >= N104t) continue;
		sum = 100.*5.12 / (3e-3*2048.*(double)(1<<p->gain) *(double)(p->I0));
		j=(int)(0.5+ (double)result104[j]*sum - (double)p->RD);
// not used now		if(p->mode) {i++;j -=(int)(0.5+ (double)tcVal[i]*sum);}
//		UL_DEBUG((LOGID, "tcCalc() - R for #%d tc%d is %d", i, tcIndex[i], j));
		sum = (double)p->alpha*1e-6*100.*(double)p->R0;
		sum = ((double)j-100.*(double)p->R0)/sum;
		sum += 0.5 + (double)p->TD;
//		UL_DEBUG((LOGID, "tcCalc() - T for #%d tc%d is %d", i, tcIndex[i], (int)sum));
		scaleCalc(setIfneedTC((short)sum,k,p->gi),p->graf);
	}
	scaleUpdate();
}

static void hzCalc(void)
{
	int i,k;
	double sum;
	struct hzItem *p;

	for(i=0; i<hzNused; i++) {
		k=hzIndex[i]; p=&hzItemCfg[k];
		sum = 0.5 + hzVal[k]*60.*(double)(p->multi)/(double)(p->divid);
		if(k==0) checkV(setIfneedHZ((short)sum,k,p->gi));
		else setIfneedHZ((short)sum,k,p->gi);
	}
}

static void uvCalc(void)
{
	int i; double val;
	for(i=0; i<N111t; i++) {
		val = 0.5 + 10.*(double)result111[i]*5.12*2./1.41421/2048.;
//		UL_DEBUG((LOGID, "uvCalc() - U for #%d is %f", i, val));
		setIfneedU((short)val,i);
	}
}

static void ibCalc(DWORD mask)
{
	int i;

	// �������� ��������� ���� ��� ��������������� ��������
	mask &= ~(
				(1<<22)  // �������� ������������
			);
#if 0
				(1<<12) | // ��������� �����
				(1<<15) | // ���������� 1
				(1<<17) | // ���������� 2
				(1<<19) | // ���������� 3
				(1<<20) | // �������� ���������� 3
				(1<<22) | // �������� ������������
				(1<<23) | // ��������� 1
				(1<<25) ); // ��������� 2
#endif

	for(i=0; i < oneN; i++) {
		int ii = ((1l<<oneItemCfg[i].number) & mask)? 1 : 0;
		if(setIfneedIB(oneItemCfg[i].value[ii], i))	PutMsg(oneItemCfg[i].msg[ii]);
	}
	for(i=0; i < twoN; i++) {
		int ii = ((1l<<twoItemCfg[i].p.w[1]) & mask)? 1 : 0;
		ii <<= 1;
		ii |= ((1l<<twoItemCfg[i].p.w[0]) & mask)? 1 : 0;
		if(setIfneedIB(twoItemCfg[i].value[ii], i+oneN)) PutMsg(twoItemCfg[i].msg[ii]);
	}
}

/***********************************************************/

static void clrChanged(void)
{
	nChanged=0; pChanged=iChanged;
	memset(iChanged,0,sizeof(iChanged));
}

static int tcInit(void)
{
	struct tcItem *p;
	int i;

	tcNused=0;
	for(i=0, p=tcItemCfg; i< tcN; i++, p++) {
		if(p->gi.used) tcIndex[tcNused++]=i;
	}
	
	UL_DEBUG((LOGID, "tcInit() Ok"));
	return 0;
}

static int hzInit(void)
{
	short InfArray[16];
	int i,j;
	struct hzItem *p;

	if(BiosLC451==NULL && getBios(&BiosLC451,"lc_451.bio")) {
		UL_ERROR((LOGID, "hzInit() - Can't open lc_451.bio"));
		return -1;
	}
	if(LOAD_BIOS_LC451(slot451, BiosLC451)) {
		UL_ERROR((LOGID, "hzInit() - LOAD_BIOS_LC451() error"));
		return -1;
	}
	SET_CALIBRATION_LC227(slot451);
	SET_IRQ_MASK_CC(1<<slot451);
	SET_LC227DATA_IRQSET_CC();
	memset(InfArray,0,sizeof(InfArray));
	InfArray[slot451]=451;
	SET_IRQ_INFOM_CC(slot451+1,InfArray);
	for(i=0; i< hzNused; i++) {
		j=hzIndex[i]; p=&hzItemCfg[j];
#define LO_THRESHOLD 0
#define HI_THRESHOLD 1
		SET_THRESHOLD_LC451(slot451,p->chan,LO_THRESHOLD,p->lo);
		SET_THRESHOLD_LC451(slot451,p->chan,HI_THRESHOLD,p->hi);
#undef LO_THRESHOLD
#undef HI_THRESHOLD
	}
	CONFIG_LC451(slot451,mode451,scale451);
	UL_DEBUG((LOGID, "hzInit() Ok"));
	return 0;
}

static void unitInit(void)
{
	int i;

	memset(&unit,0,sizeof(unit));
	Tiker=0;
	ExitTimer=StopTimer=-1;
	clrChanged();
	for(i=0; i< tcN; i++) { 
		unit.p.tc[i][0]=tcItemCfg[i].gi.init;
		if(tcItemCfg[i].gi.min != C_NONE && unit.p.tc[i][0] < tcItemCfg[i].gi.min)
			unit.p.tc[i][0] = tcItemCfg[i].gi.min;
		else if(tcItemCfg[i].gi.max != C_NONE && unit.p.tc[i][0] > tcItemCfg[i].gi.max) 
			unit.p.tc[i][0] = tcItemCfg[i].gi.max;
		if(tcItemCfg[i].gi.ph != C_NONE && unit.p.tc[i][0] > tcItemCfg[i].gi.ph)
			unit.p.tc[i][1]=LIM_PH;
		else if(tcItemCfg[i].gi.wh != C_NONE && unit.p.tc[i][0] > tcItemCfg[i].gi.wh)
			unit.p.tc[i][1]=LIM_WH;
		else if(tcItemCfg[i].gi.pl != C_NONE && unit.p.tc[i][0] < tcItemCfg[i].gi.pl)
			unit.p.tc[i][1]=LIM_PL;
		else if(tcItemCfg[i].gi.wl != C_NONE && unit.p.tc[i][0] < tcItemCfg[i].gi.wl)
			unit.p.tc[i][1]=LIM_WL;
		else 
			unit.p.tc[i][1]=LIM_NRM;
	}
	for(i=0; i< hzN; i++) unit.p.hz[i][0]=hzItemCfg[i].gi.init;
	for(i=0; i< oneN; i++) unit.p.ib[i]=oneItemCfg[i].init;
	for(i=0; i< twoN; i++) unit.p.ib[i+oneN]=twoItemCfg[i].init;
}

int ltcInit(void)
{
	int rc,i;

	SET_ERROR_HANDLER(myErrorHandler);
	READ_CALIBR_CC(); SET_CALIBR_MODE_CC(CALIBR_ALL);
	CONFIG_FIFO_CC(2,2); SET_FIFO_SIZE_CC(FIFOSIZE);
	if(EH) {UL_ERROR((LOGID,"Check ErrorHandler code. line %d",__LINE__)); return 1; }

	start=GetTickCount();

	UL_DEBUG((LOGID,"begin (compiled at %s %s) n104=%d 104d=%d 104r=%d n111=%d 111d=%d 111r=%d\n",
		__DATE__, __TIME__, N104, N104d, N104r, N111, N111d, N111r));

	if((rc=tcInit()) || (rc=hzInit())) return rc;
	unitInit();
	OUT_DIGITS_402(slot402, outBinary);
	
	for(i=0; i<N111; i++) {	chans111[2*i]=slot111; chans111[2*i+1]=i;}
	for(i=0; i<N104; i++) {	chans104[2*i]=Slot104+i; chans104[2*i+1]=GAIN; }

	UL_DEBUG((LOGID, "ltcInit() Ok"));
	return 0;
}

/*********************************************/
static void gItemInit(struct gItem *gi)
{
	gi->pl=gi->wl=gi->wh=gi->ph=gi->min=gi->max=C_NONE;
	gi->init=gi->used=0; 
}

static int tcLoadConfig(void)
{
	int i;
	struct tcItem *p=tcItemCfg;
	struct gItem *gi;

	for(i=0; i<tcN; i++, p++) {
		gItemInit(&p->gi);
		sprintf(p->name,"df%d",i);
		p->gain=3;
		p->mode=0;
		p->I0=2; p->R0=53; p->alpha=4260;
		p->RD=p->TD=0;
	}
		
	/*  */
	p=&tcItemCfg[0];
	strcpy(p->name,"tc2");
	p->slot104=14; p->chan=i=0;
	gi=&p->gi; gi->used=1;
	gi->wh=110; gi->ph=120; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=130; gi->init=gi->min; 
	p->graf = GrafC;

	/*  */
	p++; strcpy(p->name,"tc3");
	p->slot104=14; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=110; gi->ph=120; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=130; gi->init=gi->min; 
	p->graf = GrafC;

	/*  */
	p++; strcpy(p->name,"tc4");
	p->slot104=14; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=110; gi->ph=120; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=130; gi->init=gi->min; 
	p->graf = GrafC;

	/*  */
	p++; strcpy(p->name,"tc6");
	p->slot104=14; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=110; gi->ph=120; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=130; gi->init=gi->min; 
	p->graf = GrafC;

	/*  */
	p++; strcpy(p->name,"tc11");
	p->slot104=14; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=110; gi->ph=120; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=130; gi->init=gi->min; 
	p->graf = GrafC;

	/*  */
	p++; strcpy(p->name,"tc29");
	p->slot104=14; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=75; gi->ph=80; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafV;

	/*  */
	p++; strcpy(p->name,"tc34");
	p->slot104=14; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=75; gi->ph=80; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafV;

	/*  */
	p++; strcpy(p->name,"tc37");
	p->slot104=14; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=75; gi->ph=80; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafV;

	/*  */
	p++; strcpy(p->name,"tc42");
	p->slot104=13; p->chan=i=0;
	gi=&p->gi; gi->used=1;
	gi->wh=75; gi->ph=80; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafV;

	/*  */
	p++; strcpy(p->name,"tc-1H");
	p->slot104=13; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=70; gi->ph=75; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafN;

	/*  */
	p++; strcpy(p->name,"tc-1C");
	p->slot104=13; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=70; gi->ph=75; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafS;

	/*  */
	p++; strcpy(p->name,"tc-2H");
	p->slot104=13; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=70; gi->ph=75; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafN;

	/*  */
	p++; strcpy(p->name,"tc-2C");
	p->slot104=13; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=70; gi->ph=75; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafS;

	/*  */
	p++; strcpy(p->name,"tc-4H");
	p->slot104=13; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=70; gi->ph=75; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafN;

	/*  */
	p++; strcpy(p->name,"tc-3C");
	p->slot104=13; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=70; gi->ph=75; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafS;

	/*  */
	p++; strcpy(p->name,"tc-3H");
	p->slot104=13; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=70; gi->ph=75; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafN;

	/*  */
	p++; strcpy(p->name,"tc-4C");
	p->slot104=12; p->chan=i=0;
	gi=&p->gi; gi->used=1;
	gi->wh=70; gi->ph=75; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=90; gi->init=gi->min; 
	p->graf = GrafS;

	/*  */
	p++; strcpy(p->name,"14-TC");
	p->slot104=12; p->chan=++i;
	gi=&p->gi; gi->used=1;
	gi->wh=28; gi->ph=35; gi->pl=gi->wl=C_NONE;
	gi->min=0; gi->max=50; gi->init=gi->min; 
	p->graf = GrafW;

	UL_TRACE((LOGID, "tcLoadConfig() Ok"));
	return 0;
}

static int hzLoadConfig(void)
{
	int i;
	struct hzItem *p=hzItemCfg;
	
	for(i=0; i<hzN; i++,p++) {
		gItemInit(&p->gi);
		p->divid=p->multi=1;
		sprintf(p->name,"hz%d",i);
	}
		
	/*  hz?    */
	i=0;
	p=&hzItemCfg[i];
	strcpy(p->name, "V");
	p->gi.used=1; p->chan=0; p->divid=6;
#define GetHOLD451(x)   (((5000+((x)>5000 ? 5000: (x)<-5000 ? -5000 : (x)))*255)/10000)
	/* GetHOLD451 128 - 0 volt , 25 - 1 volt */
	p->lo=GetHOLD451(1000); p->hi=GetHOLD451(3000);
#undef GetHOLD451
	mask451 |= (1<<i);
	hzIndex[hzNused++]=i;

	UL_TRACE((LOGID, "hzLoadConfig() Ok"));
	return 0;
}

static int biLoadConfig(void)
{
	int i, j;
	struct oneItem *p1=oneItemCfg;
	struct twoItem *p2=twoItemCfg;
	char *tmpStr[]= { "prImax", "prDifL", "prDifW", "prIback", "prExt", "prRotor", "prGrn", ""};
	char *tmpStr1[]= {
		"������������ �������","����. ����������","����. ����������","��� �����. ������.",
			"�������", "����� �� �����", "�.�. �� �����", ""};
	char **cp=tmpStr, **cp1=tmpStr1;

	j=0;
	/* ekm & generator preserve */
	for(i=0; i < prN; i++, j++, p1++) {
		p1->number=j;
		p1->value[0]=0;
		p1->value[1]=1;
		p1->init=-1;
		if(i < prNekm) {
			p1->value[0]=1;
			p1->value[1]=0;
			sprintf(p1->name,"EKM%d",i+1);
			sprintf(p1->msg[0],"���%d - ���� ��������",i+1);
			sprintf(p1->msg[1],"���%d - ��� ��������",i+1);
		} else if(**cp != '\0') {
			p1->value[0]=1;
			p1->value[1]=0;
			strcpy(p1->name,*cp++);
			sprintf(p1->msg[1],"����. ����. ������ ���������� '%s'",*cp1);
			sprintf(p1->msg[0],"��������� ������ ���������� '%s'",*cp1++);
		} else {
			sprintf(p1->name,"pr%d",i-prNekm+1);
			sprintf(p1->msg[0],"'%s' - ������� '0'", p1->name);
			sprintf(p1->msg[1],"'%s' - ������� '1'", p1->name);
		}
	}
	/* oil tank level */
	strcpy(p2->name,"LvlOil");
	p2->p.lv.low=j++; p2->p.lv.hi=j+18;
	p2->value[1]=p2->value[3]=0;
	p2->value[0]=3;	p2->value[2]=2;
	strcpy(p2->msg[0],"������� ������� ����� � ����");
	strcpy(p2->msg[2],"������� ������� ����� � ����");
	strcpy(p2->msg[3],strcpy(p2->msg[1],"��� ����� � ����"));
	p2->init=-1;
	p2++;
	/* air pump */
	strcpy(p1->name,"pmAir");
	p1->number=j++;
	p1->value[0]=0;
	p1->value[1]=1;
	strcpy(p1->msg[0],"��������� ����� �������");
	strcpy(p1->msg[1],"��������� ����� ��������");
	p1->init=-1;
	p1++;
	/* vvg */
	strcpy(p1->name,"VVG");
	p1->number=j++;
	p1->value[0]=1;
	p1->value[1]=0;
	strcpy(p1->msg[0],"��� �������");
	strcpy(p1->msg[1],"��� ��������");
	p1->init=-1;
	p1++;
	/* arv */
	strcpy(p1->name,"ARV");
	p1->number=j++;
	p1->value[0]=1;
	p1->value[1]=0;
	strcpy(p1->msg[0],"��� �������");
	strcpy(p1->msg[1],"��� ��������");
	p1->init=-1;
	p1++;
	/* pumps */
	for(i=0; i< pmN; i++, j+=2, p2++) {
		p2->value[0]=1;
		p2->value[1]=2;
		p2->value[2]=2;
		p2->value[3]=0;
		p2->init=-1;
		p2->p.pm.power=j;
		p2->p.pm.presure=j+1;

		if(i<pmNoil) {
			sprintf(p2->name,"pmOil%d",i+1);
			sprintf(p2->msg[0],"��%d - �������, ���� ��������",i+1);
			sprintf(p2->msg[1],"��%d - ��������, ���� ��������",i+1);
			sprintf(p2->msg[2],"��%d - �������, ��� ��������",i+1);
			sprintf(p2->msg[3],"��%d - ��������",i+1);
		} else if(i<(pmNoil+pmNgp)) {
			sprintf(p2->name,"pmOilG%d",(i-pmNoil+1));
#if 0
			sprintf(p2->msg[0],"����� ��%d - �������, ���� ��������",i-pmNoil+1);
			sprintf(p2->msg[1],"����� ��%d - ��������, ���� ��������",i-pmNoil+1);
			sprintf(p2->msg[2],"����� ��%d - �������, ��� ��������",i-pmNoil+1);
			sprintf(p2->msg[3],"����� ��%d - ��������",i-pmNoil+1);
#else
		p2->value[0]=1;
		p2->value[1]=0;
		p2->value[2]=1;
		p2->value[3]=0;
			sprintf(p2->msg[0],"����� ��%d - �������",i-pmNoil+1);
			sprintf(p2->msg[1],"����� ��%d - ��������",i-pmNoil+1);
			sprintf(p2->msg[2],"����� ��%d - �������",i-pmNoil+1);
			sprintf(p2->msg[3],"����� ��%d - ��������",i-pmNoil+1);
#endif
		} else {
			int ij=i-pmNoil-pmNgp+1;
			sprintf(p2->name,"pmWtr%d",ij);
			sprintf(p2->msg[0],"��%d - �������, ���� ��������",ij);
			sprintf(p2->msg[1],"��%d - ��������, ���� ��������",ij);
			sprintf(p2->msg[2],"��%d - �������, ��� ��������",ij);
			sprintf(p2->msg[3],"��%d - ��������",ij);
		}
	}
	/* whater tank level */
	strcpy(p2->name,"LvlWht");
	p2->p.lv.low=j++; p2->p.lv.hi=j++;
	p2->value[1]=p2->value[3]=1;
	p2->value[0]=4;	p2->value[2]=3;
	strcpy(p2->msg[2],"������� ������� ���� � ����");
	strcpy(p2->msg[0],"������� ������� ���� � ����");
	strcpy(p2->msg[3],strcpy(p2->msg[1],"��� ���� � ����"));
	p2->init=-1;
	p2++;

	UL_TRACE((LOGID, "biLoadConfig() Ok  j=%d",j));
	return 0;
}

int ltcLoadConfig(void)
{
	int rc;
	if((rc=tcLoadConfig()) || (rc=hzLoadConfig()) || (rc=biLoadConfig())) return rc;
	
	UL_TRACE((LOGID, "ltcLoadConfig() Ok"));
	return 0;
}

void ltcOut(short val)
{
	if(val != outBinary) {
		outBinary=val;
		UL_DEBUG((LOGID, "OUT_DIGITS_402(%d,%x)", slot402, outBinary));
		OUT_DIGITS_402(slot402, outBinary);
		TagSet(tOutB, val);
	}
}

// wm.c 
//

/*****************************************************************
*****************************************************************/

#define myMAX(x,y)	((x) > (y) ? (x) : (y))
#define myMIN(x,y)	((x) < (y) ? (x) : (y))
#define myABS(x,y)	((x) > (y) ? (x)-(y) : (y)-(x))
#define myTIME(x,y)	{__int64 Freq, Start, Stop; BOOL rc;									\
						rc=QueryPerformanceCounter((LARGE_INTEGER *)&Start);	x;			\
						if(rc && QueryPerformanceCounter((LARGE_INTEGER *)&Stop) &&			\
							QueryPerformanceFrequency((LARGE_INTEGER *)&Freq)) {			\
								__int64 res=(Stop-Start)*1000000;							\
								UL_DEBUG((LOGID,"%s time (mkc.) - %ld",y,(DWORD)(res/Freq)));\
						} else UL_DEBUG((LOGID,"Performance operation not supported"));}
#define myDelay(x)	{__int64 Freq, Start, Stop; BOOL rc;									\
						if(QueryPerformanceFrequency((LARGE_INTEGER *)&Freq) &&				\
						QueryPerformanceCounter((LARGE_INTEGER *)&Start)) {					\
							Start += Freq/1000*(x)/1000 ; do {								\
							rc=QueryPerformanceCounter((LARGE_INTEGER *)&Stop);				\
							} while(rc && Start > Stop);}}

/*********************************************************/


static void *ltc_104e(void), *ltc_111s(void), *ltc_104s(void),
 *ltc_CC_START(void), *ltc_CC_WHILE(void), *ltc_CC_SLEEP(void), *ltc_CALC(void),
 *ltc_STOP(void), *ltc_REBOOT(void);

#define NEXT(x)		return (void *)(x)
#define checkEH if(EH) {UL_ERROR((LOGID,"Check ErrorHandler code. line %d",__LINE__)); NEXT(ltc_REBOOT); }
#define checkEH0 if(EH) {UL_ERROR((LOGID,"Check ErrorHandler code. line %d",__LINE__)); NEXT(ltc_STOP); }

static void *ltc_REBOOT(void)
{
	static int n=0;
	int rc; 
	if(n > 10) {
		UL_ERROR((LOGID,"too many ltc rebooting"));
		NEXT(ltc_STOP);
	}
	if(BiosLCI01==NULL && getBios(&BiosLCI01,"lci01.bio")) NEXT(ltc_STOP);
	if(BiosLC010==NULL && getBios(&BiosLC010,"lc_010.bio")) NEXT(ltc_STOP);


	EH=0; n++; rc=GET_STATUS();

	if(IDOK != MessageBox(0,"��������� ����� � �������� ��� �����\n����� ����� ������� ��",
		"������������ ������", MB_TOPMOST | MB_ICONEXCLAMATION | MB_OKCANCEL)) NEXT(ltc_STOP);
#if 1
	EH=0; rc=GET_STATUS();
	// LCI01
	SELECT_SLOT(0); checkEH0;
	SET_BOARD_TYPE(LCI01); checkEH0;
	SET_BASE_ADDRESS(0x300); checkEH0;
	SET_CRAIT_ADDRESS(1); checkEH0;
	SET_INTERFACE_TYPE(SERIAL_INTERFACE); checkEH0;
	SET_DEVICE_BINDING(0); checkEH0;
	if(FAST_LOADING_LCI01(BiosLCI01)) {
		UL_ERROR((LOGID,"ltc %d-reboot: Can't load lci01.bio", n));
		NEXT(ltc_STOP);
	}
	checkEH0;
	if(PLATA_TEST()) {
		UL_ERROR((LOGID,"ltc %d-reboot: Test invalid lci01", n));
		NEXT(ltc_STOP);
	}
	checkEH0;
	CONFIG_TX_CHANNEL_LCI01(0);	checkEH0;
	CONFIG_RX_CHANNEL_LCI01(0);	checkEH0;
	// LC_010
	SELECT_SLOT(1); checkEH0;
	SET_BOARD_TYPE(CRAIT); checkEH0;
	SET_BASE_ADDRESS(0x300); checkEH0;
	SET_CRAIT_ADDRESS(1); checkEH0;
	SET_INTERFACE_TYPE(SERIAL_INTERFACE); checkEH0;
	SET_DEVICE_BINDING(0); checkEH0;
	if(FAST_LOADING_LC010(BiosLC010)) {
		UL_ERROR((LOGID,"ltc %d-reboot: Can't load lc010.bio", n));
		NEXT(ltc_STOP);
	}
	checkEH0;
	if(PLATA_TEST_CC()) {
		UL_ERROR((LOGID,"ltc %d-reboot: Test invalid lc010", n));
		NEXT(ltc_STOP);
	}
	checkEH0;
	READ_CALIBR_CC(); checkEH0;
//	CONFIG_FIFO_CC(2, 3);
	CONFIG_FIFO_CC(2,2); checkEH0;
	SET_IRQ_MASK_CC(0); checkEH0;
	FORCE_INTER_DELAY_CC(0); checkEH0;
	SET_CALIBR_MODE_CC(CALIBR_ALL); checkEH0;
	SET_FIFO_SIZE_CC(FIFOSIZE); checkEH0;
	SET_ERROR_HANDLER(myErrorHandler); checkEH0;
#endif
	c104=c111=0;
	
	UL_DEBUG((LOGID,"ltc %d-reboot is OK", n));
	NEXT(ltc_104e);
}

short delay, rate, Nchans, slt301, *val=NULL, *chans;
long slp; DWORD total;
__int64 cc_start;

static void *ltc_111s(void)
{
	delay=N111d;
	rate = N111r;
	Nchans=N111;
	chans=chans111;
	slt301=-1;
	val=vals111;
	total = N111 * N111n;
	slp= N111r*N111n;
	slp -= 11*N111r;
	NEXT(ltc_CC_START);
}

static void *ltc_104s(void)
{

	delay=N104d;
	rate = N104r;
	Nchans=N104;
	chans=chans104;
	slt301=slot301;
	val=vals104;
	total = N104 * N104n;
	slp= N104r * N104n;
	slp -= 35*N104r;
	NEXT(ltc_CC_START);
}

static void *ltc_104e(void)
{

	delay=N104d;
	rate = N104r;
	Nchans=N104;
	chans=chans104;
	slt301=slot301;
	val=NULL;
	total = N104;
	slp= 150; // N104r>>2;
	NEXT(ltc_CC_START);
}

static void *ltc_CC_START(void)
{
	if(slp <= 0) slp=rate;
	FORCE_INTER_DELAY_CC(10*delay);	checkEH;
	SET_CALIBR_MODE_CC(CALIBR_ALL);	checkEH;
	SOFT_CONFIG_CC(slt301, Nchans, chans, rate); checkEH;
	SOFT_START_CC(); checkEH;
	QueryPerformanceCounter((LARGE_INTEGER *)&cc_start);
	NEXT(ltc_CC_SLEEP);
}

static void *ltc_CC_SLEEP(void)
{
	static __int64 Start=0;
	__int64 Now, Freq;
	BOOL rc;

	if(Start == 0) {
		rc=QueryPerformanceCounter((LARGE_INTEGER *)&Start);
		if(Tiker < 10) UL_DEBUG((LOGID, "Sleep set on %ld mkc.",slp));
	}
	if(!rc || !QueryPerformanceCounter((LARGE_INTEGER *)&Now) || 
				!QueryPerformanceFrequency((LARGE_INTEGER *)&Freq) ) {
		UL_ERROR((LOGID, "QueryPerformance not supported"));
		NEXT(ltc_STOP);
	}
	Now -= Start; Now *= 1000; Freq /= 1000; Now /= Freq; Now -= slp;
	if(Now > 0) {
		Start=0;
		slp += (DWORD)Now;
		if(Tiker < 10) UL_DEBUG((LOGID, "Sleep elapsed after %ld mkc.",slp));
		NEXT(ltc_CC_WHILE);
	}
	NEXT(ltc_CC_SLEEP);
}

static void *ltc_CC_WHILE(void)
{
	__int64 Now, Freq;
	DWORD nnn=0;

	if(GET_NWORDS_CC(&nnn)) { // overload
		SOFT_STOP_CC();	UL_ERROR((LOGID,"Overload FIFO")); checkEH;
		if(val) memset(val,0,sizeof(short)*total);
		goto stop;
	}
	checkEH;
	if(nnn) {
		if(Tiker < 10) UL_DEBUG((LOGID, "ready for read %ld values from %ld", nnn, total));
		nnn=myMIN(nnn,total); total -= nnn;
		if(val) { GET_SOFT_DATA_CC(val,nnn); val += nnn; }
		if(total==0) {
			SOFT_STOP_CC();	checkEH;
stop:
			QueryPerformanceFrequency((LARGE_INTEGER *)&Freq);
			QueryPerformanceCounter((LARGE_INTEGER *)&Now);
			Now -= cc_start; Now *= 1000; Freq /= 1000; Now /= Freq;
			if(Tiker < 10) UL_DEBUG((LOGID, "CC_STOP after %ld mkc.", (DWORD)Now));
			NEXT( val ? (slt301==-1? ltc_104s: ltc_CALC) : ltc_111s);
		}
		NEXT(ltc_CC_WHILE);
	}
	slp=rate>>1;
	NEXT(ltc_CC_SLEEP);
}

static void *ltc_CALC(void)
{
	int i,j;
	static DWORD r111=0;
	short *psc;

	if(c111==0 || 3000 < GetTickCount() - r111) { 	// ����� ����������� 111
		r111=GetTickCount();
		memset(reset111,1,sizeof(reset111));
		if(Tiker < 10) UL_DEBUG((LOGID,"111-chans integrator reset after %ld",GetTickCount()-start));
	}
	for(i=0; i < N111; i++, ++c111, c111 &= 15) {
		short *p=&result111[c111];
		if(reset111[c111]) reset111[c111]=*p=0;
		for(psc=osc111[c111], j=i; j< N111v; j+=N111, psc++) {
			*psc=vals111[j]; *p = myMAX(*p, myABS(*psc,0));}
	}
	c111 += 3; c111 &= 15; j=c111;
	for(i=1; i<2*N111; i+=2) { chans111[i] = j; j++; j &= 15; }

	desp=&desp104[c104];res=&result104[c104];psc=osc104[c104];
	c104++; c104 &= 7;
	for(i=1; i<2*N104; i+=2) chans104[i]=GAIN|c104;

//	{static int n=8*3;if(n>0) { fwrite(vals104,sizeof(vals104),1,b104);	n--; }}
	for(i=0; i<N104; i++, desp += N104c, res += N104c) {
		double sum=0.; int n=0; *desp=0.; 
		for(j=i; j<N104v; j+=N104) {
			short v=vals104[j];
			if(v<TC_DOWN || v>TC_UP) { vals104[j]=0; n++; }	else sum += v;
			*psc = v;	psc++;
		}
		psc += N104n*(N104c-1);
		if(n > N104n/4) { *res=0; continue; }
		sum /= (N104n-n); *res = (short)(sum+0.5);
		if(*res < (TC_DOWN+TC_DLT) || *res > (TC_UP-TC_DLT)) { *res = 0; continue; }
		for(j=i; j<N104v; j+=N104) if(vals104[j]) *desp += myABS(sum,vals104[j]);
		*desp /= (N104n-n)*sum/100.; if(*desp > 100.) *desp=-1.;
	}

	NEXT(ltc_104e);
}

static void *ltc_STOP(void)
{
	if(BiosLCI01) {	free((void *)BiosLCI01); BiosLCI01=NULL; }
	if(BiosLC010) {	free((void *)BiosLC010); BiosLC010=NULL; }
// for debug only	if(f111)fclose(f111); if(f104)fclose(f104); if(b104)fclose(b104);
	UL_ERROR((LOGID,"%ld - fatal error finish\n",GetTickCount()-start));
	exit(0); // ???
}

/**************************************************************************/

int ltcPoll(void)
{
	static void * (*state) (void) = (void *)ltc_104e;
	int i=0;

	for(i=0; i < N104c; i++) {
		do { state=state(); } while (ltc_CALC != state);
		state=state(); // do state ltc_CALC
	}
	clrChanged(); 

	tcCalc();
	
	if(STATUS_LC451(slot451) == 0xff) {
		KADR_LC451(slot451, mode451, scale451, mask451, hzVal);
		hzCalc();
	}

	uvCalc();

	ibCalc(INP_DIGITS_403(slot403));

	if(Tiker >= (8*60*60)) { // about 8 hours
		int hour=Clock/(3600*1000);
		int sec=(Clock/1000-hour*3600);
		int msec=Clock-1000*sec;
		int mnt=sec/60;
		UL_INFO((LOGID, "Tiker - ����� ���� ����� ���������� ����������� ������"));
		PutMsg("������ %0d:%02d:%02d.%d - ������ ������������ ...",hour,mnt,sec-60*mnt,msec);
		Tiker=1;
	} else
		Tiker++;
	if(StopTimer > 0) StopTimer--;
	if(ExitTimer > 0) ExitTimer--;

	if(Tiker == 6) {
		FILE *tmp;
		if(NULL!=(tmp=fopen(fullName("osc111.bin"),"wb+"))) { fwrite(osc111,sizeof(osc111),1,tmp); fclose(tmp); }
		if(NULL!=(tmp=fopen(fullName("osc104.bin"),"wb+"))) { fwrite(osc104,sizeof(osc104),1,tmp); fclose(tmp); }
	}

	return 0;
}


#if 0
int main(int argc, char* argv[])
{
	int i, sec; 

	if(ltcInit()) exit(1);
	for(sec=0; sec < 30; sec++) {
		poll();
		for(i=0; i<N111t; i++) fprintf(f111,"%6d", result111[i]); fprintf(f111,"\n");
		for(i=0; i<N111t; i++) fprintf(f111,"%6.1f", (double)result111[i]*5.12/**2.*//1.41421/2048.); fprintf(f111,"\n");
		for(i=0; i<N104t; i++) fprintf(f104,"%6d", result104[i]); fprintf(f104,"\n");
		for(i=0; i<N104t; i++) fprintf(f104,"%6.1f", desp104[i]); fprintf(f104,"\n");
		if(sec==1) { FILE *tmp=fopen("osc111.bin","wb+"); if(tmp) { fwrite(osc111,sizeof(osc111),1,tmp); fclose(tmp); }}
		if(sec==1) { FILE *tmp=fopen("osc104.bin","wb+"); if(tmp) { fwrite(osc104,sizeof(osc104),1,tmp); fclose(tmp); }}
		putchar('.'); UL_DEBUG((LOGID,"1 sek.? - %d = %ld\n",sec,GetTickCount()-start));
	}

	return 0;
}
#endif
