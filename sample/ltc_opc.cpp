/**************************************************************************
 OPC-Server by Lab43 Sep 2000

 LTC server
 **************************************************************************/
#define INITGUID
#define _WIN32_DCOM
#include <windows.h>
#include <locale.h>
#include <stdio.h>
#include <time.h>

#include "unilog.h" 

#include <opcda.h>
#include <opcerror.h>
#include "lightopc.h"

#include "lc_drv.h"

unilog *logg=NULL;

static const loVendorInfo vendor = {
	 3/*Major*/, 2/*Minor*/, 1/*Build*/, 0/*Reserv*/,
		 "LTC OPC Server #0" };
loService *my_service;
static LONG server_count;

static CRITICAL_SECTION lk;
static CRITICAL_SECTION lk_values;

static loTagId tgi[tTotal];
char ts[tTotal][32];
static loTagValue tv[tTotal];

// {782419C9-0BED-48a2-BA1F-8E73703DCC4F}
DEFINE_GUID(CLSID_LTCOPCServer, 
0x782419c9, 0xbed, 0x48a2, 0xba, 0x1f, 0x8e, 0x73, 0x70, 0x3d, 0xcc, 0x4f);

/**********************************************************************
 sample server initiation & simulation 
 **********************************************************************/
#ifdef never
void activation_monitor(loService *se, int count, loTagPair *til)
{
 int act = 1;
 if (0 > count) act = 0, count = -count;
 while(count--)
   {
    UL_DEBUG((LOGID, "MON: %u %s %s", til[count].tpTi, til[count].tpRt, act? "On": "Off"));
   }
}
#endif

static void makeTS(void)
{
	int i, j;

	memset(ts,0,sizeof(ts));
	j=0;
	for(i=0; i<tcN; i++) {
		sprintf(ts[j++],"tc/%s",tcItemCfg[i].name);
		sprintf(ts[j++],"tc-c/%s",tcItemCfg[i].name);
	}
	for(i=0; i<hzN; i++) {
		sprintf(ts[j++],"v/%s",hzItemCfg[i].name); 
		sprintf(ts[j++],"v-c/%s",hzItemCfg[i].name);
	}
	for(i=0; i<uN; i++) {
		sprintf(ts[j++],"u/u%02d",i+1); 
		sprintf(ts[j++],"u-c/u%02d",i+1);
	}
	for(i=0; i<oneN; i++) {
		sprintf(ts[j++],"ib/%s",oneItemCfg[i].name);
	}
	for(i=0; i<twoN; i++) {
		sprintf(ts[j++],"ib/%s",twoItemCfg[i].name);
	}
	sprintf(ts[j++],"sp/Overload");
	sprintf(ts[j++],"sp/Skip");
	sprintf(ts[j++],"sp/Wait");
// end of unit
	sprintf(ts[j++],"sp/Clock");
// end of I4
	for(i=0; i<timeN; i++) switch (i) {
		case timeStart:	sprintf(ts[j++],"rotate/StartTime");	break;
		case timeStop:	sprintf(ts[j++],"rotate/StopTime");		break;
		default:		sprintf(ts[j++],"rotate/Time%d",i);		break;
	}
	for(i=0; i<msgN; i++) sprintf(ts[j++],"event/Msg%d",i);
// end of area1
	sprintf(ts[j++],"ExitTimer");
	sprintf(ts[j++],"StopTimer");
	sprintf(ts[j++],"rotate/ScaleHi");
	sprintf(ts[j++],"rotate/ScaleLo");
	sprintf(ts[j++],"rotate/StartNumber");
	sprintf(ts[j++],"rotate/StopShow");
	sprintf(ts[j++],"sp/Tiker");
// end of simple I2 variables
	for(i=0; i<evnN; i++) sprintf(ts[j++],"event/EventR%d",i);
	for(i=0; i<GrafTotal; i++) {
		char *name=scaleItemCfg[i].name;
		sprintf(ts[j++],"scale/%slo",name); 
		sprintf(ts[j++],"scale/%shi",name);
		sprintf(ts[tScaleC+i],"scale/%sctrl",name);
	}
	sprintf(ts[j++],"ExitTimerC");
	sprintf(ts[j++],"StopTimerC");
	for(i=0; i<evnN; i++) sprintf(ts[j++],"event/Event%d",i);
	sprintf(ts[tOutB],"outB");
	
	UL_DEBUG((LOGID, "makeTS() - OK"));
}

#define GeneralStringInit(FN, BORDER, NUMBER, SA, SB)							\
	static int FN(void) { int i; memset(SA, 0, sizeof(SA));						\
	for(i=0; i< NUMBER; i++) { VARIANT *pV=&tv[BORDER+i].tvValue;				\
		if(NULL==(SA[i]=SafeArrayCreate(VT_UI1,1,SB))) {						\
			UL_ERROR((LOGID, "FN() - Can't create SafeArray"));	return 1;}		\
		V_VT(pV)= VT_ARRAY | VT_UI1;V_ARRAY(pV) = SA[i];}return 0;}

static SAFEARRAYBOUND sb[1]= { MsgLenB, 0 };
static SAFEARRAY *sa[msgN];
GeneralStringInit(initEvent,tMsg,msgN,sa,sb)

static SAFEARRAYBOUND sbTime[1]= { TimeLenB, 0 };
static SAFEARRAY *saTime[timeN];
GeneralStringInit(initTime,tTime,timeN,saTime,sbTime)

#undef GeneralStringInit

static int simulator_init(void)
{
	int i;

	InitializeCriticalSection(&lk_values);

	makeTS();

	memset(tv, 0, sizeof(tv));
	for(i=0; i<tTotal; i++)	VariantInit(&tv[i].tvValue);

	for(i=0; i<unitS; i++) {
		V_VT(&tv[i].tvValue) = VT_I2;
		V_I2(&tv[i].tvValue) = 0x7fff;
	}
	for(i=tI4; i<tI4e; i++) {
		V_VT(&tv[i].tvValue) = VT_I4;
		V_I4(&tv[i].tvValue) = 0x7FFFFFFFl;
	}
	for(i=tTailI2; i<tTotal; i++) {
		V_VT(&tv[i].tvValue) = VT_I2;
		V_I2(&tv[i].tvValue) = 0x7fff;
	}

	return initEvent() || initTime();
}

void simulator_destroy(void)
{
	int i;
	for(i=0; i<tTotal; i++) VariantClear(&tv[i].tvValue);
	DeleteCriticalSection(&lk_values);
}

int WriteTags( const loCaller *cactx,
                       unsigned count, const loTagPair taglist[],
                       VARIANT values[], HRESULT error[], LCID lcid)
{
 unsigned ii;
 EnterCriticalSection(&lk_values);
 
 for(ii = 0; ii < count; ii++) {
	int tI=(int)taglist[ii].tpRt-1;
	WORD val;

	UL_TRACE((LOGID, "WriteTags()..."));
	if(tI < 0 || tI >= tTotal) {
		UL_WARNING((LOGID, "WriteTags() - ignore unknown or invalid Tag=%d",tI));
		continue;
	}
	if (V_VT(&values[ii]) == VT_EMPTY) {
		error[ii] = E_FAIL;
		UL_WARNING((LOGID, "WriteTags() - ignore VT_EMPTY Tag %s",ts[tI]));
		continue;
	}
	if (V_VT(&values[ii]) != VT_I2) {
		VARIANT tv; VariantInit(&tv);
		if (S_OK != (error[ii] = VariantChangeType(&tv, &values[ii], 0, VT_I2))) {
			UL_WARNING((LOGID,
				"WriteTags() - can't convert VariantType to VT_I2 for Tag %s",ts[tI]));
			continue;
		}
		val=V_I2(&tv);
	} else {
		val=V_I2(&values[ii]);
		error[ii]=S_OK;
	}
	UL_TRACE((LOGID, "WriteTags() - new value=%d(0x%04x) for tag %s",val,val,ts[tI]));
	
	if(tI < tEnd2) switch(tI) {
		case tOutB:	ltcOut(val);					break;
		case tETc:	ExitTimer=val; ExitTimerF++;	break;
		case tSTc:	StopTimer=val; StopTimerF++;	break;
		default:	error[ii]=E_FAIL;				break;
	} else if(tI < tScaleC) {
		// Event message received
		int i=tI-tEvent+tMsgS;
		V_I2(&tv[i].tvValue)=val;
		UL_TRACE((LOGID, "WriteTags() - set value=%d(0x%04x) for tag %s",V_I2(&tv[i].tvValue),(WORD)V_I2(&tv[i].tvValue),ts[i]));
		tv[i].tvTi = tgi[i];
	} else if(tI < tTotal) {
		scaleItemCfg[tI-tScaleC].used=val;
	}
 }

 LeaveCriticalSection(&lk_values);
 return 1;
}

int driver_init(void)
{
  int ecode,i;
  loDriver ld;
  if (my_service) return 0;

  if(simulator_init()) return -1;
  memset(&ld, 0, sizeof(ld));
  ld.ldRefreshRate = 1000; // 1 ���. T0; // ?
/*  ld.ldSubscribe = activation_monitor; */
  ld.ldWriteTags = WriteTags;
  ld.ldFlags = loDf_NOFORCE | loDF_IGNCASE | loDF_NOCOMP ;
  ld.ldBranchSep = '/';

  ecode = loServiceCreate(&my_service, &ld, tTotal);
  for (i=0; i<tTotal && !ecode; i++)
    {
	 UL_TRACE((LOGID, "loAddRealTag (...%s...)[%u] = ", ts[i], V_VT(&tv[i].tvValue)));
     ecode = loAddRealTag(my_service, /* actual service context */
	 		    &tgi[i], /* returned TagId */
                (loRealTag)(i+1), /* != 0 */
				ts[i],
				0, /* loTF_XXX */
				i < tRead ? OPC_READABLE :  OPC_READABLE | OPC_WRITEABLE,
#if 0
					i < tRW ? OPC_READABLE | OPC_WRITEABLE : OPC_WRITEABLE,
#endif
				&tv[i].tvValue,0,0);
    }
  if(ecode) {
	  UL_ERROR((LOGID, "%!e driver_init()=", ecode));
	  return -1;
  }
  return 0;
}

void driver_destroy(void)
{
 int ecode = loServiceDestroy(my_service);
 UL_TRACE((LOGID, "%!e loDelete(%p) = ", ecode));
 my_service = 0;
 simulator_destroy();
}

/**********************************************************************/

class myClassFactory: public IClassFactory
{
public:
    LONG RefCount;
    myClassFactory() { RefCount = 0; };

    STDMETHODIMP         QueryInterface( REFIID iid, LPVOID* ppInterface);
    STDMETHODIMP_(ULONG) AddRef( void);
    STDMETHODIMP_(ULONG) Release( void);

    STDMETHODIMP         CreateInstance( LPUNKNOWN pUnkOuter, REFIID riid, LPVOID* ppvObject);
    STDMETHODIMP         LockServer(BOOL fLock);
    inline LONG getRefCount(void) { LONG rc;
                                    EnterCriticalSection(&lk);
                                    rc = RefCount;
                                    LeaveCriticalSection(&lk);                                    
                                    return rc; }

};

static int OPCstatus=OPC_STATUS_RUNNING;

static myClassFactory my_CF;

static void a_server_finished(void *a, loService *b, loClient *c)
{
 InterlockedDecrement(&server_count);
 if (a) ((myClassFactory*)a)->Release();
 UL_DEBUG((LOGID, "a_server_finished(%lu)", server_count));
}

/**********************************************************************/

STDMETHODIMP myClassFactory::QueryInterface(REFIID iid, LPVOID* ppInterface)
{
 if (ppInterface == NULL) return E_INVALIDARG;

 if (iid == IID_IUnknown || iid == IID_IClassFactory)
   {
    UL_DEBUG((LOGID, "myClassFactory::QueryInterface() Ok"));
    *ppInterface = this;
    AddRef();
    return S_OK;
   }
 UL_DEBUG((LOGID, "myClassFactory::QueryInterface() Failed"));

 *ppInterface = NULL;
 return E_NOINTERFACE;
}

STDMETHODIMP_(ULONG) myClassFactory::AddRef( void)
{
 ULONG rv;
 EnterCriticalSection(&lk);
 rv = (ULONG)++RefCount;
 LeaveCriticalSection(&lk);                                    
 UL_DEBUG((LOGID, "myClassFactory::AddRef(%ld)", rv));
 return rv;
}

STDMETHODIMP_(ULONG) myClassFactory::Release( void)
{
 ULONG rv;
 EnterCriticalSection(&lk);
 rv = (ULONG)--RefCount;
 LeaveCriticalSection(&lk);                                    
 UL_DEBUG((LOGID, "myClassFactory::Release(%d)", rv));
 return rv;
}

STDMETHODIMP myClassFactory::CreateInstance(LPUNKNOWN pUnkOuter, REFIID riid, LPVOID* ppvObject)
{
 if (pUnkOuter != NULL)
   return CLASS_E_NOAGGREGATION;	// Aggregation is not supported by this code

  IUnknown *server = 0;

 AddRef(); /* for a_server_finished() */
 if (loClientCreate(my_service, (loClient**)&server, 0, &vendor, a_server_finished, this))
   {
    UL_DEBUG((LOGID, "myClassFactory::loCreateClient() failed"));
    Release();
    return E_OUTOFMEMORY;
   }
 InterlockedIncrement(&server_count);

 HRESULT hr = server->QueryInterface(riid, ppvObject);
 if (FAILED(hr)) 
   {
    UL_DEBUG((LOGID, "myClassFactory::loClient QueryInterface() failed"));
   }
 else
   {
    loSetState(my_service, (loClient*)server, loOP_OPERATE, OPCstatus, 0);
    UL_DEBUG((LOGID, "myClassFactory::server_count = %ld", server_count));
   }
 server->Release(); 
 return hr;
}

STDMETHODIMP myClassFactory::LockServer(BOOL fLock)
{
 if (fLock) AddRef();
 else      Release();
 UL_DEBUG((LOGID, "myClassFactory::LockServer(%d)", fLock)); 
 return S_OK;
}

/**************************************************************/

#define GeneralTagOperation(FN, OP)																\
	void FN(int i, DWORD val) {																	\
		if(i >= 0 && i < tTotal) {																\
			if (V_VT(&tv[i].tvValue) == VT_I2)  V_I2(&tv[i].tvValue) OP (WORD)(val & 0xffff);	\
			else if (V_VT(&tv[i].tvValue) == VT_I4) V_I4(&tv[i].tvValue) OP val;				\
			else { UL_ERROR((LOGID,																\
				"FN(index, value) - Tag <%s> has't V_I2/V_I4 type. Operation ignored.",ts[i]));	\
				return; } tv[i].tvTi = tgi[i];													\
		} else																					\
			UL_ERROR((LOGID,"FN(index, value) - Invalid tag index=%d. Operation ignored.",i));}	

GeneralTagOperation(TagSet, =)
GeneralTagOperation(TagBitSet, |=)
GeneralTagOperation(TagBitInvert, ^=)
GeneralTagOperation(TagBitClr, &=~)
GeneralTagOperation(TagAdd, +=)

#undef GeneralTagOperation

void TagTime(int index, time_t *tmv)
{
	int i=index-tTime;
	wchar_t *cp;
	time_t now;

	if(i<0 || i>=timeN) {
		UL_ERROR((LOGID,"TagTime(index) - Invalid tag index=%d. Operation ignored.",index));
		return;
	}
	if(tmv == NULL) {tmv=&now, time(tmv);}
	if(S_OK!=SafeArrayAccessData(saTime[i],(void**)&cp)) {
		UL_ERROR((LOGID,
			"TagTime(%s) - invalid SafeArrayAccessData(). Operation ignored.",ts[index]));
		return;
	}
	if(wcsftime(cp, TimeLenW, L"%H:%M:%S",localtime(tmv))) {
		tv[index].tvTi = tgi[index];
		UL_DEBUG((LOGID,
			"TagTime(%s) - update to <%ls>",ts[index],cp));
	} else {
		UL_ERROR((LOGID,
			"TagTime(%s) - invalid wcsftime(). Operation ignored.",ts[index]));
		return;
	}
	SafeArrayUnaccessData(saTime[i]);
	return;
}	


/***************************************************************************
 EXE-specefic stuff
 ***************************************************************************/
char argv0[2048];

char *fullName(char *name)
{
	static char pathName[sizeof(argv0)]="\0";
	char *cp;

	if(*pathName=='\0') strcpy(pathName, argv0);
	if(NULL==(cp=strrchr(pathName,'\\'))) cp=pathName; else cp++;
	cp=strcpy(cp,name);
	return pathName;
}

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 const char eClsidName [] = "OPC.LTC-exe";
 DWORD objid, pollStart;
 int i;
 char *cp;

  objid=::GetModuleFileName(NULL, argv0, sizeof(argv0));
  if(objid==0 || objid+50 > sizeof(argv0)) return 0;

 InitializeCriticalSection(&lk);

 logg = unilog_Create("OPC-LTC-exe", fullName("ltcopc.log"), NULL,
                    0, /* Max filesize: -1 unlimited, -2 -don't change */
                    ll_DEBUG); /* level [ll_FATAL...ll_DEBUG] */

 UL_INFO((LOGID, "Start"));

 if(NULL==(cp = setlocale(LC_ALL, ".1251"))) {
	 UL_ERROR((LOGID, "setlocale() - Can't set 1251 code page"));
	 goto Finish;
 }

 cp = lpCmdLine;
 if(cp) {
#define eProgID eClsidName
	if (strstr(cp, "/r")) {
		if (loRegisterServer(&CLSID_LTCOPCServer, eClsidName, eProgID, argv0, 0))
			UL_ERROR((LOGID, "Registration <%s> <%s> Failed", eProgID, argv0));    
		else
			UL_INFO((LOGID, "Registration <%s> <%s> Ok", eProgID, argv0));    
    } else if (strstr(cp, "/u")) {
		if (loUnregisterServer(&CLSID_LTCOPCServer, eClsidName))
			UL_ERROR((LOGID, "UnReg <%s> <%s> Failed", eClsidName, argv0));    
		else 
			UL_INFO((LOGID, "UnReg <%s> <%s> Ok", eClsidName, argv0));    
    } else {
		UL_WARNING((LOGID, "Ignore unknown option <%s>", cp));
		goto Cont;
    }
    goto Finish;
#undef eProgID
#undef argv0
 }
Cont:

 if(ltcLoadConfig()) { 
     UL_ERROR((LOGID, "ltcLoadConfig() failed. Exiting..."));
	 goto Finish;
 }
 if (FAILED(CoInitializeEx(NULL, COINIT_MULTITHREADED))) {
    UL_ERROR((LOGID, "CoInitializeEx() failed. Exiting..."));
    goto Finish;
 }
 if (driver_init()) { CoUninitialize(); goto Finish; }

 if(ltcInit()) { 
     UL_ERROR((LOGID, "ltcInit() failed - OPC_STATUS_FAILED..."));
	 OPCstatus=OPC_STATUS_FAILED;
 }

 if (FAILED(CoRegisterClassObject(CLSID_LTCOPCServer, &my_CF, 
                                  CLSCTX_LOCAL_SERVER|
                                  CLSCTX_REMOTE_SERVER|
                                  CLSCTX_INPROC_SERVER, 
                                  REGCLS_MULTIPLEUSE, &objid)))
   {
    UL_ERROR((LOGID, "CoRegisterClassObject() failed. Exiting..."));
    driver_destroy();
    CoUninitialize(); 
    goto Finish;
   }

 Sleep(3000);
 my_CF.Release(); /* avoid locking by CoRegisterClassObject() */

 if(OPCstatus != OPC_STATUS_RUNNING) {
	EnterCriticalSection(&lk);
	while(my_CF.RefCount | server_count) {
		LeaveCriticalSection(&lk);
		Sleep(1000);
		EnterCriticalSection(&lk);
	}
	goto ex;
 }

 EnterCriticalSection(&lk_values);
 for(i=0; i<tTotal; i++) {
	 tv[i].tvTi = 0;
	 tv[i].tvState.tsError = S_OK;
	 tv[i].tvState.tsQuality = OPC_QUALITY_GOOD;
	 tv[i].tvState.tsTime.dwHighDateTime = 0;
     tv[i].tvState.tsTime.dwLowDateTime = 0;
	if (V_VT(&tv[i].tvValue) == VT_I2)
		V_I2(&tv[i].tvValue) = 0;
	if (V_VT(&tv[i].tvValue) == VT_I4)
		V_I2(&tv[i].tvValue) = 0;

 }
 tv[tStartC].tvTi = tgi[tStartC]; 
 tv[tStopSh].tvTi = tgi[tStopSh]; 
 for(i=0; i<evnN; i++) {
	 tv[tEvent+i].tvTi = tgi[tEvent+i]; 
 }
 TagSet(tOutB,outBinary);
 LeaveCriticalSection(&lk_values);

 pollStart=GetTickCount();
 EnterCriticalSection(&lk);
 while(my_CF.RefCount | server_count) {
	LeaveCriticalSection(&lk);

	ltcPoll();

	EnterCriticalSection(&lk_values);

	for(i=0; i<nChanged; i++) {
		int index=iChanged[i];

		UL_DEBUG((LOGID, "UpdateCache #%d for %d (%s tgi %u) new value is %d", i, index, 
			ts[index], tgi[index], unit.data[index]));
		tv[index].tvTi = tgi[index];
		V_I2(&tv[index].tvValue) = unit.data[index];
	}
	tv[tClock].tvTi = tgi[tClock];
	Clock=V_I4(&tv[tClock].tvValue) = GetTickCount()-pollStart;

	tv[tTiker].tvTi = tgi[tTiker];
	V_I4(&tv[tTiker].tvValue) = Tiker;

	if(ExitTimer >=0 || ExitTimerF) {
		ExitTimerF=0;
		tv[tET].tvTi = tgi[tET];
		V_I2(&tv[tET].tvValue) = ExitTimer;
	}

	if(StopTimer >=0 || StopTimerF) {
		StopTimerF=0;
		tv[tST].tvTi = tgi[tST];
		V_I2(&tv[tST].tvValue) = StopTimer;
	}

	if(Vchanged) {
		tv[tVhi].tvTi = tgi[tVhi];
		V_I2(&tv[tVhi].tvValue) = Vhi;
		tv[tVlo].tvTi = tgi[tVlo];
		V_I2(&tv[tVlo].tvValue) = Vlo;
		Vchanged=0;
	}

	for(i=0; i < GrafTotal; i++) if(scaleItemCfg[i].Changed) {
		int tI=tScale+i*2;
		tv[tI].tvTi = tgi[tI];
		V_I2(&tv[tI].tvValue) = scaleItemCfg[i].DynamicLo;
		tI++; scaleItemCfg[i].Changed=0;
		tv[tI].tvTi = tgi[tI];
		V_I2(&tv[tI].tvValue) = scaleItemCfg[i].DynamicHi;
		UL_DEBUG((LOGID, "UpdateCache for scale %d (%s tgi %u) new value is %d:%d",
			i, scaleItemCfg[i].name , tI, scaleItemCfg[i].DynamicLo, scaleItemCfg[i].DynamicHi));
	}

	if(msgCount >= msgN) {
		UL_ERROR((LOGID, 
			"EventMessage - buffer overload. Number of messages is %d. Used last %d",msgCount,msgN));
		msgCount=msgN;
		msgUp=msgPut;
	}
	while(msgCount) {
		wchar_t *cp;
		size_t n;
		if(S_OK==SafeArrayAccessData(sa[msgUp],(void**)&cp)) {
			tv[tMsg+msgUp].tvTi = tgi[tMsg+msgUp];
			n=mbstowcs(cp,Msgs[msgUp],MsgLenW-1);
			if(n==-1) {
				UL_ERROR((LOGID,
"EventMessage - encounter an invalid multibyte character into <%s>. Put empty string.",Msgs[msgUp]));
				*cp=L'\0';
			} else if(n==(MsgLenW-1)) *(cp+MsgLenW-1)=L'\0';
			SafeArrayUnaccessData(sa[msgUp]);
			UL_DEBUG((LOGID, "UpdateCache for Event %d <%s>",msgUp,Msgs[msgUp]));
		} else UL_ERROR((LOGID, "EventMessage - invalid SafeArrayAccessData(). Skip message."));
		msgUp++; msgUp &= msgN-1; msgCount--;
	}

	loCacheUpdate(my_service, tTotal, tv, 0);

	for(i=0; i<tTotal; i++) tv[i].tvTi = 0;

	LeaveCriticalSection(&lk_values);
	
	EnterCriticalSection(&lk);
  }

ex:
 if (FAILED(CoRevokeClassObject(objid)))
    UL_WARNING((LOGID, "CoRevokeClassObject() failed..."));
 driver_destroy();
 CoUninitialize();
 LeaveCriticalSection(&lk);

Finish:
 DeleteCriticalSection(&lk);
 UL_INFO((LOGID, "Finish"));
 unilog_Delete(logg); logg = 0;
 return 0;
}

/***************************************************************************/
